# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is a "repo expanded" version of the Plastic Logic SDK. 
* This makes it easier to browse the SDK code without installing a full linux vm.
* It has been slightly modified to get it to build without errors.
* There are multiple warnings in the build.
* [Plastic Logic SDK](https://github.com/plasticlogic/plsdk)

### How do I get set up? ###


* Clone repository
* View source files → If you want to build on a linux vm:
* Clone repository
* apt-get install python-dev
* One of the following commands to setup environment variables:

    `$ . plsdk/fast-builder/envsetup.sh # for cross-compiling, adjust paths`

    `$ . plsdk/fast-builder/native-envsetup.sh # for native build`

* make -j5
