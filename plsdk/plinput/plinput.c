/*
  Plastic Logic input device utility - plinput

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <linux/input.h>
#include <libplinput.h>
#include <signal.h>
#include <getopt.h>
#include <stdlib.h>

#define LOG_TAG "input"
#include "log.h"

static const char *APP_NAME = "plinput";
static const char *VERSION = "1.3";
static const char *LICENSE =
	"This program is distributed in the hope that it will be useful,\n"
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	"GNU General Public License for more details.\n";
static const char *COPYRIGHT =
	"Copyright (C) 2010, 2011, 2012 Plastic Logic Limited";
static const char *DEFAULT_DEVICE = "/dev/input/event0";

static int g_run = 1;

static void sigint_abort(int signum);
static int abort_cb(void);
static void print_usage(void);
static int enumerate_devices(void);
static void log_key(struct plinput_key *key, int n);
static void log_point(struct plinput_point *point, int n);

int main(int argc, char **argv)
{
	static const char *OPTS = "hveli:I:gkw:";
	struct plinput *plinput;
	const char *device = DEFAULT_DEVICE;
	const char *device_name = NULL;
	__sighandler_t sigint_original;
	unsigned wait_key = KEY_RESERVED;
	int log_info = 0;
	int do_grab = 0;
	int do_keyentry = 0;
	int i;

	while ((i = getopt(argc, argv, OPTS)) != -1) {
		switch (i) {
		case 'h':
			print_usage();
			exit(EXIT_SUCCESS);
			break;

		case 'v':
			printf("%s v%s\n%s\n%s\n", APP_NAME, VERSION,
			       COPYRIGHT, LICENSE);
			exit(EXIT_SUCCESS);
			break;

		case 'e': {
			int res = enumerate_devices();
			exit((res < 0) ? EXIT_FAILURE : EXIT_SUCCESS);
			break;
		}

		case 'l':
			log_info = 1;
			break;

		case 'i':
			device = optarg;
			break;

		case 'I':
			device_name = optarg;
			break;

		case 'g':
			do_grab = 1;
			break;

		case 'k':
			do_keyentry = 1;
			break;

		case 'w':
			wait_key = plinput_get_key_code(optarg);

			if (wait_key == KEY_RESERVED) {
				LOG("Invalid key name");
				exit(EXIT_FAILURE);
			}
			break;

		case '?':
		default:
			LOG("invalid argument");
			exit(EXIT_FAILURE);
			break;
		}
	}

	LOG("%s v%s", APP_NAME, VERSION);

	if (device_name != NULL)
		plinput = plinput_init_from_name(device_name, NULL);
	else
		plinput = plinput_init(device);

	if (plinput == NULL) {
		LOG("failed to initialise plinput");
		exit(EXIT_FAILURE);
	}

	if (do_grab) {
		LOG("grabbing the input device");
		plinput_grab(plinput, 1);
	}

	if (log_info)
		plinput_log_info(plinput);
	else
		LOG("input device name: %s", plinput_get_name(plinput));

	sigint_original = signal(SIGINT, sigint_abort);
	plinput_set_abort_cb(plinput, abort_cb);
	plinput_set_opt(plinput, PLINPUT_OPT_ACCEPT_SIGNALS, 1);
	i = 1;

	while (g_run) {
		struct plinput_point point;
		struct plinput_key key;

		if (do_keyentry) {
			if (plinput_read_key(plinput, &key) < 0) {
				LOG("failed to read key input");
				g_run = 0;
			}
		} else if (plinput_read_point(plinput, &point) < 0) {
			LOG("failed to read touch point");
			g_run = 0;
		}

		if (g_run) {
			if (do_keyentry) {
				log_key(&key, i++);

				if ((key.code == wait_key) && key.on) {
					LOG("Key detected");
					g_run = 0;
				}
			} else {
				log_point(&point, i++);
			}
		}
	}

	plinput_free(plinput);
	signal(SIGINT, sigint_original);

	LOG("done.");

	exit(EXIT_SUCCESS);
}

static void sigint_abort(int signum)
{
	if (signum == SIGINT)
		g_run = 0;
}

static int abort_cb(void)
{
	return g_run ? 0 : 1;
}

static void print_usage(void)
{
	printf(
"Usage: %s <OPTIONS>\n"
"\n"
"OPTIONS:\n"
"  -h\n"
"    Show this help message and exit\n"
"\n"
"  -v\n"
"    Show the version, copyright and license information, and exit.\n"
"\n"
"  -e\n"
"    Enumerate the devices with their descriptions, and exit.\n"
"\n"
"  -l\n"
"    Log input device information.\n"
"\n"
"  -i DEVICE\n"
"    Specify the input event device to use (default: /dev/input/event0)\n"
"\n"
"  -I DEVICE_NAME\n"
"    Specify the name of the input device to search and try to find a\n"
"    corresponding device, example: \"Plastic\".\n"
"\n"
"  -g\n"
"    Grab the device, preventing other programs from receiving the\n"
"    events from the input device being used.\n"
"\n"
"  -k\n"
"    Read key events, default is to read point events e.g. mouse, touchscreen\n"
"\n"
"  -w KEY_NAME\n"
"    Wait for an on-key event with the given KEY_NAME to occur, and return.\n"
"\n", APP_NAME);
}

static int enumerate_devices(void)
{
	struct plinput_dev_name *dev_names = plinput_get_device_names(NULL);
	struct plinput_dev_name *it = dev_names;

	if (dev_names == NULL)
		return -1;

	while (it->device != NULL) {
		printf("%s: %s\n", it->device, it->name);
		++it;
	}

	plinput_free_device_names(dev_names);

	return 0;
}

static void log_key(struct plinput_key *key, int n)
{
	LOG("[%i.%06i] #%04i %03d %-4s %s",
	    (int) key->time.tv_sec, (int) key->time.tv_usec, n,
	    key->code, (key->on ? "down" : "up"),
	    plinput_key_names[key->code]);
}

static void log_point(struct plinput_point *point, int n)
{
	char pressure_fmt[4];
	char button_str[3];
	const char *pressure_str;
	const char *status_str;

	if (point->p < 0) {
		pressure_str = "---";
	} else {
		snprintf(pressure_fmt, 4, "%3i", point->p);
		pressure_fmt[3] = '\0';
		pressure_str = pressure_fmt;
	}

	if (point->btn == 1)
		button_str[0] = 'B';
	else if (point->btn == 0)
		button_str[0] = 'x';
	else
		button_str[0] = '-';

	if (point->rubber == 1)
		button_str[1] = 'R';
	else if (point->rubber == 0)
		button_str[1] = 'x';
	else
		button_str[1] = '-';

	button_str[2] = '\0';

	if (point->on == 1)
		status_str = " down";
	else if (point->on == 0)
		status_str = " up";
	else
		status_str = "";

	LOG("[%i.%06i] #%04i (%5i, %5i) %s %s%s",
	    (int) point->time.tv_sec, (int) point->time.tv_usec, n,
	    point->x, point->y, pressure_str, button_str, status_str);
}
