LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS += -Wall -O2
LOCAL_MODULE := touchlog
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := plinput.c
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../libplinput
LOCAL_STATIC_LIBRARIES := libplinput
include $(BUILD_EXECUTABLE)
