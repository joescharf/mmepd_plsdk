/*
  E-Paper control library - libepaper

  Copyright (C) 2012 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_PLEPAPER_BUILT_IN_H
#define INCLUDE_PLEPAPER_BUILT_IN_H 1

#include <libplepaper.h>

extern int epdc_init_noep(struct epdc *epdc, const char *dev);

#endif /* INCLUDE_PLEPAPER_BUILT_IN_H */
