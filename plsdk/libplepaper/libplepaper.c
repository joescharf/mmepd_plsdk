/*
  E-Paper control library - libepaper

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "built-in.h"
#include <libplepaper.h>
#include <plsdk/plconfig.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <dlfcn.h>

#define LOG_TAG "plep"
#include <plsdk/log.h>

#define MOD_NAME_DEF_SZ 16

#ifndef min
# define min(_a, _b) ((_a) < (_b) ? (_a) : (_b))
#endif

#ifndef max
# define max(_a, _b) ((_a) > (_b) ? (_a) : (_b))
#endif

/* the opaque structure used in the public interface */
struct plep {
	struct epdc epdc;
	struct plconfig *config;
	const char *mode;
	char *mode_buffer;
	unsigned opts;
	int rot;
	int xs;
	int ys;
	void *dl_handle;
};

static const char *plep_mode_list[] = {
	PLEP_MODE_PL_EPDC,
	PLEP_MODE_IMX,
	PLEP_MODE_EPSON,
	NULL
};

static void clear_epdc(struct plep *plep);
static int init_discover(struct plep *plep, const char *dev);
static int init_with_mode(struct plep *plep, const char *dev);
static plep_init_t *load_module(struct plep *plep, const char *mode);
static void free_module(struct plep *plep);
static int make_module_name(const char *mode, char **str, size_t *sz);
static int get_wfid(const struct plep *plep, const char *type,
		    const char *palette, const char *opt);
static int do_update(struct plep *plep, const struct plep_rect *area, int wfid,
		     int opts);
static void rotate_scale_point(const struct plep *plep, struct plep_point *pt,
			       const struct plep_point *in_pt);
#define swap_int(_a, _b) do {				\
		int tmp;				\
		tmp = _a;				\
		_a = _b;				\
		_b = tmp;				\
	} while (0)


/* ----------------------------------------------------------------------------
 * public interface
 */

/* -- initialisation -- */

struct plep *plep_init(const char *dev, const char *mode,
		       const char *config_file)
{
	struct plep *plep;
	int stat;

	plep = malloc(sizeof(struct plep));

	if (plep == NULL)
		goto err_exit;

	plep->config = plconfig_init(config_file, "libplepaper");

	if (plep->config == NULL)
		goto err_free_plep;

	plep->dl_handle = NULL;

	if (mode == NULL) {
		plep->mode = plconfig_get_str(plep->config, "mode", NULL);
		plep->mode_buffer = NULL;
	} else {
		plep->mode_buffer = strdup(mode);

		if (plep->mode_buffer == NULL)
			goto err_free_plconfig;

		plep->mode = plep->mode_buffer;
	}

	if (plep->mode == NULL)
		stat = init_discover(plep, dev);
	else
		stat = init_with_mode(plep, dev);

	if (plep->mode != NULL)
		LOG("mode: %s", plep->mode);

	if (stat) {
		LOG("Failed to initialise ePDC");
		goto err_free_all;
	}

	if (plep->epdc.description != NULL)
		LOG("description: %s", plep->epdc.description);

	if (plep->epdc.dev != NULL)
		LOG("device: %s", plep->epdc.dev);

	plep->opts = 0;
	plep->rot = 0;
	plep->xs = 1;
	plep->ys = 1;

	return plep;

err_free_all:
	if (plep->mode_buffer != NULL)
		free(plep->mode_buffer);
err_free_plconfig:
	plconfig_free(plep->config);
err_free_plep:
	free(plep);
err_exit:

	return NULL;
}

void plep_free(struct plep *plep)
{
	assert(plep != NULL);

	if (plep->epdc.free != NULL)
		plep->epdc.free(&plep->epdc);

	free_module(plep);

	if (plep->mode_buffer != NULL)
		free(plep->mode_buffer);

	plconfig_free(plep->config);
	free(plep);
}

const char *plep_get_mode(const struct plep *plep)
{
	assert(plep != NULL);

	return plep->mode;
}

const char *plep_get_description(const struct plep *plep)
{
	assert(plep != NULL);

	return plep->epdc.description;
}

void plep_log_info(const struct plep *plep)
{
	assert(plep != NULL);

	if (plep->epdc.log_info)
		plep->epdc.log_info(&plep->epdc);
}

const char *plep_get_dev(struct plep *plep)
{
	assert(plep != NULL);

	return plep->epdc.dev;
}

int plep_get_waveform_info(const struct plep *plep, int n,
			   struct plep_waveform_info *wfinfo)
{
	const struct epdc_wfdef *wfdef;

	assert(plep != NULL);
	assert(wfinfo != NULL);

	if (n >= plep->epdc.wflist_n)
		return -1;

	wfdef = &plep->epdc.wflist[n];
	wfinfo->type = wfdef->type;
	wfinfo->palette = wfdef->palette;
	wfinfo->opt = wfdef->opt;
	wfinfo->duration_ms = wfdef->duration_ms;

	return 0;
}

/* -- control -- */

int plep_get_wfid(const struct plep *plep, const char *path_input)
{
	static const char *delim = "/";
	const char *args[3] = { NULL, NULL, NULL };
	char short_path[64];
	size_t path_len;
	char *path;
	char *tok;
	int wfid;
	int n;

	assert(plep != NULL);

	if (path_input == NULL)
		return 0;

	path_len = strlen(path_input);

	if (path_len < ARRAY_SIZE(short_path)) {
		path = short_path;
	} else {
		path = malloc(path_len + 1);

		if (path == NULL) {
			LOG_ERRNO("Failed to allocate path buffer");
			return -1;
		}
	}

	strcpy(path, path_input);

	for (n = 0; n < ARRAY_SIZE(args); ++n) {
		tok = strsep(&path, delim);

		if (tok == NULL)
			break;

		args[n] = tok;
	}

	wfid = get_wfid(plep, args[0], args[1], args[2]);

	if (path != short_path)
		free(path);

	return wfid;
}

int plep_get_wfid_split(const struct plep *plep, const char *type,
			const char *palette, const char *opt)
{
	assert(plep != NULL);

	if (plep->epdc.wflist_n == 0)
		return -1;

	if (type == NULL)
		return 0;

	return get_wfid(plep, type, palette, opt);
}

void plep_set_rotation(struct plep *plep, int angle)
{
	assert(plep != NULL);

	angle = angle % 360;

	if (angle < 0)
		angle += 360;

	assert(!(angle % 90));
	assert(angle >= 0);
	assert(angle <= 270);

	plep->rot = angle;
}

int plep_get_rotation(const struct plep *plep)
{
	assert(plep != NULL);

	return plep->rot;
}

void plep_set_scale(struct plep *plep, int xs, int ys)
{
	assert(plep != NULL);

	plep->xs = xs;
	plep->ys = ys;
}

void plep_get_scale(const struct plep *plep, int *xs, int *ys)
{
	assert(plep != NULL);
	assert(xs != NULL);
	assert(ys != NULL);

	*xs = plep->xs;
	*ys = plep->ys;
}

void plep_set_opt(struct plep *plep, unsigned opts, int en)
{
	assert(plep != NULL);

	if (en)
		plep->opts |= opts;
	else
		plep->opts &= ~opts;
}

int plep_get_opt(struct plep *plep, unsigned opts)
{
	assert(plep != NULL);

	return (plep->opts & opts) ? 1 : 0;
}

int plep_set_hw_opt(struct plep *plep, enum plep_hw_opt opt, int value)
{
	assert(plep != NULL);
	assert(opt < _PLEP_HW_OPT_N_);

	if (plep->epdc.set_opt == NULL)
		return -1;

	return plep->epdc.set_opt(&plep->epdc, opt, value);
}

int plep_get_hw_opt(struct plep *plep, enum plep_hw_opt opt, int *value)
{
	assert(plep != NULL);
	assert(opt < _PLEP_HW_OPT_N_);
	assert(value != NULL);

	if (plep->epdc.get_opt == NULL)
		return -1;

	return plep->epdc.get_opt(&plep->epdc, opt, value);
}

int plep_update(struct plep *plep, const struct plep_rect *area, int wfid)
{
	assert(plep != NULL);
	assert(area != NULL);
	assert(wfid >= 0);

	return do_update(plep, area, wfid, plep->opts);
}

int plep_update_opts(struct plep *plep, const struct plep_rect *area,
		     int wfid, int opts)
{
	assert(plep != NULL);
	assert(area != NULL);
	assert(wfid >= 0);

	return do_update(plep, area, wfid, opts);
}

int plep_update_screen(struct plep *plep, int wfid)
{
	struct plep_rect area;
	int xres_scaled;
	int yres_scaled;

	assert(plep != NULL);
	assert(wfid >= 0);
	assert(plep->xs != 0);
	assert(plep->ys != 0);

	xres_scaled = plep->epdc.xres / plep->xs;
	yres_scaled = plep->epdc.yres / plep->ys;
	area.a.x = 0;
	area.a.y = 0;

	if (plep->rot % 180) {
		area.b.x = yres_scaled;
		area.b.y = xres_scaled;
	} else {
		area.b.x = xres_scaled;
		area.b.y = yres_scaled;
	}

	return do_update(plep, &area, wfid, plep->opts);
}

int plep_wait_power(struct plep *plep, int state)
{
	assert(plep != NULL);

	if (plep->epdc.wait_power == NULL)
		return 0;

	return plep->epdc.wait_power(&plep->epdc, state);
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static void clear_epdc(struct plep *plep)
{
	memset(&plep->epdc, 0, sizeof(struct epdc));
}

static int init_discover(struct plep *plep, const char *dev)
{
	char *mod_name;
	size_t mod_name_sz;
	const char * const *mode;
	plep_init_t *init;
	int stat;

	mod_name_sz = MOD_NAME_DEF_SZ;
	mod_name = malloc(mod_name_sz);

	if (mod_name == NULL)
		return -1;

	stat = -1;

	/* search for a loadable driver module */

	for (mode = plep_mode_list; *mode != NULL; ++mode) {
		clear_epdc(plep);

		if (make_module_name(*mode, &mod_name, &mod_name_sz) < 0)
			break;

		assert(mod_name != NULL);

		init = load_module(plep, mod_name);

		if (init != NULL) {
			if (!(init(&plep->epdc, dev))) {
				stat = 0;
				break;
			}

			free_module(plep);
		}
	}

	if (mod_name != NULL)
		free(mod_name);

	if (!stat) {
		plep->mode = *mode;
		return 0;
	}

	/* last chance is built-in null epdc implementation */

	clear_epdc(plep);
	plep->mode = PLEP_MODE_NOEP;

	return epdc_init_noep(&plep->epdc, dev);
}


static int init_with_mode(struct plep *plep, const char *dev)
{
	char *mod_name;
	size_t mod_name_sz;
	plep_init_t *init;

	clear_epdc(plep);

	if (!strcmp(plep->mode, PLEP_MODE_NOEP))
		return epdc_init_noep(&plep->epdc, dev);

	mod_name_sz = MOD_NAME_DEF_SZ;
	mod_name = malloc(mod_name_sz);

	if (mod_name == NULL)
		return -1;

	if (make_module_name(plep->mode, &mod_name, &mod_name_sz) < 0)
		return -1;

	assert(mod_name != NULL);

	init = load_module(plep, mod_name);

	free(mod_name);

	if (init == NULL)
		return -1;

	return init(&plep->epdc, dev);
}

static plep_init_t *load_module(struct plep *plep, const char *mod_name)
{
	void *dl;
	plep_init_t *epdc_init;
	const char **epdc_version;

	dl = dlopen(mod_name, RTLD_NOW);

	if (dl == NULL) {
		LOG("Failed to open module:\n%s", dlerror());
		goto err_exit;
		return NULL;
	}

	epdc_init = dlsym(dl, "epdc_init");

	if (epdc_init == NULL) {
		LOG("Failed to find epdc_init:\n%s", dlerror());
		goto err_dlclose;
	}

	epdc_version = dlsym(dl, "epdc_version");

	if (epdc_version == NULL) {
		LOG("Failed to find epdc_version:\n%s", dlerror());
		goto err_dlclose;
	}

	if ((*epdc_version == NULL) ||
	    strncmp(*epdc_version, PLEP_VERSION, sizeof(PLEP_VERSION))) {
		LOG("Invalid module version: %s", mod_name);
		goto err_dlclose;
	}

	plep->dl_handle = dl;

	return epdc_init;

err_dlclose:
	dlclose(dl);
err_exit:
	plep->dl_handle = NULL;

	return NULL;
}

static void free_module(struct plep *plep)
{
	if (plep->dl_handle)
		dlclose(plep->dl_handle);
}

static int make_module_name(const char *mode, char **str, size_t *sz)
{
	static const char *mod_format = "mod_plepaper_%s.so";
	size_t n;

	n = snprintf(*str, *sz, mod_format, mode);

	if (n >= *sz) {
		*sz = n + 1;
		*str = realloc(*str, *sz);

		if (*str == NULL) {
			return -1;
		}
	}

	n = snprintf(*str, *sz, mod_format, mode);

	if (n >= *sz) {
		free(*str);
		*str = NULL;
		return -1;
	}

	return 0;
}

static int get_wfid(const struct plep *plep, const char *type,
		    const char *palette, const char *opt)
{
	size_t i;
	int wfid = 0;

	for (i = 0; i < plep->epdc.wflist_n; i++) {
		const struct epdc_wfdef *wf = &plep->epdc.wflist[i];

		/* Look for a matching type */

		if (strcmp(wf->type, type))
			continue;

		if (palette == NULL) {
			wfid = i;
			break;
		}

		/* Look for a matching palette, or use NULL by default */

		if (wf->palette == NULL) {
			wfid = i;
			continue;
		}

		if (strcmp(wf->palette, palette))
			continue;

		if (opt == NULL) {
			wfid = i;
			break;
		}

		/* Look for a matching option, or use NULL by default */

		if (wf->opt == NULL) {
			wfid = i;
			continue;
		}

		if (strcmp(wf->opt, opt))
			continue;

		wfid = i;
		break;
	}

	return wfid;
}

static int do_update(struct plep *plep, const struct plep_rect *raw_area,
		     int wfid, int opts)
{
	const struct epdc_wfdef *wf;
	struct plep_rect area;
	int stat;

	memset(&area, 0, sizeof area);
	rotate_scale_point(plep, &area.a, &raw_area->a);
	rotate_scale_point(plep, &area.b, &raw_area->b);

	if (area.a.x > area.b.x)
		swap_int(area.a.x, area.b.x);

	if (area.a.y > area.b.y)
		swap_int(area.a.y, area.b.y);

	area.a.x = max(0, area.a.x);
	area.a.y = max(0, area.a.y);
	area.b.x = min(plep->epdc.xres, area.b.x);
	area.b.y = min(plep->epdc.yres, area.b.y);

	if ((area.a.x == area.b.x) || (area.a.y == area.b.y)
	    || (plep->epdc.update == NULL) || (wfid >= plep->epdc.wflist_n)) {
		LOG("Warning: skipping invalid update (%d %d %d %d) %d",
		    area.a.x, area.a.y, area.b.x, area.b.y, wfid);
		wf = NULL;
		stat = 0;
	} else {
		wf = &plep->epdc.wflist[wfid];
		stat = plep->epdc.update(&plep->epdc, &area, wf, opts);
	}

	/* emulate synchronised update by sleeping during update */
	if (!stat && (opts & PLEP_SYNC_UPDATE) && !plep->epdc.can_sync
	    && (wf != NULL))
		usleep(wf->duration_ms * 1000);

	return stat;
}

static void rotate_scale_point(const struct plep *plep, struct plep_point *pt,
			       const struct plep_point *in_pt)
{
	int tmp;

	switch (plep->rot) {
	case 0:
		pt->x = in_pt->x * plep->xs;
		pt->y = in_pt->y * plep->ys;
		break;

	case 90:
		tmp = in_pt->y * plep->xs;
		pt->y = in_pt->x * plep->ys;
		pt->x = plep->epdc.xres - tmp;
		break;

	case 180:
		pt->x = plep->epdc.xres - (in_pt->x * plep->xs);
		pt->y = plep->epdc.yres - (in_pt->y * plep->ys);
		break;

	case 270:
		tmp = in_pt->x * plep->ys;
		pt->x = in_pt->y * plep->xs;
		pt->y = plep->epdc.yres - tmp;
		break;
	}
}
