#!/bin/sh

set -e

ver="$1"

sed -i s/"\(PLEP_VERSION \"\)\(.*\)\(\".*\)$"/"\1$ver\3"/ libplepaper.h
git add libplepaper.h

exit 0
