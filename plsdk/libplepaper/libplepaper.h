/*
  E-Paper control library - libepaper

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_LIBPLEPAPER_H
#define INCLUDE_LIBPLEPAPER_H 1

#include <stdlib.h>

/**
   @file libplepaper.h

   This file defines a generic e-paper library interface.
*/

#define PLEP_VERSION "1.9"           /**< string for binary compatibility */

/** 2D point coordinates */
struct plep_point {
	int x;                       /**< x coordinate from left */
	int y;                       /**< y coordinate from top */
};

/** 2D rectangle coordinates */
struct plep_rect {
	struct plep_point a;         /**< first corder coordinates */
	struct plep_point b;         /**< opposite corner coordinates */
};

/** Waveform information */
struct plep_waveform_info {
	const char *type;            /**< waveform type */
	const char *palette;         /**< waveform palette */
	const char *opt;             /**< waveform option */
	unsigned duration_ms;        /**< waveform duration in ms */
};

/** Opaque structure used by public interface */
struct plep;

/**
   @name Waveform type identifiers
   @{
 */
#define PLEP_REFRESH "refresh"       /**< all pixels are updated */
#define PLEP_DELTA "delta"           /**< only changing pixels are updated */
#define PLEP_FAST "fast"             /**< faster update but lesser quality */
#define PLEP_HIGHLIGHT "highlight"   /**< very fast for text update */
/** @} */

/**
   @name Waveform palette identifiers
   @{
 */
#define PLEP_16GL "16GL"             /**< 16 grey levels */
#define PLEP_8GL "8GL"               /**< 8 grey levels */
#define PLEP_4GL "4GL"               /**< 4 grey levels */
#define PLEP_MONO "mono"             /**< 2 grey levels */
/** @} */

/**
   @name Waveform option identifiers
   @{
 */
#define PLEP_3SUBF "3SF"             /**< fast/mono/3 */
#define PLEP_6SUBF "6SF"             /**< fast/mono/6 */
#define PLEP_9SUBF "9SF"             /**< fast/mono/9 */
#define PLEP_FLICKER "flicker"       /**< fast/16GL/flicker */
#define PLEP_DIRECT "direct"         /**< fast/16GL/direct */
/** @} */

/**
   @name Usual ePDC mode identifiers
   @{
 */
#define PLEP_MODE_DISCOVER  (NULL)   /**< automatically discover ePDC */
#define PLEP_MODE_NOEP      "noep"   /**< use no e-paper */
#define PLEP_MODE_IMX       "imx"    /**< use Freescale i.MX508 driver */
#define PLEP_MODE_EPSON     "epson"  /**< use Epson S1D135x1 driver */
#define PLEP_MODE_PL_EPDC   "plepdc" /**< use PL control interface */
/** @} */

/** Update option flags */
enum plep_update_opt {
	PLEP_SYNC_UPDATE     = 1<<0, /**< synchronous update */
	PLEP_WAIT_POWER_OFF  = 1<<1, /**< wait for power off */
	PLEP_BW_IMAGE        = 1<<2, /**< apply B&W threshold */
	PLEP_PARTIAL         = 1<<3, /**< only update pixels that change */
};

/** Hardware option flags */
enum plep_hw_opt {
	PLEP_POWER_OFF_DELAY_MS = 0,  /**< display power off delay (ms) */
	PLEP_CLEAR_ON_EXIT,           /**< clear screen when ePDC is removed */
	PLEP_TEMPERATURE_AUTO,        /**< 0 for manual, 1 for automatic */
	PLEP_TEMPERATURE,             /**< temperature for manual mode */
	_PLEP_HW_OPT_N_
};


/**
   @name Initialisation
   @{
 */

/** Create an initialised plep instance
    @param[in] dev name of the device to open
    @param[in] mode ePDC mode identifier
    @param[in] config_file optional file path, NULL for default
    @return pointer to new plep instance or NULL if error
*/
extern struct plep *plep_init(const char *dev, const char *mode,
			      const char *config_file);

/** Free the handle associated with the plep device
    @param[in] plep plep instance as created by plep_init
*/
extern void plep_free(struct plep *plep);

/** @} */


/**
   @name Statistics
   @{
 */

/** Get the mode associated with the plep device
    @param[in] plep plep instance
    @returns const string with mode name
*/
extern const char *plep_get_mode(const struct plep *plep);

/** Get the mode description associated with the plep device
    @param[in] plep plep instance
    @return const string with mode description
*/
extern const char *plep_get_description(const struct plep *plep);

/** Ask the plep to dump log information to the console
    @param[in] plep plep instance
*/
extern void plep_log_info(const struct plep *plep);

/** Get path to the ePDC device (if any)
    @param[in] plep plep instance
    @return const string with device path or NULL if not applicable
*/
extern const char *plep_get_dev(struct plep *plep);

/** Get waveform information
    @param[in] plep plep instance
    @param[in] n waveform index
    @param[out] wfinfo pointer to receive waveform information
    @return -1 if error (i.e. n out of range), 0 otherwise
*/
extern int plep_get_waveform_info(const struct plep *plep, int n,
				  struct plep_waveform_info *wfinfo);

/** @} */


/**
   @name Control
   @{
 */

/** Get the waveform ID associated with a path
    @param[in] plep plep instance
    @param[in] path string with the waveform path (type/palette/option)
    @return -1 if error, waveform ID otherwise
 */
extern int plep_get_wfid(const struct plep *plep, const char *path);

/** Get the waveform ID associated with a split text description
    @param[in] plep plep instance
    @param[in] type Waveform type (refresh, delta, fast...)
    @param[in] palette Waveform palette (16GL, mono...)
    @param[in] opt Specific options for advanced waveforms
    @return -1 if error, waveform ID otherwise
*/
extern int plep_get_wfid_split(const struct plep *plep, const char *type,
			       const char *palette, const char *opt);

/** Set the screen rotation angle
    @param[in] plep  plep instance
    @param[in] angle rotation angle in degrees
*/
extern void plep_set_rotation(struct plep *plep, int angle);

/** Recover the current screen rotation angle
    @param[in] plep plep instance
    @return rotation angle in degrees
*/
extern int plep_get_rotation(const struct plep *plep);

/** Set the coordinates scaling factors
    @param[in] plep plep instance
    @param[in] xs horizontal scaling factor
    @param[in] ys vertical scaling factor
 */
extern void plep_set_scale(struct plep *plep, int xs, int ys);

/** Get the coordinates scaling factors
    @param[in] plep plep instance
    @param[out] xs horizontal scaling factor
    @param[out] ys vertical scaling factor
 */
extern void plep_get_scale(const struct plep *plep, int *xs, int *ys);

/** Set plep instance options
    @param[in] plep plep instance
    @param[in] opts bit mask of options to be modified
    @param[in] en 1 to enable or 0 to disable
*/
extern void plep_set_opt(struct plep *plep, unsigned opts, int en);

/** Test if any of the options specified are currently selected in the plep
    device
    @param[in] plep plep instance
    @param[in] opt bit mask of options to test
    @return 1 if any of the options are set, 0 otherwise
*/
extern int plep_get_opt(struct plep *plep, enum plep_update_opt opt);

/** Set a hardware option value
    @param[in] plep plep instance
    @param[in] opt hardware option identifier
    @param[in] val value to use for the hardware option
    @return -1 if error, 0 otherwise
 */
extern int plep_set_hw_opt(struct plep *plep, enum plep_hw_opt opt, int val);

/** Get a hardware option value
    @param[in] plep plep instance
    @param[in] opt hardware option identifier
    @param[out] val hardware option value
    @return -1 if error, 0 otherwise
 */
extern int plep_get_hw_opt(struct plep *plep, enum plep_hw_opt opt, int *val);

/** Update the screen using the specified rectangle and waveform
    @param[in] plep plep instance
    @param[in] area rectangular region to update
    @param[in] wfid waveform id to use
    @return -1 if error, 0 otherwise
*/
extern int plep_update(struct plep *plep, const struct plep_rect *area,
		       int wfid);

/** Update the screen using the specified area and waveform
    @param[in] plep plep instance
    @param[in] area rectangular region to update
    @param[in] wfid waveform id to use
    @param[in] opts options to use
    @return -1 if error, 0 otherwise
*/
extern int plep_update_opts(struct plep *plep, const struct plep_rect *area,
			    int wfid, int opts);

/** Update the whole screen area using the specified waveform
    @param[in] plep plep instance
    @param[in] wfid waveform id to use for the update
    @return -1 if error, 0 otherwise
*/
extern int plep_update_screen(struct plep *plep, int wfid);

/** Wait for a power state
    @param[in] plep plep instance
    @param[in] state power state to wait for
    @return -1 if error, 0 otherwise
*/
extern int plep_wait_power(struct plep *plep, int state);

/** @} */

/* ------------------------------------------------------------------------- */

/**
   @name E-Paper display controller module interface
   @{
 */

/** Basic macro to get the number of elements in a fixed array */
#ifndef ARRAY_SIZE
# define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))
#endif

/** Waveform definition */
struct epdc_wfdef {
	const char *type;            /**< waveform type */
	const char *palette;         /**< waveform palette */
	const char *opt;             /**< waveform option */
	int epdc_id;                 /**< waveform ID */
	unsigned duration_ms;        /**< waveform duration in ms */
};

/** ePDC back-end interface definition */
struct epdc {
	const char *description;     /**< ePDC description */
	struct epdc_wfdef *wflist;   /**< waveform list */
	size_t wflist_n;             /**< number of entries in wflist */
	unsigned xres;               /**< horizontal resolution */
	unsigned yres;               /**< vertical resolution */
	int can_sync;                /**< synchronous update capability */
	char *dev;                   /**< path to device used */

	/**
	   free resources associated with ePDC
	   @param[in] e epdc instance
	*/
	void (*free)(struct epdc *e);

	/**
	   print statistics about ePDC
	   @param[in] e epdc instance
	*/
	void (*log_info)(const struct epdc *e);

	/**
	   set a hardware ePDC option
	   @param[in] e epdc instance
	   @param[in] opt option identifier
	   @param[in] value option value
	   @return -1 if error, 0 otherwise
	 */
	int (*set_opt)(struct epdc *e, enum plep_hw_opt opt, int value);

	/**
	   get a hardware ePDC option
	   @param[in] e epdc instance
	   @param[in] opt option identifier
	   @param[out] value value of the option
	   @return -1 if error, 0 otherwise
	*/
	int (*get_opt)(struct epdc *e, enum plep_hw_opt opt, int *value);

	/**
	   send a display update request
	   @param[in] e epdc instance
	   @param[in] a update area
	   @param[in] wf waveform
	   @param[in] opts option flags
	   @return -1 if error, 0 otherwise
	*/
	int (*update)(struct epdc *e, const struct plep_rect *a,
		      const struct epdc_wfdef *wf, int opts);

	/**
	   wait for the display power to reach a certain state
	   @param[in] e epdc instance
	   @param[in] state power state to wait for
	   @return -1 if error, 0 otherwise
	*/
	int (*wait_power)(struct epdc *e, int state);

	/** private data specific to each ePDC implementation */
	void *priv;
};

/** ePDC module init function */
typedef int (plep_init_t) (struct epdc *epdc, const char *dev);

/* Macro to define the module entry point symbol */
#define PLEPAPER_MOD_INIT(_epdc, _dev) \
	int epdc_init(struct epdc *_epdc, const char *_dev)

/** Macro to add version string for runtime binary compatibility check */
#define PLEPAPER_VERSION const char *epdc_version = PLEP_VERSION


/** @} */

#endif /* INCLUDE_LIBPLEPAPER_H */
