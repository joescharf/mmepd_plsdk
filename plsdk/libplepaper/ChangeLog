Release v1.8 -> v1.9 - 2013-10-11

Andrew Cox (1):
      Add PLEP_TEMPERATURE and PLEP_TEMP_MODE hw options.

Guillaume Tucker (6):
      initialise plep->dl_handle to NULL to fix error handling
      Android.mk: add include path to libplutil
      fix mode_buffer pointer initialisation
      rename PLEP_TEMP_MODE to _TEMPERATURE_AUTO + add Doxygen
      epdc-noep.c: add stubs for the hardware options
      Android.mk: fix include path to libplutil

Michele Marchetti (2):
      use libplutil to read config file + use common logging
      add optional config file path to plep_init

Release v1.7 -> v1.8 - 2013-07-19

Guillaume Tucker (3):
      Makefile: only build libplepaper.a, not libplepaper.so
      add PLEP_CLEAR_ON_EXIT hardware option
      add setver.sh script to set the library version string

Release v1.6.1 -> v1.7 - 2013-06-03

Guillaume Tucker (1):
      change PLEPAPER_MOD_INIT macro so it can be used by modules

Release v1.6 -> v1.6.1 - 2013-03-26

Guillaume Tucker (2):
      fix LGPL license files + update copyright notices
      .gitignore: depend_*.mk

Release v1.5 -> v1.6 - 2013-03-18

Guillaume Tucker (2):
      update copyright notices
      add PLEP_MODE_EPSON and include it in discovery list

Release v1.4 -> v1.5 - 2013-01-29

Guillaume Tucker (4):
      fix unclean error handling which may cause segfault
      add coordinates scaling factors
      use integer for scaling factors + fix rotation
      move the rotation and scaling into do_update

Release v1.3 -> v1.4 - 2013-01-08

Guillaume Tucker (1):
      add plep_get_description to get the ePDC mode description

Release v1.2 -> v1.3 - 2012-12-11

Guillaume Tucker (4):
      plep_update: improve input checks
      do_update: crop update area to ePDC resolution
      plep_set_rotation: fix assertion to accept angle = 0
      epdc-noep: populate xres, yres and update in epdc interface

Release v1.1 -> v1.2 - 2012-11-20

Guillaume Tucker (7):
      plep_get_wfid: return default (0) if string is NULL
      libplepaper.h: add PLEP_PARTIAL option
      libplepaper.h: fix minor errors in Doxygen
      group plep_{get|set}_opt with other similar functions
      add plep_{get|set}_hw_opt for hardware options
      load_module: tweak error message about wrong version
      add LOG_ERRNO and use it + use dlerror

Release v1.0 -> v1.1 - 2012-11-07

Guillaume Tucker (14):
      update copyright statements with authors
      implement new waveform naming scheme
      improve Doxygen + remove deprecated ePDC names
      add plep_get_waveform_info to retrieve waveform stats
      review and improve Doxygen
      use path in get_wfid + add get_wfid_split
      get_wfid: allocate extra byte for null terminator
      Android.mk: add -O2 compiler flag
      rename plepaper.c -> libplepaper.c
      improve Doxygen

Release v0.10 -> v1.0 - 2012-10-01

Guillaume Tucker (17):
      fix fast builder Makefile
      epdc-plapi: fix fast builder linkage issue
      epdc-plapi: fix build again with NO_PLDISPLAYAPI
      add rotation support
      rename `struct plep_dc' -> `struct plep'
      rename variables `epdc' -> `plep'
      epdc.c: minor tidy-up
      Makefile: tweak CFLAGS
      remove now useless NO_DISPLAYAPI directive
      rename epdc -> plepaper.c
      rename epdc -> epif
      remove plep_mode and use string identifier instead
      fix epaper mode iteration NULL test
      remote Autotools files for now
      expose plep_init_t and add epdc_version symbol in modules
      only call free_module when it has been initialised
      add LGPL v3 license + update copyright statements

Nick Terry (4):
      Making epdc drivers loadable modules
      Making epdc drivers loadable modules
      Add support for the IMX driver module
      Fixing Android.mk for NDK build

Sean Walsh (1):
      Refactor to prepare for inclusion in sdk

Release v0.9 -> v0.10 - 2012-05-02

Guillaume Tucker (11):
      epdc: add epdc_update_opts
      epdc-plepdc: use sync update also when power_off enabled
      epdc: fix compiler warning with const pointers
      epdc: add EPDC_BW_IMAGE option to enable B&W threshold
      anyfb: fix RGB565 green rounding in get_grey
      anyfb: add anyfb_apollo_grey_16 palette
      anyfb: fill coordinates in fill_rect
      epdc-plapi: update to v1.6 (B&W threshold argument)
      anyfb: fix GL13 value in palette
      epdc-plepdc: clean-up B&W threshold implementation
      bump to 0.10

Release v0.8 -> v0.9 - 2012-04-18

Guillaume Tucker (8):
      anyfb_get_grey: fix `green' effect with RGB565
      epdc: add _get_mode and _wait_power
      epdc-plapi: use PL Display API unless NO_PLDISPLAYAPI is set
      epdc: fix usage of NO_PLDISPLAYAPI to use Display API
      Revert "epdc: fix epdc_get_wfid"
      plepdc: fix waveform id's and B&W thresholds
      epdc: add more update options (wait for power off)
      bump to 0.9

Release v0.7 -> v0.8 - 2012-02-15

Guillaume Tucker (6):
      epdc: fix epdc_get_wfid
      add missing includes for fast builder
      anyfb: fix color888 macro (for 32bpp GRB 888)
      add support for plepdc with synchronous updates
      global review and clean-up
      bump to 0.8

Release v0.6 -> v0.7 - 2011-12-09

Guillaume Tucker (4):
      anyfb: add anyfb_crop to crop any area down to active area
      epdc-plapi: fix update area coordinates convention
      epdc: fix support for fast mono waveforms
      bump to 0.7

John Long (1):
      Add EPDC_MONO_9_9 and EPDC_HIGHLIGHT waveforms

Release v0.5 -> v0.6 - 2011-11-29

Guillaume Tucker (12):
      review and clean-up
      anyfb & epdc: remove null frames and init clear API
      remove anyfb control commands and support synchro in epdc
      remove all ePDC control from anyfb + add epdc mode
      epdc: complete `discover' mode
      epdc: remove epif.dev and add it as init argument
      epdc: add support for PL Display API
      epdc-plapi: remove noisy log message
      epdc: improve ePDC selection + clean-up
      anyfb: compute line length if not provided by the driver
      ChangeLog: update v0.5 -> v0.6

Release v0.4 -> v0.5 - 2011-08-15

Guillaume Tucker (9):
      configure.ac: v0.4
      epdc.c: handle new IOCTL for init clear
      epdc: add API to generate null frames
      epdc: re-order functions + tweak log messages
      epdc.c: fix log message in null_frames
      Android.mk: add pl-kernel-headers to the include path
      bump version to v0.5

Release v0.3 -> v0.4 - 2011-05-20

Guillaume Tucker (18):
      anyfb: move synchronous member to a flags structure
      anyfb: remove init_default and look for NULL dev instead
      anyfb: add log_info + clean-up init
      anyfb: add anyfb_init_epfb to also initialise an epdc instance
      anyfb: move strdup(dev) to init_fb
      anyfb: move buffer size log into anyfb_log_info
      epdc: add epdc_log_info and call it from anyfb_log_info as well
      add a few missing assert statements + minor clean-up
      anyfb: do not write outside fb + use signed coords
      anyfb: crop the update region before passing it to epdc
      anyfb: fix compiler warnings (unsigned res and singed coords)
      anyfb: add crop area + further clean-up
      anyfb: add anyfb_is_point_in_area
      anyfb: get_crop_area: populate crop area from argument
      anyfb: add anyfb_apply_border to shrink a rectangle
      anyfb: fix compiler warning + sanitise set_crop_area
      epdc: just log a warning if wrong ioctl API version
      anyfb: draw_line: fix test for invalid 0-length coordinates

Release v0.2 -> v0.3 - 2011-05-05

Guillaume Tucker (7):
      add copyright notices to all *.c and *.h files
      anyfb: store fbdevice path and add _get_dev function to get it back
      epdc: remove auto-update API
      Android.mk + epdc: use fixed header version for Android
      bump version to 0.3

Release v0.1 -> v0.2 - 2011-04-28

Guillaume Tucker (13):
      anyfb: fix compiler warning in _colcmp
      Android.mk: remove unused include path (those files are now local)
      Android.mk: use function to get all the C files in sub-dirs
      Android.mk: fix LOCAL_CFLAGS (not PRIVATE_CFLAGS)
      anyfb: look for /dev/graphics/fb0 first on Android
      anyfb: fix color888 typo
      bump version to 0.2

