LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := libplepaper
LOCAL_SRC_FILES := libplepaper.c epdc-noep.c
LOCAL_CFLAGS += -Wall -O2
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../libplutil
include $(BUILD_STATIC_LIBRARY)
