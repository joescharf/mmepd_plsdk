/*
  E-Paper control library - ePDC back-end stub

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libplepaper.h>

#define LOG_TAG "noep"
#include <plsdk/log.h>

static const int noep_hw_opt_init[_PLEP_HW_OPT_N_] = {
	[PLEP_POWER_OFF_DELAY_MS] = 0,
	[PLEP_CLEAR_ON_EXIT] = 0,
	[PLEP_TEMPERATURE_AUTO] = 0,
	[PLEP_TEMPERATURE] = 20,
};

struct noep_priv {
	int hw_opt[_PLEP_HW_OPT_N_];
};

static struct epdc_wfdef g_wflist[] = {
	{ PLEP_REFRESH,       NULL,      NULL,       0, 1000 },
	{ PLEP_REFRESH,       PLEP_MONO, NULL,       1,  500 },
	{ PLEP_DELTA,         NULL,      NULL,       2, 1000 },
	{ PLEP_DELTA,         PLEP_MONO, NULL,       3,  500 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_3SUBF, 4,   80 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_6SUBF, 5,  140 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_9SUBF, 6,  200 },
	{ PLEP_HIGHLIGHT,     PLEP_4GL,  NULL,       7,   80 },
};

static void noep_free(struct epdc *e)
{
	free(e->priv);
}

static int noep_update(struct epdc *e, const struct plep_rect *a,
		       const struct epdc_wfdef *wf, int opts)
{
	return 0;
}

static int noep_set_opt(struct epdc *e, enum plep_hw_opt opt, int value)
{
	struct noep_priv *p = e->priv;

	p->hw_opt[opt] = value;

	return 0;
}

static int noep_get_opt(struct epdc *e, enum plep_hw_opt opt, int *value)
{
	struct noep_priv *p = e->priv;

	*value = p->hw_opt[opt];

	return 0;
}

int epdc_init_noep(struct epdc *epdc, const char *dev)
{
	struct noep_priv *p;
	int hw_opt;

	p = malloc(sizeof(struct noep_priv));

	if (p == NULL)
		return -1;

	for (hw_opt = 0; hw_opt < _PLEP_HW_OPT_N_; ++hw_opt)
		p->hw_opt[hw_opt] = noep_hw_opt_init[hw_opt];

	dev = NULL;
	epdc->priv = p;
	epdc->description = "No-op E-Paper stub";
	epdc->wflist = g_wflist;
	epdc->wflist_n = ARRAY_SIZE(g_wflist);
	epdc->xres = 640;
	epdc->yres = 480;
	epdc->free = noep_free;
	epdc->update = noep_update;
	epdc->set_opt = noep_set_opt;
	epdc->get_opt = noep_get_opt;

	return 0;
}
