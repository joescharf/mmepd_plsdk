objdirs = $(addprefix $(TOP_DIR)/$(OBJDIR_ROOT)/$(ARCH)/$(RELDIR)/,$(srcdirs))
srcwildcards := $(addsuffix /*.c,$(srcdirs))
src := $(filter-out $(exclude),$(wildcard $(srcwildcards)))
obj := $(addsuffix .o,$(basename $(src)))
