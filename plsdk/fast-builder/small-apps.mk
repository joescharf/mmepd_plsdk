$(OUTDIR)/%: %.c
	@echo "  CC      " $(call relobj,$@)
	@$(CC) $(CFLAGS) -o $@ $<

apps := $(addprefix $(OUTDIR)/,$(basename $(src)))

all: mkoutputdirs mkobjdirs $(apps)
	@echo -n

apps: $(src)
