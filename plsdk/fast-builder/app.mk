# Fast Builder - app.mk

target_out = $(addprefix $(BINDIR)/,$(out))
target_obj := $(addprefix $(OBJDIR)/,$(obj))
target_libs := $(addprefix $(LIBDIR)/,$(libs))
static_libs := $(filter %.a,$(target_libs))
dynamic_libs := $(filter %.so,$(target_libs))
CFLAGS += $(addprefix -I,$(INCLUDE))

all: mkoutputdirs mkobjdirs $(ARCHDEP) $(target_out)
	@echo -n

$(target_out): $(target_obj) $(static_libs) $(dynamic_libs)
	@echo "  LINK    " $(call relout,$@)
ifeq ($(LANG),C++)
	@$(C++) $(CFLAGS) -o $@ $(target_obj) $(static_libs) $(LDFLAGS)
else
	@$(CC) $(CFLAGS) -o $@ $(target_obj) $(static_libs) $(LDFLAGS)
endif

ifneq ($(MAKECMDGOALS),clean)
-include $(ARCHDEP)
endif
