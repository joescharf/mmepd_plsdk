# Fast Builder - builder.mk

NATIVE_ARCH := $(shell uname -m)
ARCH ?= $(NATIVE_ARCH)

AS := $(CROSS_COMPILE)as
LD := $(CROSS_COMPILE)ld
CC := $(CROSS_COMPILE)gcc
C++ := $(CROSS_COMPILE)g++
CPP := $(CC) -E
AR := $(CROSS_COMPILE)ar
NM := $(CROSS_COMPILE)nm
STRIP := $(CROSS_COMPILE)strip
OBJCOPY := $(CROSS_COMPILE)objcopy
OBJDUMP := $(CROSS_COMPILE)objdump

OUTDIR_ROOT := out
OBJDIR_ROOT := obj
RELDIR := $(subst $(TOP_DIR)/,,$(realpath $(CURDIR)))
OUTDIR := $(TOP_DIR)/$(OUTDIR_ROOT)/$(ARCH)
LIBDIR := $(OUTDIR)/lib
BINDIR := $(OUTDIR)/bin
INCDIR := $(OUTDIR)/include
OBJDIR := $(TOP_DIR)/$(OBJDIR_ROOT)/$(ARCH)/$(RELDIR)
INCLUDE += $(INCDIR)
CFLAGS += -L$(LIBDIR)
CFLAGS += $(addprefix -I,$(INCLUDE))
LDFLAGS :=

.DEFAULT_GOAL := all

ARCHDEP := depend_$(ARCH).mk

ifdef srcdirs
objdirs = $(addprefix $(TOP_DIR)/$(OBJDIR_ROOT)/$(ARCH)/$(RELDIR)/,$(srcdirs))
else
objdirs := $(OBJDIR)
endif

ifndef src
src := $(filter-out $(exclude),$(wildcard *.c) $(wildcard *.cpp))
endif

headers := $(wildcard *.h)
obj := $(addsuffix .o,$(basename $(src)))

relobj = $(RELDIR)/$(subst $(OBJDIR)/,,$(1))
relout = $(subst $(OUTDIR)/,,$(1))

$(OBJDIR)/%.o: %.c
	@echo "  CC      " $(call relobj,$@)
	@$(CC) $(CFLAGS) -c -fPIC -o $@ $<

$(OBJDIR)/%.o: %.cpp
	@echo "  C++     " $(call relobj,$@)
	@$(C++) $(CFLAGS) -c -fPIC -o $@ $<

$(LIBDIR)/%.a:
	@echo "  AR      " $(call relout,$@)
	@$(AR) rcs $@ $^

$(LIBDIR)/%.so:
	@echo "  LINK    " $(call relout,$@)
	@so=$(notdir $@)
ifeq ($(LANG),C++)
	@$(C++) $(CFLAGS) -fpic -shared -Wl,-soname,$(so) -o $@ $^ $(LDFLAGS)
else
	@$(CC) $(CFLAGS) -fpic -shared -Wl,-soname,$(so) -o $@ $^ $(LDFLAGS)
endif

mkoutputdirs: $(LIBDIR) $(BINDIR) $(INCDIR)
	@echo -n

$(INCDIR):
	@mkdir -p $(INCDIR)

$(INCDIR)/%.h: %.h
	@echo "  INSTALL " $(call relout,$@)
	@install -D -m644 $< $@

$(LIBDIR):
	@mkdir -p $(LIBDIR)

$(BINDIR):
	@mkdir -p $(BINDIR)

mkobjdirs:
	@for dir in $(objdirs); do \
	    mkdir -p $$dir; \
	done

$(ARCHDEP): $(src) $(headers)
	@$(CC) -MM $(addprefix -I,$(INCLUDE)) $(src) > $@ || { \
	    rm -f $@; exit 1; }
	@python $(BUILDER_HOME)/fixup_depend.py $@ "$(src)"
	@sed -i -e s/'^\(.*\.o: \)'/'$(subst /,\/,$(OBJDIR))\/\1'/g $@

.PHONY: sub-make clean print_arch

sub-make:
	@for dir in $(subdirs); do \
	    $(MAKE) -C $$dir -s $(MAKECMDGOALS) || exit 1; \
	done

clean:
	@echo "  CLEAN   " $(RELDIR)
	-@rm -rf $(OBJDIR)
	-@rm -f $(target_out)
	-@rm -f $(target_out_lib)
	-@rm -f $(target_out_inc)
	-@rm -f $(target_pysrc)
	-@rm -f $(target_pyobj)
	-@rm -f $(ARCHDEP)
	-@rm -f pydepend_$(ARCH).mk

	@for dir in $(subdirs); do \
	    $(MAKE) -C $$dir -s clean || exit 1; \
	done

print-arch:
	@echo "  Architecture: $(ARCH)"
