# Fast Builder - python.mk

ifeq ($(PYVER),)
ifeq ($(ARCH),$(NATIVE_ARCH))
PYVER := $(shell python -V 2>&1 | sed -e s/'^Python \([0-9]\.[0-9]\)\.[0-9]$$'/'\1'/)
INCLUDE += /usr/include/python$(PYVER)
else
$(error "Please define PYVER with the target Python version number")
endif
else
INCLUDE += $(INCDIR)/python$(PYVER)
endif

PYDIR := $(TOP_DIR)/$(OUTDIR_ROOT)/$(ARCH)/python$(PYVER)
relpy = $(subst $(PYDIR)/,,$(1))

PYARCHDEP = pydepend_$(ARCH).mk

# --- Native extension modules ---

ifneq ($(src),) # with C extensions

CFLAGS += -fno-strict-aliasing

target_out = $(addprefix $(PYDIR)/,$(out))
target_out_inc = $(addprefix $(INCDIR)/,$(inc))
target_obj := $(addprefix $(OBJDIR)/,$(obj))
static_libs := $(addprefix $(LIBDIR)/,$(libs))

$(PYDIR)/%.so:
	@echo "  LINK    " python$(PYVER)/$(call relpy,$@)
	@so=$(notdir $@)
ifeq ($(LANG),C++)
	@$(C++) $(CFLAGS) -fpic -shared -Wl,-soname,$(so) -o $@ $^ $(LDFLAGS)
else
	@$(CC) $(CFLAGS) -fpic -shared -Wl,-soname,$(so) -o $@ $^ $(LDFLAGS)
endif

$(target_out): $(target_obj) $(static_libs)

$(target_out_inc): $(inc)

endif # with C extensions

# --- Python files ---

ifndef pysrc
pysrc := $(filter-out $(exclude),$(wildcard *.py))
endif

target_pysrc := $(addprefix $(PYDIR)/, $(pysrc))
target_pyobj := $(addsuffix c,$(target_pysrc))

$(PYDIR)/%.py: %.py
	@echo "  INSTALL " python$(PYVER)/$(call relpy,$@)
	@install -D -m644 $< $@

$(PYDIR)/%.pyc: $(PYDIR)/%.py
	@echo "  PYC     " python$(PYVER)/$(call relpy,$@)
	@python -c "import py_compile; import sys; py_compile.compile('$<')"

$(PYDIR):
	@mkdir -p $(PYDIR)

$(PYARCHDEP): $(pysrc)
	@for f in $(pysrc); do \
	    echo $(PYDIR)/$$f: $$f >> $@; \
	    echo $(PYDIR)/$$fc: $(PYDIR)/$$f >> $@; \
	done

ifneq ($(src),) # with C extensions
all: $(PYDIR) mkobjdirs $(ARCHDEP) $(PYARCHDEP) \
	$(target_out_inc) $(target_out) $(target_pysrc) $(target_pyobj)
	@echo -n
else # with only pure Python files
all: $(PYDIR) $(PYARCHDEP) $(target_pysrc) $(target_pyobj)
	@echo -n
endif

ifneq ($(MAKECMDGOALS),clean)
ifneq ($(src),) # with C extensions
-include $(ARCHDEP)
endif
-include $(PYARCHDEP)
endif
