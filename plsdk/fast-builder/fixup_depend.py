import sys
import re

# This is to work around an issue with gcc -MM where the sub-directory is not
# included in the object path.

def main(argv):
    if len(argv) < 3:
        print("Invalid arguments")
        return False

    depend_file_name = argv[1]
    src = argv[2]

    depend = open(depend_file_name).read()
    obj_re = re.compile(r"^(.*)\.o: (.*)")
    depend_out = ''
    dirs = dict()

    for src_file in src.split(' '):
        src_split = src_file.rsplit('/', 1)
        if len(src_split) > 1:
            dirs[src_split[1].rsplit('.', 1)[0]] = src_split[0]

    for line_in in depend.split('\n'):
        m = obj_re.match(line_in)
        if m is not None:
            src_key = m.group(1)
            if src_key in dirs:
                src_dep = m.group(2)
                dep_fix = "{0}/{1}.o: {2}".format(
                    dirs[src_key], src_key, src_dep)
                depend_out += dep_fix + "\n"
                continue
        depend_out += line_in + "\n"

    open(depend_file_name, 'wb').write(depend_out)

    return True

if __name__ == '__main__':
    if main(sys.argv) is True:
        sys.exit(0)
    else:
        sys.exit(1)
