# Fast Builder - lib.mk

target_out_lib = $(addprefix $(LIBDIR)/,$(out))
target_out_inc = $(addprefix $(INCDIR)/,$(inc))
target_obj := $(addprefix $(OBJDIR)/,$(obj))
static_libs := $(addprefix $(LIBDIR)/,$(libs))
CFLAGS += $(addprefix -I,$(INCLUDE))

all: mkobjdirs mkoutputdirs $(ARCHDEP) $(target_out_inc) $(target_out_lib)
	@echo -n

$(target_out_lib): $(target_obj) $(static_libs)

$(target_out_inc): $(inc)

ifneq ($(MAKECMDGOALS),clean)
-include $(ARCHDEP)
endif
