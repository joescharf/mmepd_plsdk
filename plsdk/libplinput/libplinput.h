/*
  Input event library - libplinput

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_LIBPLINPUT_H
#define INCLUDE_LIBPLINPUT_H 1

#include <sys/time.h>

/**
   @file libplinput.h

   This library contains general features to use various input devices.
*/

/** Library version */
#define LIBPLINPUT_VERSION "1.5"     /**< string for binary compatibility */

/** Input device capabilities */
enum plinput_capability {
	PLINPUT_CAP_BUTTONS = 0,     /**< has buttons */
	PLINPUT_CAP_SYNC,            /**< can send sync events */
	PLINPUT_CAP_ABS_COORD,       /**< has absolute coodinates */
	PLINPUT_CAP_REL_COORD,       /**< has relative coordinates */
	_PLINPUT_CAP_MAX_
};

/** Input device options */
enum plinput_opt {
	PLINPUT_OPT_USE_REL_COORDS,  /**< process relative x and y (mouse) */
	PLINPUT_OPT_ACCEPT_SIGNALS,  /**< do not interrupt read upon signal */
	PLINPUT_OPT_ROTATION_ANGLE,  /**< rotate 0, 90, 180 or 270 deg. cw */
};

/** Touch or mouse point event information */
struct plinput_point {
	struct timeval time;         /**< timestamp when the event occurs */
	int x;                       /**< x coordinate */
	int y;                       /**< y coordinate */
	int p;                       /**< pressure */
	int on;                      /**< touch on (contact or click) */
	int btn;                     /**< button status (1 = pressed) */
	int rubber;                  /**< stylus rubber status (1 = active) */
};

/** Key event information */
struct plinput_key {
	struct timeval time;         /**< timestamp when the event occurs */
	unsigned code;               /**< key code */
	int on;                      /**< 1 if key pressed, 0 otherwise */
};

/** Device name relationship */
struct plinput_dev_name {
	char *device;                /**< device path (/dev/input/event?) */
	char *name;                  /**< full device name (product name) */
};

/** Opaque structure use by public interface */
struct plinput;

/** Callback function type to handle signals while waiting for event */
typedef int (*plinput_abort_cb_t) (void);


/**
   @name Initialisation
   @{
 */

/** Create an initialised plinput instance
    @param[in] dev path to event device
    @return pointer to new plinput instance or NULL if error
 */
extern struct plinput *plinput_init(const char *dev);

/** Create an initialised plinput instance with device name search
    @param[in] regex regular expression to search for a device
    @param[in] input_dir directory with input devices, NULL for system default
    @return pointer to new plinput instance or NULL if error
*/
extern struct plinput *plinput_init_from_name(
	const char *regex, const char *input_dir);

/** Free memory and other resources associated with plinput instance
    @param[in] plinput plinput instance as created by plinput_init*
 */
extern void plinput_free(struct plinput *plinput);

/** @} */


/**
   @name Statistics
   @{
 */

/** List of strings with key names, or NULL for those not defined */
extern const char * const plinput_key_names[];

/** Length of plinput_key_names */
extern const unsigned long plinput_n_key_names;

/** Return the key code associated with the given key name
    @param[in] name key name like "KEY_HOME"
    @return key code or KEY_RESERVED if not found
 */
extern unsigned long plinput_get_key_code(const char *name);

/** Make a list of available input devices with their names
    @param[in] input_dir input device directory path or NULL for system default
    @return array of plinput_dev_name, to be freed with
            plinput_free_device_names
 */
extern struct plinput_dev_name *plinput_get_device_names(
	const char *input_dir);

/** Free array of input device names
    @param[in] dev_names array of input device names as created by
                         plinput_get_device_names
 */
extern void plinput_free_device_names(struct plinput_dev_name *dev_names);

/** Search for a device within a list given a regular expression
    @param[in] names array of device names
    @param[in] regex regular expression to use for the search
    @return the first device whose name matches, or NULL if none found
 */
extern const struct plinput_dev_name *plinput_search_device(
	const struct plinput_dev_name *names, const char *regex);

/** Print some general statistics for a given input device
    @param[in] plinput plinput instance
 */
extern void plinput_log_info(const struct plinput *plinput);

/** Enable full logging
    @param[in] plinput plinput instance
    @param[in] on enable if 1, disable if 0
 */
extern void plinput_set_full_log(struct plinput *plinput, int on);

/** Get the input device path
    @param[in] plinput plinput instance
    @return device name or NULL if error
 */
extern const char *plinput_get_dev(const struct plinput *plinput);

/** Retrieve the device name
    @param[in] plinput plinput instance
    @return device name or NULL if none
 */
extern const char *plinput_get_name(const struct plinput *plinput);

/** Test for a capability
    @param[in] plinput plinput instance
    @param[in] cap capability identifier
    @return 1 it supported, 0 otherwise
 */
extern int plinput_get_capability(
	const struct plinput *plinput, enum plinput_capability cap);

/** Get horizontal resolution
    @param[in] plinput plinput instance
    @return horizontal resolution or -1 if no x axis
 */
extern int plinput_get_xres(const struct plinput *plinput);

/** Get vertical resolution
    @param[in] plinput plinput instance
    @return vertical resolution or -1 if no y axis
 */
extern int plinput_get_yres(const struct plinput *plinput);

/** Get pressure resolution
    @param[in] plinput plinput instance
    @return pressure resolution or -1 if no pressure axis
 */
extern int plinput_get_pres(const struct plinput *plinput);

/** @} */


/**
   @name Control
   @{
 */

/** Set callback to abort waiting for event
    @param[in] plinput plinput instance
    @param[in] cb callback function pointer
 */
extern void plinput_set_abort_cb(
	struct plinput *plinput, plinput_abort_cb_t cb);

/** Set polling period
    @param[in] plinput plinput instance
    @param[in] poll_us polling period duration in microseconds
 */
extern void plinput_set_poll_us(struct plinput *plinput, unsigned poll_us);

/** Set timeout duration
    @param[in] plinput plinput instance
    @param[in] poll_us timeout duration in microseconds
 */
extern void plinput_set_timeout_us(struct plinput *plinput, unsigned poll_us);

/** Test if timeout occured
    @param[in] plinput plinput instance
    @return 1 if timeout occured, 0 otherwise
 */
extern int plinput_is_timeout(struct plinput *plinput);

/** Grab the input device
    @param[in] plinput plinput instance
    @param[in] grab do grab if 1, release if 0
    @return 0 if success, -1 if error
 */
extern int plinput_grab(struct plinput *plinput, int grab);

/** Get option
    @param[in] plinput plinput instance
    @param[in] opt option identifier
    @return -1 if error, option value otherwise
 */
extern int plinput_get_opt(const struct plinput *plinput,
			   enum plinput_opt opt);

/** Set option
    @param[in] plinput plinput instance
    @param[in] opt option identifier
    @param[in] v value to set to the option
 */
extern void plinput_set_opt(
	struct plinput *plinput, enum plinput_opt opt, int v);

/** Wait and read touch or mouse point
    @param[in] plinput plinput instance
    @param[in] pt point structure to receive the event information
    @return 0 if success, -1 if error
 */
extern int plinput_read_point(
	struct plinput *plinput, struct plinput_point *pt);

/** Wait and read key event
    @param[in] plinput plinput instance
    @param[in] key key structure to deceive the event information
    @return 0 if success, -1 if error
 */
extern int plinput_read_key(struct plinput *plinput, struct plinput_key *key);

#endif /* INCLUDE_LIBPLINPUT_H */
