LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS += -Wall -O2 -DANDROID_NDK
LOCAL_MODULE := libplinput
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := libplinput.c data.c
include $(BUILD_STATIC_LIBRARY)
