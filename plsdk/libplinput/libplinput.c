/*
  Input event library - libplinput

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "libplinput.h"

#include <linux/input.h>
#include <sys/types.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdlib.h>

#ifdef ANDROID
# include "bionic-mt.h"
#endif

#ifndef ANDROID_NDK
# include <regex.h>
# ifdef ANDROID
#  include <dirent.h>
# else
#  include <glob.h>
# endif
#endif

#define LOG_TAG "plinput"
#include "log.h"

#ifndef BITS_PER_LONG
# define BITS_PER_LONG (8 * sizeof(long))
#endif

#define TEST_BIT(n, p)							\
	((int)(1UL & (p[n / BITS_PER_LONG] >> (n & (BITS_PER_LONG-1)))))

#define EV_NAME_LEN 63
#define EV_BITS_LONGS (1 + EV_MAX / (8 * sizeof (long)))
#define EV_BITS_LEN (EV_BITS_LONGS * sizeof (long))

struct plinput_axis_data {
	struct input_absinfo info;
	int res;
};

struct plinput {
	int fd;
	fd_set fd_set;
	char name[EV_NAME_LEN + 1];
	char *dev;
	struct input_id id;
	unsigned long bits[EV_BITS_LONGS];
	struct plinput_axis_data x;
	struct plinput_axis_data y;
	struct plinput_axis_data p;
	struct {
		int rel_coord:1;
		int abs_coord:1;
		int use_rel_coords:1;
		int data_received:1;
		int log_all:1;
		int accept_signals:1;
		int timeout:1;
	} flags;
	int angle;
	struct plinput_point previous;
	plinput_abort_cb_t abort_cb;
	struct timeval poll_timeout;
};

static int init_event(struct plinput *plinput, const char *event);
static void init_axis_data(struct plinput_axis_data *data, int fd,
			   int axis_id);
static int get_event_name(struct plinput *plinput);
static void reset_point(const struct plinput *plinput,
			struct plinput_point *point);
static void rotate_point(const struct plinput *plinput,
			 struct plinput_point *point);
static int handle_abs_event(struct plinput *plinput, struct input_event *ev,
			    struct plinput_point *pt);
static int handle_rel_event(struct plinput *plinput, struct input_event *ev,
			    struct plinput_point *pt);
static int handle_key_event(struct input_event *ev, struct plinput_point *pt);
static int handle_sync_event(struct plinput *plinput, struct input_event *ev,
			     struct plinput_point *pt);
static int read_event(struct plinput *plinput, struct input_event *ev);
#ifndef ANDROID_NDK
static char *make_regerror(int errcode, const regex_t *re);
#endif

/* -- initialisation -- */

struct plinput *plinput_init(const char *dev)
{
	struct plinput *plinput;

	assert(dev != NULL);

	plinput = malloc(sizeof (struct plinput));
	assert(plinput != NULL);

	if (init_event(plinput, dev) < 0) {
		LOG("failed to initialise event device");
		plinput_free(plinput);
		plinput = NULL;
	} else {
		plinput->flags.data_received = 0;
		plinput->flags.log_all = 0;
		plinput->flags.use_rel_coords = 0;
		plinput->angle = 0;
		plinput->abort_cb = NULL;

		plinput->poll_timeout.tv_sec = 0;
		plinput->poll_timeout.tv_usec = 0;
		plinput->flags.accept_signals = 0;
		plinput->flags.timeout = 0;
		plinput->flags.abs_coord = TEST_BIT(EV_ABS, plinput->bits);

		if (TEST_BIT(EV_REL, plinput->bits)) {
			plinput->flags.rel_coord = 1;
			plinput->previous.x = plinput->x.res / 2;
			plinput->previous.y = plinput->y.res / 2;
		} else {
			plinput->flags.rel_coord = 0;
		}

		reset_point(plinput, &plinput->previous);
	}

	return plinput;
}

struct plinput *plinput_init_from_name(const char *regex,
				       const char *input_dir)
{
#ifdef ANDROID_NDK
	LOG("Not supported");

	regex = NULL;
	input_dir = NULL;

	return NULL;
#else /* !ANDROID_NDK */
	struct plinput_dev_name *dev_names;
	const struct plinput_dev_name *found;
	struct plinput *plinput;

	assert(regex != NULL);

	dev_names = plinput_get_device_names(input_dir);

	if (dev_names == NULL) {
		LOG("failed to enumerate devices");
		return NULL;
	}

	found = plinput_search_device(dev_names, regex);

	if (found == NULL) {
		LOG("failed to find device by name: %s", regex);
		plinput = NULL;
	} else {
		plinput = plinput_init(found->device);
	}

	plinput_free_device_names(dev_names);

	return plinput;
#endif /* !ANDROID_NDK */
}

void plinput_free(struct plinput *plinput)
{
	assert(plinput != NULL);

	if (plinput->fd >= 0)
		close(plinput->fd);

	if (plinput->dev != NULL)
		free(plinput->dev);

	free(plinput);
}

/* -- stats -- */

unsigned long plinput_get_key_code(const char *name)
{
	const char * const *key;
	unsigned k;

	for (k = 0, key = plinput_key_names;
	     k < plinput_n_key_names;
	     k++, key++) {
		if (*key == NULL)
			continue;

		if (!strcmp(*key, name))
			return k;
	}

	return KEY_RESERVED;
}

struct plinput_dev_name *plinput_get_device_names(const char *input_dir)
{
#ifdef ANDROID_NDK
	LOG("Not supported");

	input_dir = NULL;

	return NULL;
#else /* !ANDROID_NDK */
	static const char *DEFAULT_INPUT_DIR = "/dev/input";
	static const size_t DN_SIZE = sizeof(struct plinput_dev_name);
	struct plinput_dev_name *dev_names;
	int i;
	size_t input_dir_len;
#ifdef ANDROID
	DIR *dir;
	struct dirent *dirent;
	regex_t re;
	int stat;
#else
	glob_t glob_data;
	const int flags = 0;
	struct plinput_dev_name *dev_name;
	char **dev;
#endif

	if (input_dir == NULL)
		input_dir = DEFAULT_INPUT_DIR;

	input_dir_len = strlen(input_dir);

#ifdef ANDROID
	dir = opendir(input_dir);

	if (dir == NULL) {
		LOG("failed to open directory (%s)", input_dir);
		return NULL;
	}

	stat = regcomp(&re, "^event[0-9]*$", 0);

	if (stat) {
		char *err_str = make_regerror(stat, &re);
		LOG("regcomp failed: %s", err_str);
		free(err_str);
		closedir(dir);
		return NULL;
	}

	i = 0;
	dev_names = malloc((i + 1) * DN_SIZE);

	while ((dirent = readdir(dir)) != NULL) {
		struct plinput *plinput;
		size_t full_path_len;

		if (regexec(&re, dirent->d_name, 0, NULL, 0))
			continue;

		dev_names = realloc(dev_names, (i + 1) * DN_SIZE);
		/* +2 for the '/' and '\0' */
		full_path_len = input_dir_len + strlen(dirent->d_name) + 2;
		dev_names[i].device = malloc(full_path_len);
		snprintf(dev_names[i].device, full_path_len, "%s/%s",
			 input_dir, dirent->d_name);
		plinput = plinput_init(dev_names[i].device);

		if (plinput == NULL) {
			dev_names[i].name = NULL;
		} else {
			dev_names[i].name = strdup(plinput->name);
			plinput_free(plinput);
		}

		++i;
	}

	dev_names[i ? (i - 1) : 0].device = NULL;
	dev_names[i ? (i - 1) : 0].name = NULL;

	regfree(&re);
	closedir(dir);
#else /* !ANDROID */
	if (chdir(input_dir) < 0) {
		LOG("failed to change to %s", input_dir);
		return NULL;
	}

	if (glob("event*", flags, NULL, &glob_data) < 0) {
		LOG("failed to scan the input directory (%s)", input_dir);
		return NULL;
	}

	dev_names = malloc((glob_data.gl_pathc + 1) * DN_SIZE);

	for (dev = glob_data.gl_pathv, dev_name = dev_names, i = 0;
	     i < glob_data.gl_pathc;
	     ++dev, ++i) {
		struct plinput *plinput;

		plinput = plinput_init(*dev);

		if (plinput != NULL) {
			/* +2 for the '/' and '\0' */
			const size_t full_path_len =
				input_dir_len + strlen(*dev) + 2;

			dev_name->device = malloc(full_path_len);
			snprintf(dev_name->device, full_path_len, "%s/%s",
				 input_dir, *dev);
			dev_name->name = strdup(plinput->name);
			++dev_name;
			plinput_free(plinput);
		}

		free(*dev);
	}

	free(glob_data.gl_pathv);
	dev_name->device = NULL;
	dev_name->name = NULL;
#endif /* !ANDROID */

	return dev_names;
#endif /* !ANDROID_NDK */
}

void plinput_free_device_names(struct plinput_dev_name *dev_names)
{
	struct plinput_dev_name *it;

	assert(dev_names != NULL);

	for (it = dev_names; it->device != NULL; ++it) {
		free(it->device);

		if (it->name != NULL)
			free(it->name);
	}

	free(dev_names);
}

const struct plinput_dev_name *plinput_search_device(
	const struct plinput_dev_name *dev_names, const char *regex)
{
#ifdef ANDROID_NDK
	LOG("Not supported");

	dev_names = NULL;
	regex = NULL;

	return NULL;
#else /* !ANDROID_NDK */
	const struct plinput_dev_name *it;
	regex_t re;
	int stat;

	assert(dev_names != NULL);
	assert(regex != NULL);

	stat = regcomp(&re, regex, 0);

	if (stat) {
		char *err_str = make_regerror(stat, &re);
		LOG("regcomp failed: %s", err_str);
		free(err_str);
		return NULL;
	}

	for (it = dev_names; it->device != NULL; ++it)
		if (!regexec(&re, it->name, 0, NULL, 0))
			break;

	regfree(&re);

	return (it->device == NULL) ? NULL : it;
#endif /* !ANDROID_NDK */
}

void plinput_log_info(const struct plinput *plinput)
{
	assert(plinput != NULL);

	LOG("-- input event info --");
	LOG(". device name      : %s", plinput->name);
	LOG(". resolution       : %i x %i", plinput->x.res, plinput->y.res);
	LOG(". pressure res.    : %i", plinput->p.res);
	LOG(". rotation angle   : %d", plinput->angle);
	LOG(". has buttons      : %s",
	    plinput_get_capability(plinput, PLINPUT_CAP_BUTTONS)
	    ? "yes" : "no");
	LOG(". has abs. coords. : %s",
	    plinput_get_capability(plinput, PLINPUT_CAP_ABS_COORD)
	    ? "yes" : "no");
	LOG(". has rel. coords. : %s",
	    plinput_get_capability(plinput, PLINPUT_CAP_REL_COORD)
	    ? "yes" : "no");
}

void plinput_set_full_log(struct plinput *plinput, int on)
{
	assert(plinput != NULL);
	plinput->flags.log_all = on ? 1 : 0;
}

const char *plinput_get_dev(const struct plinput *plinput)
{
	assert(plinput != NULL);
	return plinput->dev;
}

const char *plinput_get_name(const struct plinput *plinput)
{
	assert(plinput != NULL);
	return plinput->name;
}

int plinput_get_capability(const struct plinput *plinput,
			   enum plinput_capability cap)
{
	static const int bits[_PLINPUT_CAP_MAX_] = {
		[PLINPUT_CAP_BUTTONS] = EV_KEY,
		[PLINPUT_CAP_SYNC] = EV_SYN,
		[PLINPUT_CAP_ABS_COORD] = EV_ABS,
		[PLINPUT_CAP_REL_COORD] = EV_REL,
	};

	assert(plinput != NULL);
	assert(cap >= 0);
	assert(cap < _PLINPUT_CAP_MAX_);

	return TEST_BIT(bits[cap], plinput->bits);
}

int plinput_get_xres(const struct plinput *plinput)
{
	assert(plinput != NULL);

	return (plinput->angle % 180) ? plinput->y.res : plinput->x.res;
}

int plinput_get_yres(const struct plinput *plinput)
{
	assert(plinput != NULL);

	return (plinput->angle % 180) ? plinput->x.res : plinput->y.res;
}

int plinput_get_pres(const struct plinput *plinput)
{
	assert(plinput != NULL);

	return plinput->p.res;
}

/* -- control -- */

void plinput_set_abort_cb(struct plinput *plinput, plinput_abort_cb_t cb)
{
	assert(plinput != NULL);
	plinput->abort_cb = cb;
}

void plinput_set_poll_us(struct plinput *plinput, unsigned poll_us)
{
	const unsigned sec = poll_us / 1e6;

	assert(plinput != NULL);

	plinput->poll_timeout.tv_sec = sec;
	plinput->poll_timeout.tv_usec = poll_us - (sec * 1e6);
}

void plinput_set_timeout_us(struct plinput *plinput, unsigned poll_us)
{
	plinput_set_poll_us(plinput, poll_us);
	plinput->flags.timeout = 1;
}

int plinput_is_timeout(struct plinput *plinput)
{
	assert(plinput != NULL);

	return plinput->flags.timeout ? 1 : 0;
}

int plinput_grab(struct plinput *plinput, int grab)
{
	int stat;

	assert(plinput != NULL);

	stat = ioctl(plinput->fd, EVIOCGRAB, grab ? 1 : 0);

	if (stat) {
		LOG("failed to %sgrab event device: %s",
		    grab ? "" : "un-", strerror(errno));
		return -errno;
	}

	return 0;
}

int plinput_get_opt(const struct plinput *plinput, enum plinput_opt opt)
{
	int ret;

	assert(plinput != NULL);

	switch (opt) {
	case PLINPUT_OPT_USE_REL_COORDS:
		ret = plinput->flags.use_rel_coords;
		break;

	case PLINPUT_OPT_ACCEPT_SIGNALS:
		ret = plinput->flags.accept_signals;
		break;

	case PLINPUT_OPT_ROTATION_ANGLE:
		ret = plinput->angle;
		break;

	default:
		assert(!"invalid option identifier");
		ret = -1;
		break;
	}

	return ret;
}

void plinput_set_opt(struct plinput *plinput, enum plinput_opt opt, int v)
{
	assert(plinput != NULL);

	switch (opt) {
	case PLINPUT_OPT_USE_REL_COORDS:
		plinput->flags.use_rel_coords = v ? 1 : 0;
		break;

	case PLINPUT_OPT_ACCEPT_SIGNALS:
		plinput->flags.accept_signals = v ? 1 : 0;
		break;

	case PLINPUT_OPT_ROTATION_ANGLE:
		if (v % 90)
			break;

		plinput->angle = ((v / 90) % 4) * 90;
		if (plinput->angle < 0)
			plinput->angle += 360;
		break;
	default:
		assert(!"invalid option identifier");
		break;
	}
}

int plinput_read_point(struct plinput *plinput, struct plinput_point *point)
{
	int done = 0;
	int res;

	assert(plinput != NULL);
	assert(point != NULL);

	if (!plinput->x.res && !plinput->y.res && !plinput->p.res)
		LOG("Warning: input device has no axis");

	reset_point(plinput, point);

	while (!done) {
		struct input_event ev;
		int data = 0;

		res = read_event(plinput, &ev);

		if (!res || ((res < 0) && plinput->flags.accept_signals)) {
			if ((plinput->abort_cb != NULL)
			    && (plinput->abort_cb()))
				return 0;
			else
				continue;
		} else if (res < 0) {
			return -1;
		}

		switch (ev.type) {
		case EV_ABS:
			data = handle_abs_event(plinput, &ev, point);
			break;

		case EV_REL:
			data = handle_rel_event(plinput, &ev, point);
			break;

		case EV_KEY:
			data = handle_key_event(&ev, point);
			break;

		case EV_SYN:
			done = handle_sync_event(plinput, &ev, point);
			break;

		default:
			LOG("UNEXPECTED EVENT: "
			    "type=0x%04X, code=0x%04X, value=0x%08X",
			    ev.type, ev.code, ev.value);
			break;
		}

		if (data)
			plinput->flags.data_received = 1;
	}

	rotate_point(plinput, point);

	return 0;
}

int plinput_read_key(struct plinput *plinput, struct plinput_key *key)
{
	int done = 0;
	int res;
	int data = 0;
	struct input_event ev;

	assert(plinput != NULL);
	assert(key != NULL);

	while (!done) {
		res = read_event(plinput, &ev);

		if (!res || ((res < 0) && plinput->flags.accept_signals)) {
			if ((plinput->abort_cb != NULL)
			    && (plinput->abort_cb()))
				return 0;
			else
				continue;
		} else if (res < 0) {
			return -1;
		}

		switch (ev.type) {

		case EV_KEY:
			key->code = ev.code;
			key->on = ev.value ? 1 : 0;
			data = 1;
			break;

		case EV_SYN:
			if (data) {
				memcpy(&key->time, &ev.time, sizeof key->time);
				done = 1;
			}
			break;

		case EV_MSC:
			break;

		default:
			LOG("UNEXPECTED EVENT:"
			    "type=0x%04X, code=0x%04X, value=0x%08X",
			    ev.type, ev.code, ev.value);
			break;
		}
	}

	return 0;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static int init_event(struct plinput *plinput, const char *dev)
{
	int ret = -1;

	plinput->fd = open(dev, O_RDONLY);

	if (plinput->fd < 0)
		LOG("failed to open event device (%s): %s",
		    dev, strerror(errno));
	else if (get_event_name(plinput) < 0)
		LOG("failed to get the event device name");
	else if (ioctl(plinput->fd, EVIOCGID, &plinput->id) < 0)
		LOG("failed to get the input id");
	else if (ioctl(plinput->fd, EVIOCGBIT(0, EV_BITS_LEN), plinput->bits)
		 < 0)
		LOG("failed to get the event bitmasks");
	else
		ret = 0;

	plinput->dev = strdup(dev);
	assert(plinput->dev != NULL);

	init_axis_data(&plinput->x, plinput->fd, ABS_X);
	init_axis_data(&plinput->y, plinput->fd, ABS_Y);
	init_axis_data(&plinput->p, plinput->fd, ABS_PRESSURE);

	return ret;
}

static void init_axis_data(struct plinput_axis_data *data, int fd, int axis_id)
{
	if (ioctl(fd, EVIOCGABS(axis_id), &data->info) < 0)
		data->res = 0;
	else
		data->res = data->info.maximum - data->info.minimum;
}

static int get_event_name(struct plinput *plinput)
{
	int ret;

	memset(plinput->name, '\0', (EV_NAME_LEN + 1));

	if (ioctl(plinput->fd, EVIOCGNAME(EV_NAME_LEN), plinput->name) < 0) {
		ret = -1;
		plinput->name[0] = '\0';
	} else {
		ret = 0;
	}

	return ret;
}

static void reset_point(const struct plinput *plinput,
                        struct plinput_point *point)
{
	if (plinput->flags.use_rel_coords) {
		point->x = plinput->previous.x;
		point->y = plinput->previous.y;
	} else {
		point->x = -1;
		point->y = -1;
	}

	point->p = -1;
	point->on = -1;
	point->btn = -1;
	point->rubber = -1;
}

static void rotate_point(const struct plinput *plinput,
			 struct plinput_point *point)
{
	int tmp;

	switch (plinput->angle) {
	case 0:
		break;

	case 90:
		tmp = point->x;
		point->x = point->y;
		point->y = plinput->x.res - tmp;
		break;

	case 180:
		point->x = plinput->x.res - point->x;
		point->y = plinput->y.res - point->y;
		break;

	case 270:
		tmp = point->x;
		point->x = plinput->y.res - point->y;
		point->y = tmp;
		break;
	}
}

static int handle_abs_event(struct plinput *plinput, struct input_event *ev,
			    struct plinput_point *pt)
{
	int data;

	if (plinput->flags.use_rel_coords)
		return 0;

	data = 1;

	switch (ev->code) {
	case ABS_X:
		pt->x = ev->value;
		break;

	case ABS_Y:
		pt->y = ev->value;
		break;

	case ABS_PRESSURE:
		pt->p = ev->value;
		break;

	default:
		LOG("unexpected absolute event %d", ev->code);
		data = 0;
		break;
	}

	return data;
}

static int handle_rel_event(struct plinput *plinput, struct input_event *ev,
			    struct plinput_point *pt)
{
	int data;

	if (!plinput->flags.use_rel_coords)
		return 0;

	data = 1;

	switch (ev->code) {
	case REL_X:
		plinput->previous.x += ev->value;

		if (plinput->previous.x < plinput->x.info.minimum)
			plinput->previous.x = plinput->x.info.minimum;

		if ((plinput->x.info.maximum)
		    && (plinput->previous.x > plinput->x.info.maximum))
			plinput->previous.x = plinput->x.info.maximum;

		pt->x = plinput->previous.x;
		break;

	case REL_Y:
		plinput->previous.y += ev->value;

		if (plinput->previous.y < plinput->y.info.minimum)
			plinput->previous.y = plinput->y.info.minimum;

		if ((plinput->y.info.maximum)
		    && (plinput->previous.y > plinput->y.info.maximum))
			plinput->previous.y = plinput->y.info.maximum;

		pt->y = plinput->previous.y;
		break;

	default:
		LOG("unexpected REL event: %02X %02X", ev->code, ev->value);
		data = 0;
		break;
	}

	return data;
}

static int handle_key_event(struct input_event *ev, struct plinput_point *pt)
{
	int data = 1;

	switch (ev->code) {
	case BTN_TOUCH:
		pt->on = ev->value ? 1 : 0;
		break;

	case BTN_0:
		pt->btn = ev->value ? 1 : 0;
		break;

	case BTN_TOOL_RUBBER:
		pt->rubber = ev->value ? 1 : 0;
		break;

	default:
		LOG("unexpected key event %d", ev->code);
		data = 0;
		break;
	}

	return data;
}

static int handle_sync_event(struct plinput *plinput, struct input_event *ev,
			     struct plinput_point *pt)
{
	int done = 0;

	switch (ev->code) {
	case SYN_REPORT:
		if (!plinput->flags.data_received)
			break;

		plinput->flags.data_received = 0;
		memcpy(&pt->time, &ev->time, sizeof pt->time);

		if (pt->x < 0)
			pt->x = plinput->previous.x;

		if (pt->y < 0)
			pt->y = plinput->previous.y;

		if (pt->p < 0)
			pt->p = plinput->previous.p;

		memcpy(&plinput->previous, pt, sizeof plinput->previous);

		done = 1;
		break;

	case SYN_CONFIG:
		LOG("SYN_CONFIG");
		break;

	case SYN_MT_REPORT:
		LOG("SYN_MT_REPORT");
		/* ToDo: support multi-touch as well */
		break;

	default:
		LOG("Unexpected synchro event");
		break;
	}

	return done;
}

static int read_event(struct plinput *plinput, struct input_event *ev)
{
	struct timeval timeout;
	struct timeval *pt;
	int res;

	if (plinput->poll_timeout.tv_sec || plinput->poll_timeout.tv_usec) {
		timeout.tv_sec = plinput->poll_timeout.tv_sec;
		timeout.tv_usec = plinput->poll_timeout.tv_usec;
		pt = &timeout;
	} else {
		pt = NULL;
	}

	FD_ZERO(&plinput->fd_set);
	FD_SET(plinput->fd, &plinput->fd_set);
	res = select(plinput->fd + 1, &plinput->fd_set, NULL, NULL, pt);

	if (!res && plinput->flags.timeout) {
		plinput->poll_timeout.tv_sec = 0;
		plinput->poll_timeout.tv_usec = 0;
		return 0;
	}

	plinput->flags.timeout = 0;

	if (res <= 0)
		return res;

	if (read(plinput->fd, ev, sizeof (struct input_event)) <
	    (int)(sizeof(struct input_event))) {
		LOG("failed to read input event");
		return -1;
	}

	if (plinput->flags.log_all)
		LOG("ev: %02X, %04X, %i", ev->type, ev->code, ev->value);

	return 1;
}

#ifndef ANDROID_NDK
static char *make_regerror(int errcode, const regex_t *re)
{
	size_t length = regerror(errcode, re, NULL, 0);
	char *str = malloc(length);

	if (str == NULL)
		return NULL;

	(void) regerror(errcode, re, str, length);

	return str;
}
#endif /* !ANDROID_NDK */
