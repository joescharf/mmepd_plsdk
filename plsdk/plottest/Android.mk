LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES := plottest.c
LOCAL_MODULE := plottest
LOCAL_MODULE_TAGS := eng
LOCAL_C_FLAGS += -Wall -O2
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../libplutil \
	$(LOCAL_PATH)/../libpldraw \
	$(LOCAL_PATH)/../libplepaper \
	$(LOCAL_PATH)/../libplinput
LOCAL_STATIC_LIBRARIES := libpldraw libplepaper libplinput libplutil libeglib
ifndef PLASTICLOGIC_NDK_BUILD
LOCAL_SHARED_LIBRARIES := libdl
else
LOCAL_LDLIBS := -ldl
endif
include $(BUILD_EXECUTABLE)
