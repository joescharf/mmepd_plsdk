#!/bin/sh

set -e

ver="$1"

sed -i s/"\(VERSION\[\] = \"\)\(.*\)\(\".*\)$"/"\1$ver\3"/ plottest.c
git add plottest.c

exit 0
