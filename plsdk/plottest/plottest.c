/*
  E-Paper drawing application - plottest

  Copyright (C) 2010, 2011, 2012 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libplinput.h>
#include <libpldraw.h>
#include <signal.h>
#include <getopt.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#define LOG_TAG "plottest"
#include <plsdk/log.h>

#define DEFAULT_INPUT "/dev/input/event0"
#define VALID_POINT(p) (((p)->x >= 0) && ((p)->y >= 0))

static const char APP_NAME[] = "plottest";
static const char VERSION[] = "1.2";
static const char DESCRIPTION[] = "E-Paper hand writing application";
static const char LICENSE[] =
	"This program is distributed in the hope that it will be useful,\n"
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	"GNU General Public License for more details.\n";
static const char COPYRIGHT[] =
	"Copyright (C) 2010, 2011, 2012 Plastic Logic Limited";

static struct plep_rect g_area;
static pldraw_color_t g_black;
static pldraw_color_t g_white;
static int g_refresh_wf;
static int g_mono_wf;
static int g_touch_x_res;
static int g_touch_y_res;
static int g_display_x_res;
static int g_display_y_res;
static int g_on = 0;
static int g_verbose = 0;
static int g_run = 1;

static void sigint_abort(int signum);
static int abort_cb(void);
static void print_usage(void);
static int handle_point(struct pldraw *pldraw, struct plep_point *pt, int on,
			struct plep_point *prev);
static void scale_point(struct plep_point *pt);
static int plot_point(struct pldraw *pldraw, const struct plep_point *pt,
		      const struct plep_point *prev);

int main(int argc, char **argv)
{
	static const char *OPTIONS = "hvVli:I:m:d:e:r:b:gO:";
	struct plinput *plinput;
	struct plep *plep;
	struct pldraw *pldraw;
	struct plep_point prev_pt;
	const char *mode = NULL;
	const char *fb_dev = NULL;
	const char *ep_dev = NULL;
	const char *input_dev = DEFAULT_INPUT;
	const char *input_name = NULL;
	__sighandler_t sigint_original;
	int saved_power_off_delay = 0;
	int log_info = 0;
	int angle = 0;
	int border = 0;
	int do_grab = 0;
	const char *conf_file = NULL;
	int c;

	while ((c = getopt(argc, argv, OPTIONS)) != -1) {
		switch (c) {
		case 'h':
			print_usage();
			exit(EXIT_SUCCESS);
			break;

		case 'v':
			printf("%s v%s - %s\n%s\n%s\n", APP_NAME, VERSION,
			       DESCRIPTION, COPYRIGHT, LICENSE);
			exit(EXIT_SUCCESS);
			break;

		case 'V':
			g_verbose = 1;
			break;

		case 'l':
			log_info = 1;
			break;

		case 'i':
			input_dev = optarg;
			break;

		case 'I':
			input_name = optarg;
			break;

		case 'm':
			mode = optarg;
			break;

		case 'd':
			fb_dev = optarg;
			break;

		case 'e':
			ep_dev = optarg;
			break;

		case 'r': {
			long raw_angle;

			errno = 0;
			raw_angle = strtol(optarg, NULL, 10);

			if (errno || (raw_angle < 0) || (raw_angle > 270)
			    || (raw_angle % 90)) {
				LOG("invalid angle value");
				print_usage();
				exit(EXIT_FAILURE);
			}

			angle = raw_angle;
			break;
		}
		case 'b': {
			long raw_border;

			errno = 0;
			raw_border = strtol(optarg, NULL, 10);

			if (errno || (raw_border < 0)) {
				LOG("invalid border value");
				print_usage();
				exit(EXIT_FAILURE);
			}

			border = raw_border;
		}
		case 'g':
			do_grab = 1;
			break;

		case 'O':
			conf_file = optarg;
			if (access(conf_file, F_OK)) {
				LOG_ERRNO("Configuration file");
				exit(EXIT_FAILURE);
			}
			break;

		case '?':
		default:
			LOG("Invalid arguments");
			print_usage();
			exit(EXIT_FAILURE);
			break;
		}
	}

	if (input_name != NULL)
		plinput = plinput_init_from_name(input_name, NULL);
	else
		plinput = plinput_init(input_dev);

	if (plinput == NULL) {
		LOG("failed to initialise touchpoint");
		exit(EXIT_FAILURE);
	}

	if (angle) {
		LOG("rotation angle: %d", angle);
		plinput_set_opt(plinput, PLINPUT_OPT_ROTATION_ANGLE, angle);
	}

	if (do_grab) {
		LOG("grabbing the input device");
		plinput_grab(plinput, 1);
	}

	plep = plep_init(ep_dev, mode, conf_file);

	if (plep == NULL) {
		LOG("failed to initialise ePDC");
		goto free_plinput;
	}

	pldraw = pldraw_init(fb_dev, conf_file);

	if (pldraw == NULL) {
		LOG("failed to initialise pldraw");
		goto free_plep;
	}

	pldraw_set_plep(pldraw, plep);

	g_refresh_wf = plep_get_wfid(plep, PLEP_REFRESH);
	g_mono_wf = plep_get_wfid(plep, PLEP_DELTA"/"PLEP_MONO);

	if (g_mono_wf < 0) {
		LOG("Warning: no mono waveform");
		g_mono_wf = g_refresh_wf;
	}

	if (log_info) {
		plinput_log_info(plinput);
		pldraw_log_info(pldraw);
	} else {
		LOG("input device: %s", plinput_get_name(plinput));
	}

	g_black = pldraw_get_grey(pldraw, 0x00);
	g_white = pldraw_get_grey(pldraw, 0xFF);
	g_touch_x_res = plinput_get_xres(plinput);
	g_touch_y_res = plinput_get_yres(plinput);
	g_display_x_res = pldraw_get_xres(pldraw);
	g_display_y_res = pldraw_get_yres(pldraw);

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);
	pldraw_fill_screen(pldraw, g_black);
	plep_update_screen(plep, g_refresh_wf);

	if (border) {
		LOG("border: %d pixels", border);
		pldraw_get_crop_area(pldraw, &g_area);
		pldraw_apply_border(&g_area, border);
		pldraw_set_crop_area(pldraw, &g_area);
	}

	pldraw_fill_screen(pldraw, g_white);
	plep_update_screen(plep, g_refresh_wf);
	plep_set_opt(plep, PLEP_SYNC_UPDATE, 0);
	plep_set_opt(plep, PLEP_PARTIAL, 1);
	plep_get_hw_opt(plep, PLEP_POWER_OFF_DELAY_MS, &saved_power_off_delay);
	plep_set_hw_opt(plep, PLEP_POWER_OFF_DELAY_MS, 500);

	sigint_original = signal(SIGINT, sigint_abort);
	plinput_set_abort_cb(plinput, abort_cb);
	plinput_set_opt(plinput, PLINPUT_OPT_ACCEPT_SIGNALS, 1);

	prev_pt.x = -1;
	prev_pt.y = -1;

	while (g_run) {
		struct plinput_point pt;

		if (plinput_read_point(plinput, &pt) < 0) {
			LOG("ERROR: failed to read touch point");
			g_run = 0;
		} else if (g_run) {
			struct plep_point fbpt = { .x = pt.x, .y = pt.y };

			if (handle_point(pldraw, &fbpt, pt.on, &prev_pt) < 0) {
				LOG("ERROR: failed to handle point");
				g_run = 0;
			}
		}
	}

	plep_set_hw_opt(plep, PLEP_POWER_OFF_DELAY_MS, saved_power_off_delay);
	plep_set_opt(plep, PLEP_PARTIAL, 0);
	plinput_free(plinput);
	plep_free(plep);
	pldraw_free(pldraw);
	signal(SIGINT, sigint_original);

	LOG("done.");

	return 0;

	pldraw_free(pldraw);
free_plep:
	plep_free(plep);
free_plinput:
	plinput_free(plinput);

	return -1;
}

static void sigint_abort(int signum)
{
	if (signum == SIGINT)
		g_run = 0;
}

static int abort_cb(void)
{
	return g_run ? 0 : 1;
}

static void print_usage(void)
{
	printf(
"Usage: %s <OPTIONS>\n"
"OPTIONS:\n"
"  -h\n"
"    Show this help message and exit\n"
"\n"
"  -v\n"
"    Show the version, copyright and license information.\n"
"\n"
"  -V\n"
"    Verbose mode: log each plotted point.\n"
"\n"
"  -l\n"
"    Log input device and frame buffer information.\n"
"\n"
"  -g\n"
"    Grab the device, preventing other programs from receiving the\n"
"    events from the input device being used.\n"
"\n"
"  -i DEVICE\n"
"    Specify the input event device to use (default: /dev/input/event0)\n"
"\n"
"  -I DEVICE_NAME\n"
"    Specify the name of the input device to search and try to find a\n"
"    corresponding device, for example: \"Plastic\".\n"
"\n"
"  -m EPDC_MODE\n"
"    Specify which mode to use to control the ePDC.  By default, this is \n"
"    discovered automatically by probing the system.  This is then used to \n"
"    load the corresponding mod_plepaper_EPDC_MODE.so module.  Usual values \n"
"    for EPDC_MODE are: \n"
"\n"
"    noep      Do not use any ePDC\n"
"    imx       Freescale i.MX508\n"
"    plepdc    Plastic Logic ePDC interface\n"
"\n"
"  -d DEVICE\n"
"    Specify the frame buffer device to use.  If not specified, the usual\n"
"    suspects will be looked for (/dev/fb0 and variants).\n"
"\n"
"  -e DEVICE\n"
"    Specify the ePDC device to use.  This relies on the implementation of\n"
"    the ePDC mode being used.\n"
"\n"
"  -r ANGLE\n"
"    Rotate the touch coordinates by ANGLE degrees, clockwise.\n"
"    Possible values are 0, 90, 180 and 270.\n"
"\n"
"  -b BORDER\n"
"    Apply a black border of BORDER pixels around the display.\n"
"\n"
"  -O CONFIG_FILE\n"
"    Use configuration file specified in CONFIG_FILE.\n"
"    Default .plsdk.ini will be used if nothing is provided or default\n"
"    values if .plsdk.ini is not present\n"
"\n", APP_NAME);
}

static int handle_point(struct pldraw *pldraw, struct plep_point *pt, int on,
			struct plep_point *prev)
{
	const int valid = VALID_POINT(pt) ? 1 : 0;
	int ret = 0;

	if (g_on) {
		if (!on) {
			g_on = 0;
		} else if (valid) {
			scale_point(pt);
			ret = plot_point(pldraw, pt, prev);
			prev->x = pt->x;
			prev->y = pt->y;
		}
	} else if (on && valid) {
		g_on = 1;
		scale_point(pt);
		prev->x = pt->x;
		prev->y = pt->y;
	}

	return ret;
}

static void scale_point(struct plep_point *pt)
{
	if (g_display_x_res != g_touch_x_res)
		pt->x = pt->x * g_display_x_res / g_touch_x_res;

	if (g_display_y_res != g_touch_y_res)
		pt->y = pt->y * g_display_y_res / g_touch_y_res;
}

static int plot_point(struct pldraw *pldraw, const struct plep_point *pt,
		      const struct plep_point *prev)
{
	const struct plep_rect line = {
		.a = { .x = prev->x, .y = prev->y },
		.b = { .x = pt->x, .y = pt->y }
	};
#if 0
	struct plep_rect update_area;
#endif

	if (g_verbose)
		fprintf(stderr, "plot (%i, %i) (%i, %i)\n",
			prev->x, prev->y, pt->x, pt->y);

	pldraw_draw_line(pldraw, g_black, &line);
#if 1
	return plep_update_screen(pldraw_get_plep(pldraw), g_mono_wf);
#else
	memcpy(&update_area, &line, sizeof update_area);
	update_area.b.x++;
	update_area.b.y++;
	pldraw_crop(pldraw, &update_area);

	return plep_update(pldraw_get_plep(pldraw), &update_area, g_mono_wf);
#endif
}
