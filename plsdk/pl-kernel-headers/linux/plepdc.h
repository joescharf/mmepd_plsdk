/*
 * plepdc.h -- Plastic Logic ePDC device
 *
 * Copyright (c) 2012 Plastic Logic Limited
 *
 * Authors: Guillaume Tucker <guillaume.tucker@plasticlogic.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#ifndef INCLUDE_LINUX_PLEPDC_H
#define INCLUDE_LINUX_PLEPDC_H 1

/* User API */

#ifdef __KERNEL__
# include <linux/types.h>
# include <linux/ioctl.h>
#else
# include <sys/ioctl.h>
# include <stdint.h>
#endif

enum plepdc_opt {
	PLEPDC_OPT_BW        = 1 << 0, /* use black & white threshold */
	PLEPDC_OPT_SYNC      = 1 << 1, /* synchronous update */
};

enum plepdc_power_state {
	PLEPDC_POWER_OFF = 0,
	PLEPDC_POWER_ON = 1,
};

struct plepdc_update_area {
	uint32_t top;
	uint32_t left;
	uint32_t bottom;
	uint32_t right;
	uint32_t wfid;                 /* waveform id */
	uint32_t opts;                 /* plepdc_opt flags */
	int32_t bw_threshold;          /* black & white threshold value */
};

struct plepdc_stats {
	uint32_t xres;
	uint32_t yres;
};

#define PLEPDC_IO_UPDATE            _IOW('E', 0, struct plepdc_update_area)
#define PLEPDC_IO_WAIT_POWER        _IOR('E', 1, int)
#define PLEPDC_IO_GET_STATS         _IOR('E', 2, struct plepdc_stats)
#define PLEPDC_IO_WAIT_READY        _IO('E', 3)

/* Kernel API */

#ifdef __KERNEL__

#include <linux/miscdevice.h>
#include <linux/wait.h>
#include <linux/plepdc.h>

struct plepdc {
	struct miscdevice mdev;
	void *ctx;

	/* To be populated by client code prior to calling plepdc_init */
	int (*update)(void *ctx, const struct plepdc_update_area *area);
	int (*wait_power)(void *ctx, bool state);
	int (*wait_ready)(void *ctx);
	unsigned xres;
	unsigned yres;
};

extern int plepdc_init(struct plepdc *epdc);
extern void plepdc_free(struct plepdc *epdc);

#endif /* __KERNEL__ */

#endif /* INCLUDE_LINUX_PLEPDC_H */
