/*
 * epaper.h -- E-Paper driver interface and user-space definitions
 *
 * Copyright (c) 2012 Plastic Logic Limited
 *
 * Authors: Guillaume Tucker <guillaume.tucker@plasticlogic.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 */

#ifndef _INCLUDE_LINUX_EPAPER_H_
#define _INCLUDE_LINUX_EPAPER_H_ 1

#ifdef __KERNEL__
# include <linux/ioctl.h>
#else
# include <sys/ioctl.h>
#endif

/* -- User-space IOCTL interface -- */

/*** WARNING: WORK IN PROGRESS, NOT SET IN STONE YET SO CHECK THE VERSION  ***/
#define EPAPER_API_VERSION 1ul

/* To use with EPAPER_IO_SET_POWER */
enum epaper_power_state {
	EPAPER_POWER_OFF = 0,
	EPAPER_POWER_ON  = 1,
	EPAPER_POWER_AUTO = 2,
};



#define EPAPER_IO_GET_API_VERSION   _IOR('P', 0, unsigned long *)
#define EPAPER_IO_ACQUIRE_SUBF      _IOW('P', 1, unsigned long)
#define EPAPER_IO_SEND_SUBF         _IOW('P', 2, unsigned long)
#define EPAPER_IO_SET_POWER         _IOW('P', 3, unsigned long)

#ifdef __KERNEL__

#include <linux/list.h>
#include <linux/mm.h>
#include <linux/miscdevice.h>

/* -- Internal kernel interface -- */

#define EPAPER_NAME_SIZE 16

enum epaper_bus_event {
	EPAPER_BUS_EVENT_START, /* beginning of activity on the bus */
	EPAPER_BUS_EVENT_STOP,  /* bus activity has ceased */
};

struct epaper_bus_device {
	struct epaper_device *epaper;
	struct device *parent;
	const void *config;
	void *ctx; /* private driver context data */
	int (*init)(struct epaper_bus_device *bus);
	void (*free)(struct epaper_bus_device *bus);
	int (*mmap)(struct epaper_bus_device *bus,
		    struct vm_area_struct *vma);
	int (*acquire_subf)(struct epaper_bus_device *bus, unsigned long n);
	int (*send_subf)(struct epaper_bus_device *bus, unsigned long n);
};

struct epaper_psu_device {
	struct epaper_device *epaper;
	struct device *parent;
	const void *config;
	bool power_state;
	void *ctx; /* private driver context data */
	int (*init)(struct epaper_psu_device *psu);
	void (*free)(struct epaper_psu_device *psu);
	int (*set_power)(struct epaper_psu_device *psu, bool on);
};

struct epaper_device {
	/* Set by client */
	struct epaper_bus_device *bus;
	struct epaper_psu_device *psu;

	/* Set internally */
	struct list_head node;
	struct miscdevice misc;
	char name[EPAPER_NAME_SIZE];
	enum epaper_power_state power_state;
	bool power_on;
	bool bus_active;
};

extern int epaper_register_device(struct epaper_device *ep);
extern void epaper_deregister_device(struct epaper_device *ep);
extern void epaper_bus_event(struct epaper_bus_device *data,
			     enum epaper_bus_event event,
			     unsigned long arg);

#endif /* __KERNEL__ */

#endif /* _INCLUDE_LINUX_EPAPER_H_ */
