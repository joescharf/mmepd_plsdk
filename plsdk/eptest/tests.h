/*
  E-Paper test - eptest

  Copyright (C) 2010, 2011, 2012 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>
      Nick Terry <nick.terry@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_TESTS_H
#define INCLUDE_TESTS_H 1

struct pldraw;

extern void test_initialise(struct pldraw *pldraw, int clear_wf, int quick_wf);
extern int test_palette(struct pldraw *, char * const *, int);
extern int test_frame(struct pldraw *, char * const *, int);
extern int test_regions(struct pldraw *, char * const *, int);
extern int test_radar(struct pldraw *, char * const *, int);
extern int test_check(struct pldraw *, char * const *, int);
extern int test_fill(struct pldraw *, char * const *, int);
extern int test_sweep(struct pldraw *, char * const *, int);
extern int test_none(struct pldraw *, char * const *, int);
extern int test_update(struct pldraw *, char * const *, int);
extern int test_cross(struct pldraw *, char * const *, int);
extern int test_stripes(struct pldraw *, char * const *, int);
extern int test_fastmono(struct pldraw *, char * const *, int);
extern int test_highlight(struct pldraw *, char * const *, int);
extern int test_text(struct pldraw *, char * const *, int);

#endif /* INCLUDE_TESTS_H */
