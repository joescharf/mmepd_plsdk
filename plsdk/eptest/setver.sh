#!/bin/sh

set -e

ver="$1"

sed -i s/"\(VERSION\[\] = \"\)\(.*\)\(\".*\)$"/"\1$ver\3"/ eptest.c
git add eptest.c

exit 0
