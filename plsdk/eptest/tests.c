/*
  E-Paper test - eptest

  Copyright (C) 2010, 2011, 2012, 2013, 2014 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests.h"
#include <libpldraw.h>
#include <assert.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>

#define LOG_TAG "test"
#include <plsdk/log.h>

static int g_initialised = 0;
static int g_run;
static int g_clear_wf;
static int g_quick_wf;
static pldraw_color_t g_black;
static pldraw_color_t g_white;
static pldraw_color_t g_red;
static pldraw_color_t g_green;
static pldraw_color_t g_blue;
static pldraw_color_t g_cyan;
static pldraw_color_t g_magenta;
static pldraw_color_t g_yellow;

static void draw_checkerboard(struct pldraw *pldraw,
                              const struct plep_point *origin, size_t dim,
                              int vertical, int n, int m,
			      pldraw_color_t icolor);
static void draw_stripes(struct pldraw *pldraw, pldraw_color_t col,
			 unsigned long width);
static void draw_border(struct pldraw *pldraw, pldraw_color_t col,
			const struct plep_rect *rect);
static int slide_square(struct pldraw *pldraw, const char *wf_name,
			const char *wf_opt);
static void sigint_stop(int signum);
static int str2ul(unsigned long *out, const char *str);

static inline int get_shade(int start, int offset)
{
	offset -= start;

	if (offset < 0)
		offset += 90;

	if (offset < 16)
		return offset;

	if (offset < 46)
		return 15;

	if (offset < 60)
		return 60 - offset;

	return 0;
}

void test_initialise(struct pldraw *pldraw, int clear_wf, int quick_wf)
{
	g_clear_wf = clear_wf;
	g_quick_wf = quick_wf;
	g_black = pldraw_get_grey(pldraw, 0x00);
	g_white = pldraw_get_grey(pldraw, 0xFF);
	g_red = pldraw_get_color(pldraw, 0xFF, 0x00, 0x00);
	g_green = pldraw_get_color(pldraw, 0x00, 0xFF, 0x00);
	g_blue = pldraw_get_color(pldraw, 0x00, 0x00, 0xFF);
	g_cyan = pldraw_get_color(pldraw, 0x00, 0xFF, 0xFF);
	g_magenta = pldraw_get_color(pldraw, 0xFF, 0x00, 0xFF);
	g_yellow = pldraw_get_color(pldraw, 0xFF, 0xFF, 0x00);
	g_initialised = 1;
}

int test_palette(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	static const int greys = 256;
	struct plep_rect rect;
	struct plep_rect area;
	int ret = 0;
	int cur_rotation;
	int xres;
	int yres;
	float x_grid, y_grid;
	int width;
	int tmp;
	int i;

	assert(pldraw != NULL);
	assert(g_initialised);

	plep = pldraw_get_plep(pldraw);
	plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);
	xres = pldraw_get_xres(pldraw);
	yres = pldraw_get_yres(pldraw);
	cur_rotation = pldraw_get_rotation(pldraw);

	if (yres > xres) {
		const int new_rotation =
			cur_rotation + ((cur_rotation % 180) ? -90 : 90);

		pldraw_set_rotation(pldraw, new_rotation);
		xres = pldraw_get_xres(pldraw);
		yres = pldraw_get_yres(pldraw);
	}

	x_grid = xres / 24.0;
	y_grid = yres / 18.0;

	LOG("greyscale palettes");

	area.a.x = x_grid * 0.5;
	area.a.y = y_grid * 0.5;
	area.b.x = x_grid * 23.5;
	area.b.y = y_grid * 3.0;
	width = area.b.x - area.a.x;
	memcpy(&rect, &area, sizeof rect);
	rect.b.x = rect.a.x;
	--rect.b.y;

	for (i = 0; i < width; ++i) {
		const int grey = i * greys / width;
		const pldraw_color_t col = pldraw_get_grey(pldraw, grey);
		pldraw_draw_line(pldraw, col, &rect);
		++rect.a.x;
		++rect.b.x;
	}

	draw_border(pldraw, g_black, &area);

	area.a.y = y_grid * 3.5;
	area.b.y = y_grid * 6.0;
	memcpy(&rect, &area, sizeof rect);

	for (i = 0; i < 16; ++i) {
		const int grey = pldraw_grey_16[i];
		const pldraw_color_t col = pldraw_get_grey(pldraw, grey);
		rect.b.x = area.a.x + (width * (i + 1)) / 16;
		pldraw_fill_rect(pldraw, col, &rect);
		rect.a.x = rect.b.x;
	}

	draw_border(pldraw, g_black, &area);

	if (plep_update_screen(plep, g_clear_wf) < 0)
		ret = -1;

	LOG("primary and secondary colours");

	width = x_grid * 24;
	width /= 90;
	width /= 2;
	width += 1;
	width *= 90;

	tmp = (x_grid * 21.5 - width) / 2;

	rect.a.x = x_grid * 0.5;
	rect.a.y = y_grid * 6.5;
	rect.b.x = x_grid * 0.5 + tmp;
	rect.b.y = y_grid * 9.5;
	pldraw_fill_rect(pldraw, g_red, &rect);

	rect.a.y = y_grid * 10.5;
	rect.b.y = y_grid * 13.5;
	pldraw_fill_rect(pldraw, g_green, &rect);

	rect.a.y = y_grid * 14.5;
	rect.b.y = y_grid * 17.5;
	pldraw_fill_rect(pldraw, g_blue, &rect);

	rect.a.x = x_grid * 1.5 + tmp;
	rect.a.y = y_grid * 6.5;
	rect.b.x = x_grid * 1.5 + (2 * tmp);
	rect.b.y = y_grid * 9.5;
	pldraw_fill_rect(pldraw, g_cyan, &rect);

	rect.a.y = y_grid * 10.5;
	rect.b.y = y_grid * 13.5;
	pldraw_fill_rect(pldraw, g_magenta, &rect);

	rect.a.y = y_grid * 14.5;
	rect.b.y = y_grid * 17.5;
	pldraw_fill_rect(pldraw, g_yellow, &rect);

	area.a.x = x_grid * 0.5;
	area.a.y = y_grid * 6.5;
	area.b.x = rect.b.x;
	area.b.y = y_grid * 17.5;

	if (plep_update(plep, &area, g_clear_wf) < 0)
		ret = -1;

	LOG("colour shades");

	area.a.x = x_grid * 23.5 - width;
	area.a.y = y_grid * 6.5;
	area.b.x = x_grid * 23.5;
	area.b.y = y_grid * 17.5;
	i = 0;

	rect.a.x = area.a.x;
	rect.a.y = area.a.y;
	rect.b.y = area.b.y;

	for (i = 1; i <= 90; ++i) {
		const int r = pldraw_grey_16[get_shade(0, i)];
		const int g = pldraw_grey_16[get_shade(30, i)];
		const int b = pldraw_grey_16[get_shade(60, i)];
		const pldraw_color_t col =
			pldraw_get_color(pldraw, r, g, b);
		rect.b.x = area.a.x + (width * i / 90);
		pldraw_fill_rect(pldraw, col, &rect);
		rect.a.x = rect.b.x;
	}

	if (plep_update(plep, &area, g_clear_wf) < 0)
		ret = -1;

	pldraw_set_rotation(pldraw, cur_rotation);

	return ret;
}

int test_frame(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	struct plep_rect rect;
	struct plep_rect outer;
	struct plep_rect inner;
	unsigned long grid = 12;
	pldraw_color_t draw_col = g_black;
	unsigned long gap = 1;
	unsigned long max_gap;
	float x_grid, y_grid;
	int xres, yres;
	int wfid = g_clear_wf;

	assert(pldraw != NULL);
	assert(g_initialised);

	plep = pldraw_get_plep(pldraw);
	xres = pldraw_get_xres(pldraw);
	yres = pldraw_get_yres(pldraw);

	if (opts_n > 0) {
		if (str2ul(&grid, opts[0]) < 0) {
			LOG("Invalid grid value: %s", opts[0]);
			return -1;
		}

		if (grid < 4) {
			LOG("Grid value is too small, minimum is 4");
			return -1;
		}
	}

	if (opts_n > 1) {
		if (pldraw_str2col(pldraw, &draw_col, opts[1]))
			return -1;
	}

	if (opts_n > 2) {
		if (str2ul(&gap, opts[2]) < 0) {
			LOG("Invalid gap value: %s", opts[2]);
			return -1;
		}
	}

	if (opts_n > 3) {
		wfid = plep_get_wfid(plep, opts[3]);

		if (wfid < 0) {
			LOG("Invalid waveform: %s", opts[3]);
			return -1;
		}
	}

	x_grid = xres / (float)grid;
	y_grid = yres / (float)grid;

	LOG("grid: %lu, x: %.3f, y: %.3f", grid, x_grid, y_grid);

	max_gap = (y_grid * ((grid / 2) - 2)) - 1;

	if (gap > max_gap) {
		LOG("Gap is too big, maximum is %lu", max_gap);
		return -1;
	}

	outer.a.x = x_grid;
	outer.a.y = y_grid;
	outer.b.x = x_grid * (grid - 1);
	outer.b.y = y_grid * (grid - 1);

	inner.a.x = x_grid * 2;
	inner.a.y = y_grid * 2;
	inner.b.x = x_grid * (grid - 2);
	inner.b.y = y_grid * (grid - 2);

	rect.a.x = outer.a.x;
	rect.a.y = outer.a.y;
	rect.b.x = outer.b.x;
	rect.b.y = inner.a.y;
	pldraw_fill_rect(pldraw, draw_col, &rect);

	if (plep_update(plep, &rect, wfid) < 0)
		return -1;

	rect.a.y = inner.b.y;
	rect.b.y = outer.b.y;
	pldraw_fill_rect(pldraw, draw_col, &rect);

	if (plep_update(plep, &rect, wfid) < 0)
		return -1;

	rect.a.x = outer.a.x;
	rect.a.y = inner.a.y + gap;
	rect.b.x = inner.a.x;
	rect.b.y = inner.b.y - gap;
	pldraw_fill_rect(pldraw, draw_col, &rect);

	if (plep_update(plep, &rect, wfid) < 0)
		return -1;

	rect.a.x = inner.b.x;
	rect.b.x = outer.b.x;
	pldraw_fill_rect(pldraw, draw_col, &rect);

	if (plep_update(plep, &rect, wfid) < 0)
		return -1;

	return 0;
}

int test_regions(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	struct plep_rect rect;
	int delta_wf;
	pldraw_color_t col;
	int ret = 0;

	assert(pldraw != NULL);
	assert(g_initialised);

	plep = pldraw_get_plep(pldraw);

	if (plep == NULL) {
		delta_wf = 0;
	} else {
		delta_wf = plep_get_wfid(plep, PLEP_DELTA);

		if (delta_wf < 0)
			delta_wf = g_clear_wf;
	}

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 0);

	LOG("black frame");

	rect.a.x = 50;
	rect.a.y = 350;
	rect.b.x = 100;
	rect.b.y = 550;
	pldraw_fill_rect(pldraw, g_black, &rect);

	if (plep_update(plep, &rect, g_quick_wf) < 0)
		ret = -1;

	rect.a.x += 300;
	rect.b.x += 300;
	pldraw_fill_rect(pldraw, g_black, &rect);

	if (plep_update(plep, &rect, g_quick_wf) < 0)
		ret = -1;

	rect.a.x = 100;
	rect.a.y = 350;
	rect.b.x = 350;
	rect.b.y = 400;
	pldraw_fill_rect(pldraw, g_black, &rect);

	if (plep_update(plep, &rect, g_quick_wf) < 0)
		ret = -1;

	rect.a.y += 150;
	rect.b.y += 150;
	pldraw_fill_rect(pldraw, g_black, &rect);

	if (plep_update(plep, &rect, g_quick_wf) < 0)
		ret = -1;

	LOG("grey rectangles");

	col = pldraw_get_grey(pldraw, 0x80);

	rect.a.x = 50;
	rect.a.y = 50;
	rect.b.x = 200;
	rect.b.y = 100;
	pldraw_fill_rect(pldraw, col, &rect);

	if (plep_update(plep, &rect, delta_wf) < 0)
		ret = -1;

	rect.a.y += 100;
	rect.b.y += 100;
	pldraw_fill_rect(pldraw, col, &rect);

	if (plep_update(plep, &rect, delta_wf) < 0)
		ret = -1;

	rect.a.y += 100;
	rect.b.y += 100;
	pldraw_fill_rect(pldraw, col, &rect);

	if (plep_update(plep, &rect, delta_wf) < 0)
		ret = -1;

	rect.a.x += 200;
	rect.b.x += 200;
	pldraw_fill_rect(pldraw, col, &rect);

	if (plep_update(plep, &rect, delta_wf) < 0)
		ret = -1;

	rect.a.y -= 100;
	rect.b.y -= 100;
	pldraw_fill_rect(pldraw, col, &rect);

	if (plep_update(plep, &rect, delta_wf) < 0)
		ret = -1;

	rect.a.y -= 100;
	rect.b.y -= 100;
	pldraw_fill_rect(pldraw, col, &rect);

	if (plep_update(plep, &rect, delta_wf) < 0)
		ret = -1;

	return ret;
}

int test_radar(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	enum quadrant_id {
		EDGE_TOP,
		EDGE_RIGHT,
		EDGE_BOTTOM,
		EDGE_LEFT,
	};
	static const int PITCH = 10;
	const pldraw_color_t col_rgbw[] = {
		g_red, g_green, g_blue, g_white };
	const int col_rgbw_n = ARRAY_SIZE(col_rgbw);
	struct plep *plep;
	__sighandler_t prev_sigint;
	struct plep_rect rect;
	struct plep_rect ep_area;
	struct plep_rect line;
	enum quadrant_id quad;
	pldraw_color_t draw_col = g_black;
	pldraw_color_t col;
	int col_rgbw_index = -1;
	unsigned long sleep_us;
	int wfid;
	int xres;
	int yres;
	int width;
	int height;
	int ret = 0;

	assert(pldraw != NULL);
	assert(g_initialised);

	if (opts_n > 0) {
		unsigned long sleep_ms;

		if (str2ul(&sleep_ms, opts[0]) < 0) {
			LOG("Invalid sleep value: %s", opts[0]);
			return -1;
		}

		sleep_us = sleep_ms * 1000;
	} else {
		sleep_us = 10000;
	}

	if (opts_n > 1) {
		const char *col_str = opts[1];

		if (!strcmp(col_str, "rgbw")) {
			col_rgbw_index = 0;
		} else if (pldraw_str2col(pldraw, &draw_col, col_str) < 0) {
			return -1;
		}
	}

	if (!pldraw_colcmp(pldraw, col, g_black)
	    && (pldraw_get_cfa(pldraw) == PLDRAW_CFA_NONE)) {
		wfid = g_clear_wf;
	} else {
		wfid = g_quick_wf;
	}

	plep = pldraw_get_plep(pldraw);

	xres = pldraw_get_xres(pldraw);
	yres = pldraw_get_yres(pldraw);
	width = ((xres / PITCH) - 2) * PITCH;
	height = ((yres / PITCH) - 2) * PITCH;

	rect.a.x = (xres - width) / 2;
	rect.a.y = (yres - height) / 2;
	rect.b.x = rect.a.x + width;
	rect.b.y = rect.a.y + height;

	LOG("rectangle: (%i, %i, %i, %i)",
	    rect.a.x, rect.a.y, rect.b.x, rect.b.y);

	draw_border(pldraw, draw_col, &rect);

	if (plep_update(plep, &rect, g_clear_wf) < 0)
		ret = -1;

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 0);
	plep_set_opt(plep, PLEP_PARTIAL, 1);

	++rect.a.x;
	++rect.a.y;
	--rect.b.x;
	--rect.b.y;
	memcpy(&ep_area, &rect, sizeof ep_area);
	--rect.b.x;
	--rect.b.y;

	line.a.x = (rect.a.x + rect.b.x) / 2;
	line.a.y = (rect.a.y + rect.b.y) / 2;
	line.b.x = rect.a.x;
	line.b.y = rect.a.y;

	if (col_rgbw_index < 0)
		col = draw_col;
	else
		col = col_rgbw[0];

	prev_sigint = signal(SIGINT, sigint_stop);
	quad = EDGE_TOP;
	g_run = 1;
	LOG("Press Ctrl-C to stop");

	while (!ret && g_run) {
		pldraw_draw_line(pldraw, col, &line);

		if (plep_update(plep, &ep_area, wfid) < 0) {
			ret = -1;
			break;
		}

		switch (quad)
		{
		case EDGE_TOP:
			line.b.x += PITCH;
			if (line.b.x >= rect.b.x) {
				line.b.x = rect.b.x;
				quad = EDGE_RIGHT;
			}
			break;
		case EDGE_RIGHT:
			line.b.y += PITCH;
			if (line.b.y >= rect.b.y) {
				line.b.y = rect.b.y;
				quad = EDGE_BOTTOM;
			}
			break;
		case EDGE_BOTTOM:
			line.b.x -= PITCH;
			if (line.b.x <= rect.a.x) {
				line.b.x = rect.a.x;
				quad = EDGE_LEFT;
			}
			break;
		case EDGE_LEFT:
			line.b.y -= PITCH;
			if (line.b.y <= rect.a.y) {
				line.b.y = rect.a.y;
				quad = EDGE_TOP;

				if (col_rgbw_index < 0) {
					col = pldraw_colcmp(
						pldraw, col, draw_col)
						? draw_col : g_white;
				} else {
					if (++col_rgbw_index == col_rgbw_n)
						col_rgbw_index = 0;
					col = col_rgbw[col_rgbw_index];
				}
			}
			break;
		}

		usleep(sleep_us);
	}

	signal(SIGINT, prev_sigint);

	plep_set_opt(plep, PLEP_PARTIAL, 0);

	return ret;
}

int test_check(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	const struct plep_point origin = { .x = 0, .y = 0 };
	struct plep *plep = pldraw_get_plep(pldraw);
	pldraw_color_t draw_col;
	unsigned long dim;
	int wfid;
	int n;
	int m;

	assert(pldraw != NULL);
	assert(g_initialised);

	if (opts_n > 0) {
		if (str2ul(&dim, opts[0]) < 0) {
			LOG("Invalid square dimension: %s", opts[0]);
			return -1;
		}
	} else {
		dim = pldraw_get_xres(pldraw) / 40;
	}

	if (opts_n > 1) {
		if (pldraw_str2col(pldraw, &draw_col, opts[1]))
			return -1;
	} else {
		draw_col = g_black;
	}

	if (opts_n > 2) {
		const char *wfid_str = opts[2];

		wfid = plep_get_wfid(plep, wfid_str);

		if (wfid < 0) {
			LOG("Invalid waveform: %s", wfid_str);
			return -1;
		}

		LOG("waveform name: %s", wfid_str);
	} else {
		wfid = g_clear_wf;
	}

	if (dim == 0)
		return -1;

	n = 1 + ((pldraw_get_xres(pldraw) - 1) / dim);
	m = 1 + ((pldraw_get_yres(pldraw) - 1) / dim);

	draw_checkerboard(pldraw, &origin, dim, 0, n, m, draw_col);

	return plep_update_screen(plep, wfid);
}

int test_fill(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	pldraw_color_t col;
	int wfid;

	assert(pldraw != NULL);
	assert(g_initialised);

	plep = pldraw_get_plep(pldraw);

	if (opts_n < 1)
		col = g_white;
	else if (pldraw_str2col(pldraw, &col, opts[0]))
		return -1;

	if (opts_n > 1) {
		const char *wfid_str = opts[1];

		wfid = plep_get_wfid(plep, wfid_str);

		if (wfid < 0) {
			LOG("Invalid waveform: %s", wfid_str);
			return -1;
		}

		LOG("waveform name: %s", wfid_str);
	} else {
		wfid = g_clear_wf;
	}

	pldraw_fill_screen(pldraw, col);

	return plep_update_screen(plep, wfid);
}

int test_sweep(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	unsigned long sleep_us = 10000;
	int i;

	assert(pldraw != NULL);

	plep = pldraw_get_plep(pldraw);

	if (opts_n >= 1) {
		unsigned long sleep_ms;

		if (str2ul(&sleep_ms, opts[0]) < 0) {
			LOG("Invalid sleep value: %s", opts[0]);
			return -1;
		}

		sleep_us = sleep_ms * 1000;
	}

	for (i = 0; i < 0xFF; ++i) {
		const pldraw_color_t col = pldraw_get_color(pldraw, i, i, i);

		pldraw_fill_screen(pldraw, col);
		plep_update_screen(plep, g_clear_wf);
		usleep(sleep_us);
	}

	return 0;
}

int test_none(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	pldraw = NULL;

	LOG("That's all folks!");

	return 0;
}

int test_update(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	int wfid;

	assert(pldraw != NULL);

	plep = pldraw_get_plep(pldraw);

	if (opts_n > 0) {
		wfid = plep_get_wfid(plep, opts[0]);

		if (wfid < 0) {
			LOG("Invalid waveform: %s", opts[0]);
			return -1;
		}

		LOG("waveform name: %s", opts[0]);
	} else {
		wfid = g_clear_wf;
	}

	return plep_update_screen(plep, wfid);
}

int test_cross(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep_rect area;
	int tmp_x;

	assert(pldraw != NULL);

	area.a.x = 0;
	area.a.y = 0;
	area.b.x = pldraw_get_xres(pldraw);
	area.b.y = pldraw_get_yres(pldraw);
	pldraw_fill_rect(pldraw, g_black, &area);
	pldraw_apply_border(&area, 10);
	pldraw_fill_rect(pldraw, g_white, &area);
	pldraw_draw_line(pldraw, g_black, &area);
	tmp_x = area.a.x;
	area.a.x = area.b.x;
	area.b.x = tmp_x;
	pldraw_draw_line(pldraw, g_black, &area);

	return plep_update_screen(pldraw_get_plep(pldraw), g_quick_wf);
}

int test_stripes(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	int stat;
	unsigned long width;

	assert(pldraw != NULL);

	if (opts_n > 0) {

		if (str2ul(&width, opts[0]) < 0) {
			LOG("Invalid stripes width: %s", opts[0]);
			return -1;
		}
	} else {
		width = 1;
	}

	plep = pldraw_get_plep(pldraw);

	draw_stripes(pldraw, g_black, width);
	stat = plep_update_screen(plep, g_quick_wf);
	if (stat)
		return stat;

	draw_stripes(pldraw, g_white, width);
	stat = plep_update_screen(plep, g_quick_wf);
	if (stat)
		return stat;

	return 0;
}

int test_fastmono(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	int stat = 0;

	assert(pldraw != NULL);

	if (slide_square(pldraw, PLEP_DELTA, NULL) < 0)
		stat = -1;

	if (slide_square(pldraw, PLEP_FAST, PLEP_9SUBF) < 0)
		stat = -1;

	if (slide_square(pldraw, PLEP_FAST, PLEP_6SUBF) < 0)
		stat = -1;

	if (slide_square(pldraw, PLEP_FAST, PLEP_3SUBF) < 0)
		stat = -1;

	return stat;
}

int test_highlight(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep_rect area;
	struct pldraw_pen pen;
	struct plep *plep;
	int xres;
	int yres;
	int wfid;
	int grey;

	assert(pldraw != NULL);

	plep = pldraw_get_plep(pldraw);

	assert(plep != NULL);

	wfid = plep_get_wfid(plep, PLEP_HIGHLIGHT);

	if (wfid < 0) {
		LOG("highlight waveform not available");
		return -1;
	}

	xres = pldraw_get_xres(pldraw);
	yres = pldraw_get_yres(pldraw);

	area.a.x = 0;
	area.b.x = xres;

	for (grey = 0; grey < 4; ++grey) {
		const uint8_t grey4 = pldraw_grey_4[grey];
		const pldraw_color_t col = pldraw_get_grey(pldraw, grey4);

		area.a.y = grey * yres / 4;
		area.b.y = (grey + 1) * yres / 4;
		pldraw_fill_rect(pldraw, col, &area);
	}

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);

	if (plep_update_screen(plep, g_clear_wf))
		return -1;

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 0);

	area.a.y = 0;
	area.b.y = pldraw_get_yres(pldraw);

	for (grey = 0; grey < 0x100; ++grey) {
		const pldraw_color_t col = pldraw_get_grey(pldraw, grey);

		area.a.x = grey * xres / 0x100;
		area.b.x = (grey + 1) * xres / 0x100;
		pldraw_fill_rect(pldraw, col, &area);
	}

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);

	if (plep_update_screen(plep, wfid))
		return -1;

	pen.font = pldraw_fonts[0];
	pen.foreground = g_black;
	pen.background = g_white;

	for (grey = 0; grey < 16; ++grey) {
		char txt[4];

		area.a.x = 1 + grey * xres / 16;
		area.a.y = yres - 20;
		area.b.x = 1 + (grey + 1) * xres / 16;
		area.b.y = yres;
		snprintf(txt, 4, "%d", (grey * 16));
		pldraw_draw_text(pldraw, &pen, &area.a, txt);

		area.a.x -= 1;
		area.a.y = yres - 50;
		area.b.x = area.a.x;
		pldraw_draw_line(pldraw, g_black, &area);
	}

	area.a.x = 0;
	area.a.y = yres - 50;
	area.b.x = xres;
	area.b.y = yres;
	plep_update(plep, &area, g_clear_wf);

	return 0;
}

int test_text(struct pldraw *pldraw, char * const *opts, int opts_n)
{
	struct plep *plep;
	struct pldraw_pen pen;
	struct plep_point pt;
	float grid;

	assert(pldraw != NULL);

	plep = pldraw_get_plep(pldraw);
	grid = pldraw_get_xres(pldraw) / 12.0;
	pen.font = pldraw_fonts[0];

	if (opts_n > 0) {
		if (pldraw_str2col(pldraw, &pen.foreground, opts[0]) < 0)
			return -1;
	} else {
		pen.foreground = pldraw_get_grey(pldraw, 0);
	}

	if (opts_n > 1) {
		if (pldraw_str2col(pldraw, &pen.background, opts[1]) < 0)
			return -1;
	} else {
		pen.background = pldraw_get_grey(pldraw, 192);
	}

	if (opts_n > 2) {
		const char *msg = opts[2];
		struct plep_rect area;

		pt.x = grid;
		pt.y = grid;
		pldraw_draw_text(pldraw, &pen, &pt, msg);

		area.a.x = pt.x;
		area.a.y = pt.y;
		area.b.x = pt.x + (strlen(msg) * pen.font->width);
		area.b.y = pt.y + pen.font->height;

		if (plep_update(plep, &area, g_quick_wf) < 0)
			return -1;

		return 0;
	}

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);

	pt.x = grid;
	pt.y = grid * 4.0;
	pldraw_draw_text(pldraw, &pen, &pt,
			 "This is a long text in the middle of the screen.");

	if (plep_update_screen(plep, g_clear_wf) < 0)
		return -1;

	pt.x = grid;
	pt.y = grid * 6.0;
	pldraw_draw_text(pldraw, &pen, &pt,
			 "Symbols: !\"[]$%^&*()+=\\/<>");

	if (plep_update_screen(plep, g_clear_wf) < 0)
		return -1;

	pt.x = pldraw_get_xres(pldraw) - 30;
	pt.y = grid * 2.0;
	pldraw_draw_text(pldraw, &pen, &pt, "Edge");

	if (plep_update_screen(plep, g_clear_wf) < 0)
		return -1;

	pt.x = grid;
	pt.y = pldraw_get_yres(pldraw) - 10;
	pldraw_draw_text(pldraw, &pen, &pt, "Bottom of the screen");

	if (plep_update_screen(plep, g_clear_wf) < 0)
		return -1;

	return 0;
}

/* ----------------------------------------------------------------------------
 * static functions
 */

static void draw_checkerboard(struct pldraw *pldraw,
                              const struct plep_point *origin, size_t dim,
                              int vertical, int n, int m,
			      pldraw_color_t icolor)
{
	struct plep_rect square;
	int i, j;
	pldraw_color_t color = icolor;

	square.a.x = origin->x;
	square.a.y = origin->y;
	square.b.x = origin->x + dim;
	square.b.y = origin->y + dim;

	for (j = 0; j < m; ++j) {
		const struct plep_point line = {
			.x = square.a.x,
			.y = square.a.y
		};
		const pldraw_color_t line_color = color;

		for (i = 0; i < n; ++i) {

			pldraw_fill_rect(pldraw, color, &square);
			color = pldraw_colcmp(pldraw, color, icolor) ?
				icolor : g_white;

			if (vertical) {
				square.a.y += dim;
				square.b.y += dim;
			} else {
				square.a.x += dim;
				square.b.x += dim;
			}
		}

		color = pldraw_colcmp(pldraw, line_color, icolor) ?
			icolor : g_white;

		if (vertical) {
			square.a.x = line.x + dim;
			square.a.y = line.y;
		} else {
			square.a.x = line.x;
			square.a.y = line.y + dim;
		}

		square.b.x = square.a.x + dim;
		square.b.y = square.a.y + dim;
	}
}

static void draw_stripes(struct pldraw *pldraw, pldraw_color_t col,
			 unsigned long width)
{
	struct plep_rect coords;
	const int yres = pldraw_get_yres(pldraw);
	int y;

	coords.a.x = 0;
	coords.b.x = pldraw_get_xres(pldraw);

	for (y = 0; y < yres; ++y) {
		coords.a.y = coords.b.y = y;
		pldraw_draw_line(pldraw, col, &coords);

		if (!(y % width)) {
			col = pldraw_colcmp(pldraw, col, g_black)
				? g_black : g_white;
		}
	}
}

static void draw_border(struct pldraw *pldraw, pldraw_color_t col,
			const struct plep_rect *rect)
{
	struct plep_rect line;

	/* top */
	line.a.x = rect->a.x;
	line.a.y = rect->a.y;
	line.b.x = rect->b.x - 1;
	line.b.y = rect->a.y;
	pldraw_draw_line(pldraw, col, &line);

	/* bottom */
	line.a.y = rect->b.y - 1;
	line.b.y = rect->b.y - 1;
	pldraw_draw_line(pldraw, col, &line);

	/* left */
	line.b.x = rect->a.x;
	line.b.y = rect->a.y;
	pldraw_draw_line(pldraw, col, &line);

	/* right */
	line.a.x = rect->b.x - 1;
	line.b.x = rect->b.x - 1;
	pldraw_draw_line(pldraw, col, &line);
}

static int slide_square(struct pldraw *pldraw, const char *wf_type,
			const char *wf_opt)
{
	const int xres = pldraw_get_xres(pldraw);
	const int yres = pldraw_get_yres(pldraw);
	struct plep *plep = pldraw_get_plep(pldraw);
	struct plep_rect square;
	int wfid;
	int dim;

	wfid = plep_get_wfid_split(plep, wf_type, PLEP_MONO, wf_opt);
	if (wfid < 0) {
		LOG("Waveform not found: %s/mono/%s", wf_type, wf_opt);
		return -1;
	}

	plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);
	pldraw_fill_screen(pldraw, g_white);
	plep_update_screen(plep, g_clear_wf);
	plep_set_opt(plep, PLEP_SYNC_UPDATE, 0);
	usleep(500000);

	dim = xres / 8;
	square.a.x = 0;
	square.a.y = 0;
	square.b.x = dim;
	square.b.y = dim;

	while (square.b.x <= xres) {
		pldraw_fill_rect(pldraw, g_black, &square);
		plep_update(plep, &square, wfid);
		square.a.x += dim;
		square.b.x += dim;
		usleep(80000);
	}

	dim = yres / 8;
	square.a.x = 0;
	square.a.y = 0;
	square.b.x = dim;
	square.b.y = dim;

	while (square.b.y <= yres) {
		pldraw_fill_rect(pldraw, g_black, &square);
		plep_update(plep, &square, wfid);
		square.a.y += dim;
		square.b.y += dim;
		usleep(80000);
	}

	usleep(500000);

	return 0;
}

static void sigint_stop(int signum)
{
	if (signum == SIGINT) {
		LOG("STOP");
		g_run = 0;
	}
}

static int str2ul(unsigned long *out, const char *str)
{
	errno = 0;
	*out = strtoul(str, NULL, 10);

	return errno ? -1 : 0;
}
