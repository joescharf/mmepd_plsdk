LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS += -Wall -O2
LOCAL_MODULE := eptest
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := eptest.c tests.c
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../libplutil \
	$(LOCAL_PATH)/../libplepaper \
	$(LOCAL_PATH)/../libpldraw
LOCAL_STATIC_LIBRARIES := libplepaper libpldraw libplutil libeglib
ifndef PLASTICLOGIC_NDK_BUILD
LOCAL_SHARED_LIBRARIES += libdl
else
LOCAL_LDLIBS := -ldl
endif
include $(BUILD_EXECUTABLE)

