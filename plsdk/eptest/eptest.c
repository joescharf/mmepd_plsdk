/*
  E-Paper test - eptest

  Copyright (C) 2010, 2011, 2012, 2013, 2014 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "tests.h"
#include <libplepaper.h>
#include <libpldraw.h>
#include <getopt.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h> /* exit */
#include <string.h> /* strcmp */
#include <stdio.h>

#define LOG_TAG "eptest"
#include <plsdk/log.h>

#define INVALID_MODE ((enum plep_mode)(-1))

typedef int (*test_t) (struct pldraw *pldraw, char * const *opts, int opts_n);

struct test {
	const char *name;
	test_t func;
};

static const char APP_NAME[] = "eptest";
static const char VERSION[] = "2.7";
static const char DESCRIPTION[] = "E-Paper test";
static const char LICENSE[] =
	"This program is distributed in the hope that it will be useful,\n"
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	"GNU General Public License for more details.\n";
static const char COPYRIGHT[] =
	"Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited";

static void print_usage(void);

int main(int argc, char * const *argv)
{
	static const struct test tests[] = {
		{ .name = "palette",  .func = test_palette  },
		{ .name = "frame",    .func = test_frame    },
		{ .name = "regions",  .func = test_regions  },
		{ .name = "radar",    .func = test_radar    },
		{ .name = "check",    .func = test_check    },
		{ .name = "fill",     .func = test_fill     },
		{ .name = "sweep",    .func = test_sweep    },
		{ .name = "none",     .func = test_none     },
		{ .name = "update",   .func = test_update   },
		{ .name = "cross",    .func = test_cross    },
		{ .name = "stripes",  .func = test_stripes  },
		{ .name = "fastmono", .func = test_fastmono },
		{ .name = "highlight",.func = test_highlight },
		{ .name = "text",     .func = test_text     },
		{ .name = NULL,       .func = NULL          }
	};
	static const char OPTIONS[] = "hvld:e:cm:r:f::O:";
	struct plep *plep;
	struct pldraw *pldraw;
	const char *fb_dev = NULL;
	const char *ep_dev = NULL;
	const char *test_name = NULL;
	const struct test *test;
	int refresh_wf;
	int mono_wf;
	int log_info = 0;
	int do_clear = 0;
	int rotation_angle = -1;
	const char *conf_file = NULL;
	int cfa = -1;
	const char *mode = NULL;
	char * const *opts = NULL;
	int opts_n = 0;
	int ret = EXIT_SUCCESS;
	int c;

	while ((c = getopt(argc, argv, OPTIONS)) != -1) {
		switch (c) {
		case 'h':
			print_usage();
			exit(EXIT_SUCCESS);
			break;

		case 'v':
			printf("%s v%s - %s\n%s\n%s\n", APP_NAME, VERSION,
			       DESCRIPTION, COPYRIGHT, LICENSE);
			exit(EXIT_SUCCESS);
			break;

		case 'l':
			log_info = 1;
			break;

		case 'd':
			fb_dev = optarg;
			break;

		case 'e':
			ep_dev = optarg;
			break;

		case 'c':
			do_clear = 1;
			break;

		case 'm':
			mode = optarg;
			break;

		case 'r': {
			long raw_angle;

			errno = 0;
			raw_angle = strtol(optarg, NULL, 10);

			if (errno || (raw_angle < 0) || (raw_angle > 270)
			    || (raw_angle % 90)) {
				LOG("Invalid rotation angle");
				print_usage();
				exit(EXIT_FAILURE);
			}

			rotation_angle = raw_angle;

			break;
		}

		case 'f':
			if (optarg == NULL) {
				cfa = PLDRAW_CFA_GR_BW;
			} else {
				cfa = pldraw_get_cfa_id(optarg);

				if (cfa < 0) {
					LOG("Invalid CFA identifier: %s",
					    optarg);
					print_usage();
					exit(EXIT_FAILURE);
				}
			}
			break;

		case 'O':
			conf_file = optarg;
			if (access(conf_file, F_OK)) {
				LOG_ERRNO("Configuration file");
				exit(EXIT_FAILURE);
			}
			break;

		case '?':
		default:
			LOG("Invalid arguments");
			print_usage();
			exit(EXIT_FAILURE);
			break;
		}
	}

	if (optind < argc) {
		test_name = argv[optind++];

		if (optind < argc) {
			opts = argv + optind;
			opts_n = argc - optind;
		}
	}

	LOG("%s v%s", APP_NAME, VERSION);

	plep = plep_init(ep_dev, mode, conf_file);

	if (plep == NULL) {
		LOG("failed to initialise plep");
		exit(EXIT_FAILURE);
	}

	pldraw = pldraw_init(fb_dev, conf_file);

	if (pldraw == NULL) {
		LOG("failed to initialise fb device");
		exit(EXIT_FAILURE);
	}

	pldraw_set_plep(pldraw, plep);

	if (cfa >= 0)
		pldraw_set_cfa(pldraw, cfa);
	else
		cfa = pldraw_get_cfa(pldraw);

	if (cfa != PLDRAW_CFA_NONE)
		LOG("CFA: %s", pldraw_cfa_name[cfa]);

	if (rotation_angle >= 0)
		pldraw_set_rotation(pldraw, rotation_angle);
	else
		rotation_angle = pldraw_get_rotation(pldraw);

	if (rotation_angle)
		LOG("rotation: %d", rotation_angle);

	refresh_wf = plep_get_wfid(plep, PLEP_REFRESH);
	mono_wf = plep_get_wfid(plep, PLEP_DELTA"/"PLEP_MONO);

	if (mono_wf < 0) {
		mono_wf = refresh_wf;
		LOG("Warning: no mono waveform found, using refresh");
	}

	test_initialise(pldraw, refresh_wf, mono_wf);

	if (log_info)
		pldraw_log_info(pldraw);

	if (test_name == NULL) {
		test = &tests[0];
	} else {
		const struct test *it;

		it = tests;
		test = NULL;

		while ((it->name != NULL) && (test == NULL)) {
			if (!strcmp(test_name, it->name))
				test = it;
			else
				++it;
		}
	}

	if (test == NULL) {
		LOG("invalid test name (%s)", test_name);
		print_usage();
		ret = EXIT_FAILURE;
	} else {
		LOG("starting test: %s", test->name);

		if (do_clear) {
			plep_set_opt(plep, PLEP_SYNC_UPDATE, 1);
			pldraw_fill_screen(pldraw,
					   pldraw_get_grey(pldraw, 0xFF));
			plep_update_screen(plep, refresh_wf);
			plep_set_opt(plep, PLEP_SYNC_UPDATE, 0);
		}

		if (test->func(pldraw, opts, opts_n) < 0)
			ret = EXIT_FAILURE;
		else
			ret = EXIT_SUCCESS;
	}

	pldraw_free(pldraw);
	plep_free(plep);

	if (ret != EXIT_SUCCESS)
		LOG("test failed!");
	else
		LOG("done.");

	return ret;
}

static void print_usage(void)
{
	printf(
"Usage: %s <OPTIONS> <TEST_NAME> <TEST_OPTIONS>\n"
"\n"
"TEST_NAME:\n"
"    The following test names and associated options can be used:\n"
"\n"
"    palette\n"
"      Draw grey scales, primary and seconds colours and saturated palette.\n"
"    frame GRID COLOUR GAP WAVEFORM\n"
"      Draw a solid frame.  This is useful to test contiguous update\n"
"      regions.  The GRID specifies the ratio to calculate the grid step, so\n"
"      a bigger value results in a thinner frame (default is 12).  The \n"
"      COLOUR is the colour of the frame (default is black).  The GAP gives\n"
"      the number of pixels to leave between the horizontal and vertical\n"
"      bars (default is 1).  The WAVEFORM is the waveform to use (default \n"
"      is refresh).\n"
"    regions\n"
"      Update regions with grey rectangles simultaneously.\n"
"    radar SLEEP_MS COLOUR\n"
"      Draw a line continuously rotating until SIGINT (Ctrl-C).\n"
"      Sleep for SLEEP_MS between each line step.\n"
"      Draw the line in given COLOUR and erase with white.\n"
"      The special colour value rgbw uses the 3 primary colours.\n"
"    check DIMENSION COLOUR WAVEFORM\n"
"      Fill screen with checker board with squares of the given\n"
"      DIMENSION or the horizontal screen resolution / 40 by default.\n"
"      The squares are of the given COLOUR or black by default, on white\n"
"      background.\n"
"      The display is then updated with the given WAVEFORM or refresh by\n"
"      default.\n"
"    fill COLOUR WAVEFORM\n"
"      Fill screen with given COLOUR or white by default.\n"
"      Update the screen with given WAVEFORM or refresh by default.\n"
"    sweep DELAY\n"
"      Fill screen with 256 grey levels from black to white, sleeping for\n"
"      DELAY milliseconds in between each (default is 10)\n."
"    fastmono\n"
"      Draw some fast mono waveform animation effects.\n"
"    highlight\n"
"      Draw some highlight waveform animation effects.\n"
"    text FOREGROUND BACKGROUND MESSAGE\n"
"      With no MESSAGE option, draw some text around the screen using the\n"
"      given FOREGROUND and BACKGROUND colours or black on white by default.\n"
"      If MESSAGE is provided, print this message at a fixed offset.\n"
"    none\n"
"      Do nothing.  This is useful to only query the framebuffer driver.\n"
"    update WAVEFORM\n"
"      Update the display with the given WAVEFORM or refresh by default.\n"
"\n"
"    If no test name is specified, the palette test will be run.\n"
"\n"
"TEST_OPTIONS:\n"
"   Extra options are passed on as-is to each test so they can implement\n"
"   their own parameters.\n"
"\n"
"OPTIONS:\n"
"  -h\n"
"    Show this help message and exit\n"
"\n"
"  -v\n"
"    Show the version, copyright and license information, and exit.\n"
"\n"
"  -l\n"
"    Log the frame buffer and ePDC information.\n"
"\n"
"  -d DEVICE\n"
"    Specify the frame buffer device to use.  If not specified, the usual\n"
"    suspects will be looked for (/dev/fb0 and variants).\n"
"\n"
"  -e DEVICE\n"
"    Specify the ePDC device to use.  This relies on the implementation of\n"
"    the ePDC mode being used.\n"
"\n"
"  -c\n"
"    Clear the screen before running the test.  If not specified, test\n"
"    patterns will be drawn on top of each other.\n"
"\n"
"  -m EPDC_MODE\n"
"    Specify which mode to use to control the ePDC.  By default, this is \n"
"    discovered automatically by probing the system.  This is then used to \n"
"    load the corresponding mod_plepaper_EPDC_MODE.so module.  Usual values \n"
"    for EPDC_MODE are: \n"
"\n"
"    noep      Do not use any ePDC\n"
"    imx       Freescale i.MX508\n"
"    plepdc    Plastic Logic ePDC interface\n"
"\n"
"  -r ANGLE\n"
"    Rotate the display by ANGLE degrees.\n"
"    Valid values are 0, 90, 180 and 270.\n"
"\n"
"  -O CONFIG_FILE\n"
"    Use configuration file with the specified CONFIG_FILE path.  If not\n"
"    specified, the default path (typically ~/.plsdk.ini) is used.\n"
"\n"
"  -f CFA\n"
"    Enable the colour filter array (CFA) specified by the CFA identifier,\n"
"    or the default standard GR_BW one otherwise.  This is useful for colour\n"
"    displays only.\n"
"\n", APP_NAME);
}
