LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS += -Wall -O2
LOCAL_MODULE := liblzsslib
LOCAL_SRC_FILES := lzss.c lzss-file.c
include $(BUILD_STATIC_LIBRARY)
