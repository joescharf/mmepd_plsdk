/*
  LZSS command line interface

  Copyright (C) 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>
      Nick Terry <nick.terry@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <lzss.h>
#include <getopt.h>
#include <errno.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define LOG_TAG "lzss"
#include <plsdk/log.h>

static const char APP_NAME[] = "lzss";
static const char VERSION[] = LZSS_VERSION;
static const char DESCRIPTION[] = "LZSS encoder/decoder";
static const char LICENSE[] =
	"This program is distributed in the hope that it will be useful,\n"
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	"GNU General Public License for more details.\n";
static const char COPYRIGHT[] =
	"Copyright (C) 2013 Plastic Logic Limited";

enum cmd {
	CMD_ENCODE = 0,
	CMD_DECODE,
	CMD_TEST,
	_CMD_N_
};

static void print_usage(void);
static int auto_test(struct lzss *lzss);

static int parse_uint(const char *opt, unsigned *uint, unsigned max)
{
	long raw;

	errno = 0;
	raw = strtol(opt, NULL, 10);

	if (errno || (raw < 0) || (raw > max)) {
		LOG("Invalid unsigned int");
		return -1;
	}

	*uint = raw;

	return 0;
}

int main(int argc, char **argv)
{
	static const char *CMD_TABLE[_CMD_N_] = {
		[CMD_ENCODE] = "e",
		[CMD_DECODE] = "d",
		[CMD_TEST] = "test",
	};
	enum opt {
		OPT_CMD = 0,
		OPT_IN_PATH,
		OPT_OUT_PATH,
		_OPT_N_
	};
	static const char *opts = "hvi:j:";
	const char *cmd_str;
	unsigned ei = LZSS_STD_EI;
	unsigned ej = LZSS_STD_EJ;
	const char *in_path;
	const char *out_path;
	enum cmd cmd;
	struct lzss lzss;
	int c;
	int stat;

	while ((c = getopt(argc, argv, opts)) != -1) {
		switch (c) {
		case 'h':
			print_usage();
			exit(EXIT_SUCCESS);
			break;

		case 'v':
			printf("%s v%s - %s\n%s\n%s\n", APP_NAME, VERSION,
			       DESCRIPTION, COPYRIGHT, LICENSE);
			exit(EXIT_SUCCESS);
			break;

		case 'i':
			if (parse_uint(optarg, &ei, 10))
				exit(EXIT_FAILURE);
			break;

		case 'j':
			if (parse_uint(optarg, &ej, 10))
				exit(EXIT_FAILURE);
			break;

		case '?':
		default:
			LOG("Invalid arguments");
			print_usage();
			exit(EXIT_FAILURE);
			break;
		}
	}

	if ((argc - optind) < OPT_CMD) {
		LOG("No command provided");
		print_usage();
		exit(EXIT_FAILURE);
	}

	LOG("Parameters: i=%d, j=%d", ei, ej);
	LOG("Buffer size: %zu", LZSS_BUFFER_SIZE(ei));

	if (lzss_init(&lzss, ei, ej) || lzss_alloc_buffer(&lzss)) {
		LOG("Failed to initialize lzss");
		exit(EXIT_FAILURE);
	}

	cmd_str = argv[optind + OPT_CMD];

	for (cmd = 0; cmd < _CMD_N_; ++cmd)
		if (!strcmp(CMD_TABLE[cmd], cmd_str))
			break;

	if (cmd == _CMD_N_) {
		LOG("Invalid command");
		print_usage();
		stat = -1;
	} else if (cmd == CMD_TEST) {
		stat = auto_test(&lzss);
	} else if ((argc - optind) < _OPT_N_) {
		LOG("Invalid arguments");
		stat = -1;
	} else {
		in_path = argv[optind + OPT_IN_PATH];
		out_path = argv[optind + OPT_OUT_PATH];

		if (cmd == CMD_ENCODE) {
			LOG("Encoding...");
			stat = lzss_encode_file(&lzss, in_path, out_path);
			LOG("Input size: %zu, output size: %zu, rate: %zu%%",
			    lzss.in_size, lzss.out_size,
			    lzss.in_size ?
			    ((lzss.out_size * 100) / lzss.in_size) : 0);
		} else {
			LOG("Decoding...");
			stat = lzss_decode_file(&lzss, in_path, out_path);
		}
	}

	lzss_free_buffer(&lzss);

	exit(stat ? EXIT_FAILURE : EXIT_SUCCESS);
}

struct buffer_context {
	uint8_t *buffer;
	size_t size;
	size_t offset;
};

static int auto_test_rd(struct buffer_context *ctx)
{
	if (ctx->offset == ctx->size)
		return EOF;

	return ctx->buffer[ctx->offset++];
}

static int auto_test_wr(int c, struct buffer_context *ctx)
{
	if (ctx->offset == ctx->size)
		return EOF;

	ctx->buffer[ctx->offset++] = (uint8_t)c;

	return c;
}

#if 1
static void dump_buffer(const struct buffer_context *ctx)
{
}
#else
static void dump_buffer(const struct buffer_context *ctx)
{
	size_t i;

	LOG("DUMP %p offset: %zu, size: %zu", ctx, ctx->offset, ctx->size);

	for (i = 0; i < ctx->size; ++i) {
		LOG_PRINT("%02X", ctx->buffer[i]);
		if (!((i + 1) % 64))
			LOG_PRINT("\n");
	}

	if (i % 16)
		LOG_PRINT("\n");
}
#endif

static int auto_test(struct lzss *lzss)
{
	static const char original_data[] =
"Lorem ipsum dolor sit amet, consectetur\n"
"adipisicing elit, sed do eiusmod tempor\n"
"incididunt ut labore et dolore magna\n"
"aliqua. Ut enim ad minim veniam, quis\n"
"nostrud exercitation ullamco laboris nisi\n"
"ut aliquip ex ea commodo consequat.\n"
"Duis aute irure dolor in reprehenderit in\n"
"voluptate velit esse cillum dolore eu\n"
"fugiat nulla pariatur. Excepteur sint\n"
"occaecat cupidatat non proident, sunt in\n"
"culpa qui officia deserunt mollit anim id\n"
"est laborum.\n";
	static const size_t buffer_size = sizeof(original_data);
	struct buffer_context rd_ctx;
	struct buffer_context wr_ctx;
	struct lzss_io io;
	uint8_t *plain_data;
	uint8_t *lzss_data;
	size_t i;
	int stat;

	LOG("Initialising buffers...");

	plain_data = malloc(buffer_size);
	assert(plain_data != NULL);
	lzss_data = malloc(buffer_size);
	assert(lzss_data != NULL);

	rd_ctx.buffer = (uint8_t *)original_data;
	rd_ctx.size = buffer_size;
	rd_ctx.offset = 0;

	dump_buffer(&rd_ctx);

	wr_ctx.buffer = lzss_data;
	wr_ctx.size = buffer_size;
	wr_ctx.offset = 0;

	io.rd = (lzss_rd_t)auto_test_rd;
	io.i = &rd_ctx;
	io.wr = (lzss_wr_t)auto_test_wr;
	io.o = &wr_ctx;

	LOG("Encoding...");

	stat = lzss_encode(lzss, &io);

	if (stat) {
		LOG("Failed to encode data");
		goto exit_free_all;
	}

	LOG("Data encoded OK: %zu -> %zu %zu%%",
	    rd_ctx.size, wr_ctx.offset, (wr_ctx.offset * 100) / rd_ctx.size);

	rd_ctx.buffer = lzss_data;
	rd_ctx.size = wr_ctx.offset;
	rd_ctx.offset = 0;

	dump_buffer(&rd_ctx);

	wr_ctx.buffer = plain_data;
	wr_ctx.size = buffer_size;
	wr_ctx.offset = 0;

	LOG("Decoding...");

	stat = lzss_decode(lzss, &io);

	if (stat) {
		LOG("Failed to decode data");
		goto exit_free_all;
	}

	LOG("Comparing...");

	for (i = 0; i < buffer_size; ++i) {
		if (original_data[i] != plain_data[i]) {
			LOG("Data mismatch offset %zu: %02X %02X",
			    i, original_data[i], plain_data[i]);
			stat = -1;
			goto exit_free_all;
		}
	}

	LOG("OK, test passed.");

exit_free_all:
	free(lzss_data);
	free(plain_data);

	return stat;
}

static void print_usage(void)
{
	printf(
"Usage: %s <OPTIONS> COMMAND INPUT_FILE OUTPUT_FILE\n"
"\n"
"  Encode or decode a file using the LZSS compression algorithm.\n"
"\n"
"  Important: The -i and -j parameters are not stored in the output file,\n"
"  but they need to match when encoding and decoding the data.\n"
"\n"
"COMMAND can be any of the following:\n"
"    e       Encode INPUT_FILE into OUTPUT_FILE\n"
"    d       Decode INPUT_FILE into OUTPUT_FILE\n"
"    test    Run in-memory test\n"
"\n"
"OPTIONS:\n"
"  -h\n"
"    Show this help message and exit.\n"
"\n"
"  -v\n"
"    Show the version, copyright and license information, and exit.\n"
"\n"
" -i N\n"
"    Set the compression buffer size parameter.  The size in bytes is\n"
"    2^(N+1) although not all values are valid.  Default is 10 (2048 bytes).\n"
"\n"
" -j N\n"
"    Set the reading buffer size parameter.  This is not a working buffer\n"
"    but the read-only input data examined to produce the compressed data.\n"
"    The size in bytes is (2^N) + 1.\n"
"\n", APP_NAME);
}
