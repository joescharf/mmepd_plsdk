LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := lzss
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := main.c
LOCAL_CFLAGS += -Wall -O2
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../../../libplutil \
	$(LOCAL_PATH)/../lib
LOCAL_STATIC_LIBRARIES := libplutil libeglib liblzsslib
include $(BUILD_EXECUTABLE)
