# Python module to produce Plastic Logic binary display EEPROM waveform blob

# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys
import argparse
import struct
import hashlib
import lzss
import crc16
import data_util
import blob
from blob import BlobEntry, StringEntry, VermagicEntry, CRC16Entry

# Summary of the flat binary EEPROM format v1 (fully detailed in the Plastic
# Logic Software Manual):
# ------------------
# signature
# version
# ------------------
# display ID
# display type
# VCOM
# waveform uncompressed MD5
# waveform uncompressed data length
# waveform compressed data length
# waveform identifier
# waveform target controller type
# ------------------
# header CRC16
# waveform compressed data
# waveform compressed data CRC16
# ------------------

VERSION = 1
MAGIC = "PLWF"
EI = 7
EJ = 4

class WfDataEntry(BlobEntry):

    def __init__(self, key):
        super(WfDataEntry, self).__init__(None, key)

    def prepare(self, data):
        self._fmt = '{}s'.format(data['wvf_data_length'])


class WfCRC16Entry(CRC16Entry):

    def prepare(self, data):
        self._data_size = data['wvf_data_length']


class WfBlob(blob.Blob):

    def pack(self, data):
        wvf = data['wvf']
        wvf_md5 = hashlib.md5()
        wvf_md5.update(wvf)
        wvf_data = lzss.encode(wvf, ei=EI, ej=EJ)
        data.update({
            'wvf_md5': wvf_md5.digest(),
            'wvf_full_length': len(wvf),
            'wvf_data_length': len(wvf_data),
            'wvf_data': wvf_data,
            })
        return super(WfBlob, self).pack(data)

    def pack_with_wf_file(self, data, wf_file):
        data['wvf'] = open(wf_file, 'rb').read()
        return pack(data)

    def unpack(self, blob):
        data = super(WfBlob, self).unpack(blob)
        wvf = lzss.decode(data['wvf_data'], ei=EI, ej=EJ)
        wvf_md5 = hashlib.md5()
        wvf_md5.update(wvf)
        if wvf_md5.digest() != data['wvf_md5']:
            raise Exception("Waveform MD5 mismatch")
        data['wvf'] = wvf
        return data


def get_header_entries(version=VERSION):
    vermagic = [
        VermagicEntry('>4s', 'magic', MAGIC),
        VermagicEntry('>H', 'version', version)
        ]

    if version == VERSION:
        info = [
            StringEntry('display_id'),
            StringEntry('display_type'),
            BlobEntry('>i', 'vcom'),
            BlobEntry('16s', 'wvf_md5'),
            BlobEntry('>I', 'wvf_full_length'),
            BlobEntry('>I', 'wvf_data_length'),
            StringEntry('wvf_id'),
            StringEntry('target'),
            ]
        info_crc = [
            CRC16Entry('info_crc', data_size=sum(e.size for e in info)),
            ]
    else:
        raise Exception("Unsupported version: {}".format(version))

    return vermagic + info + info_crc

def get_waveform_entries(version=VERSION):
    if version == VERSION:
        waveform = [
            WfDataEntry('wvf_data'),
            WfCRC16Entry('wvf_crc'),
            ]
    else:
        raise Exception("Unsupported version: {}".format(version))

    return waveform

def get_entries(*args):
    return get_header_entries(*args) + get_waveform_entries(*args)

def build_blob(*args):
    """
    Build a blob object for the PL Display EEPROM binary format.  The
    associated data dictionary when packing or unpacking should contain the
    following entries:
    * display_id: string with the display identifier
    * display_type: string with the display type
    * vcom: integer with the VCOM voltage in mV
    * target: string with the name of the target ePDC
    * wvf_id: string with the waveform library identifier
    * wvf: uncompressed data format as a binary string

    Optionally:
    * version: binary format version (standard latest number by default)

    The following entries are also automatically added to the data dictionary
    when packing and unpacking by invoking the relevant code to compute their
    values:
    * info_crc: CRC16 of the info section
    * wvf_md5: MD5 sum of the uncompressed waveform data (wvf)
    * wvf_full_length: length in bytes of the uncompressed waveform data (wvf)
    * wvf_data_length: length in bytes of the stored compressed waveform data
                       (wvf_data)
    * wvf_data: compressed waveform data
    * wvf_crc: CRC16 of the waveform data (wvf_data)
    """
    return WfBlob(get_entries(*args))

def build_header_blob(*args):
    return blob.Blob(get_header_entries(*args))

def test():
    sample_data = {
        'display_id': "DisplayIdentifier 1234567890",
        'display_type': "Type18",
        'vcom': 4500,
        'wvf': data_util.lorem_ipsum,
        'wvf_id': "TestWaveformIdentifier ABCDXYZ",
        'target': "S1D13541",
        }
    test_md5 = 'b017fdaf6b9dc26ef87b273042c14a39'
    return blob.test(sample_data, build_blob, test_md5)


def main(argv):
    parser = argparse.ArgumentParser(
        description="Produce PL flat display EEPROM binary waveform blob")
    parser.add_argument('--debug', action="store_true", default=False,
                        help="enable debug messages")
    parser.add_argument('--output', default='display-blob.bin',
                        help="output file, default is display-blob.bin")
    parser.add_argument('--check', default=None,
                        help="only check agains existing blob file")
    parser.add_argument('display_id', help="display identifier")
    parser.add_argument('display_type', help="display type")
    parser.add_argument('vcom', type=int, help="VCOM voltage in mV")
    parser.add_argument('waveform', help="path to the binary waveform library")
    parser.add_argument('waveform_id', help="waveform identifier")
    parser.add_argument('target', help="target ePDC controller")
    args = parser.parse_args(argv[1:])

    data = {
        'display_id': args.display_id,
        'display_type': args.display_type,
        'vcom': args.vcom,
        'target': args.target,
        'wvf_id': args.waveform_id,
        'wvf': open(args.waveform, 'rb').read(),
        }

    print("Generating display EEPROM blob v{}".format(VERSION))
    blob = build_blob().pack(data)
    print("Blob generated OK, total size: {}".format(len(blob)))

    if args.debug is True:
        print("info CRC: {:04X}, waveform CRC: {:04X}".format(
                data['info_crc'], data['wvf_crc']))

    if args.check is not None:
        ref_blob = open(args.check, 'rb').read()
        ret = build_blob().compare(ref_blob, blob)
        if ret is True:
            print("OK: blobs match")
        else:
            print("ERROR: blobs do not match")
    else:
        print("Saving into {}".format(args.output))
        open(args.output, 'wb').write(blob)
        ret = True

    return ret


if __name__ == '__main__':
    try:
        ret = main(sys.argv)
    except Exception as e:
        print("Exception: {}".format(e))
        ret = False

    sys.exit(0 if ret is True else 1)
