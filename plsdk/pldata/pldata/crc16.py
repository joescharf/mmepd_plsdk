# Python module to compute CRC16

# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import argparse
import data_util

def compute(data, crc=0xFFFF, poly=0x1021):
    """Compute the CRC16 for the given ``data`` string, using intial ``crc``
    value and ``poly`` polynomial.
    """
    for b in data:
        crc ^= ord(b) << 8
        for bit in range(8, 0, -1):
            if crc & 0x8000:
                crc = ((crc << 1) & 0xFFFF) ^ poly
            else:
                crc = (crc << 1) & 0xFFFF
    return crc

def test():
    return compute(data_util.lorem_ipsum) == 0xED9A


def main(argv):
    arg_parser = argparse.ArgumentParser(
        description="Compute CRC16 checksum and print it out.")
    arg_parser.add_argument('--init', action=data_util.store_hex,
                            help="Initial CRC value (16-bit hex)")
    arg_parser.add_argument('--poly', action=data_util.store_hex,
                            help="Polynomial (16-bit hex)")
    arg_parser.add_argument('--check', action=data_util.store_hex,
                            help="Value to check against (16-bit hex)")
    arg_parser.add_argument('file',
                            help="Input file path")
    args = arg_parser.parse_args(argv[1:])

    kw = dict()
    if args.init is not None:
        kw['crc'] = args.init
    if args.poly is not None:
        kw['poly'] = args.poly

    crc = compute(open(args.file, 'rb').read(), **kw)

    print("0x{0:04X}".format(crc))
    if args.check:
        return crc == args.check
    return True

if __name__ == '__main__':
    ret = main(sys.argv)
    sys.exit(0 if ret is True else 1)
