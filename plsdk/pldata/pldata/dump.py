# Plastic Logic binary data utilities

# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys
import argparse
import mkwfblob
import mkvcomblob

formats = {
    'display_wf': mkwfblob.build_blob,
    'vcom_psu': mkvcomblob.build_blob,
}

def dump(blob, data, maxlen=None):
    for entry in blob.entries:
        print("{}: ".format(entry.key), end='')
        entry.dump(data, maxlen)

def main(argv):
    parser = argparse.ArgumentParser(
        description="Dump binary blob file information")
    parser.add_argument('format', choices=formats.keys(),
                        help="blob format name")
    parser.add_argument('file', help="path to the blob file")
    parser.add_argument('--version', help="optional format version number")
    parser.add_argument('--maxlen', default=256, type=int,
                        help="maximum item dump length (0 for no limit)")
    args = parser.parse_args(argv[1:])

    build_blob = formats.get(args.format, None)
    if build_blob is None:
        print("Invalid format name, possible values are: {}".format(
                ', '.join(formats.keys())))
        return False

    blob_args = []
    if args.version is not None:
        blob_args.append(args.version)
    blob = build_blob(*blob_args)
    data = blob.unpack(open(args.file, 'rb').read())
    dump(blob, data, (None if args.maxlen == 0 else args.maxlen))

    return True


if __name__ == '__main__':
    ret = main(sys.argv)
    sys.exit(0 if ret is True else 1)
