# Plastic Logic binary data utilities

# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import struct
import string
import hashlib
import crc16
import data_util

class Blob(object):

    def __init__(self, entries):
        self.entries = entries

    def pack(self, data):
        blob = str()
        for entry in self.entries:
            entry.prepare(data)
            blob += entry.pack(blob, data)
        return blob

    def unpack(self, blob):
        data = dict()
        acc = str()
        rem = blob
        for entry in self.entries:
            entry.prepare(data)
            entry_blob, rem = rem[:entry.size], rem[entry.size:]
            data[entry.key] = entry.unpack(entry_blob, acc)
            acc += entry_blob
        self.rem = rem
        return data

    def compare(self, bdata1, bdata2):
        b1 = self.unpack(bdata1)
        b2 = self.unpack(bdata2)
        for entry in self.entries:
            if b1[entry.key] != b2[entry.key]:
                return False
        return True

    @property
    def size(self):
        return sum(entry.size for entry in self.entries)


class BlobEntry(object):

    def __init__(self, fmt, key, default=None):
        self._fmt = fmt
        self._key = key
        self._default = default

    def __repr__(self):
        return "({} fmt='{}' key='{}' size={})".format(
            self.__class__.__name__, self.fmt, self.key, self.size)

    @property
    def fmt(self):
        return self._fmt

    @property
    def key(self):
        return self._key

    @property
    def size(self):
        return struct.calcsize(self.fmt)

    def pack(self, blob, data):
        return struct.pack(self.fmt, data.get(self.key, self._default))

    def unpack(self, entry_blob, acc):
        return struct.unpack(self.fmt, entry_blob)[0]

    def prepare(self, data):
        pass

    def dump(self, data, maxlen=None):
        value = data[self.key]
        if isinstance(value, str) and not self._is_text(value):
            if len(value) > 16:
                print()
            data_util.dump_hex(value, maxlen)
        elif isinstance(value, int):
            print("{} (0x{:X})".format(value, value))
        elif maxlen is not None and len(value) > maxlen:
            print("{}...".format(value[:maxlen]))
        else:
            print(value)

    def _is_text(self, value):
        value = value.strip('\0')
        if value.isalnum():
            return True
        for c in value:
            if c not in string.printable:
                return False
        return True


class StringEntry(BlobEntry):

    def __init__(self, key, length=63):
        super(StringEntry, self).__init__('{}s'.format(length + 1), key)
        self._length = length

    def pack(self, blob, data):
        return struct.pack(self.fmt, data[self.key][:self._length])

    def unpack(self, b, acc):
        return struct.unpack(self.fmt, b[:self._length + 1])[0].strip('\0')


class VermagicEntry(BlobEntry):

    def __init__(self, fmt, key, vermagic):
        super(VermagicEntry, self).__init__(fmt, key)
        self._vermagic = vermagic

    def pack(self, blob, data):
        return struct.pack(self.fmt, self._vermagic)

    def unpack(self, entry_blob, acc):
        vermagic = super(VermagicEntry, self).unpack(entry_blob, acc)
        if vermagic != self._vermagic:
            raise Exception("Vermagic mismatch: {} instead of {}".format(
                    vermagic, self._vermagic))
        return vermagic


class CRC16Entry(BlobEntry):

    def __init__(self, key, data_size=None):
        super(CRC16Entry, self).__init__('>H', key)
        self._data_size = data_size

    @property
    def data_size(self):
        return self._data_size

    def pack(self, blob, data):
        data[self.key] = crc16.compute(self._truncate(blob))
        return super(CRC16Entry, self).pack(blob, data)

    def unpack(self, entry_blob, acc):
        crc_ref = super(CRC16Entry, self).unpack(entry_blob, acc)
        crc = crc16.compute(self._truncate(acc))
        if crc != crc_ref:
            raise Exception("CRC mismatch: {:X} ({:X})".format(crc, crc_ref))
        return crc_ref

    def _truncate(self, blob):
        return blob if self.data_size is None else blob[-self.data_size:]


def test(sample_data, build_blob, test_md5):
    b = build_blob()
    blob_data = b.pack(sample_data)
    blob_md5 = hashlib.md5()
    blob_md5.update(blob_data)
    if blob_md5.hexdigest() != test_md5:
        print("MD5 sum mismatch: {}".format(blob_md5.hexdigest()))
        return False
    b = build_blob()
    data = b.unpack(blob_data)
    for key in sample_data.keys():
        if sample_data[key] != data[key]:
            print("Unpacked data inconsistency found: {}".format(key))
            return False
    return True
