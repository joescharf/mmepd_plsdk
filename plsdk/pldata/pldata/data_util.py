# Plastic Logic binary data utilities

# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import argparse
import struct

def dump_hex(data, maxlen=None):
    "Debug function to print all the bytes in a binary string in hexadecimal."
    for i, b in enumerate(data):
        rem = i % 16
        if rem == 0:
            print("[{0:04X}]".format(i), end='')
        endl = '\n' if rem == 15 else ''
        if i == maxlen:
            print(" ...", end=endl)
            break
        else:
            print(" {0:02X}".format(ord(b)), end=endl)
    if rem != 15:
        print()

def pack_str(data, name, length=64):
    "Helper function to pack a string with a maximum length."
    return struct.pack('64s', data[name][:63])

def pack_list(fmt, data, keys):
    "Helper function to pack a list of values with a same format."
    return str().join(list(struct.pack(fmt, data[k]) for k in keys))


class store_hex(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, int(values, 16))


lorem_ipsum = """\
Lorem ipsum dolor sit amet, consectetur
adipisicing elit, sed do eiusmod tempor
incididunt ut labore et dolore magna
aliqua. Ut enim ad minim veniam, quis
nostrud exercitation ullamco laboris nisi
ut aliquip ex ea commodo consequat.
Duis aute irure dolor in reprehenderit in
voluptate velit esse cillum dolore eu
fugiat nulla pariatur. Excepteur sint
occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id
est laborum."""
