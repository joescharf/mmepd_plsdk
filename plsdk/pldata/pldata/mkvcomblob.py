# Python module to produce Plastic Logic binary VCOM calibration EEPROM blob

# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function

import sys
import argparse
import struct
import crc16
import data_util
import blob
from blob import BlobEntry, StringEntry, VermagicEntry, CRC16Entry

VERSION = 1

def get_entries(version=VERSION):
    entries = [VermagicEntry('>B', 'version', version)]
    if version == VERSION:
        dac_keys = ('_'.join(['dac', k]) for k in ['x1', 'y1', 'x2', 'y2'])
        entries += list(BlobEntry('>h', key) for key in dac_keys)
        voltage_keys = ['vgpos', 'vgneg', 'ideal_vgswing']
        entries += list(BlobEntry('>i', key) for key in voltage_keys)
        entries.append(StringEntry('board_type', 8))
        entries.append(BlobEntry('>B', 'board_ver_maj'))
        entries.append(BlobEntry('>B', 'board_ver_min'))
        entries.append(BlobEntry('>B', 'vcom_mode'))
        entries.append(BlobEntry('>B', 'hv_pmic'))
        entries.append(BlobEntry('>B', 'vcom_dac'))
        entries.append(BlobEntry('>B', 'vcom_adc'))
        entries.append(BlobEntry('>B', 'io_config'))
        entries.append(BlobEntry('>B', 'i2c_mode'))
        entries.append(BlobEntry('>B', 'temp_sensor'))
        entries.append(BlobEntry('>B', 'frame_buffer'))
        entries.append(BlobEntry('>B', 'epdc_ref'))
        entries.append(BlobEntry('>h', 'adc_scale_1'))
        entries.append(BlobEntry('>h', 'adc_scale_2'))
        entries.append(CRC16Entry('crc'))
    else:
        raise Exception("Unsupported version: {}".format(version))
    return entries

def build_blob(version=VERSION):
    """
    Build a Blob object for the PL VCOM EEPROM binary format.  The associated
    data dictionary when packing or unpacking should contain the following
    entries:
    * dac_x1: first DAC register value
    * dac_y1: VCOM voltage associated with dac_x1
    * dac_x2: second DAC register value
    * dac_y2: VCOM voltage associated with dac_x2
    * vgpos: VGPOS voltage in mV
    * vgneg: VGNEG voltage in mV
    * ideal_vgswing: Ideal gate swing voltage in mV
    * board_type: board type
    * board_ver_maj: board major version number
    * board_ver_min: board minor version number
    * vcom_mode: VCOM calibration mode
    * hv_pmic: HV-PMIC part identifier
    * vcom_dac: VCOM DAC part identifier
    * vcom_adc: VCOM ADC part identifier
    * io_config: configuration of the I/O signals
    * i2c_mode: I2C mode
    * temp_sensor: temperature sensor chip identifier
    * frame_buffer: frame buffer memory identifier
    * epdc_ref: ePDC part reference identifier
    * adc_scale_1: dividend of the ratio between VGSWING voltage and that
      measured by the ADC
    * adc_scale_2: divisor to be used with ADC_SCALE_1

    Optionally:
    * version: binary format version (standard latest number by default)

    The CRC16 value is automatically computed and added to the data dictionary:
    * crc
    """
    return blob.Blob(get_entries(version))

def test():
    sample_data = {
        'dac_x1': 45, 'dac_y1': 234, 'dac_x2': 1234, 'dac_y2': 8654,
        'vgpos': 987, 'vgneg': -12345678, 'ideal_vgswing': 5432, 
        'board_type': "parrot", 'board_ver_maj': 1, 'board_ver_min': 2,
        'vcom_mode': 1, 'hv_pmic': 2, 'vcom_dac': 3, 'vcom_adc': 1,
        'io_config': 1, 'i2c_mode': 2, 'temp_sensor': 0, 'frame_buffer': 0,
        'epdc_ref': 1, 'adc_scale_1': 1, 'adc_scale_2': 1,
        }
    test_md5 = '69e2855c371ce0c44eb9613899ae8583'
    return blob.test(sample_data, build_blob, test_md5)


def main(argv):
    parser = argparse.ArgumentParser(
        description="Produce PL flat VCOM calibration EEPROM binary blob")
    parser.add_argument('--debug', action="store_true", default=False,
                        help="enable debug messages")
    parser.add_argument('--output', default='vcom-blob.bin',
                        help="output file, default is vcom-blob.bin")
    parser.add_argument('--check', default=None,
                        help="only check against existing blob file")
    parser.add_argument('dac_x1', action=data_util.store_hex,
                        help="first DAC register value (hexadecimal)")
    parser.add_argument('dac_y1', type=int,
                        help="VCOM voltage associated with dac_x1 in mV")
    parser.add_argument('dac_x2', action=data_util.store_hex,
                        help="second DAC register value (hexadecimal)")
    parser.add_argument('dac_y2', type=int,
                        help="VCOM voltage associated with dac_x2 in mV")
    parser.add_argument('vgpos', type=int, help="VGPOS voltage in mV")
    parser.add_argument('vgneg', type=int, help="VGNEG voltage in mV")
    parser.add_argument('ideal_vgswing', type=int,
                        help="Ideal gate swing voltage in mV")
    parser.add_argument('board_type', help="board type")
    parser.add_argument('board_ver_maj', type=int,
                        help="board major version number")
    parser.add_argument('board_ver_min', type=int,
                        help="board minor version number")
    parser.add_argument('--vcom_mode', default=0, type=int,
                        help="VCOM calibration mode")
    parser.add_argument('--hv_pmic', default=0, type=int,
                        help="HV-PMIC part identifier")
    parser.add_argument('--vcom_dac', default=0, type=int,
                        help="VCOM DAC part identifier")
    parser.add_argument('--vcom_adc', default=0, type=int,
                        help="VCOM ADC part identifier")
    parser.add_argument('--io_config', default=0, type=int,
                        help="configuration of the I/O signals")
    parser.add_argument('--i2c_mode', default=0, type=int, help="I2C mode")
    parser.add_argument('--temp_sensor', default=0, type=int,
                        help="temperature sensor chip identifier")
    parser.add_argument('--frame_buffer', default=0, type=int,
                        help="frame buffer memory identifier")
    parser.add_argument('--epdc', default=0, type=int,
                       help="ePDC part reference identifier")
    parser.add_argument('--adc_scale_1', default=1, type=int,
                        help="dividend of the ratio between VGSWING voltage and that measured by the ADC")
    parser.add_argument('--adc_scale_2', default=1, type=int,
                        help="divisor to be used with ADC_SCALE_1")
    args = parser.parse_args(argv[1:])

    data = {
        'dac_x1': args.dac_x1,
        'dac_y1': args.dac_y1,
        'dac_x2': args.dac_x2,
        'dac_y2': args.dac_y2,
        'vgpos': args.vgpos,
        'vgneg': args.vgneg,
        'ideal_vgswing': args.ideal_vgswing,
        'board_type': args.board_type,
        'board_ver_maj': args.board_ver_maj,
        'board_ver_min': args.board_ver_min,
        'vcom_mode': args.vcom_mode,
        'hv_pmic': args.hv_pmic,
        'vcom_dac': args.vcom_dac,
        'vcom_adc': args.vcom_adc,
        'io_config': args.io_config,
        'i2c_mode': args.i2c_mode,
        'temp_sensor': args.temp_sensor,
        'frame_buffer': args.frame_buffer,
        'epdc_ref': args.epdc,
        'adc_scale_1': args.adc_scale_1,
        'adc_scale_2': args.adc_scale_2,
        }

    print("Generating VCOM EEPROM blob v{}".format(VERSION))
    blob = build_blob().pack(data)
    print("Blob generated OK, total size: {}".format(len(blob)))

    if args.debug is True:
        data_util.dump_hex(blob)
        print("CRC: {:04X}".format(data['crc']))

    if args.check is not None:
        ref_blob = open(args.check, 'rb').read()
        ret = build_blob().compare(ref_blob, blob)
        if ret:
            print("OK: blobs match")
        else:
            print("ERROR: blobs do not match")
    else:
        print("Saving into {}".format(args.output))
        open(args.output, 'wb').write(blob)
        ret = True

    return ret


if __name__ == '__main__':
    try:
        ret = main(sys.argv)
    except Exception as e:
        print("Exception: {}".format(e))
        ret = False

    sys.exit(0 if ret is True else 1)
