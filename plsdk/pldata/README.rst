Plastic Logic data structures
=============================

crc16
-----

This module can be used to compute a CRC-16 checksum for a given binary string
of data.  It can imported into another script or run on the command line::

  python crc16.py [-h] [--init INIT] [--poly POLY] [--check CHECK] file

Sample Python code:

.. code-block:: python

  import crc16

  data = open('file-name', 'rb').read()
  crc = crc16.compute(data)

lzss
----

This module can be used to encode and decode data using the LZSS algorithm.
This is a lightweight compression algorithm which can run very quickly and with
very small memory footprint.  It is composed of a C library, a C application
and a Python C extension.  For example, to open and decode a file in Python:

.. code-block:: python

  import lzss

  data = open('file_name', 'rb').read()
  compressed = lzss.encode(data)

mkwfblob
--------

This module can be used to produce Plastic Logic display EEPROM binary data
blobs.  This should be used with care and is not necessary with normal display
usage.  It internally uses the ``lzss`` and ``crc16`` modules.  It can be
imported to use the ``mkwfblob.build_blob`` function, or run on the command
line::

  python mkwfblob.py [-h] [--debug] [--output OUTPUT]
                     panel_id panel_type vcom waveform waveform_id target

mkvcomblob
----------

This module can be used to produce Plastic Logic VCOM PSU calibration EEPROM
binary data blobs.  This is typically run during manufacturing, when a VCOM PSU
board board characteristics are being measured.  Similarly to the ``mkwfblob``
module, it can be imported to use the ``mkvcomblob.build_blob`` function or run
on the command line::

  python mkvcomblob.py [-h] [--debug] [--output OUTPUT]
                       dac_x1 dac_y1 dac_x2 dac_y2 vgpos vgneg

data_util
---------

Some utilities used in several other ``pldata`` modules are stored in
``data_util``, namely:

dump_blob
  This function prints the contents of a binary string in hexadecimal format.
  It is mainly useful for debugging.
pack_str
  This is a helper to pack a string using the ``struct`` module.
pack_list
  This is to simplify the other modules' syntax by iterating over a list of
  items to pack with the same format.
store_hex
  This class can be used with ``argparse.ArgumentParser`` to parse a
  hexadecimal integer value.
lorem_ipsum
  This string is an extract of Lorem Ipsum and can be used to run automated
  tests on some arbitrary data.
