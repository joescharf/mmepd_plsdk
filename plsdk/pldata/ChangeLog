Release v007 -> v008 - 2014-03-03

Andrew Cox (1):
      Update mkvcomblob script to match VCOM EEPROM format v1

Guillaume Tucker (3):
      pldata.mkvcomblob: fix typo + test function + board_type
      pldata.mkvcomblob: tidy-up long lines and whitespace
      pldata.mkvcomblob: fix length of board_type entry

Release v006 -> v007 - 2014-01-14

Andrew Cox (1):
      pldata.mkvcomblob: upgrade to EEPROM format v1

Guillaume Tucker (2):
      pldata: add keys to vermagic entries
      pldata.mkvcomblob: tidy-up source code with very long lines

Release v005 -> v006 - 2013-12-13

Guillaume Tucker (7):
      pldata.blob.Blob.compare: remove noisy log messages
      pldata.mkvcomblob: fix bug with variable name
      pldata.mkwfblob: return --check result
      pldata.mk{vcom|wf}blob: fix log messages when running --check
      pldata.blob: add VermagicEntry for version and magic
      pldata.mk{vcom|wf}blob: use VermagicEntry
      pldata.mk{vcom|wf}blob: return error code upon exception

Release v004 -> v005 - 2013-12-02

Guillaume Tucker (24):
      pldata/crc16: fix import pldata -> data_util
      pldata.mkvcomblob: add --check option to check blob file
      pldata.mkvcomblob: some code tidy-up
      pldata.mkwfblob: add --check option to check blob file
      pldata.data_util: rename dump_blob -> dump_hex + fix newline
      pldata: add blob module
      pldata.mkvcomblob: use pldata.blob.Blob
      pldata.mkvwfblob: use pldata.blob.Blob
      pldata: add dump.py to dump the contents of a blob
      pldata.mkwfblob: create separate header Blob
      pldata: tweak dump format
      pldata.blob: strip StringEntry.unpack + tweak dump
      pldata.mkwfblob: rename panel_* -> display_* for consistency
      pldata.mkwfblob: add test function
      pldata.blob: add test function
      pldata.mkwfblob: use pldata.blob.test
      pldata.mkvcomblob: add test function
      pldata.blob: fix string dump
      pldata.dump: add format choices
      pldata.data_util.dump_hex: add maxlen optional argument
      pldata.blob.BlobEntry.dump: add maxlen argument
      pldata.dump: add --maxlen option with default of 256
      pldata.blob.Blob: add `compare' method
      pldata.mkwfblob, .mkvcomblob: use Blob.compare

Release v003 -> v004 - 2013-11-19

Guillaume Tucker (10):
      mkwfblob.py: fix typo with field order (waveform data sizes)
      test: improve log messages + remove extra new lines in data
      add data_util with common utilities for other modules
      crc16, mkwfblob: use data_util module
      add mkvcomblob.py to produce VCOM PSU EEPROM binary data
      add README.rst with brief description of each Python module
      mkwfblob.py: change default output file to display-blob.bin
      move pldata files into pldata Python package
      gitignore: add pydepend_*.mk
      add pldata Python package version + update setver.sh

Release v002 -> v003 - 2013-11-04

Guillaume Tucker (11):
      lzss: add app directory with command-line application
      lzss/python: add thin Python wrapper module
      lzss/python: add test.py
      lzss/python: add .gitignore with build artefacts
      mkwfblob.py: add waveform_id and target fields
      add Android.mk files for Android NDK build
      mkwfblob.py: tidy-up and fix field order for format v1
      .gitignore: add *.pyc
      add setver.sh to automatically update the version strings
      add missing top-level Android.mk
      lzss/app/Android.mk: fix include path to libplutil

