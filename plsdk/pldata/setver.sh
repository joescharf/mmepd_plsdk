#!/bin/sh

set -e

ver="$1"

sed -i s/"\(__version__ = '\)\(.*\)\('\)"/"\1$ver\3"/ pldata/__init__.py
git add pldata/__init__.py

ver="pldata-$ver"

sed -i s/"\(#define LZSS_VERSION \"\)\(.*\)\(\"\)$"/"\1$ver\3"/ lzss/lib/lzss.h
git add lzss/lib/lzss.h

sed -i s/"\(version='\)\(.*\)\(',.*\)$"/"\1$ver\3"/ lzss/python/setup.py
git add lzss/python/setup.py

exit 0
