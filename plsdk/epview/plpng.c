/*
  E-Paper document viewer - epview

  Copyright (C) 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libpldraw.h>
#include <png.h>
#include <errno.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "epdoc.h"

#define LOG_TAG "plpng"
#include <plsdk/log.h>

/* Set to 0 do dump PNG format info */
#define DUMP_FORMAT 0

/* Set to 0 to dump the palette */
#define DUMP_PALETTE 0

/* Note: Unsupported pixel formats are compiled out. */

enum plpng_format {
	PLPNG_Y1,
	PLPNG_Y2,
	PLPNG_Y4,
	PLPNG_Y8,
#if 0
	PLPNG_Y16,
#endif
	PLPNG_YA88,
#if 0
	PLPNG_YA1616,
#endif
	PLPNG_RGB888,
	PLPNG_RGBA8888,
#if 0
	PLPNG_RGB161616,
	PLPNG_RGB16161616,
#endif
	PLPNG_PALETTE_2,
	PLPNG_PALETTE_4,
	PLPNG_PALETTE_16,
	PLPNG_PALETTE_256,
};

#define PLPNG_UNSUPPORTED ((unsigned int)(-1))

struct plpng {
	FILE *f;
	png_structp png;
	png_infop info;
	png_bytep row_data;
	uint32_t row_bytes;
	enum plpng_format format;
};

typedef void (*draw_line_t)(struct plpng *p, struct pldraw *draw,
			    struct plep_point *pt, int offset, int width);

static void draw_line_grey_1(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_grey_2(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_grey_4(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_grey_8(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_rgb_888(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_palette_2(
	struct plpng *, struct pldraw *, struct plep_point *, int, int);
static void draw_line_palette_4(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_palette_16(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);
static void draw_line_palette_256(
	struct plpng *p, struct pldraw *draw, struct plep_point *pt,
	int offset, int width);

static const draw_line_t draw_line_table[] = {
	[PLPNG_Y1] = draw_line_grey_1,
	[PLPNG_Y2] = draw_line_grey_2,
	[PLPNG_Y4] = draw_line_grey_4,
	[PLPNG_Y8] = draw_line_grey_8,
	[PLPNG_YA88] = draw_line_grey_8,
	[PLPNG_RGB888] = draw_line_rgb_888,
	[PLPNG_RGBA8888] = draw_line_rgb_888,
	[PLPNG_PALETTE_2] = draw_line_palette_2,
	[PLPNG_PALETTE_4] = draw_line_palette_4,
	[PLPNG_PALETTE_16] = draw_line_palette_16,
	[PLPNG_PALETTE_256] = draw_line_palette_256,
};

/* ----------------------------------------------------------------------------
 * epdoc interface
 */

static int epdoc_plpng_fbdraw(struct epdoc *doc, struct pldraw *pldraw)
{
	draw_line_t draw_line;
	struct plpng *p;
	struct plep_point out;
	struct plep_point end;
	struct plep_point in_offset;
	struct plep_point out_offset;
	int out_width;
	int out_height;

	assert(doc != NULL);
	assert(pldraw != NULL);

	p = doc->priv;

	assert(p != NULL);

	if (doc->opt.do_auto_rotate)
		epdoc_auto_rotate(doc, pldraw);
	else if (doc->opt.rotation_angle >= 0)
		pldraw_set_rotation(pldraw, doc->opt.rotation_angle);

	epdoc_align_offset(doc, pldraw);

	if (doc->opt.do_fill_background)
		pldraw_fill_screen(pldraw, doc->opt.background);

	if (doc->opt.offset.x < 0) {
		in_offset.x = MAX(-doc->opt.offset.x, doc->opt.crop.a.x);
		out_offset.x = MAX(0, (doc->opt.offset.x + doc->opt.crop.a.x));
	} else {
		in_offset.x = MAX(0, doc->opt.crop.a.x);
		out_offset.x = doc->opt.offset.x + doc->opt.crop.a.x;
	}

	if (doc->opt.offset.y < 0) {
		in_offset.y = MAX(-doc->opt.offset.y, doc->opt.crop.a.y);
		out_offset.y = MAX(0, (doc->opt.offset.y + doc->opt.crop.a.y));
	} else {
		in_offset.y = MAX(0, doc->opt.crop.a.y);
		out_offset.y = doc->opt.offset.y + doc->opt.crop.a.y;
	}

	out_width = MIN((doc->opt.crop.b.x - in_offset.x),
			(pldraw_get_xres(pldraw) - out_offset.x));
	out_height = MIN((doc->opt.crop.b.y - in_offset.y),
			 (pldraw_get_yres(pldraw) - out_offset.y));

	if ((out_width <= 0) || (out_height <= 0))
		return 0;

	end.x = out_offset.x + out_width;
	end.y = out_offset.y + out_height;
	draw_line = draw_line_table[p->format];

	for (out.y = 0; out.y < in_offset.y; ++out.y)
		png_read_rows(p->png, &p->row_data, png_bytepp_NULL, 1);

	for (out.y = out_offset.y; out.y < end.y; ++out.y) {
		png_read_rows(p->png, &p->row_data, png_bytepp_NULL, 1);
		out.x = out_offset.x;
		draw_line(p, pldraw, &out, in_offset.x, out_width);
	}

	return 0;
}

static int epdoc_plpng_update(struct epdoc *doc, struct pldraw *pldraw)
{
	struct plep *plep;
	struct plep_rect area;

	assert(doc != NULL);
	assert(pldraw != NULL);

	plep = pldraw_get_plep(pldraw);

	if (doc->opt.do_fill_background)
		return plep_update_screen(plep, doc->opt.wfid);

	area.a.x = MAX(0, (doc->opt.offset.x + doc->opt.crop.a.x));
	area.a.y = MAX(0, (doc->opt.offset.y + doc->opt.crop.a.y));
	area.b.x = MIN(pldraw_get_xres(pldraw),
		       (doc->opt.offset.x + doc->opt.crop.b.x));
	area.b.y = MIN(pldraw_get_yres(pldraw),
		       (doc->opt.offset.y + doc->opt.crop.b.y));

	return plep_update(plep, &area, doc->opt.wfid);
}

static void epdoc_plpng_close(struct epdoc *doc)
{
	struct plpng *p;

	assert(doc != NULL);

	p = doc->priv;

	assert(p != NULL);
	assert(p->f != NULL);
	assert(p->row_data != NULL);

	png_free(p->png, p->row_data);
	png_destroy_read_struct(&p->png, &p->info, NULL);

	if (p->f != stdin)
		fclose(p->f);
}

int epdoc_open_png(struct epdoc *doc, const char *file_name)
{
	struct plpng *p;

	assert(doc != NULL);

	if (file_name != NULL) {
		if (epdoc_extcmp(file_name, ".png"))
			return -1;
	} else {
		if (strcmp(doc->opt.doc_type, "png"))
			return -1;
	}

	p = malloc(sizeof(struct plpng));

	if (p == NULL) {
		LOG("Failed to allocate plpng");
		return -1;
	}

	if (file_name != NULL)
		p->f = fopen(file_name, "rb");
	else
		p->f = stdin;

	if (p->f == NULL) {
		LOG("failed to open file: %s",
		    (file_name == NULL) ? "(stdin)" : file_name);
		goto exit_err;
	}

	p->png = png_create_read_struct(PNG_LIBPNG_VER_STRING,
					NULL, NULL, NULL);

	if (p->png == NULL) {
		LOG("failed to initialise PNG");
		goto exit_close_file;
	}

	p->info = png_create_info_struct(p->png);

	if (p->info == NULL) {
		LOG("failed to initialise PNG info");
		goto exit_free_png;
	}

	if (setjmp(png_jmpbuf(p->png))) {
		LOG("Error processing PNG file");
		goto exit_free_png;
	}

#ifdef PNG_STDIO_SUPPORTED
	png_init_io(p->png, p->f);
#else
#error "no stdio support, need to use png_set_read_fn()"
#endif
	png_read_info(p->png, p->info);
	p->row_bytes = png_get_rowbytes(p->png, p->info);

	p->row_data = png_malloc(p->png, p->row_bytes);

	if (p->row_data == NULL) {
		LOG("failed to allocate row buffer");
		goto exit_free_png;
	}

	p->format = PLPNG_UNSUPPORTED;

	if (p->info->interlace_type != PNG_INTERLACE_NONE) {
		LOG("Interlaced format is not supported");
		goto exit_free_png;
	}

	switch (p->info->bit_depth) {
	case 8:
		if (p->info->num_palette == 0) {
			switch (p->info->channels) {
			case 1:
				p->format = PLPNG_Y8;
				break;
			case 2:
				LOG("Warning: ignoring alpha channel "
				    "in greyscale image");
				p->format = PLPNG_YA88;
				break;
			case 3:
				p->format = PLPNG_RGB888;
				break;
			case 4:
				LOG("Warning: ignoring alpha channel "
				    "in colour image");
				p->format = PLPNG_RGBA8888;
				break;
			}
		} else if (p->info->num_palette == 256) {
			p->format = PLPNG_PALETTE_256;
		}
		break;

	case 4:
		if ((p->info->num_palette == 0) && (p->info->channels == 1))
			p->format = PLPNG_Y4;
		else if (p->info->num_palette <= 16)
			p->format = PLPNG_PALETTE_16;
		break;

	case 2:
		if ((p->info->num_palette == 0) && (p->info->channels == 1))
			p->format = PLPNG_Y2;
		else if (p->info->num_palette == 4)
			p->format = PLPNG_PALETTE_4;
		break;

	case 1:
		if ((p->info->num_palette == 0) && (p->info->channels == 1))
			p->format = PLPNG_Y1;
		else if (p->info->num_palette == 2)
			p->format = PLPNG_PALETTE_2;
		break;
	}

	if (p->format == PLPNG_UNSUPPORTED) {
		LOG("Unsupported pixel format: "
		    "channels=%d bits=%d palette=%d",
		    p->info->channels,
		    p->info->bit_depth,
		    p->info->num_palette);
		goto exit_free_png;
	}

	doc->fbdraw = epdoc_plpng_fbdraw;
	doc->update = epdoc_plpng_update;
	doc->close = epdoc_plpng_close;
	doc->width = p->info->width;
	doc->height = p->info->height;
	doc->priv = p;

#if DUMP_FORMAT
	LOG("image size: %lux%lu", p->info->width, p->info->height);
	LOG("bits per channel: %d", p->info->bit_depth);
	LOG("pixel_depth: %d", p->info->pixel_depth);
	LOG("row bytes: %lu", (long unsigned)p->row_bytes);
	LOG("colour type: %s %s",
	    (p->info->color_type & PNG_COLOR_MASK_COLOR)
	    ? "colour" : "grey scale",
	    (p->info->color_type & PNG_COLOR_MASK_PALETTE)
	    ? "palette" : "true colour");
	LOG("interlace: %sabled",
	    (p->info->interlace_type == PNG_INTERLACE_ADAM7) ? "en" : "dis");
	LOG("channels: %d", p->info->channels);

	if (p->info->valid & PNG_INFO_pHYs)
		LOG("x, y per unit: %lu, %lu",
		    p->info->x_pixels_per_unit, p->info->y_pixels_per_unit);

	if (p->info->num_palette)
		LOG("palette length: %d", p->info->num_palette);
#endif /* DUMP_FORMAT */

#if DUMP_PALETTE
	{
		int i;

		for (i = 0; i < p->info->num_palette; ++i)
			LOG("palette[%d] = %02X.%02X.%02X",
			    i,
			    p->info->palette[i].red,
			    p->info->palette[i].green,
			    p->info->palette[i].blue);
	}
#endif /* DUMP_PALETTE */

	return 0;

exit_free_png:
	png_destroy_read_struct(&p->png, &p->info, NULL);
exit_close_file:
	if (p->f != stdin)
		fclose(p->f);
exit_err:
	free(p);

	return -1;
}

/* ----------------------------------------------------------------------------
 * private functions
 */

static void draw_line_grey_1(struct plpng *p, struct pldraw *draw,
			     struct plep_point *pt, int offset, int width)
{
	const pldraw_color_t black = pldraw_get_grey(draw, 0x00);
	const pldraw_color_t white = pldraw_get_grey(draw, 0xFF);
	const unsigned char *it = p->row_data + (offset / 8);
	int bit = 0x80 >> (offset % 8);

	while (width--) {
		pldraw_set_pixel(draw, (*it & bit) ? white : black, pt);
		++pt->x;

		if (bit == 1) {
			++it;
			bit = 0x80;
		} else {
			bit >>= 1;
		}
	}
}

static void draw_line_grey_2(struct plpng *p, struct pldraw *draw,
			     struct plep_point *pt, int offset, int width)
{
	const unsigned char *it = p->row_data + (offset / 4);
	int shift = (3 - (offset % 4)) * 2;

	while (width--) {
		const int grey = pldraw_grey_4[(*it >> shift) & 0x03];

		pldraw_set_pixel(draw, pldraw_get_grey(draw, grey), pt);
		++pt->x;

		if (shift == 0) {
			++it;
			shift = 6;
		} else {
			shift -= 2;
		}
	}
}

static void draw_line_grey_4(struct plpng *p, struct pldraw *draw,
			     struct plep_point *pt, int offset, int width)
{
	const unsigned char *it = p->row_data + (offset / 2);
	int odd = offset % 2;

	while (width--) {
		pldraw_color_t color;
		int grey;

		if (odd)
			grey = *it++ & 0x0F;
		else
			grey = (*it & 0xF0) >> 4;

		color = pldraw_get_grey(draw, pldraw_grey_16[grey]);
		pldraw_set_pixel(draw, color, pt);
		++pt->x;
		odd = !odd;
	}
}

static void draw_line_grey_8(struct plpng *p, struct pldraw *draw,
			     struct plep_point *pt, int offset, int width)
{
	const int channels = p->info->channels;
	const unsigned char *it = p->row_data + (offset * channels);

	while (width--) {
		pldraw_set_pixel(draw, pldraw_get_grey(draw, *it), pt);
		it += channels;
		++pt->x;
	}
}

static void draw_line_rgb_888(struct plpng *p, struct pldraw *draw,
			      struct plep_point *pt, int offset, int width)
{
	const int channels = p->info->channels;
	const unsigned char *it = p->row_data + (offset * channels);

	while (width--) {
		pldraw_color_t color;

		color = pldraw_get_color(draw, it[0], it[1], it[2]);
		pldraw_set_pixel(draw, color, pt);
		it += channels;
		++pt->x;
	}
}

static void draw_line_palette_2(struct plpng *p, struct pldraw *draw,
				 struct plep_point *pt, int offset, int width)
{
	const unsigned char *it = p->row_data + (offset / 8);
	const struct png_color_struct * const png_palette = p->info->palette;
	unsigned char bit = 0x80 >> (offset % 8);

	while (width--) {
		const struct png_color_struct *pal;
		pldraw_color_t col;

		pal = &png_palette[(*it & bit) ? 1 : 0];
		col = pldraw_get_color(draw, pal->red, pal->green, pal->blue);
		pldraw_set_pixel(draw, col, pt);
		++pt->x;

		if (bit == 1) {
			++it;
			bit = 0x80;
		} else {
			bit >>= 1;
		}
	}
}

static void draw_line_palette_4(struct plpng *p, struct pldraw *draw,
				struct plep_point *pt, int offset, int width)
{
	const unsigned char *it = p->row_data + (offset / 4);
	const struct png_color_struct * const png_palette = p->info->palette;
	int shift = (3 - (offset % 4)) * 2;

	while (width--) {
		const struct png_color_struct *pal;
		pldraw_color_t col;

		pal = &png_palette[(*it >> shift) & 0x03];
		col = pldraw_get_color(draw, pal->red, pal->green, pal->blue);
		pldraw_set_pixel(draw, col, pt);
		++pt->x;

		if (shift == 0) {
			++it;
			shift = 6;
		} else {
			shift -= 2;
		}
	}
}

static void draw_line_palette_16(struct plpng *p, struct pldraw *draw,
				 struct plep_point *pt, int offset, int width)
{
	const unsigned char *it = p->row_data + (offset / 2);
	const struct png_color_struct * const png_palette = p->info->palette;
	int odd = offset % 2;

	while (width--) {
		const struct png_color_struct *pal;
		pldraw_color_t col;
		int index;

		if (odd)
			index = *it++ & 0xF;
		else
			index = (*it & 0xF0) >> 4;

		pal = &png_palette[index];
		col = pldraw_get_color(draw, pal->red, pal->green, pal->blue);
		pldraw_set_pixel(draw, col, pt);
		++pt->x;
		odd = !odd;
	}
}

static void draw_line_palette_256(struct plpng *p, struct pldraw *draw,
				  struct plep_point *pt, int offset, int width)
{
	const unsigned char *it = p->row_data + offset;
	const struct png_color_struct * const png_palette = p->info->palette;

	while (width--) {
		const struct png_color_struct *pal;
		pldraw_color_t col;

		pal = &png_palette[*it++];
		col = pldraw_get_color(draw, pal->red, pal->green, pal->blue);
		pldraw_set_pixel(draw, col, pt);
		++pt->x;
	}
}
