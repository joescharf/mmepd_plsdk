/*
  E-Paper document viewer - epview

  Copyright (C) 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_EPVIEW_EPDOC_H
#define INCLUDE_EPVIEW_EPDOC_H 1

#include <libpldraw.h>

#define MIN(a,b) (((long)(a)<(long)(b))?(a):(b))
#define MAX(a,b) (((long)(a)>(long)(b))?(a):(b))

#define EPDOC_ALIGN_H_BIT 0x100
#define EPDOC_ALIGN_V_BIT 0x200

enum epdoc_align_h {
	EPDOC_ALIGN_H_NONE = EPDOC_ALIGN_H_BIT,
	EPDOC_ALIGN_H_LEFT,
	EPDOC_ALIGN_H_CENTER,
	EPDOC_ALIGN_H_RIGHT,
};

enum epdoc_align_v {
	EPDOC_ALIGN_V_NONE = EPDOC_ALIGN_V_BIT,
	EPDOC_ALIGN_V_TOP,
	EPDOC_ALIGN_V_MIDDLE,
	EPDOC_ALIGN_V_BOTTOM,
};

struct epdoc_opt {
	int wfid;                   /* waveform id to use */
	int do_auto_rotate;         /* 1 to enable auto-rotate, 0 otherwise */
	int rotation_angle;         /* rotation angle, -1 if not specified */
	int do_fill_background;     /* fill the background around the doc */
	pldraw_color_t background;  /* background colour to use */
	struct plep_point offset;   /* starting point offset coordinates */
	enum epdoc_align_h align_h; /* horizontal alignment option */
	enum epdoc_align_v align_v; /* vertical alignment option */
	struct plep_rect crop;      /* crop area relative to the document */
	const char *doc_type;       /* document type identifier */
};

struct epdoc {
	struct epdoc_opt opt; /* set by caller */
	int (*fbdraw)(struct epdoc *doc, struct pldraw *pldraw);
	int (*update)(struct epdoc *doc, struct pldraw *pldraw);
	void (*close)(struct epdoc *doc);
	size_t width;
	size_t height;
	void *priv; /* private data for each document format */
};

/* document format handler entry point type */
typedef int (epdoc_open_t)(struct epdoc *doc, const char *file_name);

/* document format handlers */
extern int epdoc_open_png(struct epdoc *doc, const char *file_name);

/* common utilities */
extern void epdoc_auto_rotate(struct epdoc *doc, struct pldraw *pldraw);
extern void epdoc_align_offset(struct epdoc *doc, struct pldraw *pldraw);
extern void epdoc_fixup_crop(struct epdoc *doc);
extern int epdoc_extcmp(const char *file_name, const char *ext);

#endif /* INCLUDE_EPVIEW_EPDOC_H */
