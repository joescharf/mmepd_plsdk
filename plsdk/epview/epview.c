/*
  E-Paper document viewer - epview

  Copyright (C) 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libpldraw.h>
#include <getopt.h>
#include <errno.h>
#include <limits.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "epdoc.h"

#define LOG_TAG "epview"
#include <plsdk/log.h>

static const char APP_NAME[] = "epview";
static const char VERSION[] = "1.10";
static const char DESCRIPTION[] = "Document viewer on e-paper display";
static const char LICENSE[] =
	"This program is distributed in the hope that it will be useful,\n"
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	"GNU General Public License for more details.\n";
static const char COPYRIGHT[] =
	"Copyright (C) 2012, 2013 Plastic Logic Limited";
static const char COMMA[] = ",";

static volatile int g_run = 1;

static void print_usage(void);
static int enumerate_waveforms(struct plep *plep);
static int show_contents(struct pldraw *pldraw, char * const *file_names,
			 size_t n_files, struct epdoc_opt *opt,
			 unsigned long pause_ms, int do_infinite_loop,
			 const char *background);
static int enum_docs(struct pldraw *pldraw, char * const *file_names,
		     size_t n_files, const struct epdoc_opt *opt,
		     unsigned long pause_ms, int infinite_loop);
static int show_doc(struct pldraw *pldraw, const char *file_name,
		    const struct epdoc_opt *opt);
static int set_background_opt(struct pldraw *pldraw, struct epdoc_opt *opt,
			      const char *background);
static int parse_offset(struct plep_point *offset, char *opt);
static int parse_alignment(enum epdoc_align_h *align_h,
			   enum epdoc_align_v *align_v,
			   char *opt);
static int parse_crop(struct plep_rect *crop, char *opt);
static void log_waveform_path(const struct plep_waveform_info *wfinfo);
static int str2ul(const char *str, unsigned long int *value);
static int str2align(const char *str, enum epdoc_align_h *align_h,
		     enum epdoc_align_v *align_v);
static void sigint_abort(int signum);

int main(int argc, char **argv)
{
	static const char OPTIONS[] = "hvlpsjr:g:f::Ww:m:d:e:b:t:o:a:c:O:";
	__sighandler_t sigint_original;
	char * const *file_names = NULL;
	size_t n_files = 0;
	const char *waveform_id_str = NULL;
	int waveform_id;
	int do_enumerate_waveforms = 0;
	int do_log_info = 0;
	int do_wait_power_off = 0;
	int do_synchro = 0;
	int do_infinite_loop = 0;
	int cfa = -1;
	int do_auto_rotate = 0;
	int rotation_angle = -1;
	unsigned long pause_ms = 2000;
	const char *mode = NULL;
	const char *fbdev = NULL;
	const char *epdev = NULL;
	const char *background = NULL;
	struct plep_point offset = { 0, 0 };
	enum epdoc_align_h align_h = EPDOC_ALIGN_H_NONE;
	enum epdoc_align_v align_v = EPDOC_ALIGN_V_NONE;
	struct plep_rect crop = { { 0, 0 }, { INT_MAX, INT_MAX } };
	const char *doc_type = NULL;
	const char *conf_file = NULL;
	struct plep *plep;
	struct pldraw *pldraw;
	int c;
	int ret;

	while ((c = getopt(argc, argv, OPTIONS)) != -1) {
		switch (c) {
		case 'h':
			print_usage();
			exit(EXIT_SUCCESS);
			break;

		case 'v':
			printf("%s v%s - %s\n%s\n%s\n", APP_NAME, VERSION,
			       DESCRIPTION, COPYRIGHT, LICENSE);
			exit(EXIT_SUCCESS);
			break;

		case 'l':
			do_log_info = 1;
			break;

		case 'p':
			do_wait_power_off = 1;
			break;

		case 's':
			do_synchro = 1;
			break;

		case 'j':
			do_infinite_loop = 1;
			break;

		case 'r':
			if (!strcmp(optarg, "auto")) {
				do_auto_rotate = 1;
			} else {
				unsigned long raw_angle;

				if (str2ul(optarg, &raw_angle) < 0) {
					LOG("failed to parse rotation angle");
					print_usage();
					exit(EXIT_FAILURE);
				}

				if ((raw_angle > 270) || (raw_angle % 90)) {
					LOG("invalid rotation angle");
					print_usage();
					exit(EXIT_FAILURE);
				}

				rotation_angle = raw_angle;
			}
			break;

		case 'g':
			if (str2ul(optarg, &pause_ms) < 0) {
				LOG("failed to parse pause duration");
				print_usage();
				exit(EXIT_FAILURE);
			}
			break;

		case 'f':
			if (optarg == NULL) {
				cfa = PLDRAW_CFA_GR_BW;
			} else {
				cfa = pldraw_get_cfa_id(optarg);

				if (cfa < 0) {
					LOG("Invalid CFA identifier: %s",
					    optarg);
					print_usage();
					exit(EXIT_FAILURE);
				}
			}
			break;

		case 'W':
			do_enumerate_waveforms = 1;
			break;

		case 'w':
			waveform_id_str = optarg;
			break;

		case 'm':
			mode = optarg;
			break;

		case 'd':
			fbdev = optarg;
			break;

		case 'e':
			epdev = optarg;
			break;

		case 'b':
			background = optarg;
			break;

		case 't':
			doc_type = optarg;
			break;

		case 'o':
			if (parse_offset(&offset, optarg) < 0) {
				print_usage();
				exit(EXIT_FAILURE);
			}
			break;

		case 'a':
			if (parse_alignment(&align_h, &align_v, optarg) < 0) {
				print_usage();
				exit(EXIT_FAILURE);
			}
			break;

		case 'c':
			if (parse_crop(&crop, optarg) < 0) {
				print_usage();
				exit(EXIT_FAILURE);
			}
			break;

		case 'O':
			conf_file = optarg;
			if (access(conf_file, F_OK)) {
				LOG_ERRNO("Configuration file");
				exit(EXIT_FAILURE);
			}
			break;

		case '?':
		default:
			LOG("Invalid arguments");
			print_usage();
			exit(EXIT_FAILURE);
			break;
		}
	}

	if (optind < argc) {
		file_names = &argv[optind];
		n_files = argc - optind;
	}

	LOG("%s v%s", APP_NAME, VERSION);

	plep = plep_init(epdev, mode, conf_file);

	if (plep == NULL) {
		LOG("failed to initialise ePDC");
		goto error_plep;
	}

	pldraw = pldraw_init(fbdev, conf_file);

	if (pldraw == NULL) {
		LOG("failed to initialise pldraw");
		goto error_pldraw;
	}

	pldraw_set_plep(pldraw, plep);

	waveform_id = plep_get_wfid(plep, waveform_id_str);

	if (waveform_id < 0) {
		LOG("Invalid waveform path: %s", waveform_id_str);
		goto error_pldraw;
	}

	if (cfa >= 0)
		pldraw_set_cfa(pldraw, cfa);
	else
		cfa = pldraw_get_cfa(pldraw);

	if (cfa != PLDRAW_CFA_NONE)
		LOG("CFA: %s", pldraw_cfa_name[cfa]);

	if (rotation_angle < 0)
		rotation_angle = pldraw_get_rotation(pldraw);

	if (rotation_angle)
		LOG("rotation: %d", rotation_angle);

	if (do_log_info)
		pldraw_log_info(pldraw);

	sigint_original = signal(SIGINT, sigint_abort);

	if (do_enumerate_waveforms) {
		ret = enumerate_waveforms(plep);
	} else {
		struct epdoc_opt opt;

		plep_set_opt(plep, PLEP_SYNC_UPDATE, do_synchro);

		if (do_wait_power_off)
			plep_set_opt(plep, PLEP_WAIT_POWER_OFF, 1);

		opt.do_auto_rotate = do_auto_rotate;
		opt.rotation_angle = rotation_angle;
		opt.wfid = waveform_id;
		opt.offset.x = offset.x;
		opt.offset.y = offset.y;
		opt.align_h = align_h;
		opt.align_v = align_v;
		memcpy(&opt.crop, &crop, sizeof opt.crop);
		opt.doc_type = doc_type;

		ret = show_contents(pldraw, file_names, n_files, &opt,
				    pause_ms, do_infinite_loop, background);
	}

	signal(SIGINT, sigint_original);
	pldraw_free(pldraw);
	plep_free(plep);

	exit((ret < 0) ? EXIT_FAILURE : EXIT_SUCCESS);

error_pldraw:
	plep_free(plep);
error_plep:
	exit(EXIT_FAILURE);
}

static void print_usage(void)
{
	printf(
"Usage: %s <OPTIONS> [FILE...]\n"
"\n"
"  Show the documents contained in the FILE... files.  If no FILE argument\n"
"  is provided, data is read from stdin and the -t option must be set.\n"
"  It is not possible to mix stdin with file names, and the -j (loop) and -g\n"
"  (pause) options are not supported when using stdin.\n"
"\n"
"OPTIONS:\n"
"  -h\n"
"    Show this help message and exit.\n"
"\n"
"  -v\n"
"    Show the version, copyright and license information, and exit.\n"
"\n"
"  -l\n"
"    Log the frame buffer and ePDC information.\n"
"\n"
"  -p\n"
"    Wait for the display power to be turned off after the display update.\n"
"\n"
"  -s\n"
"    Enable synchronous mode to wait for the display update to have started\n"
"    before exiting.\n"
"\n"
"  -j\n"
"    Keep showing the documents in an infinite loop (ignored when reading\n"
"    from stdin).\n"
"\n"
"  -r ANGLE\n"
"    Rotate the display by ANGLE degrees.  Valid values are 0, 90, 180 and\n"
"    270.  Alternatively, the value \"auto\" can be used to let epview\n"
"    automatically choose 0 or 90 degrees for a best match between the\n"
"    current document and frame buffer dimensions.\n"
"\n"
"  -g PAUSE\n"
"    Wait for PAUSE milliseconds before showing the next document.  The\n"
"    default is 2000 for 2 seconds (ignored when reading from stdin).\n"
"\n"
"  -W\n"
"    Enumerate the available waveforms, do not show any document and exit.\n"
"\n"
"  -w <WAVEFORM_ID>\n"
"    Specify the waveform id to use with WAVEFORM_ID.\n"
"\n"
"  -m EPDC_MODE\n"
"    Specify which mode to use to control the ePDC.  By default, this is \n"
"    discovered automatically by probing the system.  This is then used to \n"
"    load the corresponding mod_plepaper_EPDC_MODE.so module.  Usual values \n"
"    for EPDC_MODE are: \n"
"      noep      Do not use any ePDC\n"
"      imx       Freescale i.MX508\n"
"      plepdc    Plastic Logic ePDC interface\n"
"\n"
"  -d DEVICE\n"
"    Specify the frame buffer device to use.  If not specified, the usual\n"
"    suspects will be looked for (/dev/fb0 and variants).\n"
"\n"
"  -e DEVICE\n"
"    Specify the ePDC device to use.  This relies on the implementation of\n"
"    the ePDC mode being used.\n"
"\n"
"  -f CFA\n"
"    Enable a Colour Filter Array for colour EPD.  The default one is GR_BW.\n"
"\n"
"  -b BACKGROUND\n"
"    Fill the background of the screen with the given BACKGROUND colour.\n"
"    Possible values are black, white, green, red, blue, cyan, magenta,\n"
"    and yellow.  If not specified, the background is not filled at all.\n"
"\n"
"  -t DOC_TYPE\n"
"    Specify the document type DOC_TYPE.  This is required when reading data\n"
"    from stdin or when using raw formats.  The following document types are\n"
"    currently supported:\n"
"      png: PNG images\n"
"\n"
"  -o OFFSET_X,OFFSET_Y\n"
"    Start showing the document with the top-left corner located in\n"
"    coordinates (OFFSET_X, OFFSET_Y).  By default, the document is shown\n"
"    in the top-left corner of the display (0, 0) - unless a rotation angle\n"
"    is provided.\n"
"\n"
"  -a ALIGN,ALIGN\n"
"    Align the image using the vertical and horizontal ALIGN values.  They\n"
"    must be either one or two values from these two sets:\n"
"      left, center, right\n"
"      top, middle, bottom\n"
"    A few examples: left,middle  /  center,middle  /  bottom  /  right\n"
"    Any offset specified by -o will be ignored if -a conflicts with it.\n"
"\n"
"  -c CROP_LEFT,CROP_TOP,CROP_RIGHT,CROP_BOTTOM\n"
"    Crop the input image to only show a part of it.  The four coordinates\n"
"    are relative to the image, not the screen.  The offset of the pixels\n"
"    being shown doesn't change when a crop area used.\n"
"\n"
"  -O CONFIG_FILE\n"
"    Use configuration file with the specified CONFIG_FILE path.  If not\n"
"    specified, the default path (typically ~/.plsdk.ini) is used.\n"
"\n", APP_NAME);
}

static int enumerate_waveforms(struct plep *plep)
{
	struct plep_waveform_info wfinfo;
	unsigned n;

	LOG("Available waveforms are:");

	for (n = 0; !plep_get_waveform_info(plep, n, &wfinfo); ++n) {
		LOG_N("  ");
		log_waveform_path(&wfinfo);
	}

	return 0;
}

static int show_contents(struct pldraw *pldraw, char * const *file_names,
			 size_t n_files, struct epdoc_opt *opt,
			 unsigned long pause_ms, int do_infinite_loop,
			 const char *background)
{
	struct plep_waveform_info wfinfo;

	if (plep_get_waveform_info(pldraw_get_plep(pldraw),
				   opt->wfid, &wfinfo)) {
		LOG("failed to get waveform info");
		return -1;
	}

	LOG_N("waveform: ");
	log_waveform_path(&wfinfo);

	if (set_background_opt(pldraw, opt, background) < 0) {
		LOG("Failed to set background option");
		return -1;
	}

	if (!n_files) {
		if (opt->doc_type == NULL) {
			LOG("document type required when reading from stdin");
			return -1;
		}

		LOG("reading from stdin as %s", opt->doc_type);

		return show_doc(pldraw, NULL, opt);
	}

	return enum_docs(pldraw, file_names, n_files, opt, pause_ms,
			 do_infinite_loop);
}

static int enum_docs(struct pldraw *pldraw, char * const *file_names,
		     size_t n_files, const struct epdoc_opt *opt,
		     unsigned long pause_ms, int infinite_loop)
{
	size_t n;
	int first = 1;

	do {
		for (n = 0; n < n_files; ++n) {
			const char *file_name = file_names[n];

			if (!g_run)
				return 0;

			if (first)
				first = 0;
			else
				usleep(pause_ms * 1000);

			if (!g_run)
				return 0;

			LOG("showing file: %s", file_name);

			if (show_doc(pldraw, file_name, opt) < 0)
				return -1;
		}
	} while (infinite_loop);

	return 0;
}

static int show_doc(struct pldraw *pldraw, const char *file_name,
		    const struct epdoc_opt *opt)
{
	static epdoc_open_t *epdoc_formats[] = {
		epdoc_open_png,
		NULL
	};
	epdoc_open_t **epd_open;
	struct epdoc doc;
	int stat = -1;

	for (epd_open = epdoc_formats; *epd_open != NULL; ++epd_open) {
		memcpy(&doc.opt, opt, sizeof doc.opt);

		if (!(*epd_open)(&doc, file_name))
			break;
	}

	if (*epd_open == NULL) {
		LOG("Failed to open document: %s",
		    (file_name == NULL) ? "(stdin)" : file_name);
		goto exit_now;
	}

	epdoc_fixup_crop(&doc);

	if (doc.fbdraw(&doc, pldraw) < 0) {
		LOG("Failed to show document");
		goto exit_close_doc;
	}

	if (doc.update(&doc, pldraw) < 0) {
		LOG("Failed to update e-paper");
		goto exit_close_doc;
	}

	stat = 0;

exit_close_doc:
	doc.close(&doc);
exit_now:

	return stat;
}

static int set_background_opt(struct pldraw *pldraw, struct epdoc_opt *opt,
			      const char *background)
{
	if (background == NULL) {
		opt->do_fill_background = 0;
		return 0;
	}

	opt->do_fill_background = 1;

	return pldraw_str2col(pldraw, &opt->background, background);
}

static int parse_offset(struct plep_point *offset, char *opt)
{
	const char *tok;

	tok = strtok(opt, COMMA);

	if (tok == NULL)
		goto strtok_error;

	errno = 0;
	offset->x = (int)strtol(tok, NULL, 10);

	if (errno)
		goto strtol_error;

	tok = strtok(NULL, COMMA);

	if (tok == NULL)
		goto strtok_error;

	errno = 0;
	offset->y = (int)strtol(tok, NULL, 10);

	if (errno)
		goto strtol_error;

	return 0;

strtok_error:
	LOG("Invalid coordinate string, must be of the form: 123,456");
	return -1;

strtol_error:
	LOG("Invalid coordinate: %s", strerror(errno));
	return -1;
}

static int parse_alignment(enum epdoc_align_h *align_h,
			   enum epdoc_align_v *align_v,
			   char *opt)
{
	const char *tok;

	tok = strtok(opt, COMMA);

	if (tok == NULL)
		goto strtok_error;

	if (str2align(tok, align_h, align_v) < 0)
		return -1;

	tok = strtok(NULL, COMMA);

	if (tok != NULL) {
		if (str2align(tok, align_h, align_v) < 0)
			return -1;
	}

	return 0;

strtok_error:
	LOG("Invalid alignment options");
	return -1;
}

static int parse_crop(struct plep_rect *crop, char *opt)
{
	const char *tok;
	int coords[4];
	int i;

	tok = strtok(opt, COMMA);

	for (i = 0; i < 4; ++i) {
		if (tok == NULL) {
			LOG("Failed to parse crop area coordinates");
			return -1;
		}

		errno = 0;
		coords[i] = strtoul(tok, NULL, 10);

		if (errno) {
			LOG("Invalid coordinate: %s", strerror(errno));
			return -1;
		}

		tok = strtok(NULL, COMMA);
	}

	crop->a.x = MIN(coords[0], coords[2]);
	crop->a.y = MIN(coords[1], coords[3]);
	crop->b.x = MAX(coords[0], coords[2]);
	crop->b.y = MAX(coords[1], coords[3]);

	return 0;
}

static void log_waveform_path(const struct plep_waveform_info *wfinfo)
{
	LOG_PRINT("%s", wfinfo->type);

	if (wfinfo->palette != NULL) {
		LOG_PRINT("/%s", wfinfo->palette);

		if (wfinfo->opt != NULL)
			LOG_PRINT("/%s", wfinfo->opt);
	}

	LOG_PRINT("\n");
}

static int str2ul(const char *str, unsigned long int *value)
{
	errno = 0;
	*value = strtoul(str, NULL, 10);

	return errno ? -1 : 0;
}

static int str2align(const char *str, enum epdoc_align_h *align_h,
		     enum epdoc_align_v *align_v)
{
	struct align_value {
		const char *name;
		int value;
	};
	static const struct align_value align_table[] = {
		{ "left",   EPDOC_ALIGN_H_LEFT   },
		{ "center", EPDOC_ALIGN_H_CENTER },
		{ "right",  EPDOC_ALIGN_H_RIGHT  },
		{ "top",    EPDOC_ALIGN_V_TOP    },
		{ "middle", EPDOC_ALIGN_V_MIDDLE },
		{ "bottom", EPDOC_ALIGN_V_BOTTOM },
		{ NULL,     -1                   }
	};
	const struct align_value *it;

	for (it = align_table; it->name != NULL; ++it) {
		if (!strcmp(it->name, str))
			break;
	}

	if (it->name == NULL) {
		LOG("Invalid alignment identifier: %s", str);
		return -1;
	}

	if (it->value & EPDOC_ALIGN_V_BIT) {
		if (*align_v != EPDOC_ALIGN_V_NONE) {
			LOG("Multiple vertical alignment options specified");
			return -1;
		}

		*align_v = it->value;
	} else if (it->value & EPDOC_ALIGN_H_BIT) {
		if (*align_h != EPDOC_ALIGN_H_NONE) {
			LOG("Multiple horizontal alignment options specified");
			return -1;
		}

		*align_h = it->value;
	}

	return 0;
}

static void sigint_abort(int signum)
{
	if (signum == SIGINT) {
		LOG("Stopping...");
		g_run = 0;
	}
}
