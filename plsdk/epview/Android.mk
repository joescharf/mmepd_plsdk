LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := epview
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := epview.c epdoc.c plpng.c
LOCAL_CFLAGS += -Wall -O2
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../libplutil \
	$(LOCAL_PATH)/../libplepaper \
	$(LOCAL_PATH)/../libpldraw
LOCAL_STATIC_LIBRARIES := libplepaper libpldraw libplutil libeglib libpng
ifndef PLASTICLOGIC_NDK_BUILD
LOCAL_SHARED_LIBRARIES := libdl libz
LOCAL_C_INCLUDES += external/libpng external/zlib
else
LOCAL_LDLIBS := -ldl -lz
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../external/libpng
endif
include $(BUILD_EXECUTABLE)
