/*
  E-Paper document viewer - epview

  Copyright (C) 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include "epdoc.h"

void epdoc_auto_rotate(struct epdoc *doc, struct pldraw *pldraw)
{
	const int doc_orient = (doc->width > doc->height) ? 1 : 0;
	const int fb_orient =
		(pldraw_get_xres(pldraw) > pldraw_get_yres(pldraw)) ? 1 : 0;
	const int do_rotate = (doc_orient != fb_orient);
	const int cur_rotation = pldraw_get_rotation(pldraw);
	int rot_angle;

	if (do_rotate) {
		rot_angle = cur_rotation + ((cur_rotation % 180) ? -90 : 90);
		pldraw_set_rotation(pldraw, rot_angle);
	} else {
		rot_angle = cur_rotation;
	}

	doc->opt.rotation_angle = rot_angle;
}

void epdoc_align_offset(struct epdoc *doc, struct pldraw *pldraw)
{
	switch (doc->opt.align_h) {
	case EPDOC_ALIGN_H_NONE:
		break;
	case EPDOC_ALIGN_H_LEFT:
		doc->opt.offset.x = 0;
		break;
	case EPDOC_ALIGN_H_CENTER:
		doc->opt.offset.x = (pldraw_get_xres(pldraw) - doc->width) / 2;
		break;
	case EPDOC_ALIGN_H_RIGHT:
		doc->opt.offset.x = pldraw_get_xres(pldraw) - doc->width;
		break;
	}

	switch (doc->opt.align_v) {
	case EPDOC_ALIGN_V_NONE:
		break;
	case EPDOC_ALIGN_V_TOP:
		doc->opt.offset.y = 0;
		break;
	case EPDOC_ALIGN_V_MIDDLE:
		doc->opt.offset.y = (pldraw_get_yres(pldraw) - doc->height) /2;
		break;
	case EPDOC_ALIGN_V_BOTTOM:
		doc->opt.offset.y = pldraw_get_yres(pldraw) - doc->height;
		break;
	}
}

void epdoc_fixup_crop(struct epdoc *doc)
{
	doc->opt.crop.a.x = MAX(doc->opt.crop.a.x, 0);
	doc->opt.crop.a.y = MAX(doc->opt.crop.a.y, 0);
	doc->opt.crop.b.x = MIN(doc->opt.crop.b.x, doc->width);
	doc->opt.crop.b.y = MIN(doc->opt.crop.b.y, doc->height);
}

int epdoc_extcmp(const char *file_name, const char *ext)
{
	const size_t ext_len = strlen(ext);
	const size_t ext_pos = strlen(file_name) - ext_len;

	if (ext_pos < 0)
		return -1;

	return strcmp(&file_name[ext_pos], ext);
}
