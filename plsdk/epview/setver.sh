#!/bin/sh

set -e

ver="$1"

sed -i s/"\(VERSION\[\] = \"\)\(.*\)\(\";\)$"/"\1$ver\3"/ epview.c
git add epview.c

exit 0
