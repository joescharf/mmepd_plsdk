Release v1.9 -> v1.10 - 2013-11-04

Guillaume Tucker (3):
      tidy-up waveform option and add `show_contents' function
      read from stdin if no file name specified
      plpng.c: add support for stdin

Release v1.8 -> v1.9 - 2013-10-11

Guillaume Tucker (4):
      Android.mk: add static libraries libplutil and libeglib
      remove log.h and use plsdk/log.h instead
      do not reset CFA and rotation when no user provided values
      Android.mk: add path to libplutil

Michele Marchetti (2):
      split -w option into -w WAVEFORM and -W
      add -O option for config file path

Release v1.7 -> v1.8 - 2013-07-19

Guillaume Tucker (3):
      accept a list of documents + add related options
      improve SIGINT handling
      add setver.sh script to set the application version string

Nick Terry (1):
      epview.c - allow SIGINT to cleanly exit the application

Release v1.6 -> v1.7 - 2013-06-03

Guillaume Tucker (3):
      remove misleading waveform indices in enumeration
      Makefile: use shared libplsdk.so instead of static libs
      Makefile: remove not needed -ldl flag

Release v1.5.1 -> v1.6 - 2013-04-23

Guillaume Tucker (1):
      fix Android build + compiler warnings

Release v1.5 -> v1.5.1 - 2013-04-08

Guillaume Tucker (3):
      gitignore: depend_*.mk
      update copyright message (+ 2013)
      plpng.c: fix whitespace

Release v1.4 -> v1.5 - 2013-03-11

Guillaume Tucker (24):
      move document options to `struct epdc_opt' in epdoc.h
      plpng.c: use new `struct epdoc_opt' scheme
      add `-b' option for background colour
      plpng.c: use background options to fill and update screen
      epview.c: add `-b' help message
      add `-o' option with (x, y) offset coordinates
      plpng.c: modify to cope with offset
      plpng.c: fix manual rotation angle
      update copyright notices with 2013 + tidy-up
      add `-a' option for alignment
      plpng.c: adjust e-paper update coordinates with offset
      add `-c' option to set image crop area
      plpng.c: take into account the crop area
      epview.c: use char[] arrays for static const strings
      plpng.c: apply crop area to update region
      add `epdoc_extcmp' to check file extension + add comments
      move the format detection code to the format handlers
      plpng.c: test for the file extension
      plpng.c: tweak code formatting for consistency
      fix alignment when using rotation
      add -s option to enable synchronous update
      plpng.c: fix compiler warning (unused variable)
      Android.mk: add new file epdoc.c
      add help message for `-f' option (CFA)

Release v1.3 -> v1.4 - 2013-01-25

Guillaume Tucker (2):
      Android.mk: fix include path to external/libpng
      add -f option to enable colour filter

Release v1.2 -> v1.3 - 2012-12-20

Guillaume Tucker (4):
      epdoc: add width and height
      add automatic rotation angle
      epview: update help message with -r auto
      epview: fix typo in log message

Release v1.1 -> v1.2 - 2012-11-21

Guillaume Tucker (17):
      plpng: add support for 16-bit Y8+A format
      add epdoc structure and associated basic API
      add -e option for ePDC device + update help message
      add support for 8-bit palette
      add PLPNG_PALETTE_16 format support
      re-write the tests to determine the image format
      replace PLPNG_PALETTE_16_DEFAULT with PLPNG_Y4
      add PLPNG_PALETTE_2 format support
      split grey scale and RGB to simplify drawing functions
      plpng: list missing formats defined by PNG standard
      plpng: add PLPNG_PALETTE_4 support
      epview: fix return code when no errors
      add support for PLPNG_Y1 + improve error message
      plpng: add PLPNG_Y2 support + fix format tests
      add epdoc.update and implement in plpng
      plpng: reject interlaced images as it is not supported
      Makefile: use libz.so and libpng.so

Release v1.0 -> v1.1 - 2012-11-05

Guillaume Tucker (7):
      update copyright statements with authors
      update LICENSE string with GPL disclaimer
      use new libplepaper waveform naming scheme + API
      plpng: fix compiler warning (unused variables)
      plpng: clean-up (long lines and disabled code)

Nick Terry (1):
      Make rotation work, handle 4bpp PNG's

Release v0.4 -> v1.0 - 2012-10-02

Guillaume Tucker (15):
      set version string to v0.4+grey (fix in libplrawimage)
      add -r option to set rotation angle (90, 180, 270)
      Makefile: use LDFLAGS + add -Wall
      tidy-up and update with latest library changes
      add .gitignore with depend.mk
      remote -f option and use first positional arbgument
      add PNG code and drop libplpng + tidy-up
      add -m option to set the ePDC mode
      add -d option with frame buffer device
      fix log message about bits per colour channel
      minor tidy-up
      fix build: remove ol include (libplpng.h)
      Android.mk: add missing libpng include directories
      add GPL v3 license + update copyright statements
      plpng: add support for 1 channel (8bpp) format

Nick Terry (3):
      Add dependency on libdl for module loading
      Fixing Android.mk for NDK build, early png support
      Fix problem handling -f option handling

Sean Walsh (1):
      Refactor to prepare for inclusion in sdk

Release v0.3 -> v0.4 - 2012-04-18

Guillaume Tucker (4):
      add -p option to wait for power off after update
      add help section for -p option
      always use EPDC_MODE_DISCOVER for consistency
      bump to v0.4

Release v0.2 -> v0.3 - 2012-02-15

Guillaume Tucker (2):
      use synchronous updates + use new libanyfb API
      bump to v0.3

Release v0.1 -> v0.2 - 2012-01-26

Guillaume Tucker (2):
      Android.mk: fix include paths
      bump version number to v0.2

Release v0.1 - 2012-01-20

Guillaume Tucker (2):
      initial commit: basic app to show a PL V4 raw image
      add +image-format to version string

