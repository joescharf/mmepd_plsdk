#!/bin/bash

# Frame buffer graphics library - libpldraw - generate-font-file.sh
#
# Copyright (C) 2012, 2013 Plastic Logic Limited
#
#    Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

# Note: This depends on the Python Imaging Library (PIL)

# To get the X11 classic 100dpi fonts on Debian:
# sudo apt-get install xfonts-100dpi

# For more fonts:
# apt-cache search xfonts

src="$1"
name="$2"
size="$3"

[ -z "$src" ] || [ -z "$name" ] || [ -z "$size" ] && {
    echo "invalid arguments"
    echo "usage: $0 FONT_PATH FONT_NAME FONT_SIZE"
    echo "example: generate-font-file.sh /usr/share/fonts/X11/100dpi/courR10.pcf.gz Courier 10"
    exit 1
}

echo "file: $src, name: $name, size: $size"

pilfont=$(which pilfont.py)

[ -z "$pilfont" ] && {
    echo "pilfont.py is not installed!"
    exit 1
}

cp "$src" .
font=$(basename "$src" .pcf.gz)
gunzip $font.pcf.gz
python $pilfont $font.pcf
python process_font.py $font.pil $name $size > font-$name$size.c
rm $font.pil
rm $font.pcf
rm $font.pbm

./make-font-list.sh > fonts.c

exit 0
