# Frame buffer graphics library - libpldraw - process_font.py
#
# Copyright (C) 2012, 2013 Plastic Logic Limited
#
#    Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function
import sys
import string
import Image, ImageDraw, ImageFont

# Fonts are with fixed character sizes (monospace), ASCII only (128 characters)
# and monochrome (one bit per pixel).

def make_data_name(font_name, font_size, i):
    return "{0}{1}_{2:03d}_data".format(font_name, font_size, i)

def generate_struct(font_name, font_size, i, data):
    def print_char_data(b, n):
        if n == 0:
            print('\t', end='')
        else:
            print(' ', end='')
        print('0x{0:02X},'.format(b), end='')

    data_name = make_data_name(font_name, font_size, i)
    print("static const char {0}[] = {{".format(data_name))
    b = 0
    one = 0x01
    n = 0
    for bit in data:
        if not bit:
            b += one
        if one == 0x80:
            print_char_data(b, n)
            if n == 11:
                print()
                n = 0
            else:
                n += 1
            b = 0
            one = 0x01
        else:
            one = one << 1
    if one:
        print_char_data(b, n)
        print()
    elif n:
        print()
    print('};')
    return data_name

def main(argv):
    if len(argv) < 4:
        print("invalid arguments")
        print("usage: python process_font.py FONT.pil FONT_NAME FONT_SIZE")
        return False

    file_name = argv[1]
    font_name = argv[2]
    font_size = argv[3]

    try:
        font = ImageFont.load(file_name)
    except IOError, e:
        print("Failed to open font file! {0}".format(e))
        return False

    size = font.getsize('Z')
    alpha = string.ascii_letters + string.digits + string.punctuation + ' '
    alpha_data = [None for x in range(0, 128)]

    print("/* automatically generated - do not edit manually */\n")
    print("#include \"../libpldraw.h\"\n")
    print("/* Font: {0} {1} */\n".format(font_name, font_size))

    for c in alpha:
        print("/* {0} */".format(c))
        out = Image.new("1", size, 1)
        draw = ImageDraw.Draw(out)
        draw.text((0, 0), c, font=font)
        data = out.getdata()
        i = ord(c)
        alpha_data[i] = generate_struct(font_name, font_size, i, data)

    print("const struct pldraw_font pldraw_font_{0}{1} = {{".format(
            font_name, font_size))
    print("\t.name = \"{0}\",".format(font_name))
    print("\t.size = {0},".format(font_size))
    print("\t.width = {0},".format(size[0]))
    print("\t.height = {0},".format(size[1]))
    print("\t.data = {")
    place_holder = make_data_name(font_name, font_size, ord(' '))
    for i, data_name in enumerate(alpha_data):
        if not data_name:
            data_name = place_holder
        print("\t\t{1},".format(i, data_name))
    print("\t},")
    print("};")

    return True

if __name__ == '__main__':
    if main(sys.argv) == True:
        sys.exit(0)
    else:
        sys.exit(1)
