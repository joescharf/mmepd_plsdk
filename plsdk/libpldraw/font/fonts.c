/* automatically generated - do not edit manually */

#include "../libpldraw.h"
#include <stdlib.h>

extern const struct pldraw_font pldraw_font_Courier10;
extern const struct pldraw_font pldraw_font_Courier12;
extern const struct pldraw_font pldraw_font_Courier14;
extern const struct pldraw_font pldraw_font_Courier18;
extern const struct pldraw_font pldraw_font_Courier24;
extern const struct pldraw_font pldraw_font_Courier8;
extern const struct pldraw_font pldraw_font_Lucida10;
extern const struct pldraw_font pldraw_font_Lucida12;
extern const struct pldraw_font pldraw_font_Lucida14;
extern const struct pldraw_font pldraw_font_Lucida18;
extern const struct pldraw_font pldraw_font_Lucida24;
extern const struct pldraw_font pldraw_font_Lucida8;

static const struct pldraw_font *_fonts[] = {
	&pldraw_font_Courier10,
	&pldraw_font_Courier12,
	&pldraw_font_Courier14,
	&pldraw_font_Courier18,
	&pldraw_font_Courier24,
	&pldraw_font_Courier8,
	&pldraw_font_Lucida10,
	&pldraw_font_Lucida12,
	&pldraw_font_Lucida14,
	&pldraw_font_Lucida18,
	&pldraw_font_Lucida24,
	&pldraw_font_Lucida8,
	NULL
};

const struct pldraw_font * const *pldraw_fonts = _fonts;
