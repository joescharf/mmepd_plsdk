#!/bin/bash

# Frame buffer graphics library - libpldraw - make-font-list.sh
#
# Copyright (C) 2012, 2013 Plastic Logic Limited
#
#    Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

echo "/* automatically generated - do not edit manually */"
echo
echo "#include \"../libpldraw.h\""
echo "#include <stdlib.h>"
echo

fonts=
for f in font-*.c
do
    g=`echo $f | sed -e s/'^font-\(.*\)\.c$'/'pldraw_font_\1'/`
    fonts="$fonts $g"
done

for f in $fonts
do
    echo "extern const struct pldraw_font $f;"
done

echo
echo "static const struct pldraw_font *_fonts[] = {"

for f in $fonts
do
    echo -e "\t&$f,"
done

echo -e "\tNULL"
echo "};"
echo
echo "const struct pldraw_font * const *pldraw_fonts = _fonts;"

exit 0
