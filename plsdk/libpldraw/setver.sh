#!/bin/sh

set -e

ver="$1"

sed -i s/"\(LIBPLDRAW_VERSION \"\)\(.*\)\(\".*\)$"/"\1$ver\3"/ libpldraw.h
git add libpldraw.h

exit 0
