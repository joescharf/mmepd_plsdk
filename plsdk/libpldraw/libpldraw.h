/*
  Frame buffer graphics library - libpldraw

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_LIBPLDRAW_H
#define INCLUDE_LIBPLDRAW_H 1

#include <stdint.h>
#include <libplepaper.h>

/**
   @file libpldraw.h

   This library provides simple and fast functions to draw in a framebuffer.
*/

/** Library version */
#define LIBPLDRAW_VERSION "1.10"      /**< string for binary compatibility */


/**
   @name Graphics
   @{
 */

/** Colour definition */
typedef union pldraw_color {
	uint32_t rgb888;             /**< RGB888 value */
	uint16_t rgb565;             /**< RGB565 value */
	uint8_t rgb222;              /**< RGB222 value */
	uint8_t b[4];                /**< direct access to 4 bytes */
	uint32_t u32;                /**< direct access to 32 bit word */
	struct {
		uint8_t r;           /**< RGBW red */
		uint8_t g;           /**< RGBW green */
		uint8_t b;           /**< RGBW blue */
		uint8_t w;           /**< RGBW white */
	} rgbw;                      /**< RGBW 32-bit representation */
} pldraw_color_t;

/** Colour filter array identifiers */
enum pldraw_cfa_id {
	PLDRAW_CFA_NONE = 0,         /**< No CFA mapping in use */
	PLDRAW_CFA_GR_BW,            /**< GR_BW 2x2 mapping */
	PLDRAW_CFA_GR_BW_TR180,      /**< GR_BW top half rotated by 180 deg */
	_PLDRAW_CFA_N_
};

/** Pixel format identifiers */
enum pldraw_pixel_format {
	PLDRAW_PIX_RGB888 = 0,       /**< RGB888 */
	PLDRAW_PIX_RGB888X,          /**< RGB8888 without alpha channel */
	PLDRAW_PIX_RGB565,           /**< RGB565 */
	PLDRAW_PIX_RGB222,           /**< RGB222 */
	_PLDRAW_PIX_N_
};

/** Frame buffer statistics */
struct pldraw_stats {
	int xres;                    /**< horixontal 'x' resolution */
	int yres;                    /**< vertical 'y' resolution */
	int vxres;                   /**< virtual horizontal 'x' resolution */
	int vyres;                   /**< virtual vertical 'y' resolution */
	int xoffset;                 /**< horizontal 'x' offset */
	int yoffset;                 /**< vertical 'y' offset */
	int bpp;                     /**< number of bits per pixel */
	enum pldraw_pixel_format pixel_format; /**< pixel format identifier */
};

/** @} */


/**
   @name Font
   @{
*/

/** Font definition */
struct pldraw_font {
	const char *name;            /**< standard font name */
	unsigned size;               /**< standard font size */
	unsigned width;              /**< character width in pixels */
	unsigned height;             /**< character height in pixels */
	const char * const data[128]; /**< font character pixel data */
};

/** Pen definition */
struct pldraw_pen {
	const struct pldraw_font *font; /**< font used to draw text */
	pldraw_color_t foreground;   /**< foreground colour */
	pldraw_color_t background;   /**< background colour */
};

/** @} */


/** Opaque structure used in public interface */
struct pldraw;

/** 16 grey palette evenly distributed on 8-bit values */
extern const uint8_t pldraw_grey_16[16];

/** 4 grey palette evenly distributed on 8-bit values */
extern const uint8_t pldraw_grey_4[4];

/**
   @name Initialisation
   @{
 */

/** Create a drawing context
    @param[in] dev path to the framebuffer device to use
    @param[in] config_file optional file path, NULL for default
    @returns new pldraw instance
*/
extern struct pldraw *pldraw_init(const char *dev, const char *config_file);

/** Free resources associated with a pldraw instance
    @param[in] pldraw pldraw instance as created by pldraw_init
*/
extern void pldraw_free(struct pldraw *pldraw);

/** Set the plep instance that will be used to update the display
    @param[in] pldraw pldraw instance
    @param[in] plep   plep instance
*/
extern void pldraw_set_plep(struct pldraw *pldraw, struct plep *plep);

/** Get the plep instance associated with the drawing context
    @param[in] pldraw pldraw instance
    @return plep instance
*/
extern struct plep *pldraw_get_plep(const struct pldraw *pldraw);

/** Get the framebuffer device path
    @param[in] pldraw pldraw instance
    @return string with path to framebuffer device
*/
extern const char *pldraw_get_dev(const struct pldraw *pldraw);

/** @} */


/**
   @name Statistics
   @{
 */

/** Log general framebuffer statistics
    @param[in] pldraw pldraw instance
*/
extern void pldraw_log_info(const struct pldraw *pldraw);

/** Get horizontal resolution (taking rotation into account)
    @param[in] pldraw pldraw instalce
    @return horizontal resolution
*/
extern int pldraw_get_xres(const struct pldraw *pldraw);

/** Get vertical resolution (taking rotation into account)
    @param[in] pldraw pldraw instance
    @return vertical resolution
*/
extern int pldraw_get_yres(const struct pldraw *pldraw);

/** Get framebuffer statistics
    @param[in] pldraw pldraw instalce
    @param[out] stats structure to receive stastics
*/
extern void pldraw_get_stats(const struct pldraw *pldraw,
			     struct pldraw_stats *stats);

/** @} */


/**
   @name Colours
   @{
 */

/** Get a colour definition for given RGB values
    @param[in] pldraw pldraw instance
    @param[in] r Red
    @param[in] g Green
    @param[in] b Blue
    @return colour definition
*/
extern pldraw_color_t pldraw_get_color(struct pldraw *pldraw,
				       int r, int g, int b);

/** Get a colour definition for a given grey level
    @param[in] pldraw pldraw instance
    @param[in] grey grey level between 0 and 255
    @return colour definition
*/
extern pldraw_color_t pldraw_get_grey(struct pldraw *pldraw,
				      int grey /* 0-255 */);

/** Compare two colour definitions
    @param[in] pldraw pldraw instance
    @param[in] a first colour definition
    @param[in] b second colour definition
    @return 0 if same colour, 1 if not, -1 if error
*/
extern int pldraw_colcmp(const struct pldraw *pldraw, pldraw_color_t a,
			 pldraw_color_t b);

/** Convert a colour name to a colour value
    @param[in] pldraw pldraw instance
    @param[out] col colour found
    @param[in] name colour name
    (black, white, red, green, blue, cyan, magenta, yellow)
    @return 0 if colour found, -1 if error
 */
extern int pldraw_str2col(struct pldraw *pldraw, pldraw_color_t *col,
			  const char *name);

/** Set the colour filter array identifier
    @param[in] pldraw pldraw instance
    @param[in] cfa colour filter array identifier
 */
extern void pldraw_set_cfa(struct pldraw *pldraw, enum pldraw_cfa_id cfa);

/** Get the colour filter array identifier
    @param[in] pldraw pldraw instance
    @return colour filter array identifier
 */
extern int pldraw_get_cfa(const struct pldraw *pldraw);

/** Array with CFA name strings */
extern const char *pldraw_cfa_name[_PLDRAW_CFA_N_];

/** Get a CFA identifier from a name string
    @param[in] cfa_name string containing the CFA name
    @return CFA identifier or -1 if not found
 */
extern int pldraw_get_cfa_id(const char *cfa_name);

/** @} */


/**
   @name Coordinates
   @{
 */

/** Determine if specified point is within the specified area
    @param[in] area area coordinates
    @param[in] point point coordinates
    @return 1 if pixel in the area, 0 otherwise
*/
extern int pldraw_is_point_in_area(const struct plep_rect *area,
				   const struct plep_point *point);

/** Apply a border to the specified area
    @param[in, out] area area coordinates
    @param[in] border width of the border in pixels
*/
extern void pldraw_apply_border(struct plep_rect *area, int border);

/** Crop the supplied area to that supported by the drawing context
    @param[in] pldraw pldraw instance
    @param[in, out] area area coordinates to be cropped
*/
extern void pldraw_crop(struct pldraw *pldraw, struct plep_rect *area);

/** @} */


/**
   @name Drawing
   @{
 */

/** Get the framebuffer device file descriptor
    @param[in] pldraw pldraw instance
    @return framebuffer file descriptor
*/
extern int pldraw_get_fd(struct pldraw *pldraw);

/** Get the framebuffer base address
    @param[in] pldraw pldraw instance
    @return framebuffer base address
*/
extern char *pldraw_get_fbuf(struct pldraw *pldraw);

/** Set the crop area (limited to the size of the framebuffer)
    @param[in] pldraw pldraw instance
    @param[in] a crop area coordinates
*/
extern void pldraw_set_crop_area(struct pldraw *pldraw,
				 const struct plep_rect *a);

/** Get crop area
    @param[in] pldraw pldraw instance
    @param[out] a crop area coordinates
*/
extern void pldraw_get_crop_area(const struct pldraw *pldraw,
				 struct plep_rect *a);

/** Set rotation angle
    @param[in] pldraw pldraw instance
    @param[in] angle rotation angle in degrees (0, 90, 180, 270)
*/
extern void pldraw_set_rotation(struct pldraw *pldraw, int angle);

/** Get rotation angle
    @param[in] anyfb pldraw instance
    @return rotation angle in degrees
*/
extern int pldraw_get_rotation(const struct pldraw *anyfb);

/** Draw a pixel
    @param[in] pldraw pldraw instance
    @param[in] col colour
    @param[in] pt pixel coordinates
*/
extern void pldraw_set_pixel(struct pldraw *pldraw, pldraw_color_t col,
			     const struct plep_point *pt);

/** Fill the screen with a solid color
    @param[in] pldraw pldraw instance
    @param[in] col colour to use
*/
extern void pldraw_fill_screen(struct pldraw *pldraw, pldraw_color_t col);

/** Fill a rectangle with a solid color
    @param[in] pldraw pldraw instance
    @param[in] col colour
    @param[in] rect rectangle coordinates
*/
extern void pldraw_fill_rect(struct pldraw *pldraw, pldraw_color_t col,
			     const struct plep_rect *rect);

/** Draw a line with a solid color
    @param[in] pldraw pldraw instance
    @param[in] col colour
    @param[in] coords line start and finish coordinates
*/
extern void pldraw_draw_line(struct pldraw *pldraw, pldraw_color_t col,
			     const struct plep_rect *coords);

/** @} */


/**
   @name Text
   @{
 */

/** List of all available fonts */
extern const struct pldraw_font * const *pldraw_fonts;

/** String with all the supported text characters */
extern const char *pldraw_text_characters;

/** Get a font for the given description
    @param[in] pldraw pldraw instance
    @param[in] name standard font name (Courier...)
    @param[in] size standard font size (10...)
    @return pointer to a font if found, NULL otherwise
 */
extern const struct pldraw_font *pldraw_get_font(
	const struct pldraw *pldraw, const char *name, unsigned size);

/** Draw a string of text
    @param[in] pldraw pldraw instance
    @param[in] pen pen instance to use
    @param[in] coords coordinates where to start drawing the text
    @param[in] text string of text to draw
 */
extern void pldraw_draw_text(struct pldraw *pldraw,
			     const struct pldraw_pen *pen,
			     const struct plep_point *coords,
			     const char *text);

/** @} */

#endif /* INCLUDE_LIBPLDRAW_H */
