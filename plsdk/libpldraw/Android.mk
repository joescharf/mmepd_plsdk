LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS += -Wall -O2
LOCAL_MODULE := libpldraw
LOCAL_SRC_FILES := \
	core/libpldraw.c \
	font/font-Courier10.c \
	font/font-Courier12.c \
	font/font-Courier14.c \
	font/font-Courier18.c \
	font/font-Courier24.c \
	font/font-Courier8.c \
	font/font-Lucida10.c \
	font/font-Lucida12.c \
	font/font-Lucida14.c \
	font/font-Lucida18.c \
	font/font-Lucida24.c \
	font/font-Lucida8.c \
	font/fonts.c
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../libplepaper \
	$(LOCAL_PATH)/../libplutil
include $(BUILD_STATIC_LIBRARY)
