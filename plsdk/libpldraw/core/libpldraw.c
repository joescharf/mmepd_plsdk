/*
  Frame buffer graphics library - libpldraw

  Copyright (C) 2010, 2011, 2012, 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <libpldraw.h>
#include <plsdk/plconfig.h>
#include <linux/fb.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/wait.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define LOG_TAG "pldraw"
#include <plsdk/log.h>

#define color888(r, g, b) \
	((b << 16) | (g << 8) | (r << 0))

#define color565(r, g, b) \
	(((b >> 3) & 0x001F) | ((g << 3) & 0x07E0) | ((r << 8) & 0xF800))

#define color222(r, g, b) \
	(((b >> 6) & 0x03) | ((g >> 4) & 0x0C) | ((r >> 2) & 0x30))

#ifndef min
# define min(a, b) (((a) < (b)) ? (a) : (b))
#endif

typedef char *(* set_pixel_t) (struct pldraw *, char *, pldraw_color_t);
typedef pldraw_color_t (* get_color_t) (int r, int g, int b);

#ifdef ANDROID
static const char default_fb[] = "/dev/graphics/fb0";
#else
static const char default_fb[] = "/dev/fb0";
#endif

const char *pldraw_text_characters =
	"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	"!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~ ";

const uint8_t pldraw_grey_16[16] = {
	0, 17, 34, 51, 68, 85, 102, 119, 136, 153, 170, 187, 204, 221, 238, 255
};

const uint8_t pldraw_grey_4[4] = { 0, 85, 170, 255 };

const char *pldraw_cfa_name[_PLDRAW_CFA_N_] = {
	[PLDRAW_CFA_NONE] = "none",
	[PLDRAW_CFA_GR_BW] = "GR_BW",
	[PLDRAW_CFA_GR_BW_TR180] = "GR_BW_TR180",
};

/* cached values */
struct fb_cached_values {
	unsigned bytes_per_pixel;       /* = (bits per pixel / 8) */
	enum pldraw_pixel_format pixel_format;
	size_t line_length;
};

/* the opaque structure used in the public interface */
struct pldraw {
	int fd;                         /* device file descriptor */
	struct plconfig *config;	/* PLSDK configuration file */
	char *dev;                      /* device name (full path) */
	char *fbuf;                     /* mmap'ed frame buffer */
	struct fb_fix_screeninfo finfo; /* fixed fb stuff */
	struct fb_var_screeninfo vinfo; /* variable fb stuff */
	struct fb_cached_values cval;   /* cached values for this fb */
	struct plep_rect crop_area;     /* crop area, screen res by default */
	int rot;                        /* rotation angle */
	int xs;                         /* horizontal scaling factor */
	int ys;                         /* vertical scaling factor */
	enum pldraw_cfa_id cfa;         /* colour filter array identifier */
	struct plep *plep;              /* plep instance */
	set_pixel_t set_pixel;          /* function to set a pixel */
	get_color_t get_color;          /* function to get a packed colour */
};

static int init_fb(struct pldraw *pldraw, const char *dev);
static void update_cached_values(struct pldraw *pldraw);
static void set_scaling(struct pldraw *pldraw);
static void reset_crop_area(struct pldraw *pldraw);
static void set_cropped_point(struct pldraw *pldraw,
			      struct plep_point *cropped,
			      const struct plep_point *pt);
static int crop(int coord, int min, int max);
static int is_point_in_area(const struct plep_rect *a,
			    const struct plep_point *pt);
static void fixup_coord_order(struct plep_rect *rect);
#define swap_int(_a, _b) do {				\
		int tmp;				\
		tmp = _a;				\
		_a = _b;				\
		_b = tmp;				\
	} while (0)
static void rotate_point_angle(const struct pldraw *pldraw,
			       struct plep_point *pt,
			       const struct plep_point *pt_in,
			       int angle);
#define rotate_point(pldraw, pt, pt_in)					\
	rotate_point_angle((pldraw), (pt), (pt_in), (pldraw)->rot)
#define get_xres(pldraw)			\
	(((pldraw)->rot % 180)			\
	 ? (pldraw->vinfo.yres / pldraw->ys)	\
	 : (pldraw->vinfo.xres / pldraw->xs))
#define get_yres(pldraw)			\
	(((pldraw)->rot % 180)			\
	 ? (pldraw->vinfo.xres / pldraw->xs)	\
	 : (pldraw->vinfo.yres / pldraw->ys))
static int get_opposite_angle(int base_angle);
static char *get_pixel_address(struct pldraw *pldraw,
                               const struct plep_point *pt);
static void fill_rect(struct pldraw *pldraw, const struct plep_rect *rect,
                      pldraw_color_t color);
static void draw_char(struct pldraw *pldraw, const struct pldraw_pen *pen,
		      const struct plep_point *pt, char c);
static int get_cfa_id(const char *cfa_name);
static void update_cfa_settings(struct pldraw *pldraw);
static char *set_pixel_888(struct pldraw *pldraw, char *p,
			   pldraw_color_t color);
static char *set_pixel_888x(struct pldraw *pldraw, char *p,
			    pldraw_color_t col);
static char *set_pixel_565(struct pldraw *pldraw, char *p, pldraw_color_t col);
static char *set_pixel_222(struct pldraw *pldraw, char *p, pldraw_color_t col);
static char *set_pixel_565_gr_bw(struct pldraw *pldraw, char *p,
				 pldraw_color_t col);
static char *set_pixel_565_gr_bw_tr180(struct pldraw *pldraw, char *p,
				       pldraw_color_t col);
static set_pixel_t get_set_pixel(const struct pldraw *pldraw);
static pldraw_color_t get_color_888(int r, int g, int b);
static pldraw_color_t get_color_565(int r, int g, int b);
static pldraw_color_t get_color_222(int r, int g, int b);
static pldraw_color_t get_color_cfa(int r, int g, int b);
static get_color_t get_get_color(const struct pldraw *pldraw);

/* ----------------------------------------------------------------------------
 * public interface
 */

/* -- initialisation -- */

struct pldraw *pldraw_init(const char *dev, const char *config_file)
{
	struct pldraw *pldraw;
	const char *cfa_str;
	int res;

	pldraw = malloc(sizeof(struct pldraw));

	if (pldraw == NULL)
		goto err_exit;

	pldraw->config = plconfig_init(config_file, "libpldraw");

	if (pldraw->config == NULL)
		goto err_free_pldraw;

	if (dev == NULL)
		dev = plconfig_get_str(pldraw->config, "fb", default_fb);

	res = init_fb(pldraw, dev);

	if (res < 0) {
		LOG("failed to initialise frame buffer device");
		goto err_free_plconfig;
	}

	pldraw->plep = NULL;
	update_cached_values(pldraw);
	pldraw->rot = plconfig_get_int(pldraw->config, "rotation", 0);

	cfa_str = plconfig_get_str(pldraw->config, "cfa", "none");
	pldraw->cfa = get_cfa_id(cfa_str);

	if (pldraw->cfa == -1) {
		LOG("invalid CFA identifier: %s", cfa_str);
		goto err_free_plconfig;
	}

	update_cfa_settings(pldraw);

	return pldraw;

err_free_plconfig:
	plconfig_free(pldraw->config);
err_free_pldraw:
	free(pldraw);
err_exit:
	return NULL;
}

void pldraw_free(struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	munmap(pldraw->fbuf, pldraw->finfo.smem_len);
	close(pldraw->fd);
	free(pldraw->dev);
	free(pldraw);
}

void pldraw_set_plep(struct pldraw *pldraw, struct plep *plep)
{
	assert(pldraw != NULL);

	pldraw->plep = plep;
	plep_set_rotation(pldraw->plep, pldraw->rot);
	plep_set_scale(pldraw->plep, pldraw->xs, pldraw->ys);
}

struct plep *pldraw_get_plep(const struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	return pldraw->plep;
}

const char *pldraw_get_dev(const struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	return pldraw->dev;
}

/* -- statistics -- */

void pldraw_log_info(const struct pldraw *pldraw)
{
	static const char *PIXEL_FORMAT[] = {
		[PLDRAW_PIX_RGB888] = "RGB 888",
		[PLDRAW_PIX_RGB888X] = "RGB 8888",
		[PLDRAW_PIX_RGB565] = "RGB 565",
		[PLDRAW_PIX_RGB222] = "RGB 222"
	};

	assert(pldraw != NULL);

	LOG("-- pldraw info --");
	LOG(". id            : %s", pldraw->finfo.id);
	LOG(". resolution    : %ix%i", pldraw->vinfo.xres, pldraw->vinfo.yres);
	LOG(". buffer size   : %u", pldraw->finfo.smem_len);
	LOG(". virtual res.  : %ix%i",
	    pldraw->vinfo.xres_virtual, pldraw->vinfo.yres_virtual);
	LOG(". bits per pixel: %i", pldraw->vinfo.bits_per_pixel);
	LOG(". palette       : %s", pldraw->vinfo.grayscale ? "greys":"color");
	LOG(". pixel format  : %s", PIXEL_FORMAT[pldraw->cval.pixel_format]);

	if (pldraw->plep != NULL)
		plep_log_info(pldraw->plep);
}

int pldraw_get_xres(const struct pldraw *pldraw)
{
	assert(pldraw != NULL);
	assert(pldraw->xs != 0);
	assert(pldraw->ys != 0);

	return get_xres(pldraw);
}

int pldraw_get_yres(const struct pldraw *pldraw)
{
	assert(pldraw != NULL);
	assert(pldraw->xs != 0);
	assert(pldraw->ys != 0);

	return get_yres(pldraw);
}

void pldraw_get_stats(const struct pldraw *pldraw, struct pldraw_stats *stats)
{
	assert(pldraw != NULL);
	assert(stats != NULL);

	stats->xres = pldraw->vinfo.xres;
	stats->yres = pldraw->vinfo.yres;
	stats->vxres = pldraw->vinfo.xres_virtual;
	stats->vyres = pldraw->vinfo.yres_virtual;
	stats->xoffset = pldraw->vinfo.xoffset;
	stats->yoffset = pldraw->vinfo.yoffset;
	stats->bpp = pldraw->vinfo.bits_per_pixel;
	stats->pixel_format = pldraw->cval.pixel_format;
}

/* -- colours -- */

pldraw_color_t pldraw_get_color(struct pldraw *pldraw, int r, int g, int b)
{
	assert(pldraw != NULL);

	return pldraw->get_color(r, g, b);
}

pldraw_color_t pldraw_get_grey(struct pldraw *pldraw, int grey)
{
	assert(pldraw != NULL);
	assert((grey >= 0) && (grey < 256));

	if (pldraw->cval.pixel_format == PLDRAW_PIX_RGB565) {
		grey &= 0xF8;

		if (grey & 0x80)
			grey |= 0x4;
	}

	return pldraw->get_color(grey, grey, grey);
}

int pldraw_colcmp(const struct pldraw *pldraw, pldraw_color_t a,
		  pldraw_color_t b)
{
	int ret;

	assert(pldraw != NULL);

	switch (pldraw->cval.pixel_format) {
	case PLDRAW_PIX_RGB888:
	case PLDRAW_PIX_RGB888X:
		ret = (a.rgb888 == b.rgb888) ? 0 : 1;
		break;

	case PLDRAW_PIX_RGB565:
		ret = (a.rgb565 == b.rgb565) ? 0 : 1;
		break;

	case PLDRAW_PIX_RGB222:
		ret = (a.rgb222 == b.rgb222) ? 0 : 1;
		break;

	case _PLDRAW_PIX_N_:
	default:
		assert(!"unsupported pixel format");
		ret = -1;
		break;
	}

	return ret;
}

int pldraw_str2col(struct pldraw *pldraw, pldraw_color_t *col,
		   const char *name)
{
	struct col_name {
		const char *name;
		int r;
		int g;
		int b;
	};
	static const struct col_name col_name_table[] = {
		{ "black",      0x00, 0x00, 0x00 },
		{ "white",      0xFF, 0xFF, 0xFF },
		{ "red",        0xFF, 0x00, 0x00 },
		{ "green",      0x00, 0xFF, 0x00 },
		{ "blue",       0x00, 0x00, 0xFF },
		{ "cyan",       0x00, 0xFF, 0xFF },
		{ "magenta",    0xFF, 0x00, 0xFF },
		{ "yellow",     0xFF, 0xFF, 0x00 },
		{ NULL }
	};
	const struct col_name *it;

	assert(pldraw != NULL);
	assert(col != NULL);
	assert(name != NULL);

	if (!strncmp(name, "0x", 2) && (strlen(name) == 8)) {
		unsigned rgb;

		if (sscanf(name, "0x%X", &rgb) != 1) {
			LOG("Failed to parse RGB value: %s", name);
			return -1;
		}

		*col = pldraw->get_color((rgb >> 16) & 0xFF,
					 (rgb >> 8) & 0xFF,
					 rgb & 0xFF);

		return 0;
	}

	for (it = col_name_table; it->name != NULL; ++it) {
		if (!strcmp(it->name, name)) {
			*col = pldraw->get_color(it->r, it->g, it->b);
			return 0;
		}
	}

	LOG("Invalid colour name: %s", name);

	return -1;
}

void pldraw_set_cfa(struct pldraw *pldraw, enum pldraw_cfa_id cfa)
{
	assert(pldraw != NULL);
	assert(cfa >= 0);
	assert(cfa < _PLDRAW_CFA_N_);

	pldraw->cfa = cfa;
	update_cfa_settings(pldraw);
}

int pldraw_get_cfa(const struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	return pldraw->cfa;
}

int pldraw_get_cfa_id(const char *cfa_name)
{
	assert(cfa_name != NULL);

	return get_cfa_id(cfa_name);
}

/* -- coordinates -- */

int pldraw_is_point_in_area(const struct plep_rect *area,
			   const struct plep_point *point)
{
	assert(area != NULL);
	assert(point != NULL);

	return is_point_in_area(area, point);
}

void pldraw_apply_border(struct plep_rect *area, int border)
{
	int dx;
	int dy;

	assert(area != NULL);
	assert(area->b.x >= area->a.x);
	assert(area->b.y >= area->a.y);

	dx = area->b.x - area->a.x;
	dy = area->b.y - area->a.y;

	if (dx < (2 * border)) {
		area->a.x += (dx / 2);
		area->b.x = area->a.x;
	} else {
		area->a.x += border;
		area->b.x -= border;
	}

	if (dy < (2 * border)) {
		area->a.y += (dy / 2);
		area->b.y = area->a.y;
	} else {
		area->a.y += border;
		area->b.y -= border;
	}
}

void pldraw_crop(struct pldraw *pldraw, struct plep_rect *area)
{
	const struct plep_rect *crop_area;

	assert(pldraw != NULL);
	assert(area != NULL);

	crop_area = &pldraw->crop_area;
	area->a.x = crop(area->a.x, crop_area->a.x, crop_area->b.x);
	area->b.x = crop(area->b.x, crop_area->a.x, crop_area->b.x);
	area->a.y = crop(area->a.y, crop_area->a.y, crop_area->b.y);
	area->b.y = crop(area->b.y, crop_area->a.y, crop_area->b.y);
}

/* -- drawing -- */

int pldraw_get_fd(struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	return pldraw->fd;
}

char *pldraw_get_fbuf(struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	return pldraw->fbuf;
}

void pldraw_set_crop_area(struct pldraw *pldraw, const struct plep_rect *area)
{
	struct plep_rect r_area;

	assert(pldraw != NULL);
	assert(area != NULL);
	assert(area->b.x >= area->a.x);
	assert(area->b.y >= area->a.y);

	rotate_point(pldraw, &r_area.a, &area->a);
	rotate_point(pldraw, &r_area.b, &area->b);
	pldraw->crop_area.a.x = crop(r_area.a.x, 0, pldraw->vinfo.xres);
	pldraw->crop_area.a.y = crop(r_area.a.y, 0, pldraw->vinfo.yres);
	pldraw->crop_area.b.x = crop(r_area.b.x, 0, pldraw->vinfo.xres);
	pldraw->crop_area.b.y = crop(r_area.b.y, 0, pldraw->vinfo.yres);
}

void pldraw_get_crop_area(const struct pldraw *pldraw, struct plep_rect *area)
{
	const struct plep_rect *crop;
	int angle;

	assert(pldraw != NULL);
	assert(area != NULL);

	crop = &pldraw->crop_area;
	angle = get_opposite_angle(pldraw->rot);
	rotate_point_angle(pldraw, &area->a, &crop->a, angle);
	rotate_point_angle(pldraw, &area->b, &crop->b, angle);
}

void pldraw_set_rotation(struct pldraw *pldraw, int angle)
{
	assert(pldraw != NULL);

	angle = angle % 360;

	if (angle < 0)
		angle += 360;

	assert(!(angle % 90));
	assert(angle >= 0);
	assert(angle <= 270);

	pldraw->rot = angle;

	if (pldraw->plep != NULL)
		plep_set_rotation(pldraw->plep, angle);
}

int pldraw_get_rotation(const struct pldraw *pldraw)
{
	assert(pldraw != NULL);

	return pldraw->rot;
}

void pldraw_set_pixel(struct pldraw *pldraw, pldraw_color_t color,
		      const struct plep_point *pt)
{
	struct plep_point r_pt;

	assert(pldraw != NULL);
	assert(pt != NULL);

	rotate_point(pldraw, &r_pt, pt);

	if (is_point_in_area(&pldraw->crop_area, &r_pt))
		pldraw->set_pixel(pldraw, get_pixel_address(pldraw, &r_pt),
				  color);
}

void pldraw_fill_screen(struct pldraw *pldraw, pldraw_color_t color)
{
	assert(pldraw != NULL);

	fill_rect(pldraw, &pldraw->crop_area, color);
}

void pldraw_fill_rect(struct pldraw *pldraw, pldraw_color_t color,
		      const struct plep_rect *rect)
{
	struct plep_rect r_rect;

	assert(pldraw != NULL);
	assert(rect != NULL);

	rotate_point(pldraw, &r_rect.a, &rect->a);
	rotate_point(pldraw, &r_rect.b, &rect->b);
	fill_rect(pldraw, &r_rect, color);
}

void pldraw_draw_line(struct pldraw *pldraw, pldraw_color_t color,
		      const struct plep_rect *coords)
{
	struct plep_rect rc;
	struct plep_point *a, *b;

	struct plep_point p;
	set_pixel_t set_pixel;
	int dx, dy;
	int vertical;

	assert(pldraw != NULL);
	assert(coords != NULL);

	rotate_point(pldraw, &rc.a, &coords->a);
	rotate_point(pldraw, &rc.b, &coords->b);

	dx = rc.b.x - rc.a.x;
	dy = rc.b.y - rc.a.y;
	set_pixel = pldraw->set_pixel;

	if (!dx && !dy) {
		if (is_point_in_area(&pldraw->crop_area, &rc.a))
			set_pixel(pldraw, get_pixel_address(pldraw, &rc.a),
				  color);
		return;
	}

	vertical = (abs(dx) < abs(dy)) ? 1 : 0;

	if ((vertical && (dy > 0)) || (!vertical && (dx > 0))) {
		a = &rc.a;
		b = &rc.b;
	} else {
		a = &rc.b;
		b = &rc.a;
	}

	if (vertical) {
		const int x0 = a->x - ((dx * a->y) / dy);

		for (p.x = a->x, p.y = a->y;
		     p.y <= b->y;
		     p.x = ((dx * (++p.y)) / dy) + x0) {
			char * const pix = get_pixel_address(pldraw, &p);

			if (is_point_in_area(&pldraw->crop_area, &p))
				set_pixel(pldraw, pix, color);
		}
	} else {
		const int y0 = a->y - ((dy * a->x) / dx);

		for (p.x = a->x, p.y = a->y;
		     p.x <= b->x;
		     p.y = ((dy * (++p.x)) / dx) + y0) {
			char * const pix = get_pixel_address(pldraw, &p);

			if (is_point_in_area(&pldraw->crop_area, &p))
				set_pixel(pldraw, pix, color);
		}
	}
}

const struct pldraw_font *pldraw_get_font(const struct pldraw *pldraw,
					  const char *name, unsigned size)
{
	const struct pldraw_font * const *font;

	assert(pldraw != NULL);

	if (name == NULL)
		name = plconfig_get_str(pldraw->config, "font-name", NULL);

	if (name == NULL)
		return pldraw_fonts[0];

	if (!size)
		size = plconfig_get_int(pldraw->config, "font-size", 0);

	for (font = pldraw_fonts; *font != NULL; ++font) {
		if (strcmp((*font)->name, name))
			continue;

		if (size && ((*font)->size != size))
			continue;

		return *font;
	}

	return NULL;
}

void pldraw_draw_text(struct pldraw *pldraw, const struct pldraw_pen *pen,
		      const struct plep_point *coords, const char *text)
{
	const char *c;
	struct plep_point pt;

	assert(pldraw != NULL);
	assert(pen != NULL);
	assert(pen->font != NULL);
	assert(coords != NULL);
	assert(pldraw->xs != 0);
	assert(pldraw->ys != 0);

	if (text == NULL)
		return;

	memcpy(&pt, coords, sizeof pt);

	for (c = text; *c != '\0'; ++c) {
		struct plep_point rot_pt;

		rotate_point(pldraw, &rot_pt, &pt);

		if (!is_point_in_area(&pldraw->crop_area, &rot_pt))
			break;

		draw_char(pldraw, pen, &pt, *c);
		pt.x += pen->font->width;
	}
}


/* ----------------------------------------------------------------------------
 * static functions
 */

static int init_fb(struct pldraw *pldraw, const char *dev)
{
	int ret = -1;

	pldraw->dev = NULL;
	pldraw->fd = open(dev, O_RDWR);

	if (pldraw->fd < 0)
		LOG_ERRNO("failed to open device %s", dev);
	else if (ioctl(pldraw->fd, FBIOGET_FSCREENINFO, &pldraw->finfo))
		LOG_ERRNO("failed to get fixed screen info");
	else if (ioctl(pldraw->fd, FBIOGET_VSCREENINFO, &pldraw->vinfo))
		LOG_ERRNO("failed to get variable screen info");
	else {
		pldraw->dev = strdup(dev);
		assert(pldraw->dev != NULL);

		pldraw->fbuf = mmap(0, pldraw->finfo.smem_len,
				    (PROT_READ | PROT_WRITE), MAP_SHARED,
				    pldraw->fd, 0);

		if (pldraw->fbuf == MAP_FAILED)
			LOG_ERRNO("failed to mmap frame buffer");
		else
			ret = 0;
	}

	if (ret < 0) {
		if (pldraw->dev != NULL)
			free(pldraw->dev);
		if (pldraw->fd >= 0)
			close(pldraw->fd);
	} else {
		LOG("initialised with %s", dev);
	}

	return ret;
}

static void update_cached_values(struct pldraw *pldraw)
{
	pldraw->cval.bytes_per_pixel = pldraw->vinfo.bits_per_pixel / 8;

	switch (pldraw->cval.bytes_per_pixel) {
	case 4:
		pldraw->cval.pixel_format = PLDRAW_PIX_RGB888X;
		break;

	case 3:
		pldraw->cval.pixel_format = PLDRAW_PIX_RGB888;
		break;

	case 2:
		pldraw->cval.pixel_format = PLDRAW_PIX_RGB565;
		break;

	case 1:
		pldraw->cval.pixel_format = PLDRAW_PIX_RGB222;
		break;

	default:
		LOG("unsupported pixel format");
#if 0
		return -1;
#else
		break;
#endif
	}

	if (pldraw->finfo.line_length)
		pldraw->cval.line_length = pldraw->finfo.line_length;
	else
		pldraw->cval.line_length =
			pldraw->cval.bytes_per_pixel * pldraw->vinfo.xres;
}

static void set_scaling(struct pldraw *pldraw)
{
	switch (pldraw->cfa) {
	case PLDRAW_CFA_NONE:
		pldraw->xs = 1;
		pldraw->ys = 1;
		break;
	case PLDRAW_CFA_GR_BW:
	case PLDRAW_CFA_GR_BW_TR180:
		pldraw->xs = 2;
		pldraw->ys = 2;
		break;
	case _PLDRAW_CFA_N_:
	default:
		assert(!"invalid CFA identifier");
		return;
	}
}

static void reset_crop_area(struct pldraw *pldraw)
{
	int crop_lx;
	int crop_rx;
	int crop_top;
	int crop_btm;

	crop_lx = plconfig_get_int(pldraw->config, "crop-left", 0);
	crop_top = plconfig_get_int(pldraw->config, "crop-top", 0);
	crop_rx = plconfig_get_int(pldraw->config, "crop-right", 0);
	crop_btm = plconfig_get_int(pldraw->config, "crop-bottom", 0);

	pldraw->crop_area.a.x = crop_lx;
	pldraw->crop_area.a.y = crop_top;
	pldraw->crop_area.b.x = (pldraw->vinfo.xres / pldraw->xs) - crop_rx;
	pldraw->crop_area.b.y = (pldraw->vinfo.yres / pldraw->ys) - crop_btm;
}

static void set_cropped_point(struct pldraw *pldraw,
			      struct plep_point *cropped,
			      const struct plep_point *pt)
{
	cropped->x = crop(pt->x, pldraw->crop_area.a.x, pldraw->crop_area.b.x);
	cropped->y = crop(pt->y, pldraw->crop_area.a.y, pldraw->crop_area.b.y);
}

static int crop(int coord, int min, int max)
{
	if (coord < min)
		return min;

	if (coord > max)
		return max;

	return coord;
}

static int is_point_in_area(const struct plep_rect *a,
			    const struct plep_point *pt)
{
	if ((pt->x < a->a.x)
	    || (pt->x >= a->b.x)
	    || (pt->y < a->a.y)
	    || (pt->y >= a->b.y))
		return 0;

	return 1;
}

static void fixup_coord_order(struct plep_rect *rect)
{
	if (rect->a.x > rect->b.x)
		swap_int(rect->a.x, rect->b.x);

	if (rect->a.y > rect->b.y)
		swap_int(rect->a.y, rect->b.y);
}

static void rotate_point_angle(const struct pldraw *pldraw,
			       struct plep_point *pt,
			       const struct plep_point *in_pt,
			       int angle)
{
	int tmp;

	switch (angle) {
	case 0:
		pt->x = in_pt->x;
		pt->y = in_pt->y;
		break;

	case 90:
		tmp = in_pt->y;
		pt->y = in_pt->x;
		pt->x = (pldraw->vinfo.xres / pldraw->xs) - 1 - tmp;
		break;

	case 180:
		pt->x = (pldraw->vinfo.xres / pldraw->xs) - 1 - in_pt->x;
		pt->y = (pldraw->vinfo.yres / pldraw->ys) - 1 - in_pt->y;
		break;

	case 270:
		tmp = in_pt->x;
		pt->x = in_pt->y;
		pt->y = (pldraw->vinfo.yres / pldraw->ys) - 1 - tmp;
		break;
	}
}

static int get_opposite_angle(int base_angle)
{

	int angle;

	switch (base_angle) {
	case 90:
		angle = 270;
		break;

	case 270:
		angle = 90;
		break;

	default:
		angle = base_angle;
		break;
	}

	return angle;
}

static char *get_pixel_address(struct pldraw *pldraw,
                               const struct plep_point *pt)
{
	int x;
	int y;
	unsigned long offset;

	x = pt->x * pldraw->xs;
	y = pt->y * pldraw->ys;

	offset = ((pldraw->vinfo.yoffset + y)
		  * pldraw->cval.line_length);

	offset += ((pldraw->vinfo.xoffset + x)
		   * pldraw->cval.bytes_per_pixel);

	return pldraw->fbuf + offset;
}

static void draw_char(struct pldraw *pldraw, const struct pldraw_pen *pen,
		      const struct plep_point *pt, char c)
{
	const char *data;
	unsigned char mask;
	unsigned row;
	unsigned col;
	unsigned width;
	unsigned height;

	assert(pldraw->xs != 0);
	assert(pldraw->ys != 0);

	data = pen->font->data[c & 0x7F];
	mask = 1;

	width = min(pen->font->width, (get_xres(pldraw) - pt->x));
	height = min(pen->font->height, (get_yres(pldraw) - pt->y));

	for (row = 0; row < height; ++row) {
		for (col = 0; col < pen->font->width; ++col) {
			if (col < width) {
				const pldraw_color_t color = (*data & mask) ?
					pen->foreground : pen->background;
				const struct plep_point row_pt = {
					.x = pt->x + col, .y = pt->y + row
				};
				struct plep_point pix_pt;
				char *pix;

				rotate_point(pldraw, &pix_pt, &row_pt);
				pix = get_pixel_address(pldraw, &pix_pt);
				pldraw->set_pixel(pldraw, pix, color);
			}

			if (mask == 0x80) {
				++data;
				mask = 1;
			} else {
				mask <<= 1;
			}
		}
	}
}

static void fill_rect(struct pldraw *pldraw, const struct plep_rect *rect,
                      pldraw_color_t color)
{
	struct plep_rect cropped;
	set_pixel_t set_pixel = pldraw->set_pixel;
	int y;

	set_cropped_point(pldraw, &cropped.a, &rect->a);
	set_cropped_point(pldraw, &cropped.b, &rect->b);
	fixup_coord_order(&cropped);

	for (y = cropped.a.y; y < cropped.b.y; ++y) {
		const struct plep_point start = { cropped.a.x, y };
		char *p = get_pixel_address(pldraw, &start);
		int x;

		for (x = cropped.a.x; x < cropped.b.x; ++x)
			p = set_pixel(pldraw, p, color);
	}
}

static int get_cfa_id(const char *cfa_name)
{
	enum pldraw_cfa_id id;

	for (id = 0; id < _PLDRAW_CFA_N_; ++id)
		if (!strcmp(cfa_name, pldraw_cfa_name[id]))
			return id;

	return -1;
}

static void update_cfa_settings(struct pldraw *pldraw)
{
	pldraw->set_pixel = get_set_pixel(pldraw);
	pldraw->get_color = get_get_color(pldraw);
	set_scaling(pldraw);
	reset_crop_area(pldraw);

	if (pldraw->plep != NULL)
		plep_set_scale(pldraw->plep, pldraw->xs, pldraw->ys);
}

static char *set_pixel_888x(struct pldraw *pldraw, char *p,
			    pldraw_color_t color)
{
	*p++ = color.rgb888 & 0xFF;
	*p++ = (color.rgb888 >> 8) & 0xFF;
	*p++ = (color.rgb888 >> 16) & 0xFF;
	*p++ = 0;

	return p;
}

static char *set_pixel_888(struct pldraw *pldraw, char *p,
			    pldraw_color_t color)
{
	*p++ = color.rgb888 & 0xFF;
	*p++ = (color.rgb888 >> 8) & 0xFF;
	*p++ = (color.rgb888 >> 16) & 0xFF;

	return p;
}

static char *set_pixel_565(struct pldraw *pldraw, char *p, pldraw_color_t color)
{
	*p++ = (color.rgb565 & 0xFF);
	*p++ = ((color.rgb565 >> 8) & 0xFF);

	return p;
}

static char *set_pixel_222(struct pldraw *pldraw, char *p, pldraw_color_t color)
{
	*p++ = color.rgb222 & 0xFF;

	return p;
}

static char *set_pixel_565_gr_bw(struct pldraw *pldraw, char *p,
				 pldraw_color_t color)
{
	uint16_t pix;
	char *p0;
	char *p1;

	p0 = p;

	/* green */
	pix = color565(color.rgbw.g, color.rgbw.g, color.rgbw.g);
	*p++ = pix & 0xFF;
	*p++ = (pix >> 8) & 0xFF;

	/* red */
	pix = color565(color.rgbw.r, color.rgbw.r, color.rgbw.r);
	*p++ = pix & 0xFF;
	*p++ = (pix >> 8) & 0xFF;

	p1 = p;
	p = p0 + pldraw->cval.line_length;

	/* blue */
	pix = color565(color.rgbw.b, color.rgbw.b, color.rgbw.b);
	*p++ = pix & 0xFF;
	*p++ = (pix >> 8) & 0xFF;

	/* white */
	pix = color565(color.rgbw.w, color.rgbw.w, color.rgbw.w);
	*p++ = pix & 0xFF;
	*p = (pix >> 8) & 0xFF;

	return p1;
}

static char *set_pixel_565_gr_bw_tr180(struct pldraw *pldraw, char *p,
				       pldraw_color_t color)
{
	const size_t offset = p - pldraw->fbuf;
	uint16_t pix;
	char *p0;
	char *p1;

	if ((offset / pldraw->cval.line_length) >= (pldraw->vinfo.yres / 2))
		return set_pixel_565_gr_bw(pldraw, p, color);

	p0 = p;

	/* white */
	pix = color565(color.rgbw.w, color.rgbw.w, color.rgbw.w);
	*p++ = pix & 0xFF;
	*p++ = (pix >> 8) & 0xFF;

	/* blue */
	pix = color565(color.rgbw.b, color.rgbw.b, color.rgbw.b);
	*p++ = pix & 0xFF;
	*p++ = (pix >> 8) & 0xFF;

	p1 = p;
	p = p0 + pldraw->cval.line_length;

	/* red */
	pix = color565(color.rgbw.r, color.rgbw.r, color.rgbw.r);
	*p++ = pix & 0xFF;
	*p++ = (pix >> 8) & 0xFF;

	/* green */
	pix = color565(color.rgbw.g, color.rgbw.g, color.rgbw.g);
	*p++ = pix & 0xFF;
	*p = (pix >> 8) & 0xFF;

	return p1;
}

static set_pixel_t get_set_pixel(const struct pldraw *pldraw)
{
	static const set_pixel_t set_pixel_table
		[_PLDRAW_PIX_N_][_PLDRAW_CFA_N_] = {
		[0 ... _PLDRAW_PIX_N_-1][0 ... _PLDRAW_CFA_N_-1] = NULL,
		[PLDRAW_PIX_RGB888][PLDRAW_CFA_NONE] = set_pixel_888,
		[PLDRAW_PIX_RGB888X][PLDRAW_CFA_NONE] = set_pixel_888x,
		[PLDRAW_PIX_RGB565][PLDRAW_CFA_NONE] = set_pixel_565,
		[PLDRAW_PIX_RGB565][PLDRAW_CFA_GR_BW] = set_pixel_565_gr_bw,
		[PLDRAW_PIX_RGB565][PLDRAW_CFA_GR_BW_TR180] =
			set_pixel_565_gr_bw_tr180,
		[PLDRAW_PIX_RGB222][PLDRAW_CFA_NONE] = set_pixel_222,
	};

	assert(pldraw->cval.pixel_format < _PLDRAW_PIX_N_);
	assert(pldraw->cfa < _PLDRAW_CFA_N_);

	return set_pixel_table[pldraw->cval.pixel_format][pldraw->cfa];
}

static pldraw_color_t get_color_888(int r, int g, int b)
{
	const pldraw_color_t color =
		{ .rgb888 = color888(r, g, b) };

	return color;
}

static pldraw_color_t get_color_565(int r, int g, int b)
{
	const pldraw_color_t color =
		{ .rgb565 = color565(r, g, b) };

	return color;
}

static pldraw_color_t get_color_222(int r, int g, int b)
{
	const pldraw_color_t color =
		{ .rgb222 = color222(r, g, b) };

	return color;
}

static pldraw_color_t get_color_cfa(int r, int g, int b)
{
	const int w = ((r * 299) + (g * 587) + (b * 114)) / 1000;
	const pldraw_color_t color =
		{ .rgbw = { .r = r, .g = g, .b = b, .w = w } };

	return color;
}

static get_color_t get_get_color(const struct pldraw *pldraw)
{
	static const get_color_t get_color_table[_PLDRAW_PIX_N_] = {
		[0 ... _PLDRAW_PIX_N_-1] = NULL,
		[PLDRAW_PIX_RGB888] = get_color_888,
		[PLDRAW_PIX_RGB888X] = get_color_888,
		[PLDRAW_PIX_RGB565] = get_color_565,
		[PLDRAW_PIX_RGB222] = get_color_222,
	};

	if ((pldraw->cfa == PLDRAW_CFA_GR_BW)
	    || (pldraw->cfa == PLDRAW_CFA_GR_BW_TR180))
		return get_color_cfa;

	assert(pldraw->cval.pixel_format < _PLDRAW_PIX_N_);

	return get_color_table[pldraw->cval.pixel_format];
}
