LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := _imaging
LOCAL_SRC_FILES := \
	_imaging.c \
	decode.c \
	encode.c \
	map.c \
	display.c \
	outline.c \
	path.c \
	libImaging/Access.c \
	libImaging/Antialias.c \
	libImaging/Bands.c \
	libImaging/BitDecode.c \
	libImaging/Blend.c \
	libImaging/Chops.c \
	libImaging/Convert.c \
	libImaging/ConvertYCbCr.c \
	libImaging/Copy.c \
	libImaging/Crc32.c \
	libImaging/Crop.c \
	libImaging/Dib.c \
	libImaging/Draw.c \
	libImaging/Effects.c \
	libImaging/EpsEncode.c \
	libImaging/File.c \
	libImaging/Fill.c \
	libImaging/Filter.c \
	libImaging/FliDecode.c \
	libImaging/Geometry.c \
	libImaging/GetBBox.c \
	libImaging/GifDecode.c \
	libImaging/GifEncode.c \
	libImaging/HexDecode.c \
	libImaging/Histo.c \
	libImaging/JpegDecode.c \
	libImaging/JpegEncode.c \
	libImaging/LzwDecode.c \
	libImaging/Matrix.c \
	libImaging/ModeFilter.c \
	libImaging/MspDecode.c \
	libImaging/Negative.c \
	libImaging/Offset.c \
	libImaging/Pack.c \
	libImaging/PackDecode.c \
	libImaging/Palette.c \
	libImaging/Paste.c \
	libImaging/Quant.c \
	libImaging/QuantHash.c \
	libImaging/QuantHeap.c \
	libImaging/PcdDecode.c \
	libImaging/PcxDecode.c \
	libImaging/PcxEncode.c \
	libImaging/Point.c \
	libImaging/RankFilter.c \
	libImaging/RawDecode.c \
	libImaging/RawEncode.c \
	libImaging/Storage.c \
	libImaging/SunRleDecode.c \
	libImaging/TgaRleDecode.c \
	libImaging/Unpack.c \
	libImaging/UnpackYCC.c \
	libImaging/UnsharpMask.c \
	libImaging/XbmDecode.c \
	libImaging/XbmEncode.c \
	libImaging/ZipDecode.c \
	libImaging/ZipEncode.c
LOCAL_MODULE_TAGS := eng
LOCAL_CFLAGS += -Wall -O2 -fno-strict-aliasing -DHAVE_LIBJPEG -DHAVE_LIBZ
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/libImaging \
	$(LOCAL_PATH)/../../python-headers/python2.4 \
	$(LOCAL_PATH)/../../external/libjpeg \
	$(LOCAL_PATH)/../../pyplsdk \
	$(LOCAL_PATH)/../../libpldraw \
	$(LOCAL_PATH)/../../libplepaper
LOCAL_LDLIBS := -lz
LOCAL_SHARED_LIBRARIES := libpng libjpeg libz
LOCAL_STATIC_LIBRARIES := libpldraw libplepaper libplutil libeglib
LOCAL_LDLIBS += $(LOCAL_PATH)/../../prebuilt/p4a-2.6.2/libpython2.6.so
LOCAL_PRELINK_MODULE := false
include $(BUILD_SHARED_LIBRARY)
