LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)
LOCAL_SRC_FILES:= \
	gmain.c \
	gmarkup.c \
	gmodule.c \
	gunidecomp.c \
	guniprop.c \
	gutf8.c
LOCAL_CFLAGS += -O3
LOCAL_MODULE := libeglib
include $(BUILD_STATIC_LIBRARY)
