#ifndef INCLUDE_GMARKUP_PRIVATE_H
#define INCLUDE_GMARKUP_PRIVATE_H 1

#include <glib.h>

extern GMarkupParseContext *g_markup_parse_context_new1 (
	const GMarkupParser *parser,
	GMarkupParseFlags    flags,
	gpointer             user_data,
	GDestroyNotify       user_data_dnotify);
extern void g_markup_parse_context_free1 (GMarkupParseContext *context);
extern gboolean g_markup_parse_context_parse1 (GMarkupParseContext *context,
					       const gchar         *text,
					       gssize               text_len,  
					       GError             **error);

#endif /* INCLUDE_GMARKUP_PRIVATE_H */
