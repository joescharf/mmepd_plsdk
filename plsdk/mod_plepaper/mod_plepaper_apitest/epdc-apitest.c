/*
  E-Paper module support - API test

  Copyright (C) 2012 Plastic Logic Limited

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include "libplepaper.h"

#define LOG_TAG "apitest"
#include <plsdk/log.h>

PLEPAPER_VERSION;

static struct epdc_wfdef g_wflist[] = {
	{ PLEP_REFRESH,       NULL,      NULL,       0, 1000 },
	{ PLEP_REFRESH,       PLEP_MONO, NULL,       1,  500 },
	{ PLEP_DELTA,         NULL,      NULL,       2, 1000 },
	{ PLEP_DELTA,         PLEP_MONO, NULL,       3,  500 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_3SUBF, 4,   80 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_6SUBF, 5,  140 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_9SUBF, 6,  200 },
	{ PLEP_HIGHLIGHT,     PLEP_4GL,  NULL,       7,   80 },
};

PLEPAPER_MOD_INIT(epdc, dev)
{
	dev = NULL;
	epdc->description = "E-Paper test module";
	epdc->wflist = g_wflist;
	epdc->wflist_n = ARRAY_SIZE(g_wflist);

	return 0;
}
