/*
  E-Paper module support - Generic Plastic Logic ePDC interface

  Copyright (C) 2012 Plastic Logic Limited

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <linux/plepdc.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <libplepaper.h>

#define LOG_TAG "plepdc"
#include <plsdk/log.h>

PLEPAPER_VERSION;

/* default fixed B&W threshold assuming 16 grey levels
   ToDo: improve separation between image format and ePDC control
*/
#define PLEPDC_DEFAULT_BW_THRESHOLD 12

struct plepdc_priv {
	int fd;
};

static struct epdc_wfdef g_wflist[] = {
	{ PLEP_REFRESH,       NULL,      NULL,       0, 1000 },
	{ PLEP_REFRESH,       PLEP_MONO, NULL,       3,  500 },
	{ PLEP_DELTA,         NULL,      NULL,       2, 1000 },
	{ PLEP_DELTA,         PLEP_MONO, NULL,       1,  500 },
};

static const char *plepdc_default_dev = "/dev/epdc";

static void plepdc_free(struct epdc *epdc)
{
	struct plepdc_priv *p = epdc->priv;

	close(p->fd);
	free(p);
	free(epdc->dev);
}

static int plepdc_update(struct epdc *epdc, const struct plep_rect *area,
			 const struct epdc_wfdef *wf, int opts)
{
	struct plepdc_priv *p = epdc->priv;
	struct plepdc_update_area pl_area;

	pl_area.top = area->a.y;
	pl_area.left = area->a.x;
	pl_area.bottom = area->b.y;
	pl_area.right = area->b.x;
	pl_area.wfid = wf->epdc_id;
	pl_area.opts = 0;
	pl_area.bw_threshold = PLEPDC_DEFAULT_BW_THRESHOLD;

	if ((opts & PLEP_SYNC_UPDATE) || (opts & PLEP_WAIT_POWER_OFF))
		pl_area.opts |= PLEPDC_OPT_SYNC;

	if (opts & PLEP_BW_IMAGE)
		pl_area.opts |= PLEPDC_OPT_BW;

	if (ioctl(p->fd, PLEPDC_IO_UPDATE, &pl_area) < 0)
		return -1;

	if (opts & PLEP_WAIT_POWER_OFF)
		if (ioctl(p->fd, PLEPDC_IO_WAIT_POWER, PLEPDC_POWER_OFF) < 0)
			return -1;

	return 0;
}

static int plepdc_wait_power(struct epdc *epdc, int state)
{
	struct plepdc_priv *p = epdc->priv;
	enum plepdc_power_state plepdc_state =
		state ? PLEPDC_POWER_ON : PLEPDC_POWER_OFF;

	return ioctl(p->fd, PLEPDC_IO_WAIT_POWER, plepdc_state);
}

PLEPAPER_MOD_INIT(epdc, dev)
{
	struct plepdc_priv *p;
	struct plepdc_stats stats;

	p = malloc(sizeof(struct plepdc_priv));

	if (p == NULL)
		goto err_exit;

	if (dev == NULL)
		dev = plepdc_default_dev;

	epdc->dev = strdup(dev);

	if (epdc->dev == NULL)
		goto err_free_p;

	epdc->priv = p;
	epdc->description = "Plastic Logic ePDC control interface";
	epdc->wflist = g_wflist;
	epdc->wflist_n = ARRAY_SIZE(g_wflist);
	epdc->free = plepdc_free;
	epdc->update = plepdc_update;
	epdc->wait_power = plepdc_wait_power;
	epdc->can_sync = 1;

	p->fd = open(dev, O_RDWR);

	if (p->fd < 0)
		goto err_free_p;

	if (ioctl(p->fd, PLEPDC_IO_GET_STATS, &stats) < 0)
		goto err_close_fd;

	epdc->xres = stats.xres;
	epdc->yres = stats.yres;

	return 0;

err_close_fd:
	close(p->fd);
err_free_p:
	free(p);
err_exit:
	return -1;
}
