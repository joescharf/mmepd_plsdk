/*
  E-Paper module support - Epson S1D135x1 fb ePDC driver

  Copyright (C) 2012 Plastic Logic Limited

      John Long <john.long@plasticlogic.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <linux/fb.h>
#include <sys/ioctl.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <libplepaper.h>
#include <stdio.h>
#include <errno.h>

#define LOG_TAG "epson"
#include <plsdk/log.h>

PLEPAPER_VERSION;

struct epson_priv {
	int fd;
	FILE *fpcontrol;
};

static struct epdc_wfdef g_wflist[] = {
	{ PLEP_REFRESH,	NULL,		NULL,	1,	660 },
	{ PLEP_REFRESH, PLEP_MONO,      NULL,   4,      340 },
	{ PLEP_DELTA,   NULL,           NULL,   3,      660 },
	{ PLEP_DELTA,	PLEP_MONO,	NULL,	2,	340 },
};

#ifdef ANDROID
static const char default_fb[] = "/dev/graphics/fb0";
#else
static const char default_fb[] = "/dev/fb0";
#endif
static const char ep_control[] = "/sys/devices/platform/modelffb/control";
static const char ep_opt[] = "/sys/devices/platform/modelffb/opt";

static int wait_sync_update(struct epson_priv *p, const char *status);
static int wait_sync_power(struct epson_priv *p, int state);

static void epson_free(struct epdc *e)
{
	struct epson_priv *p = e->priv;

	if (e->dev != NULL)
		free(e->dev);

	fclose(p->fpcontrol);
	close(p->fd);
	free(p);
}

static int epson_set_opt(struct epdc *e, enum plep_hw_opt opt, int value)
{
	static const char *opt_name_table[_PLEP_HW_OPT_N_] = {
		[PLEP_POWER_OFF_DELAY_MS] = NULL,
		[PLEP_CLEAR_ON_EXIT] = "clear_on_exit",
		[PLEP_TEMPERATURE] = "temperature",
		[PLEP_TEMPERATURE_AUTO] = "temperature_auto",
	};
	const char *opt_name;
	FILE *sysfsfile = NULL;
	char sysfspath[64];
	int ret;

	if (opt >= _PLEP_HW_OPT_N_) {
		LOG("Invalid hardware option id: %d", opt);
		return -1;
	}

	opt_name = opt_name_table[opt];

	if (opt_name == NULL) {
		LOG("Unsupported hardware option id: %d", opt);
		return -1;
	}

	snprintf(sysfspath, sizeof(sysfspath), "%s/%s", ep_opt, opt_name);
	sysfsfile = fopen(sysfspath, "w");

	if (sysfsfile == NULL) {
		LOG_ERRNO("Failed to open sysfs file %s", sysfspath);
		return -1;
	}

	if (fprintf(sysfsfile, "%d\n", value) < 0) {
		LOG_ERRNO("Failed to write to sysfs opt %s", sysfspath);
		ret = -1;
	} else {
		fflush(sysfsfile);
		ret = 0;
	}

	fclose(sysfsfile);

	return ret;
}

static int epson_get_opt(struct epdc *e, enum plep_hw_opt opt, int *value)
{
	static const char *opt_name_table[_PLEP_HW_OPT_N_] = {
		[PLEP_POWER_OFF_DELAY_MS] = NULL,
		[PLEP_CLEAR_ON_EXIT] = "clear_on_exit",
		[PLEP_TEMPERATURE] = "temperature",
		[PLEP_TEMPERATURE_AUTO] = "temperature_auto",
	};
	const char *opt_name;
	FILE *sysfsfile = NULL;
	char sysfspath[64];
	char result[32];
	int ret;

	if (opt >= _PLEP_HW_OPT_N_) {
		LOG("Invalid hardware option id: %d", opt);
		return -1;
	}

	opt_name = opt_name_table[opt];

	if (opt_name == NULL) {
		LOG("Unsupported hardware option id: %d", opt);
		return -1;
	}

	snprintf(sysfspath, sizeof(sysfspath), "%s/%s", ep_opt, opt_name);
	sysfsfile = fopen(sysfspath, "r");

	if (sysfsfile == NULL) {
		LOG_ERRNO("Failed to open sysfs file %s", sysfspath);
		return -1;
	}

	if (fgets(result, sizeof(result), sysfsfile) == NULL) {
		LOG_ERRNO("Error reading sysfs file %s", sysfspath);
		ret = -1;
	} else {
		*value = atoi(result);
		ret = 0;
	}

	fclose(sysfsfile);

	return ret;
}

static int epson_update(struct epdc *e, const struct plep_rect *a,
			const struct epdc_wfdef *wf, int opts)
{
	struct epson_priv *p = e->priv;
	int width;
	int height;

	if (opts & PLEP_SYNC_UPDATE)
		if (wait_sync_update(p, "idle") < 0)
			return -1;

	if (opts & PLEP_WAIT_POWER_OFF)
		if (wait_sync_power(p, 0) < 0)
			return -1;

	width = a->b.x - a->a.x;
	height = a->b.y - a->a.y;

	/* use mode where S1D135x1 ignores unchanged pixels */
	if (fprintf(p->fpcontrol, "cleanup %d %d %d %d %d\n",
		    wf->epdc_id, a->a.x, a->a.y, width, height) < 0) {
		LOG("Failed to write to control: %s", strerror(errno));
		return -1;
	}

	fflush(p->fpcontrol);

	/* Note: There is a tiny possibility for race condition here if the
	 * display update finishes before this call gets into the driver.  Safe
	 * logic needs to be implemented into the Epson driver but that's more
	 * intrusive.  */
	if ((opts & PLEP_SYNC_UPDATE) || (opts & PLEP_WAIT_POWER_OFF))
		if (wait_sync_update(p, "busy") < 0)
			return -1;

	if (opts & PLEP_WAIT_POWER_OFF)
		if (wait_sync_power(p, 0) < 0)
			return -1;

	return 0;
}

static int epson_wait_power(struct epdc *e, int state)
{
	struct epson_priv *p = e->priv;

	return wait_sync_power(p, state);
}

PLEPAPER_MOD_INIT(epdc, dev)
{
	struct fb_fix_screeninfo finfo;
	struct fb_var_screeninfo vinfo;
	struct epson_priv *p;

	p = malloc(sizeof (struct epson_priv));

	if (p == NULL)
		goto err_exit;

	if (dev == NULL)
		dev = default_fb;

	epdc->priv = p;
	epdc->description = "Epson S1D135x1 frame buffer driver";
	epdc->wflist = g_wflist;
	epdc->wflist_n = ARRAY_SIZE(g_wflist);
	epdc->free = epson_free;
	epdc->set_opt = epson_set_opt;
	epdc->get_opt = epson_get_opt;
	epdc->update = epson_update;
	epdc->wait_power = epson_wait_power;
	epdc->can_sync = 1;

	p->fd = open(dev, O_RDWR);

	if (p->fd < 0)
		goto err_free_p;

	if (ioctl(p->fd, FBIOGET_FSCREENINFO, &finfo))
		goto err_close_fd;

	if (ioctl(p->fd, FBIOGET_VSCREENINFO, &vinfo))
		goto err_close_fd;

	if (strcmp(finfo.id, "modelffb"))
		goto err_close_fd;

	p->fpcontrol = fopen(ep_control, "w");

	if (p->fpcontrol == NULL)
		goto err_close_fd;

	epdc->dev = strdup(ep_control);
	epdc->xres = vinfo.xres;
	epdc->yres = vinfo.yres;

	return 0;

err_close_fd:
	close(p->fd);
err_free_p:
	free(p);
err_exit:

	return -1;
}

/* ----------------------------------------------------------------------------
 * private functions
 */

static int wait_sync_update(struct epson_priv *p, const char *status)
{
	if (fprintf(p->fpcontrol, "sync %s\n", status) < 0) {
		LOG("Failed to write to control: %s", strerror(errno));
		return -1;
	}

	fflush(p->fpcontrol);

	return 0;
}

static int wait_sync_power(struct epson_priv *p, int state)
{
	if (fprintf(p->fpcontrol, "power %s\n", state ? "on" : "off") < 0) {
		LOG("Failed to write to control: %s", strerror(errno));
		return -1;
	}

	fflush(p->fpcontrol);

	return 0;
}
