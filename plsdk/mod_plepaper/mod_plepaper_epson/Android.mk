LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CFLAGS := -Wall -O2
LOCAL_SRC_FILES := epdc-epson.c
LOCAL_MODULE := mod_plepaper_epson
LOCAL_MODULE_FILENAME := mod_plepaper_epson
LOCAL_MODULE_TAGS := eng
LOCAL_PRELINK_MODULE := false
LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../libplepaper \
	$(LOCAL_PATH)/../../libplutil

include $(BUILD_SHARED_LIBRARY)
