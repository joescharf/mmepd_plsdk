LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_CFLAGS := -Wall -O2
LOCAL_SRC_FILES := epdc-imx.c
LOCAL_MODULE := mod_plepaper_imx
LOCAL_MODULE_FILENAME := mod_plepaper_imx
LOCAL_MODULE_TAGS := eng
LOCAL_PRELINK_MODULE := false
LOCAL_C_INCLUDES := \
	$(LOCAL_PATH)/../../libplepaper \
	$(LOCAL_PATH)/../../libplutil
ifndef PLASTICLOGIC_NDK_BUILD
LOCAL_C_INCLUDES += external/pl-kernel-headers
else
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../pl-kernel-headers
endif
include $(BUILD_SHARED_LIBRARY)
