/*
  E-Paper module support - i.MX508 mxcfb ePDC driver

  Copyright (C) 2012 Plastic Logic Limited

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <linux/mxcfb.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <libplepaper.h>

#define LOG_TAG "imx"
#include <plsdk/log.h>

PLEPAPER_VERSION;

struct imx_priv {
	int fd;
};

static struct epdc_wfdef g_wflist[] = {
	{ PLEP_REFRESH,       NULL,      NULL,       2,  800 },
	{ PLEP_REFRESH,       PLEP_MONO, NULL,       1,  400 },
	{ PLEP_DELTA,         NULL,      NULL,       3,  800 },
	{ PLEP_DELTA,         PLEP_MONO, NULL,       4,  400 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_3SUBF, 7,  100 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_6SUBF, 6,  160 },
	{ PLEP_FAST,          PLEP_MONO, PLEP_9SUBF, 5,  220 },
	{ PLEP_HIGHLIGHT,     PLEP_4GL,  NULL,       8,  100 },
};

#ifdef ANDROID
static const char *default_fb0 = "/dev/graphics/fb0";
#else
static const char *default_fb0 = "/dev/fb0";
#endif

static void imx_free(struct epdc *e)
{
	struct imx_priv *p = e->priv;

	if (e->dev != NULL)
		free(e->dev);

	close(p->fd);
	free(p);
}

static int imx_set_opt(struct epdc *e, enum plep_hw_opt opt, int value)
{
	static const unsigned long int ioctl_table[_PLEP_HW_OPT_N_] = {
		[PLEP_POWER_OFF_DELAY_MS] = MXCFB_SET_PWRDOWN_DELAY,
		[PLEP_CLEAR_ON_EXIT] = 0,
		[PLEP_TEMPERATURE] = MXCFB_SET_TEMPERATURE,
		[PLEP_TEMPERATURE_AUTO] = MXCFB_SET_AUTO_TEMPERATURE_MODE,
	};
	const unsigned long int ioctl_cmd = ioctl_table[opt];
	struct imx_priv *p = e->priv;
	int32_t val32 = value;

	if (!ioctl_cmd) {
		LOG("Unsupported hardware option id: %d", opt);
		return -1;
	}

	return ioctl(p->fd, ioctl_cmd, &val32);
}

static int imx_get_opt(struct epdc *e, enum plep_hw_opt opt, int *value)
{
	static const unsigned long int ioctl_table[_PLEP_HW_OPT_N_] = {
		[PLEP_POWER_OFF_DELAY_MS] = MXCFB_GET_PWRDOWN_DELAY,
		[PLEP_CLEAR_ON_EXIT] = 0,
		[PLEP_TEMPERATURE] = MXCFB_GET_TEMPERATURE,
		[PLEP_TEMPERATURE_AUTO] = MXCFB_GET_AUTO_TEMPERATURE_MODE,
	};
	const unsigned long int ioctl_cmd = ioctl_table[opt];
	struct imx_priv *p = e->priv;
	int32_t val32;

	if (!ioctl_cmd) {
		LOG("Unsupported hardware option id: %d", opt);
		return -1;
	}

	if (ioctl(p->fd, ioctl_cmd, &val32))
		return -1;

	*value = val32;

	return 0;
}

static int imx_update(struct epdc *e, const struct plep_rect *a,
		      const struct epdc_wfdef *wf, int opts)
{
	struct imx_priv *p = e->priv;
	struct mxcfb_update_data data;

	data.update_region.top = a->a.y;
	data.update_region.left = a->a.x;
	data.update_region.width = a->b.x - a->a.x;
	data.update_region.height = a->b.y - a->a.y;
	data.waveform_mode = wf->epdc_id;
	data.update_mode =
		(opts & PLEP_PARTIAL) ? UPDATE_MODE_PARTIAL : UPDATE_MODE_FULL;
	data.update_marker = 0;
	data.temp = 24; /*TEMP_USE_AMBIENT;*/
	data.flags = 0;

	while (ioctl(p->fd, MXCFB_SEND_UPDATE, &data)) {
		if (errno == -EAGAIN) {
			LOG("update retry");
			usleep(50000);
		} else {
			LOG("SEND_UPDATE error: %d %s",
			    errno, strerror(errno));
			return -1;
		}
	}

	return 0;
}

PLEPAPER_MOD_INIT(epdc, dev)
{
	struct fb_fix_screeninfo finfo;
	struct fb_var_screeninfo vinfo;
	struct imx_priv *p;

	p = malloc(sizeof (struct imx_priv));

	if (p == NULL)
		goto err_exit;

	if (dev == NULL)
		dev = default_fb0;

	epdc->dev = strdup(dev);

	if (epdc->dev == NULL)
		goto err_free_p;

	epdc->priv = p;
	epdc->description = "Freescale i.MX508 frame buffer driver";
	epdc->wflist = g_wflist;
	epdc->wflist_n = ARRAY_SIZE(g_wflist);
	epdc->free = imx_free;
	epdc->log_info = NULL;
	epdc->set_opt = imx_set_opt;
	epdc->get_opt = imx_get_opt;
	epdc->update = imx_update;
	epdc->wait_power = NULL;
	epdc->can_sync = 0;

	p->fd = open(dev, O_RDWR);

	if (p->fd < 0)
		goto err_free_p;

	if (ioctl(p->fd, FBIOGET_FSCREENINFO, &finfo))
		goto err_close_fd;

	if (strncmp(finfo.id, "mxc_epdc_fb", 16))
		goto err_close_fd;

	if (ioctl(p->fd, FBIOGET_VSCREENINFO, &vinfo))
		goto err_close_fd;

	epdc->xres = vinfo.xres;
	epdc->yres = vinfo.yres;

	return 0;

err_close_fd:
	close(p->fd);
err_free_p:
	free(p);
err_exit:

	return -1;
}
