# Python package for the Plastic Logic SDK - plsdk.inputdev
#
# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import _plsdk

class Touchscreen(_plsdk.Input):
    pass # ToDo


class Keyboard(_plsdk.Input):
    pass # ToDo

# -----------------------------------------------------------------------------
# Dummy input devices

class DummyInput(object):
    def _set_sys_path(self):
        ev_name = self.dev.split('/dev/input/')[1]
        self._sys_path = '/sys/class/input/%s/device' % ev_name

    def _write_attribute(self, name, value):
        attr = open('/'.join([self._sys_path, name]), 'a')
        attr.write(value)
        attr.close()

    def _read_attribute(self, name):
        attr = open('/'.join([self._sys_path, name]), 'r')
        value = attr.read()
        attr.close()
        return value

    def sync(self):
        self._write_attribute('sync', '1')


class DummyTouchscreen(Touchscreen, DummyInput):
    def __init__(self, *args, **kw):
        kw.setdefault('regex', "Dummy touch")
        super(DummyTouchscreen, self).__init__(*args, **kw)
        self._set_sys_path()

    def get_x(self):
        return int(self._read_attribute('x'))

    def set_x(self, value):
        self._write_attribute('x', str(value))

    x = property(get_x, set_x)

    def get_y(self):
        return int(self._read_attribute('y'))

    def set_y(self, value):
        self._write_attribute('y', str(value))

    y = property(get_y, set_y)

    def get_on(self):
        return bool(int(self._read_attribute('touch')))

    def set_on(self, value):
        if value:
            str_value = '1'
        else:
            str_value = '0'
        self._write_attribute('touch', str_value)

    on = property(get_on, set_on)


class DummyKeyboard(Keyboard, DummyInput):
    def __init__(self, *args, **kw):
        kw.setdefault('regex', "Dummy key")
        super(DummyKeyboard, self).__init__(*args, **kw)
        self._set_sys_path()

    def get_key(self):
        return int(self._read_attribute('key'))

    def set_key(self, value):
        return self._write_attribute('key', str(value))

    key = property(get_key, set_key)

    def get_on(self):
        return bool(int(self._read_attribute('on')))

    def set_on(self, value):
        if value:
            str_value = '1'
        else:
            str_value = '0'
        self._write_attribute('on', str_value)

    on = property(get_on, set_on)
