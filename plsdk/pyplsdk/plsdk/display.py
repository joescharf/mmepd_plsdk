# Python package for the Plastic Logic SDK - plsdk.display
#
# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# Quick back-port of the `wraps' decorator if not available (Python 2.4)
try:
    from functools import wraps
except ImportError:
    def wraps(src):
        def newfunc(dest):
            dest.__module__ = src.__module__
            dest.__name__ = src.__name__
            dest.__doc__ = src.__doc__
            d = getattr(src, '__dict__', {})
            if hasattr(dest, '__dict__'):
                dest.__dict__.update(d)
            else:
                dest.__dict__ = d.copy()
            return dest
        return newfunc


import _plsdk


class Display(object):

    """E-Paper Display, mixing framebuffer graphics and e-paper control.

    This class is composed of a *framebuffer* object
    :py:attr:`plsdk.Display.fb` and an *e-paper* object
    :py:attr:`plsdk.Display.ep`, which are typically instances of
    :py:class:`_plsdk.Buffer` and :py:class:`_plsdk.Epdc` respectively.  They
    form the basis of this ``Display`` class, which adds more functionality on
    top of them.  The main purpose is to make it easy to **mix graphical
    operations with e-paper control**.

    Even though you can use drawing methods of :py:class:`_plsdk.Buffer` and
    e-paper control methods of :py:class:`_plsdk.Epdc` or make derived classes
    from them, the standard way of using them is via the ``Display`` class.

    Each drawing method is decorated with :py:meth:`plsdk.Display.drawing`
    which, if :py:attr:`plsdk.Display.auto_update` is enabled, then calls
    :py:meth:`plsdk.Display.update` with any extra keyword arguments such as
    ``wf`` or ``synchro``.  These drawing methods are:

        :py:meth:`plsdk.Display.pixel` to draw a single pixel

        :py:meth:`plsdk.Display.rectangle` to draw a rectangle

        :py:meth:`plsdk.Display.clear` to clear the screen

        :py:meth:`plsdk.Display.line` to draw a line

        :py:meth:`plsdk.Display.text` to draw a string of text

    .. note:: Coordinates conventions

        The coordinates for **rectangles** and **lines** used in the
        ``Display`` methods are represented by a 4-tuple:

        ``(x0, y0, x1, y1)``

        For **rectangles**, if ``(x1 < x0)`` or ``(y1 < y0)`` then they are
        automatically swapped to guarantee that ``(x0, y0)`` is the top-left
        corner and ``(x1, y1)`` is the bottom-right corner.  The convention is
        to include the pixels within the area but excluding the bottom-right
        coordinates.  For example, ``(0, 0, 1, 1)`` represents a 1x1
        rectangular area (i.e. a single pixel).  If ``(x0 == y0)`` or ``(x1 ==
        y1)`` then the rectangle is *flat*, its surface is 0 so usually nothing
        happens on the display.

        For **lines**, the convention is slightly different.  All coordinates
        are inclusive, mainly for practical reasons.  So for example:

            single pixel: ``(1, 1, 1, 1)``

            10-pixels long horizontal line: ``(5, 5, 15, 5)``
    """

    _update_kw_list = ['power_off', 'synchro', 'bw_thr']

    def __init__(self, fb=None, ep=None, fb_kw={}, ep_kw={}):
        """**Display arguments**

        The ``fb`` argument can be used to override the framebuffer instance.
        By default, a :py:class:`_plsdk.Buffer` object is created.  The
        ``fb_kw`` dictionary can be used to pass keyword arguments to the
        framebuffer class such as ``device`` and ``cfa`` for ``Buffer``.

        Similarly, the ``ep`` argument can be used to override the e-paper
        instance.  By default, a :py:class:`_plsdk.Epdc` object is created with
        keyword arguments in ``ep_kw`` such as ``device`` and ``mode``.
        """
        if fb is None:
            self._fb = _plsdk.Buffer(**fb_kw)
        else:
            self._fb = fb

        if ep is None:
            self._ep = _plsdk.Epdc(**ep_kw)
        else:
            self._ep = ep

        self._fb.set_epdc(self._ep)
        self._wf = None
        self._fg = None
        self._bg = None
        self._auto_update = True

    @property
    def fb(self):
        """Framebuffer instance, usually a :py:class:`_plsdk.Buffer` object."""
        return self._fb

    @property
    def ep(self):
        """E-Paper instance, usually a :py:class:`_plsdk.Epdc` object."""
        return self._ep

    @property
    def full(self):
        """A 4-tuple with the full screen rectangle coordinates."""
        return (0, 0, self._fb.xres, self._fb.yres)

    def get_fg(self):
        if self._fg is None:
            return self.fb.black
        else:
            return self._fg

    def set_fg(self, value):
        self._fg = value

    fg = property(get_fg, set_fg)
    """Default foreground colour used in drawing operations, initialised to
    black."""

    def get_bg(self):
        if self._bg is None:
            return self.fb.white
        else:
            return self._bg

    def set_bg(self, value):
        self._bg = value

    bg = property(get_bg, set_bg)
    """Default background colour used in drawing operations, initialised to
    white."""

    def get_wf(self):
        if self._wf is None:
            return self.ep.refresh
        else:
            return self._wf

    def set_wf(self, value):
        self._wf = value

    wf = property(get_wf, set_wf)
    """Default waveform used in e-paper updates, initialised to ``refresh``."""

    def get_auto_update(self):
        return self._auto_update

    def set_auto_update(self, value):
        if not isinstance(value, bool):
            raise TypeError("auto_update needs to be a boolean")
        self._auto_update = value

    auto_update = property(get_auto_update, set_auto_update)
    """Enable automatic updates.

    This is set to ``True`` by default.  When a drawing operation is performed,
    the corresponding display area is then updated with either a specified
    waveform (``wf`` argument) or the default :py:attr:`plsdk.Display.wf`.
    """

    def update(self, area=None, wf=None, **kw):
        """Request an e-paper update.

        The ``area`` must be a 4-tuple with the coordinates of the rectangle to
        update.  If not specified, the full screen is updated.  As a
        convenience, It can also contain a ``list`` of 4-tuples to update a
        series of areas one after the other.

        The ``wf`` argument specifies the name of the waveform to use, or
        :py:attr:`plsdk.Display.wf` if ``None``.

        Some additional e-paper update options can be passed as keyword
        arguments (see :py:meth:`_plsdk.Epdc.update`):

            power_off: :py:attr:`_plsdk.Epdc.power_off`

            synchro: :py:attr:`_plsdk.Epdc.synchro`

            bw: :py:attr:`_plsdk.Epdc.bw`

            partial: :py:attr:`_plsdk.Epdc.partial`

        This method returns the update area rectangle coordinates for chaining
        operations.
        """
        if area is None:
            area = self.full
        if wf is None:
            wf = self.wf
        if wf is not None:
            if isinstance(area, list):
                for a in area:
                    self.ep.update(wf, a, **kw)
            else:
                self.ep.update(wf, area, **kw)
        return area

    def drawing(f):
        """Decorator to automate e-paper updates with drawing operations.

        Drawing methods decorated with ``drawing`` must return rectangle
        coordinates that are then passed on to :py:meth:`plsdk.Display.update`
        to automatically request a display update.  This only happens if
        :py:attr:`plsdk.Display.auto_update` is set to ``True``.

        The ``wf`` argument is taken from the keyword dictionary and specifies
        the waveform identifier to use.  If ``None``, the default
        :py:attr:`plsdk.Display.wf` is used.

        The update rectangle coordinates are then returned again for chaining
        operations.
        """
        @wraps(f)
        def drawing_decorator(self, *args, **kw):
            wf = kw.pop('wf', None)
            update_kw = dict()
            for key in self._update_kw_list:
                value = kw.pop(key, None)
                if value is not None:
                    update_kw[key] = value
            area = f(self, *args, **kw)
            if self.auto_update is True:
                self.update(area=area, wf=wf, **update_kw)
            return area
        return drawing_decorator

    # Note: The Sphinx autodoc doesn't pick up the decorated method signatures
    # so they need to be manually defined in the first docstring line.

    @drawing
    def pixel(self, (x, y), col=None):
        """pixel((x, y), col=None, **kw)

        Set a pixel value, return the corresponding 1x1 update area.

        The ``(x, y)`` 2-tuple contains the coordinates of the pixel, and
        ``col`` contains the colour to use.  By default, the foreground colour
        :py:attr:`plsdk.Display.fg` is used.
        """
        if col is None:
            col = self.fg
        self.fb.set_pixel(col, (x, y))
        return (x, y, (x + 1), (y + 1))

    @drawing
    def rectangle(self, area=None, col=None):
        """rectangle(area=None, col=None, **kw)

        Fill a solid rectangle, return the corresponding update area.

        The ``area`` argument contains the coordinates of the rectangle to fill
        with the colour specified by ``col``.  By default, the foreground
        colour :py:attr:`plsdk.Display.fg` is used and the full screen is
        filled.
        """
        if col is None:
            col = self.fg
        if area is None:
            area = self.full
        self.fb.fill_rect(col, area)
        return area

    @drawing
    def line(self, coords, col=None):
        """line(coords, col=None, **kw)

        Draw a straight thin line, return the corresponding update area.

        The ``coords`` argument contains the beginning and end coordinates of
        the line which are inclusive.  The ``col`` argument contains the colour
        to use when drawing the line.  By default, the foreground colour
        :py:attr:`plsdk.Display.fg` is used.

        The line width is always 1 pixel.

        """
        if col is None:
            col = self.fg
        self.fb.draw_line(col, coords)
        return (coords[0], coords[1], (coords[2] + 1), (coords[3] + 1))

    @drawing
    def text(self, text, offset, fg_col=None, bg_col=None, font=None):
        """text(text, offset=None, fg_col=None, bg_col=None, **kw)

        Draw a string of text, return the corresponding update area.

        The ``text`` argument is the string to be printed on the display.  No
        escape characters are handled by this method, only the basic ASCII
        alphanumeric characters, symbols and single space.  The text line will
        not wrap on the edge of the display; it is treated as a purely
        graphical operation.

        The ``offset`` argument is a ``(x, y)`` tuple with the starting point
        coordinates.

        The ``fg_col`` and ``bg_col`` arguments are the foreground and
        background colours respectively.  The text cannot be drawn in a
        *transparent* way, and if not specified the default
        :py:attr:`plsdk.Display.fg` and :py:attr:`plsdk.Display.bg` will be
        used.

        Finally, the ``font`` argument can contain a dictionary used to select
        a font.  The dictionary may contain ``name`` and ``size``, for
        example::

            d.text("Hello", (50, 50), font={ 'name': "Lucida", 'size': 14 })

        If not specified, the default font ``Courier 10`` will be used.  All
        fonts are mono-space so each character occupies the same space on the
        display.  Only 2 free mono-space built-in fonts are currently
        available: ``Courier`` and ``Lucida`` each with the following sizes: 8,
        10, 12, 14, 18, 24.  They were imported from the `X.Org freedesktop.org
        <http://xorg.freedesktop.org/releases/individual/font/>`_ project.
        """
        if fg_col is None:
            fg_col = self.fg
        if bg_col is None:
            bg_col = self.bg
        return self.fb.draw_text(fg_col, bg_col, offset, text, font)

    def clear(self, col=None, wf=None, synchro=True, **kw):
        """Clear the full screen, return the corresponding update area.

        This is equivalent to filling the screen with
        :py:meth:`plsdk.Display.rectangle` except in this case the default
        waveform is always *refresh* and the default colour is always *white*.
        These can be manually specified with the ``wf`` and ``col`` arguments
        respectively.  Also the ``synchro`` e-paper update option is enabled by
        default.
        """
        if col is None:
            col = self._fb.white
        if wf is None:
            wf = self._ep.refresh
        return self.rectangle(area=self.full, col=col, wf=wf,
                              synchro=synchro, **kw)
