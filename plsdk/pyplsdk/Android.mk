LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := _plsdk
LOCAL_SRC_FILES := _plsdk.c
LOCAL_MODULE_TAGS := eng
LOCAL_CFLAGS += -Wall -O2 -fno-strict-aliasing
LOCAL_C_INCLUDES += \
	$(LOCAL_PATH)/../python-headers/python2.4 \
	$(LOCAL_PATH)/../libplepaper \
	$(LOCAL_PATH)/../libpldraw \
	$(LOCAL_PATH)/../libplinput
LOCAL_STATIC_LIBRARIES := libplepaper libpldraw libplinput libplutil libeglib
LOCAL_LDLIBS += $(LOCAL_PATH)/../prebuilt/p4a-2.6.2/libpython2.6.so
LOCAL_PRELINK_MODULE := false
include $(BUILD_SHARED_LIBRARY)
