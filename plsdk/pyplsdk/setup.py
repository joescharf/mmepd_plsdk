# Python package for the Plastic Logic SDK - plsdk - setup.py
#
# Copyright (C) 2013 Plastic Logic Limited
#
#     Guillaume Tucker <guillaume.tucker@plasticlogic.com>
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from distutils.core import setup, Extension

lib_dir = '../out/x86_64/lib'
static_libs = ['{0}/lib{1}.a'.format(lib_dir, lib)
               for lib in ['plutil', 'plepaper', 'pldraw', 'plinput']]

setup(name="plsdk",
      version='1.6',
      description="Plastic Logic Software Development Kit",
      author="Guillaume Tucker",
      author_email="guillaume.tucker@plasticlogic.com",
      packages=['plsdk'],
      ext_modules=[Extension(
            "_plsdk", ["_plsdk.c"],
            include_dirs=['../libplutil', '../libpldraw', '../libplepaper',
                          '../libplinput'],
            extra_objects=static_libs)])
