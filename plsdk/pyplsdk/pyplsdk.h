/*
  Python extension for the Plastic Logic SDK - _plsdk

  Copyright (C) 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INCLUDE_PYPLSDK_MODULE_H
#define INCLUDE_PYPLSDK_MODULE_H 1

#include <Python.h>

#if (PY_VERSION_HEX < 0x02070000)
# define PYPLSDK_USE_COBJ 1
#endif

/* Pre-inquisition */
#if (PY_VERSION_HEX < 0x02050000)
# define PYPLSDK_OLD_PYTHON 1
#endif

#define PYPLSDK_MODULE_NAME "_plsdk"
#define PYPLSDK_API_NAME "_C_API"
#define PYPLSDK_API_FULL_NAME PYPLSDK_MODULE_NAME"."PYPLSDK_API_NAME

struct plep;
struct pldraw;
struct plinput;

struct pyplsdk_api {
	struct plep *(*Epdc_get_plep)(PyObject *plep_obj);
	struct pldraw *(*Buffer_get_pldraw)(PyObject *buffer_obj);
	struct plinput *(*Input_get_plinput)(PyObject *input_obj);
};

#ifndef PYPLSDK_MODULE

#if PYPLSDK_USE_COBJ

/* -- Use the PyCObject -- */

static const struct pyplsdk_api *pyplsdk_get_api(void)
{
	PyObject *m;
	PyObject *c_api_obj;
	struct pyplsdk_api *api;

	api = NULL;
	m = PyImport_ImportModule(PYPLSDK_MODULE_NAME);

	if (m == NULL) {
		PyErr_SetString(PyExc_ImportError, "failed to import plsdk");
		return NULL;
	}

	c_api_obj = PyObject_GetAttrString(m, PYPLSDK_API_NAME);

	if (c_api_obj == NULL) {
		PyErr_SetString(PyExc_AttributeError,
				"failed to get PLSDK C API");
		Py_DECREF(m);
		return NULL;
	}

	if (!PyCObject_Check(c_api_obj)) {
		PyErr_SetString(PyExc_TypeError, "invalid plsdk C API object");
		Py_DECREF(c_api_obj);
		Py_DECREF(m);
		return NULL;
	}

	api = PyCObject_AsVoidPtr(c_api_obj);

	Py_DECREF(c_api_obj);
	Py_DECREF(m);

	return api;
}

#else

/* -- Use the PyCapsule (Python 2.7 onwards) -- */

#define pyplsdk_get_api() \
	((const struct pyplsdk_api *)PyCapsule_Import(PYPLSDK_API_FULL_NAME,0))

#endif /* !PYPLSDK_USE_COBJ */

#endif /* PYPLSDK_MODULE */

#endif /* INCLUDE_PYPLSDK_MODULE_H */
