/*
  Python extension for the Plastic Logic SDK - _plsdk

  Copyright (C) 2013 Plastic Logic Limited

      Guillaume Tucker <guillaume.tucker@plasticlogic.com>

  This program is free software: you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published by
  the Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define PYPLSDK_MODULE 1
#include "pyplsdk.h"
#include <structmember.h>
#include <libpldraw.h>
#include <libplinput.h>

#if 1 /* set to 1 to enable crude log messages */
# define LOG(msg, ...) \
	fprintf(stderr, "[plsdk    %4i] "msg"\n", __LINE__, ##__VA_ARGS__)
#else
# define LOG(msg, ...)
#endif

/* ----------------------------------------------------------------------------
 * Forward declarations
 */

#ifdef PYPLSDK_OLD_PYTHON
typedef size_t Py_size_t;
typedef ssize_t Py_ssize_t;
#endif

static PyObject *plsdk_error;
static PyObject *plsdk_keycodes;
static PyObject *plsdk_palettes;
static PyObject *plsdk_fonts;

struct Buffer_object;
typedef struct Buffer_object Buffer;
static PyTypeObject BufferType;

struct Epdc_object;
typedef struct Epdc_object Epdc;
static PyTypeObject EpdcType;
static struct plep *Epdc_get_plep(Epdc *epdc);

struct Input_object;
typedef struct Input_object Input;
static PyTypeObject InputType;

#if PYPLSDK_OLD_PYTHON
static int plsdk_rect_from_tuple(struct plep_rect *r, PyObject *t);
#endif
static int plsdk_parse_rotation_angle(PyObject *value, int *angle);
static int plsdk_obj2long(PyObject *obj, long *value);


/* ----------------------------------------------------------------------------
 * Buffer class
 */

PyDoc_STRVAR(buffer_type_doc,
"Image buffer with drawing methods.\n"
"\n"
"This class can be used to draw simple things into a framebuffer such as "
"lines, pixels, rectangles and also mono-space text.  It currently only "
"supports standard Linux framebuffer devices with RGB888, RGB565 and RGB222 "
"pixel formats.  It is based on the ``libpldraw`` C library which is part of "
"the PLSDK.\n"
"\n"
".. note:: Colour values\n"
"\n"
"    For performance and portability reasons, the colours are packed into "
"    **32-bit values** following the current framebuffer pixel format.  These "
"    are  handled as Python ``int`` objects.  They can be built for a given "
"    **RGB** combination using the :py:meth:`_plsdk.Buffer.get_color` "
"    method, and for a given greylevel using "
"    :py:meth:`_plsdk.Buffer.get_grey`.  The return value can then be passed "
"    to other drawing methods.  Some shorthands are stored as instance "
"    attributes for common values: ``black``, ``white``, ``red``, ``green``, "
"    ``blue``, ``cyan``, ``magenta`` and ``yellow``.\n"
"\n"
".. note:: E-Paper Colour Filter Arrays\n"
"\n"
"    Colour on e-paper devices is usually achieved with a *Colour Filter "
"    Array* (CFA) applied on top of a regular monochrome display.  The CFA "
"    layout may vary from one display to another, so the ``Buffer`` class "
"    provides support for a variety of possibilities.  In most cases, the CFA "
"    contains red, green, blue and white filters which mean the resulting "
"    display resolution is divided by 4.  All the ``Buffer`` attributes "
"    linked to the CFA layout are automatically adjusted when switching to "
"    a different layout, in particular ``xres``, ``yres``, ``gl`` and the 8 "
"    common colours as previously listed.\n");

struct Buffer_object {
	PyObject_HEAD
	struct pldraw *pldraw;
	PyObject *palettes;
	PyObject *gl;
	PyObject *fonts;
	long black;
	long white;
	long red;
	long green;
	long blue;
	long cyan;
	long magenta;
	long yellow;
};

static int Buffer_init_attrs(void)
{
	PyObject *dict = BufferType.tp_dict;
	int i;

	for (i = 0; i < _PLDRAW_CFA_N_; ++i) {
		char cfa_name[64];

		snprintf(cfa_name, 64, "CFA_%s", pldraw_cfa_name[i]);

		if (PyDict_SetItemString(dict, cfa_name, PyLong_FromLong(i)))
			return -1;
	}

	return 0;
}

static PyObject *Buffer_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Buffer *self;

	self = (Buffer *)type->tp_alloc(type, 0);
	self->pldraw = NULL;
	self->palettes = NULL;
	self->gl = NULL;
	self->fonts = NULL;

	return (PyObject *)self;
}

static int Buffer_update_cached_colors(Buffer *self)
{
	int i;

	self->black = pldraw_get_grey(self->pldraw, 0x00).u32;
	self->white = pldraw_get_grey(self->pldraw, 0xFF).u32;
	self->red = pldraw_get_color(self->pldraw, 0xFF, 0x00, 0x00).u32;
	self->green =  pldraw_get_color(self->pldraw, 0x00, 0xFF, 0x00).u32;
	self->blue = pldraw_get_color(self->pldraw, 0x00, 0x00, 0xFF).u32;
	self->cyan = pldraw_get_color(self->pldraw, 0x00, 0xFF, 0xFF).u32;
	self->magenta = pldraw_get_color(self->pldraw, 0xFF, 0x00,0xFF).u32;
	self->yellow = pldraw_get_color(self->pldraw, 0xFF, 0xFF, 0x00).u32;

	if (self->gl == NULL) {
		self->gl = PyList_New(16);

		if (self->gl == NULL) {
			PyErr_NoMemory();
			return -1;
		}
	}

	for (i = 0; i < 16; ++i) {
		const uint8_t grey = pldraw_grey_16[i];
		const pldraw_color_t col = pldraw_get_grey(self->pldraw, grey);

		if (PyList_SetItem(self->gl, i,
				   PyLong_FromLong(col.u32)) < 0)
			return -1;
	}

	return 0;
}

static int Buffer_init(PyObject *obj, PyObject *args, PyObject *kw)
{
	static char *kwlist[] = { "device", "cfa", "config", NULL };
	Buffer *self = (Buffer *)obj;
	char *dev = NULL;
	PyObject *cfa = NULL;
	const char *conf_file = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kw, "|sO!s", kwlist, &dev,
					 &PyLong_Type, &cfa, &conf_file))
		return -1;

	self->pldraw = pldraw_init(dev, conf_file);

	if (self->pldraw == NULL) {
		PyErr_SetString(plsdk_error, "Failed to initialise pldraw");
		return -1;
	}

	if (cfa != NULL) {
		int cfa_int = PyLong_AsLong(cfa);

		if ((cfa_int >= _PLDRAW_CFA_N_) || (cfa_int < 0)) {
			PyErr_SetString(PyExc_ValueError,
					"Invalid CFA identifier");
			return -1;
		}

		pldraw_set_cfa(self->pldraw, cfa_int);
	}

	if (Buffer_update_cached_colors(self) < 0)
		return -1;

	Py_INCREF(plsdk_palettes);
	self->palettes = plsdk_palettes;
	Py_INCREF(plsdk_fonts);
	self->fonts = plsdk_fonts;

	return 0;
}

static void Buffer_dealloc(PyObject *obj)
{
	Buffer *self = (Buffer *)obj;

	if (self->pldraw != NULL) {
		pldraw_free(self->pldraw);
		self->pldraw = NULL;
	}

	if (self->palettes != NULL) {
		Py_DECREF(self->palettes);
		self->palettes = NULL;
	}

	if (self->gl != NULL) {
		Py_DECREF(self->gl);
		self->gl = NULL;
	}
}

/* -- Buffer methods -- */

PyDoc_STRVAR(buffer_set_epdc_doc,
"set_epdc(epdc)\n"
"\n"
"Set an associated E-Paper Display Controller object, typically a "
":py:class:`_plsdk.Epdc` instance.  This is useful to keep some parameters "
"synchronised such as rotation angle and scaling factors.\n");

static PyObject *BufferObj_set_epdc(Buffer *self, PyObject *args)
{
	PyObject *epdc;

	if (!PyArg_ParseTuple(args, "O!", &EpdcType, &epdc))
		return NULL;

	pldraw_set_plep(self->pldraw, Epdc_get_plep((Epdc *)epdc));

	Py_RETURN_NONE;
}

PyDoc_STRVAR(buffer_get_color_doc,
"get_color(red, green, blue)\n"
"\n"
"Return a colour value for the given ``red``, ``green`` and ``blue`` numbers "
"which should each be within the [0..255] range.  The returned value can "
"then be used with drawing methods such as ``fill_rect`` etc...\n");

static PyObject *BufferObj_get_color(Buffer *self, PyObject *args)
{
	int r, g, b;
	pldraw_color_t col;

	if (!PyArg_ParseTuple(args, "iii", &r, &g, &b))
		return NULL;

	col = pldraw_get_color(self->pldraw, r, g, b);

	return Py_BuildValue("l", col.u32);
}

PyDoc_STRVAR(buffer_get_grey_doc,
"get_grey(grey)\n"
"\n"
"Return a colour value for the given ``grey`` greylevel, which can then be "
"used with drawing methods.  The greylevel should be within the [0..255] "
"range.\n");

static PyObject *BufferObj_get_grey(Buffer *self, PyObject *args)
{
	int grey;
	pldraw_color_t col;

	if (!PyArg_ParseTuple(args, "i", &grey))
		return NULL;

	col = pldraw_get_grey(self->pldraw, grey);

	return Py_BuildValue("l", col.u32);
}

PyDoc_STRVAR(buffer_set_pixel_doc,
"set_pixel(colour, coordinates)\n"
"\n"
"Set a pixel with the given ``colour`` located at the given ``coordinates``. "
"The coordinates is a double with ``(x, y)`` values.\n");

static PyObject *BufferObj_set_pixel(Buffer *self, PyObject *args)
{
	struct plep_point pt;
	pldraw_color_t col;

	if (!PyArg_ParseTuple(args, "l(ii)", &col.u32, &pt.x, &pt.y))
		return NULL;

	pldraw_set_pixel(self->pldraw, col, &pt);

	Py_RETURN_NONE;
}

PyDoc_STRVAR(buffer_fill_screen_doc,
"fill_screen(colour)\n"
"\n"
"Fill the whole screen with the given ``colour``.\n");

static PyObject *BufferObj_fill_screen(Buffer *self, PyObject *args)
{
	pldraw_color_t col;

	if (!PyArg_ParseTuple(args, "l", &col.u32))
		return NULL;

	pldraw_fill_screen(self->pldraw, col);

	Py_RETURN_NONE;
}

PyDoc_STRVAR(buffer_fill_rect_doc,
"fill_rect(colour, area)\n"
"\n"
"Fill a rectangle with the given ``colour``.  The ``area`` must be a 4-tuple "
"containing the ``(x0, y0, x1, y1)`` coordinates of the rectangular "
"area to fill.\n");

static PyObject *BufferObj_fill_rect(Buffer *self, PyObject *args)
{
	struct plep_rect r;
	pldraw_color_t col;

	if (!PyArg_ParseTuple(args, "l(iiii)", &col.u32,
			      &r.a.x, &r.a.y, &r.b.x, &r.b.y))
		return NULL;

	pldraw_fill_rect(self->pldraw, col, &r);

	Py_RETURN_NONE;
}

PyDoc_STRVAR(buffer_draw_line_doc,
"draw_line(colour, coordinates)\n"
"\n"
"Draw a straight thin line with the given ``colour`` between the "
" ``coordinates`` from the first 2 ``(x, y)`` coordinates to the last 2 in "
"a 4-tuple: ``(from_x, from_y, to_x, to_y)``.\n");

static PyObject *BufferObj_draw_line(Buffer *self, PyObject *args)
{
	struct plep_rect line;
	pldraw_color_t col;

	if (!PyArg_ParseTuple(args, "l(iiii)", &col.u32,
			      &line.a.x, &line.a.y, &line.b.x, &line.b.y))
		return NULL;

	pldraw_draw_line(self->pldraw, col, &line);

	Py_RETURN_NONE;
}

PyDoc_STRVAR(buffer_draw_text_doc,
"draw_text(foreground, background, offset, text, font=None)\n"
"\n"
"Draw a string of ``text`` with the given ``foreground`` and ``background`` "
"colours starting at the ``offset`` coordinates.  An optional ``font`` "
"dictionary can be supplied to choose which font to use.\n");

static PyObject *BufferObj_draw_text(Buffer *self, PyObject *args)
{
	struct plep_point pt;
	char *txt;
	PyObject *font_obj = NULL;
	struct pldraw_pen pen;
	struct plep_rect area;

	if (!PyArg_ParseTuple(args, "ll(ii)s|O",
			      &pen.foreground, &pen.background,
			      &pt.x, &pt.y, &txt, &font_obj))
		return NULL;

	if ((font_obj == NULL) || (font_obj == Py_None)) {
		pen.font = pldraw_fonts[0];
	} else if (PyDict_Check(font_obj)) {
		PyObject *name_obj;
		PyObject *size_obj;
		const char *font_name;
		long font_size = 0;

		name_obj = PyDict_GetItemString(font_obj, "name");

		if (name_obj == NULL) {
			font_name = NULL;
		} else {
			font_name = PyString_AsString(name_obj);
		}

		if (font_name == NULL) {
			PyErr_SetString(PyExc_ValueError, "No font name");
			return NULL;
		}

		size_obj = PyDict_GetItemString(font_obj, "size");

		if (size_obj != NULL)
			if (plsdk_obj2long(size_obj, &font_size))
				return NULL;

		pen.font = pldraw_get_font(self->pldraw, font_name, font_size);

		if (pen.font == NULL) {
			PyErr_SetString(plsdk_error, "Font not found");
			return NULL;
		}
	} else {
		PyErr_SetString(PyExc_ValueError,
				"Font description must be a dictionary");
		return NULL;
	}

	pldraw_draw_text(self->pldraw, &pen, &pt, txt);

	area.a.x = pt.x;
	area.a.y = pt.y;
	area.b.x = pt.x + (strlen(txt) * pen.font->width);
	area.b.y = pt.y + pen.font->height;

	return Py_BuildValue("(iiii)", area.a.x, area.a.y, area.b.x, area.b.y);
}

static PyMethodDef Buffer_methods[] = {
	{ "set_epdc", (PyCFunction)BufferObj_set_epdc, METH_VARARGS,
	  buffer_set_epdc_doc },
	{ "get_color", (PyCFunction)BufferObj_get_color, METH_VARARGS,
	  buffer_get_color_doc },
	{ "get_grey", (PyCFunction)BufferObj_get_grey, METH_VARARGS,
	  buffer_get_grey_doc, },
	{ "set_pixel", (PyCFunction)BufferObj_set_pixel, METH_VARARGS,
	  buffer_set_pixel_doc },
	{ "fill_screen", (PyCFunction)BufferObj_fill_screen, METH_VARARGS,
	  buffer_fill_screen_doc },
	{ "fill_rect", (PyCFunction)BufferObj_fill_rect, METH_VARARGS,
	  buffer_fill_rect_doc },
	{ "draw_line", (PyCFunction)BufferObj_draw_line, METH_VARARGS,
	  buffer_draw_line_doc },
	{ "draw_text", (PyCFunction)BufferObj_draw_text, METH_VARARGS,
	  buffer_draw_text_doc },
	{ NULL }
};

/* -- Buffer getters & setters -- */

PyDoc_STRVAR(buffer_dev_doc,
"String with the framebuffer device name, typically ``/dev/fb0``.");

static PyObject *BufferObj_get_dev(Buffer *self, void *_)
{
	return PyString_FromString(pldraw_get_dev(self->pldraw));
}

PyDoc_STRVAR(buffer_xres_doc,
"Horizontal resolution of the buffer.");

static PyObject *BufferObj_get_xres(Buffer *self, void *_)
{
	return PyInt_FromLong((long)pldraw_get_xres(self->pldraw));
}

PyDoc_STRVAR(buffer_yres_doc,
"Vertical resolution of the buffer.");

static PyObject *BufferObj_get_yres(Buffer *self, void *_)
{
	return PyInt_FromLong((long)pldraw_get_yres(self->pldraw));
}

PyDoc_STRVAR(buffer_rotation_doc,
"Display rotation in degrees, must be a multiple of 90.");

static PyObject *BufferObj_get_rotation(Buffer *self, void *_)
{
	return PyInt_FromLong((long)pldraw_get_rotation(self->pldraw));
}

static int BufferObj_set_rotation(Buffer *self, PyObject *value, void *_)
{
	int angle;

	if (plsdk_parse_rotation_angle(value, &angle) < 0)
		return -1;

	pldraw_set_rotation(self->pldraw, angle);

	return 0;
}

PyDoc_STRVAR(buffer_cfa_doc,
"Colour Filter Array identifier.");

static PyObject *BufferObj_get_cfa(Buffer *self, void *_)
{
	return PyLong_FromLong(pldraw_get_cfa(self->pldraw));
}

static int BufferObj_set_cfa(Buffer *self, PyObject *value, void *_)
{
	long cfa_id;

	if (PyInt_Check(value))
		cfa_id = PyInt_AS_LONG(value);
	else if (PyLong_Check(value))
		cfa_id = PyLong_AsLong(value);
	else
		return -1;

	if (cfa_id >= _PLDRAW_CFA_N_) {
		PyErr_SetString(PyExc_ValueError, "Invalid CFA identifier");
		return -1;
	}

	pldraw_set_cfa(self->pldraw, cfa_id);

	return Buffer_update_cached_colors(self);
}

static PyGetSetDef Buffer_getsetters[] = {
	{ "dev", (getter)BufferObj_get_dev, NULL, buffer_dev_doc },
	{ "xres", (getter)BufferObj_get_xres, NULL, buffer_xres_doc },
	{ "yres", (getter)BufferObj_get_yres, NULL, buffer_yres_doc },
	{ "rotation", (getter)BufferObj_get_rotation,
	  (setter)BufferObj_set_rotation, buffer_rotation_doc },
	{ "cfa", (getter)BufferObj_get_cfa, (setter)BufferObj_set_cfa,
	  buffer_cfa_doc },
	{ NULL }
};

/* -- Buffer members -- */

PyDoc_STRVAR(buffer_black_doc, "Colour shorthand for black.");
PyDoc_STRVAR(buffer_white_doc, "Colour shorthand for white.");
PyDoc_STRVAR(buffer_red_doc, "Colour shorthand for red.");
PyDoc_STRVAR(buffer_green_doc, "Colour shorthand for green.");
PyDoc_STRVAR(buffer_blue_doc, "Colour shorthand for blue.");
PyDoc_STRVAR(buffer_cyan_doc, "Colour shorthand for cyan.");
PyDoc_STRVAR(buffer_magenta_doc, "Colour shorthand for magenta.");
PyDoc_STRVAR(buffer_yellow_doc, "Colour shorthand for yellow.");
PyDoc_STRVAR(buffer_gl_doc, "List of grey levels supported by the display.");
PyDoc_STRVAR(buffer_palettes_doc, "Dictionary of standard palettes.");
PyDoc_STRVAR(buffer_fonts_doc,
"List of dictionaries with the available font names and sizes.");

static PyMemberDef Buffer_members[] = {
	{ "black", T_LONG, offsetof(Buffer, black), READONLY,
	  buffer_black_doc },
	{ "white", T_LONG, offsetof(Buffer, white), READONLY,
	  buffer_white_doc },
	{ "red", T_LONG, offsetof(Buffer, red), READONLY, buffer_red_doc },
	{ "green", T_LONG, offsetof(Buffer, green), READONLY,
	  buffer_green_doc },
	{ "blue", T_LONG, offsetof(Buffer, blue), READONLY,
	  buffer_blue_doc },
	{ "cyan", T_LONG, offsetof(Buffer, cyan), READONLY,
	  buffer_cyan_doc },
	{ "magenta", T_LONG, offsetof(Buffer, magenta), READONLY,
	  buffer_magenta_doc },
	{ "yellow", T_LONG, offsetof(Buffer, yellow), READONLY,
	  buffer_yellow_doc },
	{ "gl", T_OBJECT, offsetof(Buffer, gl), READONLY,
	  buffer_gl_doc },
	{ "palettes", T_OBJECT, offsetof(Buffer, palettes), READONLY,
	  buffer_palettes_doc },
	{ "fonts", T_OBJECT, offsetof(Buffer, fonts), READONLY,
	  buffer_fonts_doc },
	{ NULL }
};

/* -- Buffer type -- */

static PyTypeObject BufferType = {
	PyObject_HEAD_INIT(NULL)
	0,                                        /* ob_size */
	"_plsdk.Buffer",                          /* tp_name */
	sizeof(Buffer),                           /* tp_basicsize */
	0,                                        /* tp_itemsize */
	Buffer_dealloc,                           /* tp_dealloc */
	0,                                        /* tp_print */
	0,                                        /* tp_getattr */
	0,                                        /* tp_setattr */
	0,                                        /* tp_compare */
	0,                                        /* tp_repr */
	0,                                        /* tp_as_number */
	0,                                        /* tp_as_sequence */
	0,                                        /* tp_as_mapping */
	0,                                        /* tp_hash */
	0,                                        /* tp_call */
	0,                                        /* tp_str */
	0,                                        /* tp_getattro */
	0,                                        /* tp_setattro */
	0,                                        /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
	buffer_type_doc,                          /* tp_doc */
	0,                                        /* tp_traverse */
	0,                                        /* tp_clear */
	0,                                        /* tp_richcompare */
	0,                                        /* tp_weaklistoffset */
	0,                                        /* tp_iter */
	0,                                        /* tp_iternext */
	Buffer_methods,                           /* tp_methods */
	Buffer_members,                           /* tp_members */
	Buffer_getsetters,                        /* tp_getset */
	0,                                        /* tp_base */
	0,                                        /* tp_dict */
	0,                                        /* tp_descr_get */
	0,                                        /* tp_descr_set */
	0,                                        /* tp_dictoffset */
	Buffer_init,                              /* tp_init */
	0,                                        /* tp_alloc */
	Buffer_new,                               /* tp_new */
};


/* ----------------------------------------------------------------------------
 * Epdc class
 */

PyDoc_STRVAR(epdc_type_doc,
"E-Paper Display Controller\n"
"\n"
"An E-Paper Display Controller or *ePDC* device has specific features that "
"are handled by this class.  The main method :py:meth:`_plsdk.Epdc.update` is "
"used to request a display update over a given area with a given waveform "
"identifier.  Several options are "
"available to adjust the behaviour of the ePDC during the update.\n"
"\n"
"The ``libplepaper`` C library is used by this class which therefore inherits "
"most of its features.  One important point is that it relies on "
"loadable back-end modules for each ePDC implementaion since there is no "
"standard Linux device interface for this.  Each module is identified by a "
"string which is also used in its file name, as ``MODE`` in "
"``mod_plepaper_MODE.so`` .  This can be retrieved with "
":py:attr:`_plsdk.Epdc.mode`.\n");

struct Epdc_object {
	PyObject_HEAD;
	struct plep *plep;
	int refresh;
	int delta;
	int mono;
};

static PyObject *Epdc_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Epdc *self;

	self = (Epdc *)type->tp_alloc(type, 0);
	self->plep = NULL;

	return (PyObject *)self;
}

static int Epdc_init(PyObject *obj, PyObject *args, PyObject *kw)
{
	static char *kwlist[] = { "device", "mode", "config", NULL };
	Epdc *self = (Epdc *)obj;
	char *dev = NULL;
	char *mode = NULL;
	const char *conf_file = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kw, "|sss", kwlist, &dev,
					 &mode, &conf_file))
		return -1;

	self->plep = plep_init(dev, mode, conf_file);

	if (self->plep == NULL) {
		PyErr_SetString(plsdk_error, "Failed to initialise plep");
		return -1;
	}

	self->refresh = plep_get_wfid(self->plep, PLEP_REFRESH);
	self->delta = plep_get_wfid(self->plep, PLEP_DELTA);
	self->mono = plep_get_wfid(self->plep, PLEP_DELTA"/"PLEP_MONO);

	return 0;
}

static void Epdc_dealloc(PyObject *obj)
{
	Epdc *self = (Epdc *)obj;

	if (self->plep != NULL) {
		plep_free(self->plep);
		self->plep = NULL;
	}
}

/* -- Epdc methods -- */

PyDoc_STRVAR(epdc_get_wfid_doc,
"get_wfid(waveform)\n"
"\n"
"Get a waveform identifier for a given waveform name.  The return value can "
"then be used again in :py:meth:`_plsdk.Epdc.update`.  If the waveform is "
"not found, this method returns ``None``.\n");

static PyObject *EpdcObj_get_wfid(Epdc *self, PyObject *args)
{
	char *wfstr;
	int wfid;

	if (!PyArg_ParseTuple(args, "s", &wfstr))
		return NULL;

	wfid = plep_get_wfid(self->plep, wfstr);

	if (wfid < 0)
		Py_RETURN_NONE;

	return Py_BuildValue("i", wfid);
}

PyDoc_STRVAR(epdc_update_doc,
"update(wfid, area, synchro=False, power_off=False, bw=False, partial=False)\n"
"\n"
"Request an e-paper display update with the given waveform identifier "
"``wfid`` and area coordinates given by ``area``.\n"
"Several keyword options can be used to adjust the behaviour of the ePDC "
"for this particular update, as listed below.  They may also be set "
"persistently using attributes of the ``Epdc`` object.\n"
"\n"
"    synchro: :py:attr:`_plsdk.Epdc.synchro`\n\n"
"    power_off: :py:attr:`_plsdk.Epdc.power_off`\n\n"
"    bw: :py:attr:`_plsdk.Epdc.bw`\n\n"
"    partial: :py:attr:`_plsdk.Epdc.partial`\n\n"
"\n"
".. note:: Update options are ePDC specific\n"
"\n"
"    Each ePDC has different capabilities, so all these options may not "
"    always be available or may behave slightly differently depending on "
"    the current ePDC module being used.\n");

static PyObject *EpdcObj_update(Epdc *self, PyObject *args, PyObject *kw)
{
	enum opt_id {
		OPT_SYNC = 0,
		OPT_OFF,
		OPT_BW,
		OPT_PARTIAL,
		_OPT_N_
	};
	struct opt_desc {
		enum plep_update_opt flag;
		PyObject *en;
	};
	struct opt_desc opts[_OPT_N_] = {
		{ PLEP_SYNC_UPDATE, NULL },
		{ PLEP_WAIT_POWER_OFF, NULL },
		{ PLEP_BW_IMAGE, NULL },
		{ PLEP_PARTIAL, NULL },
	};
	static char *kwlist[] = {
		"wfid", "area", "synchro", "power_off", "bw", "partial", NULL};
	struct plep_rect area;
	enum opt_id id;
	int opt_flags;
	int wfid;
	int stat;

#if PYPLSDK_OLD_PYTHON /* Can't use both tuples and keywords */
	PyObject *area_py;

	if (!PyArg_ParseTupleAndKeywords(args, kw, "iO!|O!O!O!O!", kwlist,
					 &wfid,
					 &PyTuple_Type, &area_py,
					 &PyBool_Type, &opts[OPT_SYNC].en,
					 &PyBool_Type, &opts[OPT_OFF].en,
					 &PyBool_Type, &opts[OPT_BW].en,
					 &PyBool_Type, &opts[OPT_PARTIAL].en))
		return NULL;

	if (plsdk_rect_from_tuple(&area, area_py))
		return NULL;
#else
	if (!PyArg_ParseTupleAndKeywords(args, kw, "i(iiii)|O!O!O!O!", kwlist,
					 &wfid,
					 &area.a.x, &area.a.y,
					 &area.b.x, &area.b.y,
					 &PyBool_Type, &opts[OPT_SYNC].en,
					 &PyBool_Type, &opts[OPT_OFF].en,
					 &PyBool_Type, &opts[OPT_BW].en,
					 &PyBool_Type, &opts[OPT_PARTIAL].en))
		return NULL;
#endif

	opt_flags = 0;

	for (id = 0; id < _OPT_N_; ++id)
		if (opts[id].en == Py_True)
			opt_flags |= opts[id].flag;

	if (opt_flags)
		stat = plep_update_opts(self->plep, &area, wfid, opt_flags);
	else
		stat = plep_update(self->plep, &area, wfid);

	if (stat) {
		PyErr_SetString(plsdk_error, "Display update failed");
		return NULL;
	}

	Py_RETURN_NONE;
}

PyDoc_STRVAR(epdc_wait_power_doc,
"wait_power(state)\n"
"\n"
"Wait for the display power supply to reach the given ``state`` which may be "
"``True`` for power on or ``False`` for power off.  This is a blocking call "
"with no timeout, and returns ``None``.\n");

static PyObject *EpdcObj_wait_power(Epdc *self, PyObject *args)
{
	PyObject *py_power_state;
	int power_state;

	if (!PyArg_ParseTuple(args, "O!", &PyBool_Type, &py_power_state))
		return NULL;

	power_state = (py_power_state == Py_True) ? 1 : 0;

	if (plep_wait_power(self->plep, power_state) < 0) {
		PyErr_SetString(plsdk_error, "Failed to wait for power state");
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyMethodDef Epdc_methods[] = {
	{ "get_wfid", (PyCFunction)EpdcObj_get_wfid, METH_VARARGS,
	  epdc_get_wfid_doc },
	{ "update", (PyCFunction)EpdcObj_update, METH_VARARGS | METH_KEYWORDS,
	  epdc_update_doc },
	{ "wait_power", (PyCFunction)EpdcObj_wait_power, METH_VARARGS,
	  epdc_wait_power_doc },
	{ NULL }
};

/* -- Epdc getters & setters -- */

PyDoc_STRVAR(epdc_dev_doc,
"String with a device name used by the underlying ePDC module.  This may be "
"``None``, a ``/dev/`` Linux device or any other string depending on the "
"ePDC module (see :py:attr:`_plsdk.Epdc.mode`).");

static PyObject *EpdcObj_get_dev(Epdc *self, void *_)
{
	const char *dev = plep_get_dev(self->plep);

	if (dev == NULL)
		Py_RETURN_NONE;

	return PyString_FromString(dev);
}

PyDoc_STRVAR(epdc_mode_doc,
"String identifying the current ePDC module in use, as MODE in the "
"corresponding mod_plepaper_MODE.so file name.");

static PyObject *EpdcObj_get_mode(Epdc *self, void *_)
{
	const char *mode = plep_get_mode(self->plep);

	if (mode == NULL)
		Py_RETURN_NONE;

	return PyString_FromString(mode);
}

PyDoc_STRVAR(epdc_description_doc,
"String with a full description of the current ePDC module in use.");

static PyObject *EpdcObj_get_description(Epdc *self, void *_)
{
	const char *description = plep_get_description(self->plep);

	if (description == NULL)
		Py_RETURN_NONE;

	return PyString_FromString(description);
}

static PyObject *Epdc_get_opt(Epdc *self, enum plep_update_opt opt)
{
	PyObject *ret;

	ret = plep_get_opt(self->plep, opt) ? Py_True : Py_False;
	Py_INCREF(ret);

	return ret;
}

static int Epdc_set_opt(Epdc *self, enum plep_update_opt opt, PyObject *value)
{
	if (!PyBool_Check(value))
		return -1;

	plep_set_opt(self->plep, opt, (value == Py_True) ? 1 : 0);

	return 0;
}

static PyObject *Epdc_get_hw_opt(Epdc *self, enum plep_hw_opt opt, int as_bool)
{
	int value;

	if (plep_get_hw_opt(self->plep, opt, &value) < 0) {
		PyErr_SetString(plsdk_error, "Failed to get hardware option");
		return NULL;
	}

	if (as_bool)
		return PyBool_FromLong((long)value);

	return PyInt_FromLong((long)value);
}

static int Epdc_set_hw_opt(Epdc *self, enum plep_hw_opt opt, PyObject *value)
{
	int int_value;

	if (PyInt_Check(value)) {
		int_value = PyInt_AS_LONG(value);
	} else if (PyLong_Check(value)) {
		int_value = PyLong_AsLong(value);
	} else if (PyBool_Check(value)) {
		int_value = (value == Py_True) ? 1 : 0;
	} else {
		PyErr_SetString(PyExc_TypeError,
				"E-Paper hardware option must be an integer "
				"or a boolean");
		return -1;
	}

	if (plep_set_hw_opt(self->plep, opt, int_value) < 0) {
		PyErr_SetString(plsdk_error, "Failed to set hardware option");
		return -1;
	}

	return 0;
}

PyDoc_STRVAR(epdc_synchro_doc,
"Enable synchronous updates.\n"
"\n"
"A *synchronous update* will wait for any on-going update to complete before "
"it can start.  This typically results in back-to-back updates with the "
"display power left turned on and is useful when generated animations "
"synchronised with the display state.\n");

static PyObject *EpdcObj_get_synchro(Epdc *self, void *_)
{
	return Epdc_get_opt(self, PLEP_SYNC_UPDATE);
}

static int EpdcObj_set_synchro(Epdc *self, PyObject *value, void *_)
{
	return Epdc_set_opt(self, PLEP_SYNC_UPDATE, value);
}

PyDoc_STRVAR(epdc_power_off_doc,
"Wait for the display power to be turned off at the end of display update.\n");

static PyObject *EpdcObj_get_power_off(Epdc *self, void *_)
{
	return Epdc_get_opt(self, PLEP_WAIT_POWER_OFF);
}

static int EpdcObj_set_power_off(Epdc *self, PyObject *value, void *_)
{
	return Epdc_set_opt(self, PLEP_WAIT_POWER_OFF, value);
}

PyDoc_STRVAR(epdc_bw_doc,
"Enable black & white thresholding in the ePDC.");

static PyObject *EpdcObj_get_bw(Epdc *self, void *_)
{
	return Epdc_get_opt(self, PLEP_BW_IMAGE);
}

static int EpdcObj_set_bw(Epdc *self, PyObject *value, void *_)
{
	return Epdc_set_opt(self, PLEP_BW_IMAGE, value);
}

PyDoc_STRVAR(epdc_partial_doc,
"Enable partial updates, only the pixels that change get updated.\n"
"\n"
"This is similar to ``delta`` waveforms but is performed at the ePDC level "
"instead.  It should be enabled nevertheless in some circumstances with a "
"``delta`` waveform in order to achieve best ePDC performance.\n");

static PyObject *EpdcObj_get_partial(Epdc *self, void *_)
{
	return Epdc_get_opt(self, PLEP_PARTIAL);
}

static int EpdcObj_set_partial(Epdc *self, PyObject *value, void *_)
{
	return Epdc_set_opt(self, PLEP_PARTIAL, value);
}

PyDoc_STRVAR(epdc_temp_auto_doc,
"Enable/disable automatic temperature updates.  In this mode the ePDC will "
"automatically measure the display temperature to select the corresponding "
"waveform table.\n");

static PyObject *EpdcObj_get_temp_auto(Epdc *self, void *_)
{
	return Epdc_get_hw_opt(self, PLEP_TEMPERATURE_AUTO, 1);
}

static int EpdcObj_set_temp_auto(Epdc *self, PyObject *value, void *_)
{
	return Epdc_set_hw_opt(self, PLEP_TEMPERATURE_AUTO, value);
}

PyDoc_STRVAR(epdc_temperature_doc,
"Set a custom temperature for waveform selection.\n"
"\n"
"This is typically used in manual temperature mode, when "
":py:attr:`_Epdc.temperature_auto` has been disabled.  Otherwise, this "
"temperature will be overridden at the next temperature update.\n");

static PyObject *EpdcObj_get_temperature(Epdc *self, void *_)
{
	return Epdc_get_hw_opt(self, PLEP_TEMPERATURE, 0);
}

static int EpdcObj_set_temperature(Epdc *self, PyObject *value, void *_)
{
	return Epdc_set_hw_opt(self, PLEP_TEMPERATURE, value);
}

static PyGetSetDef Epdc_getsetters[] = {
	{ "dev", (getter)EpdcObj_get_dev, NULL, epdc_dev_doc },
	{ "mode", (getter)EpdcObj_get_mode, NULL, epdc_mode_doc },
	{ "description", (getter)EpdcObj_get_description, NULL,
	  epdc_description_doc },
	{ "synchro", (getter)EpdcObj_get_synchro, (setter)EpdcObj_set_synchro,
	  epdc_synchro_doc },
	{ "power_off",
	  (getter)EpdcObj_get_power_off, (setter)EpdcObj_set_power_off,
	  epdc_power_off_doc },
	{ "bw", (getter)EpdcObj_get_bw, (setter)EpdcObj_set_bw, epdc_bw_doc },
	{ "partial", (getter)EpdcObj_get_partial, (setter)EpdcObj_set_partial,
	  epdc_partial_doc },
	{ "temperature_auto",
	  (getter)EpdcObj_get_temp_auto, (setter)EpdcObj_set_temp_auto,
	  epdc_temp_auto_doc },
	{ "temperature",
	  (getter)EpdcObj_get_temperature, (setter)EpdcObj_set_temperature,
	  epdc_temperature_doc },
	{ NULL }
};

/* -- Epdc members -- */

PyDoc_STRVAR(epdc_refresh_doc,
	     "Shorthand for ``\"refresh\"`` waveform identifier, suitable to "
	     "be used with :py:meth:`_plsdk.Epdc.update`.");
PyDoc_STRVAR(epdc_delta_doc,
	     "Shorthand for ``\"delta\"`` waveform identifier, suitable to "
	     "be used with :py:meth:`_plsdk.Epdc.update`.");
PyDoc_STRVAR(epdc_mono_doc,
	     "Shorthand for ``\"delta/mono\"`` waveform identifier, suitable "
	     "to be used with :py:meth:`_plsdk.Epdc.update`.");

static PyMemberDef Epdc_members[] = {
	{ "refresh", T_INT, offsetof(Epdc, refresh), READONLY,
	  epdc_refresh_doc },
	{ "delta", T_INT, offsetof(Epdc, delta), READONLY,
	  epdc_delta_doc },
	{ "mono", T_INT, offsetof(Epdc, mono), READONLY,
	  epdc_mono_doc },
	{ NULL }
};

/* Epdc type */

static PyTypeObject EpdcType = {
	PyObject_HEAD_INIT(NULL)
	0,                                        /* ob_size */
	"_plsdk.Epdc",                            /* tp_name */
	sizeof(Epdc),                             /* tp_basicsize */
	0,                                        /* tp_itemsize */
	Epdc_dealloc,                             /* tp_dealloc */
	0,                                        /* tp_print */
	0,                                        /* tp_getattr */
	0,                                        /* tp_setattr */
	0,                                        /* tp_compare */
	0,                                        /* tp_repr */
	0,                                        /* tp_as_number */
	0,                                        /* tp_as_sequence */
	0,                                        /* tp_as_mapping */
	0,                                        /* tp_hash */
	0,                                        /* tp_call */
	0,                                        /* tp_str */
	0,                                        /* tp_getattro */
	0,                                        /* tp_setattro */
	0,                                        /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
	epdc_type_doc,                            /* tp_doc */
	0,                                        /* tp_traverse */
	0,                                        /* tp_clear */
	0,                                        /* tp_richcompare */
	0,                                        /* tp_weaklistoffset */
	0,                                        /* tp_iter */
	0,                                        /* tp_iternext */
	Epdc_methods,                             /* tp_methods */
	Epdc_members,                             /* tp_members */
	Epdc_getsetters,                          /* tp_getset */
	0,                                        /* tp_base */
	0,                                        /* tp_dict */
	0,                                        /* tp_descr_get */
	0,                                        /* tp_descr_set */
	0,                                        /* tp_dictoffset */
	Epdc_init,                                /* tp_init */
	0,                                        /* tp_alloc */
	Epdc_new,                                 /* tp_new */
};

/* Epdc local functions */

static struct plep *Epdc_get_plep(Epdc *epdc)
{
	return epdc->plep;
}


/* ----------------------------------------------------------------------------
 * Input class
 */

struct Input_object {
	PyObject_HEAD
	struct plinput *plinput;
	PyObject *keycodes_dict;
};

static PyObject *Input_new(PyTypeObject *type, PyObject *args, PyObject *kw)
{
	Input *self;

	self = (Input *)type->tp_alloc(type, 0);
	self->plinput = NULL;
	self->keycodes_dict = NULL;

	return (PyObject *)self;
}

static int Input_init(PyObject *obj, PyObject *args, PyObject *kw)
{
	static char *kwlist[] = { "device", "regex", "devdir", NULL };
	Input *self = (Input *)obj;
	char *dev = NULL;
	char *regex = NULL;
	char *devdir = NULL;

	if (!PyArg_ParseTupleAndKeywords(args, kw, "|sss", kwlist, &dev,
					 &regex, &devdir))
		return -1;

	if ((dev == NULL) && (regex == NULL)) {
		PyErr_SetString(PyExc_ValueError, "No input device specified");
		return -1;
	}

	if (regex != NULL)
		self->plinput = plinput_init_from_name(regex, devdir);
	else
		self->plinput = plinput_init(dev);

	if (self->plinput == NULL) {
		PyErr_SetString(plsdk_error, "Failed to initialise plinput");
		return -1;
	}

	Py_INCREF(plsdk_keycodes);
	self->keycodes_dict = plsdk_keycodes;

	return 0;
}

static void Input_dealloc(PyObject *obj)
{
	Input *self = (Input *)obj;

	if (self->plinput != NULL) {
		plinput_free(self->plinput);
		self->plinput = NULL;
	}

	if (self->keycodes_dict != NULL) {
		Py_DECREF(self->keycodes_dict);
		self->keycodes_dict = NULL;
	}
}

/* -- Input methods -- */

static PyObject *InputObj_grab(Input *self, PyObject *args)
{
	PyObject *do_grab;

	if (!PyArg_ParseTuple(args, "O!", &PyBool_Type, &do_grab))
		return NULL;

	if (plinput_grab(self->plinput, (do_grab == Py_True) ? 1 : 0) < 0) {
		PyErr_SetString(plsdk_error, "Failed to grab/un-grab device");
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyObject *InputObj_read_point(Input *self)
{
	struct plinput_point pt;
	PyObject *on_obj;
	int ret;

	Py_BEGIN_ALLOW_THREADS;
	ret = plinput_read_point(self->plinput, &pt);
	Py_END_ALLOW_THREADS;

	if (ret < 0) {
		PyErr_SetString(plsdk_error, "Failed to read input point");
		return NULL;
	}

	on_obj = pt.on ? Py_True : Py_False;
	Py_INCREF(on_obj);

	return Py_BuildValue("{s:i,s:i,s:i,s:O}", "x", pt.x, "y", pt.y,
			     "p", pt.p, "on", on_obj);
}

static PyObject *InputObj_read_xy(Input *self)
{
	struct plinput_point pt;
	int ret;

	Py_BEGIN_ALLOW_THREADS;
	ret = plinput_read_point(self->plinput, &pt);
	Py_END_ALLOW_THREADS;

	if (ret < 0) {
		PyErr_SetString(plsdk_error, "Failed to read input point");
		return NULL;
	}

	if (pt.on)
		return Py_BuildValue("(ii)", pt.x, pt.y);
	else
		Py_RETURN_NONE;
}

static PyObject *InputObj_read_key(Input *self)
{
	struct plinput_key key;
	PyObject *on_obj;
	int ret;

	Py_BEGIN_ALLOW_THREADS;
	ret = plinput_read_key(self->plinput, &key);
	Py_END_ALLOW_THREADS;

	if (ret < 0) {
		PyErr_SetString(plsdk_error, "Failed to read key input");
		return NULL;
	}

	on_obj = key.on ? Py_True : Py_False;
	Py_INCREF(on_obj);

	return Py_BuildValue("{s:i,s:O}", "code", key.code,  "on", on_obj);
}

static PyMethodDef Input_methods[] = {
	{ "grab", (PyCFunction)InputObj_grab, METH_VARARGS,
	  "Grab or un-grab input device" },
	{ "read_point", (PyCFunction)InputObj_read_point, METH_NOARGS,
	  "Read an input device point event (i.e. mouse, touch screen...)" },
	{ "read_xy", (PyCFunction)InputObj_read_xy, METH_NOARGS,
	  "Read an input device point event in (x, y) format or None" },
	{ "read_key", (PyCFunction)InputObj_read_key, METH_NOARGS,
	  "Rean an input key event" },
	{ NULL }
};

/* -- Input getters & setters -- */

static PyObject *InputObj_get_dev(Input *self, void *_)
{
	return PyString_FromString(plinput_get_dev(self->plinput));
}

static PyObject *InputObj_get_name(Input *self, void *_)
{
	return PyString_FromString(plinput_get_name(self->plinput));
}

static PyObject *InputObj_get_xres(Input *self, void *_)
{
	return PyInt_FromLong((long)plinput_get_xres(self->plinput));
}

static PyObject *InputObj_get_yres(Input *self, void *_)
{
	return PyInt_FromLong((long)plinput_get_yres(self->plinput));
}

static PyObject *InputObj_get_pres(Input *self, void *_)
{
	return PyInt_FromLong((long)plinput_get_pres(self->plinput));
}

static PyObject *InputObj_get_rotation(Input *self, void *_)
{
	const int angle = plinput_get_opt(self->plinput,
					  PLINPUT_OPT_ROTATION_ANGLE);
	return PyInt_FromLong((long)angle);
}

static int InputObj_set_rotation(Input *self, PyObject *value, void *_)
{
	int angle;

	if (plsdk_parse_rotation_angle(value, &angle) < 0)
		return -1;

	plinput_set_opt(self->plinput, PLINPUT_OPT_ROTATION_ANGLE, angle);

	return 0;
}

static PyGetSetDef Input_getsetters[] = {
	{ "dev", (getter)InputObj_get_dev, NULL, "Device path" },
	{ "name", (getter)InputObj_get_name, NULL, "Input device name" },
	{ "xres", (getter)InputObj_get_xres, NULL, "Horizontal resoltion" },
	{ "yres", (getter)InputObj_get_yres, NULL, "Vertical resoltion" },
	{ "pres", (getter)InputObj_get_pres, NULL, "Pressure resoltion" },
	{ "rotation", (getter)InputObj_get_rotation,
	  (setter)InputObj_set_rotation, "Rotation angle" },
	{ NULL }
};

/* -- Input members -- */

static PyMemberDef Input_members[] = {
	{ "keycodes", T_OBJECT, offsetof(Input, keycodes_dict), READONLY,
	  "Keyboard key codes" },
	{ NULL }
};

/* Input type */

static PyTypeObject InputType = {
	PyObject_HEAD_INIT(NULL)
	0,                                        /* ob_size */
	"_plsdk.Input",                           /* tp_name */
	sizeof(Input),                            /* tp_basicsize */
	0,                                        /* tp_itemsize */
	Input_dealloc,                            /* tp_dealloc */
	0,                                        /* tp_print */
	0,                                        /* tp_getattr */
	0,                                        /* tp_setattr */
	0,                                        /* tp_compare */
	0,                                        /* tp_repr */
	0,                                        /* tp_as_number */
	0,                                        /* tp_as_sequence */
	0,                                        /* tp_as_mapping */
	0,                                        /* tp_hash */
	0,                                        /* tp_call */
	0,                                        /* tp_str */
	0,                                        /* tp_getattro */
	0,                                        /* tp_setattro */
	0,                                        /* tp_as_buffer */
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /* tp_flags */
	"E-Paper display controller",             /* tp_doc */
	0,                                        /* tp_traverse */
	0,                                        /* tp_clear */
	0,                                        /* tp_richcompare */
	0,                                        /* tp_weaklistoffset */
	0,                                        /* tp_iter */
	0,                                        /* tp_iternext */
	Input_methods,                            /* tp_methods */
	Input_members,                            /* tp_members */
	Input_getsetters,                         /* tp_getset */
	0,                                        /* tp_base */
	0,                                        /* tp_dict */
	0,                                        /* tp_descr_get */
	0,                                        /* tp_descr_set */
	0,                                        /* tp_dictoffset */
	Input_init,                               /* tp_init */
	0,                                        /* tp_alloc */
	Input_new,                                /* tp_new */
};


/* ----------------------------------------------------------------------------
 * native interface
 */

static struct plep *pyplsdk_Epdc_get_plep(PyObject *epdc_obj)
{
	if (epdc_obj->ob_type != &EpdcType) {
		PyErr_SetString(PyExc_TypeError, "Epdc instance required");
		return NULL;
	}

	return ((Epdc *)(epdc_obj))->plep;
}

static struct pldraw *pyplsdk_Buffer_get_pldraw(PyObject *buffer_obj)
{
	if (buffer_obj->ob_type != &BufferType) {
		PyErr_SetString(PyExc_TypeError, "Buffer instance required");
		return NULL;
	}

	return ((Buffer *)(buffer_obj))->pldraw;
}

static struct plinput *pyplsdk_Input_get_plinput(PyObject *input_obj)
{
	if (input_obj->ob_type != &InputType) {
		PyErr_SetString(PyExc_TypeError, "Input instance required");
		return NULL;
	}

	return ((Input *)(input_obj))->plinput;
}

static struct pyplsdk_api plsdk_api = {
	.Epdc_get_plep = pyplsdk_Epdc_get_plep,
	.Buffer_get_pldraw = pyplsdk_Buffer_get_pldraw,
	.Input_get_plinput = pyplsdk_Input_get_plinput,
};

static int plsdk_init_native_api(PyObject *m)
{
	PyObject *c_api_obj;

#if PYPLSDK_USE_COBJ
	c_api_obj = PyCObject_FromVoidPtr(&plsdk_api, NULL);
#else
	c_api_obj = PyCapsule_New(&plsdk_api, PYPLSDK_API_FULL_NAME, NULL);
#endif

	if (c_api_obj == NULL) {
		PyErr_NoMemory();
		return -1;
	}

	PyModule_AddObject(m, PYPLSDK_API_NAME, c_api_obj);

	return 0;
}


/* ----------------------------------------------------------------------------
 * _plsdk module
 */

static PyMethodDef plsdk_methods[] = {
	{ NULL, NULL, 0, NULL }
};

struct plsdk_type {
	const char *name;
	PyTypeObject *py_type;
};
static const struct plsdk_type types[] = {
	{ "Buffer", &BufferType },
	{ "Epdc", &EpdcType },
	{ "Input", &InputType },
	{ NULL, NULL }
};

static int plsdk_init_types(void)
{
	const struct plsdk_type *type;

	for (type = types; type->name != NULL; ++type)
		if (PyType_Ready(type->py_type) < 0)
			return -1;

	return 0;
}

static void plsdk_add_types(PyObject *m)
{
	const struct plsdk_type *type;

	for (type = types; type->name != NULL; ++type) {
		Py_INCREF(type->py_type);
#if PYPLSDK_OLD_PYTHON
		PyModule_AddObject(m, (char *)type->name,
				   (PyObject *)type->py_type);
#else
		PyModule_AddObject(m, type->name, (PyObject *)type->py_type);
#endif
	}
}

static int plsdk_init_palettes(PyObject *m)
{
	struct palette_init {
		const char *name;
		size_t length;
		const uint8_t *data;
	};
	static const struct palette_init palette_init_table[] = {
		{ "default_grey_16", 16, pldraw_grey_16 },
		{ "default_grey_4", 4, pldraw_grey_4 },
		{ NULL, 0, NULL }
	};
	const struct palette_init *p;

	plsdk_palettes = PyDict_New();

	if (plsdk_palettes == NULL) {
		PyErr_NoMemory();
		return -1;
	}

	for (p = palette_init_table; p->name != NULL; ++p) {
		PyObject *o;
		Py_ssize_t i;

		o = PyTuple_New(p->length);

		if (o == NULL) {
			PyErr_NoMemory();
			goto err_decref;
		}

		for (i = 0; i < p->length; ++i) {
			PyObject *grey = PyLong_FromLong(p->data[i]);

			if (grey == NULL)
				goto err_decref;

			if (PyTuple_SetItem(o, i, grey) < 0)
				goto err_decref;
		}

		if (PyDict_SetItemString(plsdk_palettes, p->name, o) < 0)
			goto err_decref;
	}

	PyModule_AddObject(m, "palettes", plsdk_palettes);

	return 0;

err_decref:
	Py_DECREF(plsdk_palettes);
	plsdk_palettes = NULL;

	return -1;
}

static int plsdk_init_fonts(PyObject *m)
{
	const struct pldraw_font * const *font;
	size_t n;

	for (font = pldraw_fonts, n = 0; *font != NULL; ++font, ++n);

	plsdk_fonts = PyList_New(n);

	if (plsdk_fonts == NULL) {
		PyErr_NoMemory();
		return -1;
	}

	for (font = pldraw_fonts, n = 0; *font != NULL; ++font, ++n) {
		PyObject *font_dict = PyDict_New();
		PyObject *name_obj = PyString_FromString((*font)->name);
		PyObject *size_obj = PyLong_FromLong((*font)->size);

		if ((font_dict == NULL) || (name_obj == NULL) ||
		    (size_obj == NULL)) {
			if (font_dict == NULL)
				PyErr_NoMemory();
			else
				Py_DECREF(font_dict);

			Py_XDECREF(name_obj);
			Py_XDECREF(size_obj);
			goto err_decref;
		}

		if ((PyDict_SetItemString(font_dict, "name", name_obj) < 0) ||
		    (PyDict_SetItemString(font_dict, "size", size_obj) < 0)) {
			Py_DECREF(font_dict);
			goto err_decref;
		}

		if (PyList_SetItem(plsdk_fonts, n, font_dict) < 0)
			goto err_decref;
	}

	PyModule_AddObject(m, "fonts", plsdk_fonts);

	return 0;

err_decref:
	Py_DECREF(plsdk_fonts);
	plsdk_fonts = NULL;

	return -1;
}

static int plsdk_init_keycodes(PyObject *m)
{
	const char * const *name;
	int key;

	plsdk_keycodes = PyDict_New();

	if (plsdk_keycodes == NULL) {
		PyErr_NoMemory();
		return -1;
	}

	for (key = 0, name = plinput_key_names;
	     key < plinput_n_key_names;
	     ++key, ++name) {
		if (*name == NULL)
			continue;

		if (PyDict_SetItem(plsdk_keycodes,
				   PyString_FromString(*name),
				   PyInt_FromLong(key)) < 0) {
			Py_DECREF(plsdk_keycodes);
			plsdk_keycodes = NULL;
			return -1;
		}
	}

	PyModule_AddObject(m, "keys", plsdk_keycodes);

	return 0;
}

PyMODINIT_FUNC init_plsdk(void)
{
	PyObject *m;

	if (plsdk_init_types())
		return;

	m = Py_InitModule(PYPLSDK_MODULE_NAME, plsdk_methods);

	if (m == NULL)
		return;

	PyModule_AddIntConstant(m, "version", 1);

	plsdk_error = PyErr_NewException("_plsdk.error", NULL, NULL);
	Py_INCREF(plsdk_error);
	PyModule_AddObject(m, "error", plsdk_error);

	plsdk_add_types(m);

	if (Buffer_init_attrs() < 0)
		return;

	if (plsdk_init_palettes(m) < 0)
		return;

	if (plsdk_init_fonts(m) < 0)
		return;

	if (plsdk_init_keycodes(m) < 0)
		return;

	if (plsdk_init_native_api(m) < 0)
		return;
}

/* ----------------------------------------------------------------------------
 * utilities
 */

#if PYPLSDK_OLD_PYTHON
static int plsdk_rect_from_tuple(struct plep_rect *r, PyObject *t)
{
	if (!PyTuple_Check(t)) {
		PyErr_SetString(PyExc_TypeError, "Coordinates not as tuple");
		return -1;
	}

	if (PyTuple_GET_SIZE(t) != 4) {
		PyErr_SetString(PyExc_TypeError, "Invalid area coordinates");
		return -1;
	}

	r->a.x = PyLong_AsLong(PyTuple_GET_ITEM(t, 0));
	r->a.y = PyLong_AsLong(PyTuple_GET_ITEM(t, 1));
	r->b.x = PyLong_AsLong(PyTuple_GET_ITEM(t, 2));
	r->b.y = PyLong_AsLong(PyTuple_GET_ITEM(t, 3));

	return 0;
}
#endif

static int plsdk_parse_rotation_angle(PyObject *value, int *angle)
{
	*angle = (int)(PyLong_AsLong(value) % 360);

	if ((*angle % 90) || (*angle < 0)) {
		PyErr_SetString(plsdk_error, "Invalid rotation angle value");
		return -1;
	}

	return 0;
}

static int plsdk_obj2long(PyObject *obj, long *value)
{
	if (PyInt_Check(obj)) {
		*value = PyInt_AsLong(obj);
		return 0;
	}

	if (PyLong_Check(obj)) {
		*value = PyLong_AsLong(obj);
		return 0;
	}

	PyErr_SetString(PyExc_TypeError, "Expected integer (int or long)");

	return -1;
}
