#!/bin/sh

set -e

ver="$1"

sed -i s/"\(__version__ = '\)\(.*\)\('.*\)$"/"\1$ver\3"/ plsdk/__init__.py
git add plsdk/__init__.py

sed -i s/"\(version='\)\(.*\)\(',.*\)$"/"\1$ver\3"/ setup.py
git add setup.py

exit 0
