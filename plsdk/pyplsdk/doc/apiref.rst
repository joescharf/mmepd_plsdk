*************
API reference
*************

Overview
========

The aim of the ``plsdk`` package is to provide a flexible framework to easily
use the Plastic Logic EPD technology.  It can be used to perform basic
graphical operations and e-paper control and can also be the basis of more
advanced e-paper friendly applications.  It internally uses a C Python
extension module named ``_plsdk`` which directly wraps around the ``PLSDK`` C
libraries.  This means all the EPD Controller (EPDC) implementations supported
by the native ``PLSDK`` are automatically supported by this Python package.

.. note::

   This version can be used in conjunction with the Python Imaging Library
   (PIL), for which Plastic Logic has added a ``Image.fbdump`` method to render
   an image object directly into a framebuffer (see :ref:`examples`).  This is
   an interim solution until extra graphical methods are added to the ``plsdk``
   package to draw arbitrary image data efficiently.

The main class is :py:class:`plsdk.Display`.  It can easily be created on any
platform thanks to automatic discovery of the underlying EPDC implementation.
It contains an ``fb`` attribute for things that are specific to framebuffer
graphics and an ``ep`` attribute for e-paper control.  For example, to fill the
screen with white and then draw an arbitrary black rectangle over its left half
with a ``mono`` waveform (see :ref:`waveforms`)::

    import plsdk

    d = plsdk.Display()
    d.clear()
    d.rectangle((0, 0, (d.fb.xres / 2), d.fb.yres), wf=d.ep.mono)

The area where the rectangle is drawn is then automatically updated on the
e-paper (as :py:attr:`plsdk.Display.auto_update` is enabled by default).

Lower level classes are :py:class:`_plsdk.Buffer` and :py:class:`_plsdk.Epdc`
which wrap around the ``libpldraw`` and ``libplepaper`` libraries respectively.
They provide some convenient Python interfaces to these libraries and are
typically used through :py:class:`plsdk.Display`.

.. raw:: latex

   \newpage


Display objects
===============

.. autoclass:: plsdk.Display
   :members:

.. rubric:: Direct access to the ``ep`` and ``fb`` objects

As previously mentioned, each ``Display`` object contains two attributes ``ep``
and ``fb`` which represent the e-paper and framebuffer parts.  It is
recommended to use the following ``Epdc`` members of ``ep`` directly from a
``Display`` object:

* ``ep.refresh``: :py:attr:`_plsdk.Epdc.refresh`
* ``ep.delta``: :py:attr:`_plsdk.Epdc.delta`
* ``ep.mono``: :py:attr:`_plsdk.Epdc.mono`

Likewise, the following ``Buffer`` members of ``fb`` can be used directly from
a ``Display`` object:

* ``fb.gl``: :py:attr:`_plsdk.Buffer.gl`
* ``fb.black``: :py:attr:`_plsdk.Buffer.black`
* ``fb.white``: :py:attr:`_plsdk.Buffer.white`
* ``fb.red``: :py:attr:`_plsdk.Buffer.red`
* ``fb.green``: :py:attr:`_plsdk.Buffer.green`
* ``fb.blue``: :py:attr:`_plsdk.Buffer.blue`
* ``fb.cyan``: :py:attr:`_plsdk.Buffer.cyan`
* ``fb.magenta``: :py:attr:`_plsdk.Buffer.magenta`
* ``fb.yellow``: :py:attr:`_plsdk.Buffer.yellow`
* ``fb.xres``: :py:attr:`_plsdk.Buffer.xres`
* ``fb.yres``: :py:attr:`_plsdk.Buffer.yres`
* ``fb.fonts``: :py:attr:`_plsdk.Buffer.fonts`
* ``fb.cfa``: :py:attr:`_plsdk.Buffer.cfa`


Buffer objects
==============

.. autoclass:: _plsdk.Buffer
   :members:


EPDC objects
============

.. autoclass:: _plsdk.Epdc
   :members:
