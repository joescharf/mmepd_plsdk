Introduction
============

.. _rationale:

Rationale
---------

The Plastic Logic Software Development Kit (PLSDK) contains a collection of C
libraries specialised in **electrophoretic displays (EPD)**, also known as
**E-Paper**.  This Python ``plsdk`` package provides an interface to the PLSDK
functionalities as well as higher level features.  It is designed to let
developers easily evaluate the Plastic Logic EPD technology and is most suited
to developing demonstrators and prototypes.

EPDs behave differently than most current common display technologies (LCD,
CRT...).  A first difference is that a sequence of signals (:ref:`waveforms`)
need to be generated in order to **update each pixel from one state to
another**.  This means some extra work has to be done by the software to
describe *how* to update the pixels, not just *what* colour the pixels should
show.  Another difference is that **the image remains on the display** after
the power has been turned off.  In other words, the display only needs to be
powered while changing its contents.

Most graphical user interface (GUI) software frameworks rely on the fact that
the output device is an LCD-like display.  It is expected that the refresh rate
is at least 25 frames per second and that the image is being continuously
refreshed.  These two expectations are not met with EPDs.  It takes a
significant software effort to modify existing GUI frameworks such as Android,
Qt or GTK in order to take into account EPD characteristics.  The aim of the
PLSDK is to **provide a direct access to EPDs** and convenient graphical tools,
not to solve the problem of adding EPD support to exising GUI frameworks and
applications.

.. raw:: latex

   \vfill
   \begin{center}Copyright © 2013 Plastic Logic.
   All rights reserved.\end{center}
   \newpage


.. _waveforms:

Waveforms
---------

A *waveform* in the context of EPDs is a series of voltages that need to be
applied to a pixel in order to change its value.  There are three main
*waveform types* defined by Plastic Logic, each with different properties.
Each waveform is characterised by a combination of an update mode and a number
of possible pixel values (greyscale or colour depth).  Waveforms are identified
in the PLSDK with the following path-like syntax:

``type/depth/options``

If only ``type`` is provided, then the default ``depth`` and ``options`` will
be used whenever possible.  Only a few special waveforms use ``options``.  The
three main waveforms listed below provide a simple rule of thumb which covers
most of the usual application cases:

* ``refresh``

Each pixel is refreshed, which causes a flashing effect but achieves best
resulting image quality.  This should normally be used over the whole screen to
clear any :ref:`ghosting` produced by previous updates.  Using the ``refresh``
waveform over a limited display area usually causes some :ref:`edge_artefact`
so it should be avoided.

By default, the ``refresh`` waveform uses **full greyscale/colour depth** for
maximum image quality.

* ``delta``

Only the pixels which values need to change are refreshed.  This is useful to
show shapes that are not rectangular or to produce animation effects.  It can
also be used when turning pages in a document with a minimal flashing effect,
although this will build up some :ref:`ghosting` and :ref:`edge_artefact` which
can eventually be cleared with a ``refresh`` update.

By default, the ``delta`` waveform uses **full greyscale/colour depth** for
maximum image quality.

* ``delta/mono`` (also casually refered to as ``mono``)

This waveform only supports pure **black and white** and does not cause any
flashing as it is a *direct drive waveform*.  Like the standard ``delta``
waveform, only the pixels that need to change are updated.  The main difference
being that only target full black and white pixels are updated, intermediate
target grey levels are left unchanged.

It is faster than the full greyscale/color ``refresh`` and ``delta`` waveforms,
so it usually constitutes a good choice to make fast and responsive user
interface elements such as menus, handwriting tools and virtual keyboards.

When used with a colour display (fitted with a :ref:`colour_filter_array`), the
``mono`` waveform can be used to show white, black but also primary (red,
green, blue) and secondary (cyan, magenta, yellow) colours.  These eight
*colours* are recommended in order to build efficient user interfaces.

.. raw:: latex

   \vfill
   \begin{center}Copyright © 2013 Plastic Logic.
   All rights reserved.\end{center}
   \newpage


.. _colour_filter_array:

Colour Filter Array
-------------------

Colour EPD is achieved by applying a **C**\ olour **F**\ ilter **A**\ rray on
top of the basic monochrome EPD.  This typically converts each pixel into a
red, green, blue or white sub-pixel as shown in the example below:

.. figure:: cfa-gr_bw-350.png
   :align: center
   :scale: 50%

   Green, red, blue and white CFA (standard name: ``GR_BW``)

The logical resolution of the display is then halved on both horizontal and
vertical axes.  So for a native monochrome resolution of 1280x960, the
equivalent colour resolution becomes 640x480.

Many CFA configurations are possible.  The naming convention is to use a letter
for each colour on a row of sub-pixels, and each row is separated by an
underscore "``_``".  For example, the name used to identify the CFA shown above
is ``GR_BW``.

The PLSDK can handle all CFA configurations produced by Plastic Logic which can
be set with :py:attr:`_plsdk.Buffer.cfa` like this::

    d = plsdk.Display()
    d.fb.cfa = d.fb.CFA_GR_BW

For example, when ``GR_BW`` CFA mode is enabled, all the graphical operations
will populate 4 actual sub-pixels for each logical pixel as seen by the
application.  This is platform-independent and can be switched on and off
between graphical operations individually in each :py:class:`plsdk.Display`
object.

The following CFA modes are supported by this ``plsdk`` version:

* ``GR_BW``:

    Green, red, blue and white 2x2 pattern.  This is the standard
    configuration.

* ``GR_BW_TR180``:

    Same as GR_BW on the bottom half of the screen, and WB_RG on the top half
    (split a half of the vertical resolution).  This is typically used with 1x2
    tiled displays where the top one is rotated by 180 degrees relatively to
    the bottom one.

.. raw:: latex

   \vfill
   \begin{center}Copyright © 2013 Plastic Logic.
   All rights reserved.\end{center}
   \newpage


.. _epaper_considerations:

E-Paper considerations
----------------------

.. _ghosting:

Ghosting
^^^^^^^^

Ghosting manifests itself with remnants of the previous state of the display
after some pixels have been updated.  This is due to an error between the ideal
target value of a pixel and its actual luminance; it is intrinsic to any EPD
technology.  The ``refresh`` waveforms with full greyscale or colour depth are
not direct drive; they produce a flashing effect to reduce ghosting by first
driving the pixels to full black or white before the target grey level.

.. figure:: ghosting.png
   :align: center

   Effects of ghosting using a *direct drive waveform* (no flashing)


.. _edge_artefact:

Edge artefact
^^^^^^^^^^^^^

E-Paper media is composed of many more small particles than the number of
pixels.  When driving one pixel, particles on the edges of neighbouring pixels
can also be altered if they are not actively driven.  The result is
particularly visible along shapes with a high contrast contour, for example
black text on white background.  Driving only the black pixels will interfere
with the surrounding white pixels, changing their state slightly.  This
manifests itself with a reminiscent outline refered to as *edge artefact*, as
illustrated on the figure below.

.. figure:: edge-artefact.png
   :align: center
   :scale: 50%

   Edge artefact shown after deleting a shape with a sharp contour

This typically occurs when turning a page in a text document with a ``delta``
waveform.  The simplest way to avoid this issue is to perform a full screen
update with a ``refresh`` waveform, so all the pixels are actively driven in
order to prevent any interference between them.  It is also sometimes possible
to hide this issue by making the update area match an existing black frame.

.. raw:: latex

   \vfill
   \begin{center}Copyright © 2013 Plastic Logic.
   All rights reserved.\end{center}
   \newpage



.. _user_interface:

E-Paper user interface elements
-------------------------------

.. rubric:: Page turning

A text document can either use pure black & white or anti-aliased fonts with
intermediate grey levels.  In the former case, a ``mono`` waveform may be used,
but the latter case is the most common and requires a full colour depth
waveform.  Then to avoid :ref:`ghosting` and :ref:`edge_artefact`, a
``refresh`` waveform should be used.  However this causes a flashing effect
which may be disturbing for the user reading the document.  The usual approach
is to find a good compromise beteen flashing cased by ``refresh`` updates and
other side effects caused by ``delta`` updates.  For example, when turning
several pages the following waveforms may be used:

``refresh → delta → delta → delta → refresh ...``

The number of consecutive ``delta`` updates is limited by the cumulative amount
of visual side effects that is acceptable for a given product, document,
context and generally speaking user expectactions.  Since this is very
subjective, it needs to be evaluated and adjusted on a case-by-case basis.

.. rubric:: Menus

Generally speaking, menus work best with "pure" colours, so black and white on
monochrome displays and also red, green, blue, cyan, magenta and yellow on
colour displays.  This allows the ``mono`` waveform to be used, which means
lower latency when the user interacts with the menu.  Then as previously
described, hiding a menu usually requires a full screen ``refresh`` update in
order to avoid :ref:`ghosting` and :ref:`edge_artefact`.

.. rubric:: Handwriting

It is important for handwriting that the lines do not flash when they are being
drawn, so the ``mono`` waveform must be used.  Another important aspect to take
into account is the maximum number of update regions the e-paper controller can
process in parallel.  Even a mono waveform is slow enough to let the
application perform many drawing operations in the meantime, so requesting an
update for each small region may saturate the controller.  This very much
depends on the system, some controllers can merge update requests to avoid this
issue.  If some lag is observed during handrwiting operation, adjusting the
size and frequency of update requests can improve things.

Some e-paper controllers may also not handle the case where a line is crossing
itself (for example an "8" shape) while the display is being updated as this
causes some update regions to overlap.  To reduce the visual effects caused by
this issue, the regions have to be tailored by the drawing application in order
to minimise the overlapping areas.

.. rubric:: Virtual keyboards

When using a virtual keyboard, several areas of the screen are driven
simultaneously: the keys show animations when pressed, the characters are
displayed in an edit box with a moving cursor, and potentially other things
are displayed like automatic word completion.

It is very difficult to avoid :ref:`edge_artefact` when moving a cursor with a
``delta`` waveform, so a ``refresh`` update must be performed when it becomes
necessary to restore optimal image quality.  The area may be limited to the
contents of an input edit box if it is rectangular.  A ``refresh/mono``
waveform may be a good choice when using a pure black & white font (no
anti-aliasing) as it is faster than the standard greyscale ``refresh``.

Key press animations can also be achieved by taking the advantage of the
flashing nature of ``refresh`` waveforms.  So without doing any drawing
operation, requesting a ``refresh/mono`` update will create a quick flash on a
rectangular area.

.. raw:: latex

   \vfill
   \begin{center}Copyright © 2013 Plastic Logic.
   All rights reserved.\end{center}
   \newpage
