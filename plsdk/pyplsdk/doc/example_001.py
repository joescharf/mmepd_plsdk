from PIL import Image
import plsdk

# Create a Display object
d = plsdk.Display()

# Clear the screen
d.clear()

# Use mono waveform
d.wf = d.ep.mono

# Fill a rectangle with black
d.rectangle(((d.fb.xres / 4), (d.fb.yres / 4),
             (d.fb.xres * 3 / 4), (d.fb.yres * 3 / 4)))

# Disable automatic updates
d.auto_update = False

# Draw diagonal lines across the screen
d.line((0, 0, (d.fb.xres - 1), (d.fb.yres - 1)))
d.line(((d.fb.xres - 1), 0, 0, (d.fb.yres - 1)))

# Update now
d.update()

# Show an image in the top-left corner using special PIL fbdump method
Image.open("my-picture.jpeg").convert("RGB").fbdump(d.fb)

# Update the display with a refresh waveform
d.update(wf=d.ep.refresh)

# Enable CFA for a colour display
d.fb.cfa = d.fb.CFA_GR_BW

# Show a colour picture
Image.open("my-colour-picture.jpeg").convert("RGB").fbdump(d.fb)

# Update the display with a refresh waveform
d.update(wf=d.ep.refresh)
