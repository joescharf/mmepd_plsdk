.. _examples:

********
Examples
********

This basic example shows how to use all the main features of the ``plsdk``:

.. literalinclude:: example_001.py
